import React, { useRef, useState } from 'react';
import styles from './App.module.css';
// import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter, Link, Outlet, Route, Routes } from 'react-router-dom';
import Table from './pages/Table';
import ImportWords from './pages/ImportWords';
import Difference from './pages/Difference';
import NN from './pages/NN';
import Graphs from './pages/Graphs';
import Tree from './pages/Tree';

const importWordsPath = "import-words";
const differencePAth = "difference";
const NNPath = "nn";
const graphsPath = "graphs";
const treePath = "tree";

const tree = {
  type: 'input',
  innerElements: [
    {
      type: 'input',
      innerElements: [
        {
          type: 'dropdown',
          innerElements: [
            {
              type: 'input',
              innerElements: [
                {
                  type: 'dropdown',
                  innerElements: [],
                },
              ],
            },
          ],
        },
      ],
    },
    {
      type: 'input',
      innerElements: [
        {
          type: 'dropdown',
          innerElements: [
            {
              type: 'input',
              innerElements: [
                {
                  type: 'dropdown',
                  innerElements: [],
                },
              ],
            },
          ],
        },
        {
          type: 'dropdown',
          innerElements: [
            {
              type: 'input',
              innerElements: [
                {
                  type: 'dropdown',
                  innerElements: [],
                },
              ],
            },
          ],
        },
        {
          type: 'dropdown',
          innerElements: [
            {
              type: 'input',
              innerElements: [
                {
                  type: 'dropdown',
                  innerElements: [
                    {
                      type: 'dropdown',
                      innerElements: [
                        {
                          type: 'input',
                          innerElements: [
                            {
                              type: 'dropdown',
                              innerElements: [],
                            },
                          ],
                        },
                      ],
                    },
                    {
                      type: 'dropdown',
                      innerElements: [
                        {
                          type: 'input',
                          innerElements: [
                            {
                              type: 'dropdown',
                              innerElements: [],
                            },
                          ],
                        },
                      ],
                    },
                    {
                      type: 'dropdown',
                      innerElements: [
                        {
                          type: 'input',
                          innerElements: [
                            {
                              type: 'dropdown',
                              innerElements: [],
                            },
                          ],
                        },
                      ],
                    },
                  ],
                },
              ],
            },
          ],
        },
      ],
    },
  ],
}

/**
 * Base component
 */
export default function App() {

  // @ts-ignore
  return (
    <div className={styles.App}>
      <BrowserRouter>
        {/*@ts-ignore */}
        <Routes>
          {/*@ts-ignore */}
          <Route path="/" element={<Layout />}>
            {/*@ts-ignore */}
            <Route index element={<Table />} />
            {/*@ts-ignore */}
            <Route path={importWordsPath} element={<ImportWords />} />
            {/*@ts-ignore */}
            <Route path={differencePAth} element={<Difference />} />
            {/*@ts-ignore */}
            <Route path={NNPath} element={<NN />} />
            {/*@ts-ignore */}
            <Route path={graphsPath} element={<Graphs />} />
            {/*@ts-ignore */}
            <Route path="*" element={<h1>No Page</h1>} />
            {/*@ts-ignore */}
            <Route path={treePath} element={<Tree tree={tree} />} />
          </Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

const Layout = () => {
  return (
    <>
      <nav>
        <ul>
          <li>
            {/*@ts-ignore */}
            <Link to="/">Home</Link>
          </li>
          <li>
            {/*@ts-ignore */}
            <Link to={`/${importWordsPath}`}>Import Words</Link>
          </li>
          <li>
            {/*@ts-ignore */}
            <Link to={`/${differencePAth}`}>Difference</Link>
          </li>
          <li>
            {/*@ts-ignore */}
            <Link to={`/${NNPath}`}>NN</Link>
          </li>
          <li>
            {/*@ts-ignore */}
            <Link to={`/${graphsPath}`}>Graphs</Link>
          </li>
          <li>
            {/*@ts-ignore */}
            <Link to={`/${treePath}`}>Tree</Link>
          </li>
        </ul>
      </nav >

      {/*@ts-ignore */}
      <Outlet />
    </>
  )
};
