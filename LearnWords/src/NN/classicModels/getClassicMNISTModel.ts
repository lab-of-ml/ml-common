import * as tf from '@tensorflow/tfjs';
import getCallbacksForVisualizeTraining from "../helpers/getCallbacksForVisualizeTraining";
import { getNumbersSetPatchFromCsv } from "../helpers/getNumbersSetFromCSV";
import normalizeMNISTDataForTraining from "../helpers/normalizeMNISTDataForTraining";
import predictMNISTModel from "../helpers/predictMNISTModel";
import fitModel from '../helpers/fitModel';

export default () => {
  const model = tf.sequential();

  model.add(tf.layers.dense({ inputShape: [784], units: 784, activation: 'relu' }));
  model.add(tf.layers.dropout({ rate: 0.3 }));
  model.add(tf.layers.dense({ units: 32, activation: 'relu' }));
  model.add(tf.layers.dropout({ rate: 0.3 }));
  model.add(tf.layers.dense({ units: 10, activation: 'softmax' }));

  model.compile({
    optimizer: 'adam',
    loss: 'categoricalCrossentropy',
    metrics: ['accuracy'],
  });

  return model;
}
