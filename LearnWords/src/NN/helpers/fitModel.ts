import * as tf from '@tensorflow/tfjs';
import getCallbacksForVisualizeTraining from "./getCallbacksForVisualizeTraining";
import normalizeMNISTDataForTraining from "./normalizeMNISTDataForTraining";

export default async (model: tf.Sequential,
  trainDataTensor: tf.Tensor<tf.Rank>,
  trainLabelsTensor: tf.Tensor<tf.Rank>,
  setTest: [number, number[]][]) =>

  await model.fit(trainDataTensor, trainLabelsTensor, {
    epochs: 100,
    callbacks: getCallbacksForVisualizeTraining(),
    validationData: normalizeMNISTDataForTraining(setTest),
    batchSize: 1000,
  });