import { show } from "@tensorflow/tfjs-vis";

export default () => {
  const metrics = ['loss', 'val_loss', 'acc', 'val_acc'];

  const container = {
    name: 'show.fitCallbacks',
    tab: 'Training',
    styles: {
      height: '1000px'
    }
  };

  return show.fitCallbacks(container, metrics);
}