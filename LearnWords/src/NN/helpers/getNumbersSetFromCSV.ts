export const getNumbersSetPatchFromCsv = async (fileName: string, rowCount: number): Promise<[number, number[]][]> =>
  (await getNumbersSetFromCSV(fileName)).slice(0, rowCount);

const getNumbersSetFromCSV = async (fileName: string): Promise<[number, number[]][]> => {
  const CSVFile = await fetch(`./data/${fileName}.csv`);
  const text = await CSVFile.text();

  return text.split('\n').map(x => {
    const pictureDataArr = x.split(',').map(y => Number(y));

    return [pictureDataArr[0], pictureDataArr.splice(1)];
  });
}

export default getNumbersSetFromCSV;