import * as tf from '@tensorflow/tfjs';

export default (set: readonly [number, readonly number[]][]): [tf.Tensor, tf.Tensor] => {
  const data = set.map(x => x[1]);
  const labels = getLabel(set as any);

  return [
    tf.tensor(data).div(255).reshape([data.length, 28, 28, 1]),
    tf.tensor(labels).reshape([labels.length, 10]),
  ];
}

const getLabel = (set: readonly [number, readonly number[]][]) => set.map(x => {
  const arr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  arr[x[0]] = 1;

  return arr;
});
