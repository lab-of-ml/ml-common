import * as tf from '@tensorflow/tfjs';

export default async (model: tf.Sequential, set: readonly [number, readonly number[]][]) => {
  const testData = set.map(x => x[1]);
  const testLabels = set.map(x => x[0]);

  const testDataTensor = tf.tensor(testData).div(255);

  const predict = (model.predict(testDataTensor) as tf.Tensor);
  const predictData = await predict.array();

  const testResultData = await predict
    .reshape([set.length, 10])
    .argMax(1)
    .array() as number[];

  const rightResults = testLabels.filter((x, i) => x === testResultData[i]);

  return rightResults.length / testLabels.length;
}
