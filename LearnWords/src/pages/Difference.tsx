import React, { useRef, useState } from "react";
import { IWord, getWords, importWords } from "../wordsRepository";
import { addIterableMethodsInArray } from "iterate_library";

export default () => {
  const [words, setWords] = useState<IWord[]>([]);
  const textarea = useRef<HTMLTextAreaElement>(null as any);

  const showDifference = () => {
    const inputWords = textarea.current.value.split("\n");
    const allWordsSet = getWords();
    const allWords = addIterableMethodsInArray(allWordsSet.allWords).enumerableToMap(x => x.engWord, x => x);

    const selectedWords = inputWords
      .filter(x => !allWordsSet.wordsToKnowToMe.has(x) && allWords.has(x))
      .map(x => allWords.get(x) as IWord);

    setWords(selectedWords);
    // textarea.current.value = "";
  }

  return (
    <>
      <div>
        <textarea ref={textarea}></textarea>
        <button onClick={showDifference}>Show difference</button>
      </div>

      <table>
        <thead>
          <tr>
            <td>Count</td>
            <td>Word</td>
            <td>Translate</td>
          </tr>
        </thead>
        <tbody>
          {words.map((x, i, arr) =>
            <tr key={x.engWord}>
              <td>{arr.length - i}</td>
              <td>{x.engWord}</td>
              <td>{x.rusWord}</td>
            </tr>)}
        </tbody>
      </table>
    </>
  );
}