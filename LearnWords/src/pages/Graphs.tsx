import { render } from '@tensorflow/tfjs-vis';

export default () => {
  show();

  return <div></div>;
}

const show = () => {
  let values = getCombinationsPoints(10);

  // const metrics = ['loss', 'val_loss', 'acc', 'val_acc'];

  // const container = {
  //   name: 'show.fitCallbacks',
  //   tab: 'Training',
  //   styles: {
  //     height: '1000px'
  //   }
  // };

  render.linechart({ name: 'my Lines' }, { values });
}

const getCombinationsPoints = (variants: number) =>
  Array(variants + 1)
    .fill(0)
    .map((_, i) => ({ x: i, y: combinationsCount(variants, i) }));

const combinationsCount = (variants: number, places: number) => {
  const variantsFact = factorial(variants);
  const minusFact = factorial(variants - places);
  const remindFact = factorial(places);

  return variantsFact / (minusFact * remindFact);
}

const factorial = (n: number) => n === 0 ? 1 : Array(n).fill(null).map((e, i) => i + 1).reduce((p, c) => p * c);