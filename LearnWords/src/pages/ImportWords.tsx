import React, { useRef } from "react";
import { importWords } from "../wordsRepository";

export default () => {
  const textarea = useRef<HTMLTextAreaElement>(null as any);

  const importEnglandWords = () => {
    importWords(textarea.current.value.split("\n"), []);
    textarea.current.value = "";
  }

  return (
    <div>
      <textarea ref={textarea}></textarea>
      <button onClick={importEnglandWords}>Import words</button>
    </div>
  );
}

const strings = ['a', 'b', 'g', 'y', 'r', 't', 'a', 'r', 't', 'f', 'l', 'h', 's'];
const inputs = ['a', 't', 'r'];

const getInputs = (arr: string[], inp: string[]) => {
  let amount: { [key: string]: number } = {};
  let result: { [key: string]: number } = {};

  arr.forEach(s => amount[s] !== undefined ? amount[s]++ : amount[s] = 1);
  inp.forEach(i => result[i] = amount[i]);

  return result;
}

console.log(getInputs(strings, inputs));
