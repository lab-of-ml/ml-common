import * as tf from '@tensorflow/tfjs';
import { useState } from 'react';
import { getNumbersSetPatchFromCsv } from '../NN/helpers/getNumbersSetFromCSV';
import getCallbacksForVisualizeTraining from '../NN/helpers/getCallbacksForVisualizeTraining';
import normalizeMNISTDataForTraining from '../NN/helpers/normalizeMNISTDataForTraining';
import predictMNISTModel from '../NN/helpers/predictMNISTModel';
import fitModel from '../NN/helpers/fitModel';
import getClassicMNISTModel from '../NN/classicModels/getClassicMNISTModel';
import getConvolutionalMNISTModel from '../NN/convolutionalModels/getConvolutionalMNISTModel';

export default () => {
  const [accuracy, setAccuracy] = useState([0, 0]);
  const [isInTraining, setIsInTraining] = useState(false);

  const trainClassicModel = async () =>
    executeTraining(async () => {
      const accuracy = await getAccuracy(getClassicMNISTModel());
      setAccuracy(accuracy);
    });

  const trainConvolutionalModel = async () =>
    executeTraining(async () => {
      const accuracy = await getAccuracy(getConvolutionalMNISTModel());
      setAccuracy(accuracy);
    })

  const executeTraining = async (trainingModelFunc: () => Promise<void>) => {
    setIsInTraining(true);
    await trainingModelFunc();
    setIsInTraining(false);
  }

  return (
    <div id="demo">
      <button disabled={isInTraining} onClick={trainClassicModel}>Train classic model</button>
      <button disabled={isInTraining} onClick={trainConvolutionalModel}>Train convolutional model</button>
      {`Train data accuracy: ${accuracy[0]}. Test data accuracy: ${accuracy[1]}`}
    </div>
  );
}

const getAccuracy = async (model: tf.Sequential) => {
  const setTest = await getNumbersSetPatchFromCsv('mnist_test', 10_000);
  const setTrain = await getNumbersSetPatchFromCsv('mnist_train', 60_000);

  const [trainDataTensor, trainLabelsTensor] = normalizeMNISTDataForTraining(setTrain);
  await fitModel(model, trainDataTensor, trainLabelsTensor, setTest);

  // await model.save('downloads://picture-model');

  return [await predictMNISTModel(model, setTrain), await predictMNISTModel(model, setTest)];
}
