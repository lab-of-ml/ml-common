import { useState } from "react";
import { addWordsToKnowToMe, addWordsToUnknownToMe, getWords, undo } from "../wordsRepository";

export default () => {
  const [words, setWords] = useState(getWords());

  const knownWord = (wordInEnglish: string) => {
    addWordsToKnowToMe(wordInEnglish);
    setWords(getWords());
  };

  const dontKnownWord = (wordInEnglish: string) => {
    addWordsToUnknownToMe(wordInEnglish);
    setWords(getWords());
  };
  const undoLastAction = () => {
    undo();
    setWords(getWords());
  }

  return (
    <table>
      <thead>
        <div>{`Words you know: ${words.wordsToKnowToMe.size}`}</div>
        <div>{`Words you don't know: ${words.wordsToUnknownToMe.size}`}</div>
        <button onClick={undoLastAction}>Undo</button>
      </thead>
      <tbody>
        {words.remainWords.map((x, i, arr) =>
          <tr key={x.engWord}>
            <td>{arr.length - i}</td>
            <td>{x.engWord}</td>
            <td><button onClick={() => knownWord(x.engWord)}>V</button></td>
            <td><button onClick={() => dontKnownWord(x.engWord)}>X</button></td>
            <td>{x.rusWord}</td>
          </tr>)}
      </tbody>
    </table>
  );
}