import { useEffect, useRef } from "react";
import styles from "./Tree.module.css";
import { useState } from "react";
import { memo } from "react";

const Tree = memo(<TTree extends { innerElements: TTree[] }>(props: { tree: TTree }) => {
  const ref = useRef(0);
  useEffect(() => { ref.current += 1 },);
  // const [val, setVal] = useState(false);

  return (
    <>
      {/* <button onClick={() => setVal(!val)} >{val + ""}</button> */}
      <div className={styles.node}>
        <div className={styles.information}>
          {`Count of render ${ref.current}`}
        </div>
        {
          props.tree.innerElements.length > 0 ?
            <div className={styles["inner-elements"]}>
              {/*@ts-ignore */}
              <InnerElementsContainer innerElements={props.tree.innerElements} />
            </div>
            : null
        }
      </div >
    </>
  );
});

const InnerElementsContainer =
  memo(<TTree extends { innerElements: TTree[] }>(props: { innerElements: TTree[] }) =>
    <>
      {/*@ts-ignore */}
      {props.innerElements.map(x => <Tree tree={x} />)}
    </>);

export default Tree;
