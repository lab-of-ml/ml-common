type RepositoryWordsNames = typeof wordsToKnowToMeRepositoryName |
  typeof wordsToUnknownToMeRepositoryName;

type RepositoryNames = typeof wordsToKnowToMeRepositoryName |
  typeof wordsToUnknownToMeRepositoryName |
  typeof historyRepositoryName;

enum Actions {
  addWordsToKnowToMe = 0,
  addWordsToUnknownToMe = 1,
}

const wordsToKnowToMeRepositoryName = "wordsToKnowToMe";
const wordsToUnknownToMeRepositoryName = "wordsToUnknownToMe";
const historyRepositoryName = "history";

const initRepository = () => {
  initEmptyRepositoryByName(wordsToKnowToMeRepositoryName);
  initEmptyRepositoryByName(wordsToUnknownToMeRepositoryName);
  initEmptyRepositoryByName(historyRepositoryName);
}

const initEmptyRepositoryByName = (repositoryName: RepositoryNames) => (localStorage.getItem(repositoryName) ??
  localStorage.setItem(repositoryName, "[]"));


initRepository();

export const addWordsToKnowToMe = (wordInEnglish: string) => {
  addWordInRepository([wordInEnglish], wordsToKnowToMeRepositoryName);
  addActionInHistory(Actions.addWordsToKnowToMe);
}

export const addWordsToUnknownToMe = (wordInEnglish: string) => {
  addWordInRepository([wordInEnglish], wordsToUnknownToMeRepositoryName);
  addActionInHistory(Actions.addWordsToUnknownToMe);
}

export const importWords = (wordsToKnowToMe: string[], wordsToUnknownToMe: string[]) => {
  addWordInRepository(wordsToKnowToMe, wordsToKnowToMeRepositoryName);
  addWordInRepository(wordsToUnknownToMe, wordsToUnknownToMeRepositoryName);
}

export const exportWords = () => ({
  wordsToKnowToMe: getRepositoryByName(wordsToKnowToMeRepositoryName),
  wordsToUnknownToMe: getRepositoryByName(wordsToUnknownToMeRepositoryName),
});

export const undo = () =>
  saveHistory(oldHistory => {
    const lastAction = oldHistory.pop();

    if (lastAction !== undefined)
      undoLastAction(lastAction);

    return oldHistory;
  });

const undoLastAction = (lastAction: Actions) => {
  switch (lastAction) {
    case Actions.addWordsToKnowToMe: removeLastWordFromRepository(wordsToKnowToMeRepositoryName);
      break;
    case Actions.addWordsToUnknownToMe: removeLastWordFromRepository(wordsToUnknownToMeRepositoryName);
      break;
    default: throw new TypeError("Unknown action!");
  }
}

const addWordInRepository = (wordsInEnglish: string[], repositoryName: RepositoryWordsNames) => {
  const repository = getRepositoryByName(repositoryName);
  wordsInEnglish.forEach(x => repository.add(x));
  localStorage.setItem(repositoryName, JSON.stringify([...repository]));
}

const removeLastWordFromRepository = (repositoryName: RepositoryWordsNames) => {
  const repository = getRepositoryByName(repositoryName);
  const newRepository = [...repository];
  newRepository.pop();
  localStorage.setItem(repositoryName, JSON.stringify(newRepository));
}

const addActionInHistory = (action: Actions) =>
  saveHistory(oldHistory => {
    oldHistory.push(action);
    return oldHistory;
  });

const saveHistory = (gwtNewHistory: (oldHistory: Actions[]) => Actions[]) => {
  const oldHistory = getHistory();
  const newHistory = gwtNewHistory(oldHistory)
  setHistory(newHistory);
}

const getHistory = () => JSON.parse(localStorage.getItem(historyRepositoryName)!) as Actions[];
const setHistory = (newHistory: Actions[]) => localStorage.setItem(historyRepositoryName, JSON.stringify(newHistory));

export const getWords = () => {
  const wordsToKnowToMe = getRepositoryByName(wordsToKnowToMeRepositoryName);
  const wordsToUnknownToMe = getRepositoryByName(wordsToUnknownToMeRepositoryName);

  return {
    remainWords: allWords
      .filter(x => !wordsToKnowToMe.has(x.engWord) && !wordsToUnknownToMe.has(x.engWord))
      .sort((x, y) => x.engWord < y.engWord ? 0 : 1),

    wordsToKnowToMe,
    wordsToUnknownToMe,
    allWords,
  };
}

const getRepositoryByName = (repositoryName: RepositoryNames) =>
  new Set<string>(JSON.parse(localStorage.getItem(repositoryName)!));

export interface IWord {
  engWord: string;
  rusWord: string;
  transcription: string;
}

const allWords: IWord[] = [
  {
    "engWord": "corrupt",
    "rusWord": "развращать",
    "transcription": "[kəˈrʌpt]"
  },
  {
    "engWord": "neutral",
    "rusWord": "нейтральный",
    "transcription": "[ˈnjuːtrəl]"
  },
  {
    "engWord": "signing",
    "rusWord": "подписание",
    "transcription": "[ˈsaɪnɪŋ]"
  },
  {
    "engWord": "latter",
    "rusWord": "последний",
    "transcription": "[ˈlætə]"
  },
  {
    "engWord": "granted",
    "rusWord": "предоставленный",
    "transcription": "[ˈgrɑːntɪd]"
  },
  {
    "engWord": "merely",
    "rusWord": "просто",
    "transcription": "[ˈmɪəlɪ]"
  },
  {
    "engWord": "commercial",
    "rusWord": "коммерческий",
    "transcription": "[kəˈmɜːʃəl]"
  },
  {
    "engWord": "pine",
    "rusWord": "сосна",
    "transcription": "[paɪn]"
  },
  {
    "engWord": "disgusting",
    "rusWord": "отвратительный",
    "transcription": "[dɪsˈgʌstɪŋ]"
  },
  {
    "engWord": "settled",
    "rusWord": "установившийся",
    "transcription": "[setld]"
  },
  {
    "engWord": "becoming",
    "rusWord": "подобающий",
    "transcription": "[bɪˈkʌmɪŋ]"
  },
  {
    "engWord": "score",
    "rusWord": "счет",
    "transcription": "[skɔː]"
  },
  {
    "engWord": "proper",
    "rusWord": "правильный",
    "transcription": "[ˈprɒpə]"
  },
  {
    "engWord": "would",
    "rusWord": "бы",
    "transcription": "[wʊd]"
  },
  {
    "engWord": "animal",
    "rusWord": "животное",
    "transcription": "[ˈænɪməl]"
  },
  {
    "engWord": "lying",
    "rusWord": "лежащий",
    "transcription": "[ˈlaɪɪŋ]"
  },
  {
    "engWord": "topic",
    "rusWord": "тема",
    "transcription": "[ˈtɒpɪk]"
  },
  {
    "engWord": "harass",
    "rusWord": "беспокоить",
    "transcription": "[ˈhærəs]"
  },
  {
    "engWord": "exile",
    "rusWord": "ссыльный",
    "transcription": "[ˈeksaɪl]"
  },
  {
    "engWord": "foreigner",
    "rusWord": "иностранец",
    "transcription": "[ˈfɒrɪnə]"
  },
  {
    "engWord": "cascade",
    "rusWord": "каскад",
    "transcription": "[kæsˈkeɪd]"
  },
  {
    "engWord": "magical",
    "rusWord": "магический",
    "transcription": "[ˈmæʤɪkəl]"
  },
  {
    "engWord": "suite",
    "rusWord": "комплект",
    "transcription": "[swiːt]"
  },
  {
    "engWord": "initiative",
    "rusWord": "инициатива",
    "transcription": "[ɪˈnɪʃətɪv]"
  },
  {
    "engWord": "eighty",
    "rusWord": "восемьдесят",
    "transcription": "[ˈeɪtɪ]"
  },
  {
    "engWord": "undertake",
    "rusWord": "предпринимать",
    "transcription": "[ʌndəˈteɪk]"
  },
  {
    "engWord": "pie",
    "rusWord": "пирог",
    "transcription": "[paɪ]"
  },
  {
    "engWord": "clinic",
    "rusWord": "лечебница",
    "transcription": "[ˈklɪnɪk]"
  },
  {
    "engWord": "badly",
    "rusWord": "дурно",
    "transcription": "[ˈbædlɪ]"
  },
  {
    "engWord": "royal",
    "rusWord": "королевский",
    "transcription": "[ˈrɔɪəl]"
  },
  {
    "engWord": "word",
    "rusWord": "слово",
    "transcription": "[wɜːd]"
  },
  {
    "engWord": "equal",
    "rusWord": "равный",
    "transcription": "[ˈiːkwəl]"
  },
  {
    "engWord": "overestimate",
    "rusWord": "переоценивать",
    "transcription": "[əʊvərˈestɪmɪt]"
  },
  {
    "engWord": "modify",
    "rusWord": "модифицировать",
    "transcription": "[ˈmɒdɪfaɪ]"
  },
  {
    "engWord": "wizard",
    "rusWord": "волшебник",
    "transcription": "[ˈwɪzəd]"
  },
  {
    "engWord": "ant",
    "rusWord": "муравей",
    "transcription": "[ænt]"
  },
  {
    "engWord": "sweater",
    "rusWord": "свитер",
    "transcription": "[ˈswetə]"
  },
  {
    "engWord": "institution",
    "rusWord": "учреждение",
    "transcription": "[ɪnstɪˈtjuːʃn]"
  },
  {
    "engWord": "erosion",
    "rusWord": "выветривание",
    "transcription": "[ɪˈrəʊʒən]"
  },
  {
    "engWord": "swimming",
    "rusWord": "плавание",
    "transcription": "[ˈswɪmɪŋ]"
  },
  {
    "engWord": "pretend",
    "rusWord": "притворяться",
    "transcription": "[prɪˈtend]"
  },
  {
    "engWord": "suspect",
    "rusWord": "подозревать",
    "transcription": "[]"
  },
  {
    "engWord": "narrow",
    "rusWord": "узкий",
    "transcription": "[ˈnærəʊ]"
  },
  {
    "engWord": "around",
    "rusWord": "вокруг",
    "transcription": "[əˈraʊnd]"
  },
  {
    "engWord": "note",
    "rusWord": "записка",
    "transcription": "[nəʊt]"
  },
  {
    "engWord": "high",
    "rusWord": "высокий",
    "transcription": "[haɪ]"
  },
  {
    "engWord": "select",
    "rusWord": "выбирать",
    "transcription": "[sɪˈlekt]"
  },
  {
    "engWord": "core",
    "rusWord": "ядро",
    "transcription": "[kɔː]"
  },
  {
    "engWord": "robe",
    "rusWord": "мантия",
    "transcription": "[rəʊb]"
  },
  {
    "engWord": "outlook",
    "rusWord": "прогноз",
    "transcription": "[ˈaʊtlʊk]"
  },
  {
    "engWord": "sofa",
    "rusWord": "диван",
    "transcription": "[ˈsəʊfə]"
  },
  {
    "engWord": "tips",
    "rusWord": "типсы",
    "transcription": "[tɪps]"
  },
  {
    "engWord": "swell",
    "rusWord": "набухать",
    "transcription": "[swel]"
  },
  {
    "engWord": "October",
    "rusWord": "октябрь",
    "transcription": "[ɒkˈtəʊbə]"
  },
  {
    "engWord": "mate",
    "rusWord": "приятель",
    "transcription": "[meɪt]"
  },
  {
    "engWord": "shave",
    "rusWord": "брить",
    "transcription": "[ʃeɪv]"
  },
  {
    "engWord": "surgeon",
    "rusWord": "хирург",
    "transcription": "[ˈsɜːʤən]"
  },
  {
    "engWord": "administration",
    "rusWord": "администрация",
    "transcription": "[ədmɪnɪsˈtreɪʃn]"
  },
  {
    "engWord": "motive",
    "rusWord": "мотив",
    "transcription": "[ˈməʊtɪv]"
  },
  {
    "engWord": "severe",
    "rusWord": "суровый",
    "transcription": "[sɪˈvɪə]"
  },
  {
    "engWord": "anytime",
    "rusWord": "в любое время",
    "transcription": "[ˈenɪtaɪm]"
  },
  {
    "engWord": "bedroom",
    "rusWord": "спальня",
    "transcription": "[ˈbedrum]"
  },
  {
    "engWord": "evil",
    "rusWord": "зло",
    "transcription": "[iːvl]"
  },
  {
    "engWord": "department",
    "rusWord": "отдел",
    "transcription": "[dɪˈpɑːtmənt]"
  },
  {
    "engWord": "seal",
    "rusWord": "печать",
    "transcription": "[siːl]"
  },
  {
    "engWord": "great",
    "rusWord": "отличный",
    "transcription": "[greɪt]"
  },
  {
    "engWord": "guide",
    "rusWord": "руководство",
    "transcription": "[gaɪd]"
  },
  {
    "engWord": "stop",
    "rusWord": "остановить",
    "transcription": "[stɒp]"
  },
  {
    "engWord": "green",
    "rusWord": "зеленый",
    "transcription": "[griːn]"
  },
  {
    "engWord": "continue",
    "rusWord": "продолжить",
    "transcription": "[kənˈtɪnjuː]"
  },
  {
    "engWord": "town",
    "rusWord": "город",
    "transcription": "[taʊn]"
  },
  {
    "engWord": "peach",
    "rusWord": "персик",
    "transcription": "[piːʧ]"
  },
  {
    "engWord": "musician",
    "rusWord": "музыкант",
    "transcription": "[mjuːˈzɪʃn]"
  },
  {
    "engWord": "gown",
    "rusWord": "платье",
    "transcription": "[gaʊn]"
  },
  {
    "engWord": "carbon",
    "rusWord": "углерод",
    "transcription": "[ˈkɑːbən]"
  },
  {
    "engWord": "escort",
    "rusWord": "конвоир",
    "transcription": "[ˈeskɔːt]"
  },
  {
    "engWord": "stuffed",
    "rusWord": "набитый",
    "transcription": "[stʌft]"
  },
  {
    "engWord": "disappearance",
    "rusWord": "исчезновение",
    "transcription": "[dɪsəˈpɪərəns]"
  },
  {
    "engWord": "accomplish",
    "rusWord": "выполнять",
    "transcription": "[əˈkʌmplɪʃ]"
  },
  {
    "engWord": "burden",
    "rusWord": "бремя",
    "transcription": "[bɜːdn]"
  },
  {
    "engWord": "civilize",
    "rusWord": "цивилизовать",
    "transcription": "[ˈsɪv(ə)laɪz]"
  },
  {
    "engWord": "prayer",
    "rusWord": "молитва",
    "transcription": "[preə]"
  },
  {
    "engWord": "height",
    "rusWord": "высота",
    "transcription": "[haɪt]"
  },
  {
    "engWord": "pile",
    "rusWord": "куча",
    "transcription": "[paɪl]"
  },
  {
    "engWord": "justify",
    "rusWord": "оправдывать",
    "transcription": "[ˈʤʌstɪfaɪ]"
  },
  {
    "engWord": "lawyer",
    "rusWord": "адвокат",
    "transcription": "[ˈlɔːjə]"
  },
  {
    "engWord": "training",
    "rusWord": "обучение",
    "transcription": "[ˈtreɪnɪŋ]"
  },
  {
    "engWord": "however",
    "rusWord": "однако",
    "transcription": "[haʊˈevə]"
  },
  {
    "engWord": "meal",
    "rusWord": "еда",
    "transcription": "[miːl]"
  },
  {
    "engWord": "service",
    "rusWord": "обслуживание",
    "transcription": "[ˈsɜːvɪs]"
  },
  {
    "engWord": "belong",
    "rusWord": "принадлежать",
    "transcription": "[bɪˈlɒŋ]"
  },
  {
    "engWord": "stupid",
    "rusWord": "глупый",
    "transcription": "[ˈstjuːpɪd]"
  },
  {
    "engWord": "fantastic",
    "rusWord": "фантастический",
    "transcription": "[fænˈtæstɪk]"
  },
  {
    "engWord": "vegetable",
    "rusWord": "овощ",
    "transcription": "[ˈveʤ(ɪ)təb(ə)l]"
  },
  {
    "engWord": "shout",
    "rusWord": "крик",
    "transcription": "[ʃaʊt]"
  },
  {
    "engWord": "real",
    "rusWord": "реальный",
    "transcription": "[rɪəl]"
  },
  {
    "engWord": "west",
    "rusWord": "запад",
    "transcription": "[west]"
  },
  {
    "engWord": "seem",
    "rusWord": "казаться",
    "transcription": "[siːm]"
  },
  {
    "engWord": "boy",
    "rusWord": "мальчик",
    "transcription": "[bɔɪ]"
  },
  {
    "engWord": "stretch",
    "rusWord": "простираться",
    "transcription": "[streʧ]"
  },
  {
    "engWord": "phrase",
    "rusWord": "фраза",
    "transcription": "[freɪz]"
  },
  {
    "engWord": "reconsider",
    "rusWord": "пересматривать",
    "transcription": "[riːkənˈsɪdə]"
  },
  {
    "engWord": "worthless",
    "rusWord": "бесполезный",
    "transcription": "[ˈwɜːθlɪs]"
  },
  {
    "engWord": "warden",
    "rusWord": "надзиратель",
    "transcription": "[wɔːdn]"
  },
  {
    "engWord": "manufacture",
    "rusWord": "производство",
    "transcription": "[mænjʊˈfækʧə]"
  },
  {
    "engWord": "basis",
    "rusWord": "основа",
    "transcription": "[ˈbeɪsɪs]"
  },
  {
    "engWord": "importantly",
    "rusWord": "важно",
    "transcription": "[ɪmˈpɔːtntlɪ]"
  },
  {
    "engWord": "outward",
    "rusWord": "внешний",
    "transcription": "[ˈaʊtwəd]"
  },
  {
    "engWord": "messy",
    "rusWord": "беспорядочный",
    "transcription": "[ˈmesɪ]"
  },
  {
    "engWord": "pudding",
    "rusWord": "пудинг",
    "transcription": "[ˈpʊdɪŋ]"
  },
  {
    "engWord": "kidney",
    "rusWord": "почка",
    "transcription": "[ˈkɪdnɪ]"
  },
  {
    "engWord": "highway",
    "rusWord": "шоссе",
    "transcription": "[ˈhaɪweɪ]"
  },
  {
    "engWord": "metre",
    "rusWord": "метр",
    "transcription": "[ˈmiːtə]"
  },
  {
    "engWord": "herself",
    "rusWord": "сама",
    "transcription": "[(h)əˈself]"
  },
  {
    "engWord": "following",
    "rusWord": "следующий",
    "transcription": "[ˈfɒləʊɪŋ]"
  },
  {
    "engWord": "doze",
    "rusWord": "дремать",
    "transcription": "[dəʊz]"
  },
  {
    "engWord": "similar",
    "rusWord": "схожий",
    "transcription": "[ˈsɪm(ə)lə]"
  },
  {
    "engWord": "result",
    "rusWord": "результат",
    "transcription": "[rɪˈzʌlt]"
  },
  {
    "engWord": "oneself",
    "rusWord": "сам",
    "transcription": "[wɒnˈself]"
  },
  {
    "engWord": "advertise",
    "rusWord": "рекламировать",
    "transcription": "[ˈædvətaɪz]"
  },
  {
    "engWord": "expose",
    "rusWord": "разоблачать",
    "transcription": "[ɪkˈspəʊz]"
  },
  {
    "engWord": "triumph",
    "rusWord": "триумф",
    "transcription": "[ˈtraɪəmf]"
  },
  {
    "engWord": "tuck",
    "rusWord": "подгибать",
    "transcription": "[tʌk]"
  },
  {
    "engWord": "portrait",
    "rusWord": "портрет",
    "transcription": "[ˈpɔːtrɪt]"
  },
  {
    "engWord": "hopeless",
    "rusWord": "безнадежный",
    "transcription": "[ˈhəʊplɪs]"
  },
  {
    "engWord": "border",
    "rusWord": "граница",
    "transcription": "[ˈbɔːdə]"
  },
  {
    "engWord": "depressed",
    "rusWord": "подавленный",
    "transcription": "[dɪˈprest]"
  },
  {
    "engWord": "identify",
    "rusWord": "идентифицировать",
    "transcription": "[aɪˈdentɪfaɪ]"
  },
  {
    "engWord": "courage",
    "rusWord": "мужество",
    "transcription": "[ˈkʌrɪʤ]"
  },
  {
    "engWord": "settlement",
    "rusWord": "поселок",
    "transcription": "[ˈsetlmənt]"
  },
  {
    "engWord": "college",
    "rusWord": "колледж",
    "transcription": "[ˈkɒlɪʤ]"
  },
  {
    "engWord": "convince",
    "rusWord": "убеждать",
    "transcription": "[kənˈvɪns]"
  },
  {
    "engWord": "salad",
    "rusWord": "салат",
    "transcription": "[ˈsæləd]"
  },
  {
    "engWord": "tourist",
    "rusWord": "турист",
    "transcription": "[ˈtʊərɪst]"
  },
  {
    "engWord": "fall",
    "rusWord": "падать",
    "transcription": "[fɔːl]"
  },
  {
    "engWord": "poem",
    "rusWord": "поэма",
    "transcription": "[ˈpəʊɪm]"
  },
  {
    "engWord": "tree",
    "rusWord": "дерево",
    "transcription": "[triː]"
  },
  {
    "engWord": "notebook",
    "rusWord": "блокнот",
    "transcription": "[ˈnəʊtbʊk]"
  },
  {
    "engWord": "commitment",
    "rusWord": "обязательство",
    "transcription": "[kəˈmɪtmənt]"
  },
  {
    "engWord": "tame",
    "rusWord": "приручать",
    "transcription": "[teɪm]"
  },
  {
    "engWord": "creek",
    "rusWord": "залив",
    "transcription": "[kriːk]"
  },
  {
    "engWord": "coercive",
    "rusWord": "принудительный",
    "transcription": "[kəʊˈɜːsɪv]"
  },
  {
    "engWord": "fibber",
    "rusWord": "выдумщик",
    "transcription": "[ˈfɪbə]"
  },
  {
    "engWord": "gradual",
    "rusWord": "постепенный",
    "transcription": "[ˈgræʤʊəl]"
  },
  {
    "engWord": "possess",
    "rusWord": "обладать",
    "transcription": "[pəˈzes]"
  },
  {
    "engWord": "adventure",
    "rusWord": "приключение",
    "transcription": "[ədˈvenʧə]"
  },
  {
    "engWord": "calculate",
    "rusWord": "вычислять",
    "transcription": "[ˈkælkjʊleɪt]"
  },
  {
    "engWord": "pilot",
    "rusWord": "летчик",
    "transcription": "[ˈpaɪlɒt]"
  },
  {
    "engWord": "changing",
    "rusWord": "меняющийся",
    "transcription": "[ˈʧeɪnʤɪŋ]"
  },
  {
    "engWord": "lesson",
    "rusWord": "урок",
    "transcription": "[lesn]"
  },
  {
    "engWord": "brush",
    "rusWord": "щетка",
    "transcription": "[brʌʃ]"
  },
  {
    "engWord": "board",
    "rusWord": "правление",
    "transcription": "[bɔːd]"
  },
  {
    "engWord": "end",
    "rusWord": "конец",
    "transcription": "[end]"
  },
  {
    "engWord": "ago",
    "rusWord": "назад",
    "transcription": "[əˈgəʊ]"
  },
  {
    "engWord": "rule",
    "rusWord": "правило",
    "transcription": "[ruːl]"
  },
  {
    "engWord": "omission",
    "rusWord": "упущение",
    "transcription": "[əˈmɪʃn]"
  },
  {
    "engWord": "December",
    "rusWord": "декабрь",
    "transcription": "[dɪˈsembə]"
  },
  {
    "engWord": "bumper",
    "rusWord": "бампер",
    "transcription": "[ˈbʌmpə]"
  },
  {
    "engWord": "denying",
    "rusWord": "отрицающий",
    "transcription": "[dɪˈnaɪɪŋ]"
  },
  {
    "engWord": "cocktail",
    "rusWord": "коктейль",
    "transcription": "[ˈkɒkteɪl]"
  },
  {
    "engWord": "ankle",
    "rusWord": "лодыжка",
    "transcription": "[æŋkl]"
  },
  {
    "engWord": "weigh",
    "rusWord": "весить",
    "transcription": "[weɪ]"
  },
  {
    "engWord": "execution",
    "rusWord": "исполнение",
    "transcription": "[eksɪˈkjuːʃn]"
  },
  {
    "engWord": "Italy",
    "rusWord": "Италия",
    "transcription": "[ˈɪtəlɪ]"
  },
  {
    "engWord": "precisely",
    "rusWord": "точно",
    "transcription": "[prɪˈsaɪslɪ]"
  },
  {
    "engWord": "strength",
    "rusWord": "сила",
    "transcription": "[streŋθ]"
  },
  {
    "engWord": "dig",
    "rusWord": "копать",
    "transcription": "[dɪg]"
  },
  {
    "engWord": "darling",
    "rusWord": "любимец",
    "transcription": "[ˈdɑːlɪŋ]"
  },
  {
    "engWord": "fix",
    "rusWord": "исправить",
    "transcription": "[fɪks]"
  },
  {
    "engWord": "yes",
    "rusWord": "да",
    "transcription": "[jes]"
  },
  {
    "engWord": "speed",
    "rusWord": "скорость",
    "transcription": "[spiːd]"
  },
  {
    "engWord": "little",
    "rusWord": "маленький",
    "transcription": "[lɪtl]"
  },
  {
    "engWord": "rain",
    "rusWord": "дождь",
    "transcription": "[reɪn]"
  },
  {
    "engWord": "that",
    "rusWord": "что",
    "transcription": "[ðæt]"
  },
  {
    "engWord": "skin",
    "rusWord": "кожа",
    "transcription": "[skɪn]"
  },
  {
    "engWord": "blank",
    "rusWord": "пустой",
    "transcription": "[blæŋk]"
  },
  {
    "engWord": "difficulty",
    "rusWord": "трудность",
    "transcription": "[ˈdɪfɪkəltɪ]"
  },
  {
    "engWord": "depressing",
    "rusWord": "унылый",
    "transcription": "[dɪˈpresɪŋ]"
  },
  {
    "engWord": "literary",
    "rusWord": "буквальный",
    "transcription": "[ˈlɪtərərɪ]"
  },
  {
    "engWord": "celebrity",
    "rusWord": "знаменитость",
    "transcription": "[sɪˈlebrɪtɪ]"
  },
  {
    "engWord": "flirting",
    "rusWord": "флирт",
    "transcription": "[ˈflɜːtɪŋ]"
  },
  {
    "engWord": "thoughtful",
    "rusWord": "задумчивый",
    "transcription": "[ˈθɔːtf(ə)l]"
  },
  {
    "engWord": "seaweed",
    "rusWord": "морская водоросль",
    "transcription": "[ˈsiːwiːd]"
  },
  {
    "engWord": "estate",
    "rusWord": "имущество",
    "transcription": "[ɪsˈteɪt]"
  },
  {
    "engWord": "league",
    "rusWord": "Лига",
    "transcription": "[liːg]"
  },
  {
    "engWord": "urban",
    "rusWord": "городской",
    "transcription": "[ˈɜːbən]"
  },
  {
    "engWord": "preference",
    "rusWord": "предпочтение",
    "transcription": "[ˈprefərəns]"
  },
  {
    "engWord": "input",
    "rusWord": "вход",
    "transcription": "[ˈɪnpʊt]"
  },
  {
    "engWord": "identity",
    "rusWord": "идентичность",
    "transcription": "[aɪˈdentɪtɪ]"
  },
  {
    "engWord": "pity",
    "rusWord": "жалость",
    "transcription": "[ˈpɪtɪ]"
  },
  {
    "engWord": "confession",
    "rusWord": "признание",
    "transcription": "[kənˈfeʃn]"
  },
  {
    "engWord": "government",
    "rusWord": "правительство",
    "transcription": "[ˈgʌv(ə)mənt]"
  },
  {
    "engWord": "fancy",
    "rusWord": "маскарадный",
    "transcription": "[ˈfænsɪ]"
  },
  {
    "engWord": "taught",
    "rusWord": "наученный",
    "transcription": "[tɔːt]"
  },
  {
    "engWord": "litre",
    "rusWord": "литр",
    "transcription": "[ˈliːtə]"
  },
  {
    "engWord": "wedding",
    "rusWord": "свадьба",
    "transcription": "[ˈwedɪŋ]"
  },
  {
    "engWord": "article",
    "rusWord": "статья",
    "transcription": "[ˈɑːtɪkl]"
  },
  {
    "engWord": "should",
    "rusWord": "должны",
    "transcription": "[ʃʊd]"
  },
  {
    "engWord": "piece",
    "rusWord": "кусок",
    "transcription": "[piːs]"
  },
  {
    "engWord": "perpendicular",
    "rusWord": "перпендикуляр",
    "transcription": "[pɜːpənˈdɪkjʊlə]"
  },
  {
    "engWord": "persist",
    "rusWord": "настаивать",
    "transcription": "[pəˈsɪst]"
  },
  {
    "engWord": "hypnotize",
    "rusWord": "гипнотизировать",
    "transcription": "[ˈhɪpnətaɪz]"
  },
  {
    "engWord": "viewpoint",
    "rusWord": "точка зрения",
    "transcription": "[ˈvjuːpɔɪnt]"
  },
  {
    "engWord": "weekly",
    "rusWord": "еженедельно",
    "transcription": "[ˈwiːklɪ]"
  },
  {
    "engWord": "shiny",
    "rusWord": "блестящий",
    "transcription": "[ˈʃaɪnɪ]"
  },
  {
    "engWord": "monument",
    "rusWord": "памятник",
    "transcription": "[ˈmɒnjʊmənt]"
  },
  {
    "engWord": "bow",
    "rusWord": "лук",
    "transcription": "[bəʊ]"
  },
  {
    "engWord": "oblivion",
    "rusWord": "забвение",
    "transcription": "[əˈblɪvɪən]"
  },
  {
    "engWord": "recover",
    "rusWord": "восстановиться",
    "transcription": "[rɪˈkʌvə]"
  },
  {
    "engWord": "lecture",
    "rusWord": "лекция",
    "transcription": "[ˈlekʧə]"
  },
  {
    "engWord": "freezing",
    "rusWord": "заморозка",
    "transcription": "[ˈfriːzɪŋ]"
  },
  {
    "engWord": "loser",
    "rusWord": "неудачник",
    "transcription": "[ˈluːzə]"
  },
  {
    "engWord": "march",
    "rusWord": "март",
    "transcription": "[mɑːʧ]"
  },
  {
    "engWord": "mission",
    "rusWord": "миссия",
    "transcription": "[mɪʃn]"
  },
  {
    "engWord": "give",
    "rusWord": "дать",
    "transcription": "[gɪv]"
  },
  {
    "engWord": "weight",
    "rusWord": "вес",
    "transcription": "[weɪt]"
  },
  {
    "engWord": "cell",
    "rusWord": "ячейка",
    "transcription": "[sel]"
  },
  {
    "engWord": "watch",
    "rusWord": "часы",
    "transcription": "[wɒʧ]"
  },
  {
    "engWord": "chaos",
    "rusWord": "бардак",
    "transcription": "[ˈkeɪɒs]"
  },
  {
    "engWord": "humour",
    "rusWord": "юмор",
    "transcription": "[ˈhjuːmə]"
  },
  {
    "engWord": "multiple",
    "rusWord": "многократный",
    "transcription": "[ˈmʌltɪpl]"
  },
  {
    "engWord": "safely",
    "rusWord": "безопасно",
    "transcription": "[ˈseɪflɪ]"
  },
  {
    "engWord": "mere",
    "rusWord": "простой",
    "transcription": "[mɪə]"
  },
  {
    "engWord": "conscience",
    "rusWord": "совесть",
    "transcription": "[ˈkɒnʃns]"
  },
  {
    "engWord": "probably",
    "rusWord": "возможно",
    "transcription": "[ˈprɒbəblɪ]"
  },
  {
    "engWord": "joke",
    "rusWord": "шутка",
    "transcription": "[ʤəʊk]"
  },
  {
    "engWord": "week",
    "rusWord": "неделя",
    "transcription": "[wiːk]"
  },
  {
    "engWord": "since",
    "rusWord": "с",
    "transcription": "[sɪns]"
  },
  {
    "engWord": "corner",
    "rusWord": "угол",
    "transcription": "[ˈkɔːnə]"
  },
  {
    "engWord": "coupon",
    "rusWord": "купон",
    "transcription": "[ˈkuːpɒn]"
  },
  {
    "engWord": "thankful",
    "rusWord": "благодарный",
    "transcription": "[ˈθæŋkf(ə)l]"
  },
  {
    "engWord": "vomit",
    "rusWord": "рвота",
    "transcription": "[ˈvɒmɪt]"
  },
  {
    "engWord": "gesture",
    "rusWord": "жест",
    "transcription": "[ˈʤesʧə]"
  },
  {
    "engWord": "punish",
    "rusWord": "наказывать",
    "transcription": "[ˈpʌnɪʃ]"
  },
  {
    "engWord": "hemisphere",
    "rusWord": "полусфера",
    "transcription": "[ˈhemɪsfɪə]"
  },
  {
    "engWord": "loft",
    "rusWord": "лофт",
    "transcription": "[lɒft]"
  },
  {
    "engWord": "quote",
    "rusWord": "цитата",
    "transcription": "[kwəʊt]"
  },
  {
    "engWord": "actor",
    "rusWord": "актер",
    "transcription": "[ˈæktə]"
  },
  {
    "engWord": "fries",
    "rusWord": "картофель фри",
    "transcription": "[fraɪz]"
  },
  {
    "engWord": "responsible",
    "rusWord": "ответственный",
    "transcription": "[rɪsˈpɒnsəbl]"
  },
  {
    "engWord": "clown",
    "rusWord": "клоун",
    "transcription": "[klaʊn]"
  },
  {
    "engWord": "formally",
    "rusWord": "формально",
    "transcription": "[ˈfɔːməlɪ]"
  },
  {
    "engWord": "previously",
    "rusWord": "ранее",
    "transcription": "[ˈpriːvɪəslɪ]"
  },
  {
    "engWord": "carrying",
    "rusWord": "перевозка",
    "transcription": "[ˈkærɪɪŋ]"
  },
  {
    "engWord": "restaurant",
    "rusWord": "ресторан",
    "transcription": "[]"
  },
  {
    "engWord": "university",
    "rusWord": "университет",
    "transcription": "[juːnɪˈvɜːsɪtɪ]"
  },
  {
    "engWord": "pleasure",
    "rusWord": "удовольствие",
    "transcription": "[ˈpleʒə]"
  },
  {
    "engWord": "these",
    "rusWord": "эти",
    "transcription": "[ðiːz]"
  },
  {
    "engWord": "enter",
    "rusWord": "входить",
    "transcription": "[ˈentə]"
  },
  {
    "engWord": "solution",
    "rusWord": "решение",
    "transcription": "[səˈluːʃn]"
  },
  {
    "engWord": "lady",
    "rusWord": "леди",
    "transcription": "[ˈleɪdɪ]"
  },
  {
    "engWord": "manufacturer",
    "rusWord": "производитель",
    "transcription": "[mænjʊˈfækʧərə]"
  },
  {
    "engWord": "pail",
    "rusWord": "ведро",
    "transcription": "[peɪl]"
  },
  {
    "engWord": "screech",
    "rusWord": "визг",
    "transcription": "[skriːʧ]"
  },
  {
    "engWord": "oblige",
    "rusWord": "обязывать",
    "transcription": "[əˈblaɪʤ]"
  },
  {
    "engWord": "eyeshadow",
    "rusWord": "тени для век",
    "transcription": "[ˈaɪʃædəʊ]"
  },
  {
    "engWord": "peel",
    "rusWord": "кожура",
    "transcription": "[piːl]"
  },
  {
    "engWord": "activity",
    "rusWord": "деятельность",
    "transcription": "[ækˈtɪvɪtɪ]"
  },
  {
    "engWord": "education",
    "rusWord": "образование",
    "transcription": "[edjʊˈkeɪʃn]"
  },
  {
    "engWord": "cure",
    "rusWord": "лечение",
    "transcription": "[kjʊə]"
  },
  {
    "engWord": "absorb",
    "rusWord": "поглощать",
    "transcription": "[əbˈzɔːb]"
  },
  {
    "engWord": "slope",
    "rusWord": "склон",
    "transcription": "[sləʊp]"
  },
  {
    "engWord": "hell",
    "rusWord": "ад",
    "transcription": "[hel]"
  },
  {
    "engWord": "math",
    "rusWord": "математика",
    "transcription": "[mæθ]"
  },
  {
    "engWord": "head",
    "rusWord": "голова",
    "transcription": "[hed]"
  },
  {
    "engWord": "apple",
    "rusWord": "яблоко",
    "transcription": "[æpl]"
  },
  {
    "engWord": "radiation",
    "rusWord": "излучение",
    "transcription": "[reɪdɪˈeɪʃn]"
  },
  {
    "engWord": "kingdom",
    "rusWord": "царство",
    "transcription": "[ˈkɪŋdəm]"
  },
  {
    "engWord": "horseback",
    "rusWord": "верхом",
    "transcription": "[ˈhɔːsbæk]"
  },
  {
    "engWord": "definitely",
    "rusWord": "определенно",
    "transcription": "[ˈdefɪnɪtlɪ]"
  },
  {
    "engWord": "magnitude",
    "rusWord": "значимость",
    "transcription": "[ˈmægnɪtjuːd]"
  },
  {
    "engWord": "potion",
    "rusWord": "зелье",
    "transcription": "[pəʊʃn]"
  },
  {
    "engWord": "sunset",
    "rusWord": "закат",
    "transcription": "[ˈsʌnset]"
  },
  {
    "engWord": "rape",
    "rusWord": "надругаться",
    "transcription": "[reɪp]"
  },
  {
    "engWord": "strictly",
    "rusWord": "строго",
    "transcription": "[ˈstrɪktlɪ]"
  },
  {
    "engWord": "patrol",
    "rusWord": "патруль",
    "transcription": "[pəˈtrəʊl]"
  },
  {
    "engWord": "failure",
    "rusWord": "неудача",
    "transcription": "[ˈfeɪljə]"
  },
  {
    "engWord": "shrink",
    "rusWord": "сокращаться",
    "transcription": "[ʃrɪŋk]"
  },
  {
    "engWord": "sometime",
    "rusWord": "иногда",
    "transcription": "[ˈsʌmtaɪm]"
  },
  {
    "engWord": "bail",
    "rusWord": "залог",
    "transcription": "[beɪl]"
  },
  {
    "engWord": "library",
    "rusWord": "библиотека",
    "transcription": "[ˈlaɪbrərɪ]"
  },
  {
    "engWord": "sing",
    "rusWord": "петь",
    "transcription": "[sɪŋ]"
  },
  {
    "engWord": "swim",
    "rusWord": "плавать",
    "transcription": "[swɪm]"
  },
  {
    "engWord": "indicate",
    "rusWord": "указывать",
    "transcription": "[ˈɪndɪkeɪt]"
  },
  {
    "engWord": "above",
    "rusWord": "выше",
    "transcription": "[əˈbʌv]"
  },
  {
    "engWord": "start",
    "rusWord": "начать",
    "transcription": "[stɑːt]"
  },
  {
    "engWord": "saw",
    "rusWord": "пилить",
    "transcription": "[sɔː]"
  },
  {
    "engWord": "publisher",
    "rusWord": "издательство",
    "transcription": "[ˈpʌblɪʃə]"
  },
  {
    "engWord": "Spanish",
    "rusWord": "испанский",
    "transcription": "[ˈspænɪʃ]"
  },
  {
    "engWord": "internet",
    "rusWord": "интернет",
    "transcription": "[ˈɪntənɛt]"
  },
  {
    "engWord": "spicy",
    "rusWord": "пряный",
    "transcription": "[ˈspaɪsɪ]"
  },
  {
    "engWord": "facet",
    "rusWord": "грань",
    "transcription": "[ˈfæsɪt]"
  },
  {
    "engWord": "throughout",
    "rusWord": "через",
    "transcription": "[θruːˈaʊt]"
  },
  {
    "engWord": "daily",
    "rusWord": "ежедневный",
    "transcription": "[ˈdeɪlɪ]"
  },
  {
    "engWord": "officially",
    "rusWord": "официально",
    "transcription": "[əˈfɪʃəlɪ]"
  },
  {
    "engWord": "agreement",
    "rusWord": "соглашение",
    "transcription": "[əˈgriːmənt]"
  },
  {
    "engWord": "somehow",
    "rusWord": "как-то",
    "transcription": "[ˈsʌmhaʊ]"
  },
  {
    "engWord": "awesome",
    "rusWord": "потрясающий",
    "transcription": "[ˈɔːsəm]"
  },
  {
    "engWord": "lord",
    "rusWord": "Господь",
    "transcription": "[lɔːd]"
  },
  {
    "engWord": "sensible",
    "rusWord": "здравомыслящий",
    "transcription": "[ˈsensəbl]"
  },
  {
    "engWord": "grade",
    "rusWord": "класс",
    "transcription": "[greɪd]"
  },
  {
    "engWord": "arrest",
    "rusWord": "арест",
    "transcription": "[əˈrest]"
  },
  {
    "engWord": "funeral",
    "rusWord": "похороны",
    "transcription": "[ˈfjuːnərəl]"
  },
  {
    "engWord": "concern",
    "rusWord": "беспокойство",
    "transcription": "[kənˈsɜːn]"
  },
  {
    "engWord": "behind",
    "rusWord": "позади",
    "transcription": "[bɪˈhaɪnd]"
  },
  {
    "engWord": "direct",
    "rusWord": "прямой",
    "transcription": "[dɪˈrekt]"
  },
  {
    "engWord": "right",
    "rusWord": "право",
    "transcription": "[raɪt]"
  },
  {
    "engWord": "myth",
    "rusWord": "миф",
    "transcription": "[mɪθ]"
  },
  {
    "engWord": "centigrade",
    "rusWord": "стоградусный",
    "transcription": "[ˈsentɪgreɪd]"
  },
  {
    "engWord": "gossip",
    "rusWord": "сплетня",
    "transcription": "[ˈgɒsɪp]"
  },
  {
    "engWord": "active",
    "rusWord": "деятельный",
    "transcription": "[ˈæktɪv]"
  },
  {
    "engWord": "disturbing",
    "rusWord": "беспокоящий",
    "transcription": "[dɪˈstɜːbɪŋ]"
  },
  {
    "engWord": "comprehensive",
    "rusWord": "всесторонний",
    "transcription": "[kɒmprɪˈhensɪv]"
  },
  {
    "engWord": "harassment",
    "rusWord": "беспокойство",
    "transcription": "[ˈhærəsmənt]"
  },
  {
    "engWord": "garrison",
    "rusWord": "гарнизон",
    "transcription": "[ˈgærɪsn]"
  },
  {
    "engWord": "consciousness",
    "rusWord": "сознание",
    "transcription": "[ˈkɒnʃəsnɪs]"
  },
  {
    "engWord": "determination",
    "rusWord": "определение",
    "transcription": "[dɪtɜːmɪˈneɪʃn]"
  },
  {
    "engWord": "frame",
    "rusWord": "рамка",
    "transcription": "[freɪm]"
  },
  {
    "engWord": "discovery",
    "rusWord": "открытие",
    "transcription": "[dɪsˈkʌvərɪ]"
  },
  {
    "engWord": "uncomfortable",
    "rusWord": "неудобный",
    "transcription": "[ʌnˈkʌmf(ə)təb(ə)l]"
  },
  {
    "engWord": "compere",
    "rusWord": "конферансье",
    "transcription": "[ˈkɒmpeə]"
  },
  {
    "engWord": "engagement",
    "rusWord": "помолвка",
    "transcription": "[ɪnˈgeɪʤmənt]"
  },
  {
    "engWord": "absence",
    "rusWord": "отсутствие",
    "transcription": "[ˈæbsəns]"
  },
  {
    "engWord": "careful",
    "rusWord": "осторожный",
    "transcription": "[ˈkeəf(ə)l]"
  },
  {
    "engWord": "hat",
    "rusWord": "шляпа",
    "transcription": "[hæt]"
  },
  {
    "engWord": "life",
    "rusWord": "жизнь",
    "transcription": "[laɪf]"
  },
  {
    "engWord": "auto",
    "rusWord": "авто",
    "transcription": "[ˈɔːtəʊ]"
  },
  {
    "engWord": "campus",
    "rusWord": "студгородок",
    "transcription": "[ˈkæmpəs]"
  },
  {
    "engWord": "bent",
    "rusWord": "изогнутый",
    "transcription": "[bent]"
  },
  {
    "engWord": "trim",
    "rusWord": "отделка",
    "transcription": "[trɪm]"
  },
  {
    "engWord": "casino",
    "rusWord": "казино",
    "transcription": "[kəˈsiːnəʊ]"
  },
  {
    "engWord": "lace",
    "rusWord": "кружево",
    "transcription": "[leɪs]"
  },
  {
    "engWord": "stern",
    "rusWord": "суровый",
    "transcription": "[stɜːn]"
  },
  {
    "engWord": "sworn",
    "rusWord": "названный",
    "transcription": "[swɔːn]"
  },
  {
    "engWord": "grape",
    "rusWord": "виноград",
    "transcription": "[greɪp]"
  },
  {
    "engWord": "navy",
    "rusWord": "морское ведомство",
    "transcription": "[ˈneɪvɪ]"
  },
  {
    "engWord": "tragedy",
    "rusWord": "трагедия",
    "transcription": "[ˈtræʤɪdɪ]"
  },
  {
    "engWord": "mom",
    "rusWord": "мама",
    "transcription": "[mɒm]"
  },
  {
    "engWord": "building",
    "rusWord": "здание",
    "transcription": "[ˈbɪldɪŋ]"
  },
  {
    "engWord": "gray",
    "rusWord": "серый",
    "transcription": "[greɪ]"
  },
  {
    "engWord": "gather",
    "rusWord": "собирать",
    "transcription": "[ˈgæðə]"
  },
  {
    "engWord": "happy",
    "rusWord": "счастливый",
    "transcription": "[ˈhæpɪ]"
  },
  {
    "engWord": "reward",
    "rusWord": "вознаграждение",
    "transcription": "[rɪˈwɔːd]"
  },
  {
    "engWord": "goddess",
    "rusWord": "богиня",
    "transcription": "[ˈgɒdɪs]"
  },
  {
    "engWord": "greater",
    "rusWord": "более значительный",
    "transcription": "[ˈgreɪtə]"
  },
  {
    "engWord": "overdo",
    "rusWord": "переусердствовать",
    "transcription": "[əʊvəˈduː]"
  },
  {
    "engWord": "critical",
    "rusWord": "критический",
    "transcription": "[ˈkrɪtɪkəl]"
  },
  {
    "engWord": "separation",
    "rusWord": "разделение",
    "transcription": "[sepəˈreɪʃn]"
  },
  {
    "engWord": "summit",
    "rusWord": "саммит",
    "transcription": "[ˈsʌmɪt]"
  },
  {
    "engWord": "dressing",
    "rusWord": "перевязочный",
    "transcription": "[ˈdresɪŋ]"
  },
  {
    "engWord": "is",
    "rusWord": "является",
    "transcription": "[]"
  },
  {
    "engWord": "globe",
    "rusWord": "глобус",
    "transcription": "[gləʊb]"
  },
  {
    "engWord": "loyal",
    "rusWord": "лояльный",
    "transcription": "[ˈlɔɪəl]"
  },
  {
    "engWord": "shrug",
    "rusWord": "пожимать",
    "transcription": "[ʃrʌg]"
  },
  {
    "engWord": "matt",
    "rusWord": "Мэтт",
    "transcription": "[mæt]"
  },
  {
    "engWord": "steady",
    "rusWord": "устойчивый",
    "transcription": "[ˈstedɪ]"
  },
  {
    "engWord": "plastic",
    "rusWord": "пластик",
    "transcription": "[ˈplæstɪk]"
  },
  {
    "engWord": "anti",
    "rusWord": "против",
    "transcription": "[ˈæntɪ]"
  },
  {
    "engWord": "whereas",
    "rusWord": "поскольку",
    "transcription": "[we(ə)ˈræz]"
  },
  {
    "engWord": "kid",
    "rusWord": "ребенок",
    "transcription": "[kɪd]"
  },
  {
    "engWord": "bike",
    "rusWord": "велосипед",
    "transcription": "[baɪk]"
  },
  {
    "engWord": "easy",
    "rusWord": "простой",
    "transcription": "[ˈiːzɪ]"
  },
  {
    "engWord": "pause",
    "rusWord": "пауза",
    "transcription": "[pɔːz]"
  },
  {
    "engWord": "predict",
    "rusWord": "прогнозировать",
    "transcription": "[prɪˈdɪkt]"
  },
  {
    "engWord": "toward",
    "rusWord": "в направлении",
    "transcription": "[təˈwɔːd]"
  },
  {
    "engWord": "polite",
    "rusWord": "вежливый",
    "transcription": "[pəˈlaɪt]"
  },
  {
    "engWord": "daylight",
    "rusWord": "дневной свет",
    "transcription": "[ˈdeɪlaɪt]"
  },
  {
    "engWord": "bloom",
    "rusWord": "цветение",
    "transcription": "[bluːm]"
  },
  {
    "engWord": "amusing",
    "rusWord": "забавный",
    "transcription": "[əˈmjuːzɪŋ]"
  },
  {
    "engWord": "an",
    "rusWord": "Неопределенный артикль",
    "transcription": "[]"
  },
  {
    "engWord": "cocoa",
    "rusWord": "какао",
    "transcription": "[ˈkəʊkəʊ]"
  },
  {
    "engWord": "announce",
    "rusWord": "объявлять",
    "transcription": "[əˈnaʊns]"
  },
  {
    "engWord": "display",
    "rusWord": "дисплей",
    "transcription": "[dɪsˈpleɪ]"
  },
  {
    "engWord": "resource",
    "rusWord": "ресурс",
    "transcription": "[]"
  },
  {
    "engWord": "prefer",
    "rusWord": "предпочитать",
    "transcription": "[prɪˈfɜː]"
  },
  {
    "engWord": "crazy",
    "rusWord": "сумасшедший",
    "transcription": "[ˈkreɪzɪ]"
  },
  {
    "engWord": "ownership",
    "rusWord": "собственность",
    "transcription": "[ˈəʊnəʃɪp]"
  },
  {
    "engWord": "indeed",
    "rusWord": "действительно",
    "transcription": "[ɪnˈdiːd]"
  },
  {
    "engWord": "task",
    "rusWord": "задача",
    "transcription": "[tɑːsk]"
  },
  {
    "engWord": "knife",
    "rusWord": "нож",
    "transcription": "[naɪf]"
  },
  {
    "engWord": "kitchen",
    "rusWord": "кухня",
    "transcription": "[ˈkɪʧɪn]"
  },
  {
    "engWord": "disappear",
    "rusWord": "исчезнуть",
    "transcription": "[dɪsəˈpɪə]"
  },
  {
    "engWord": "collect",
    "rusWord": "собирать",
    "transcription": "[kəˈlekt]"
  },
  {
    "engWord": "hill",
    "rusWord": "холм",
    "transcription": "[hɪl]"
  },
  {
    "engWord": "flower",
    "rusWord": "цветок",
    "transcription": "[ˈflaʊə]"
  },
  {
    "engWord": "windy",
    "rusWord": "ветреный",
    "transcription": "[ˈwɪndɪ]"
  },
  {
    "engWord": "incomplete",
    "rusWord": "незавершенный",
    "transcription": "[ɪnkəmˈpliːt]"
  },
  {
    "engWord": "reunion",
    "rusWord": "воссоединение",
    "transcription": "[riːˈjuːnɪən]"
  },
  {
    "engWord": "flush",
    "rusWord": "промывать",
    "transcription": "[flʌʃ]"
  },
  {
    "engWord": "bribe",
    "rusWord": "взятка",
    "transcription": "[braɪb]"
  },
  {
    "engWord": "banana",
    "rusWord": "банан",
    "transcription": "[bəˈnɑːnə]"
  },
  {
    "engWord": "liberal",
    "rusWord": "либерал",
    "transcription": "[ˈlɪbərəl]"
  },
  {
    "engWord": "dessert",
    "rusWord": "десерт",
    "transcription": "[dɪˈzɜːt]"
  },
  {
    "engWord": "outstanding",
    "rusWord": "выдающийся",
    "transcription": "[aʊtˈstændɪŋ]"
  },
  {
    "engWord": "deny",
    "rusWord": "отрицать",
    "transcription": "[dɪˈnaɪ]"
  },
  {
    "engWord": "flying",
    "rusWord": "летающий",
    "transcription": "[ˈflaɪɪŋ]"
  },
  {
    "engWord": "flight",
    "rusWord": "полет",
    "transcription": "[flaɪt]"
  },
  {
    "engWord": "hook",
    "rusWord": "крюк",
    "transcription": "[hʊk]"
  },
  {
    "engWord": "claim",
    "rusWord": "претензия",
    "transcription": "[kleɪm]"
  },
  {
    "engWord": "slave",
    "rusWord": "рабыня",
    "transcription": "[sleɪv]"
  },
  {
    "engWord": "best",
    "rusWord": "лучший",
    "transcription": "[best]"
  },
  {
    "engWord": "clock",
    "rusWord": "часы",
    "transcription": "[klɒk]"
  },
  {
    "engWord": "pitch",
    "rusWord": "подача",
    "transcription": "[pɪʧ]"
  },
  {
    "engWord": "hundred",
    "rusWord": "сто",
    "transcription": "[ˈhʌndrəd]"
  },
  {
    "engWord": "comedy",
    "rusWord": "комедия",
    "transcription": "[ˈkɒmədɪ]"
  },
  {
    "engWord": "kin",
    "rusWord": "род",
    "transcription": "[kɪn]"
  },
  {
    "engWord": "resolution",
    "rusWord": "разрешение",
    "transcription": "[rezəˈluːʃn]"
  },
  {
    "engWord": "solitary",
    "rusWord": "одиночный",
    "transcription": "[ˈsɒlɪtərɪ]"
  },
  {
    "engWord": "examine",
    "rusWord": "исследовать",
    "transcription": "[ɪgˈzæmɪn]"
  },
  {
    "engWord": "assure",
    "rusWord": "уверять",
    "transcription": "[əˈʃʊə]"
  },
  {
    "engWord": "basket",
    "rusWord": "корзина",
    "transcription": "[ˈbɑːskɪt]"
  },
  {
    "engWord": "shake",
    "rusWord": "трясти",
    "transcription": "[ʃeɪk]"
  },
  {
    "engWord": "brother",
    "rusWord": "брат",
    "transcription": "[ˈbrʌðə]"
  },
  {
    "engWord": "determine",
    "rusWord": "определять",
    "transcription": "[dɪˈtɜːmɪn]"
  },
  {
    "engWord": "courageous",
    "rusWord": "смелый",
    "transcription": "[kəˈreɪʤəs]"
  },
  {
    "engWord": "construction",
    "rusWord": "строительство",
    "transcription": "[kənˈstrʌkʃn]"
  },
  {
    "engWord": "locker",
    "rusWord": "шкаф",
    "transcription": "[ˈlɒkə]"
  },
  {
    "engWord": "paid",
    "rusWord": "оплачиваемый",
    "transcription": "[peɪd]"
  },
  {
    "engWord": "sting",
    "rusWord": "ужалить",
    "transcription": "[stɪŋ]"
  },
  {
    "engWord": "reference",
    "rusWord": "ссылка",
    "transcription": "[ˈrefrəns]"
  },
  {
    "engWord": "refuse",
    "rusWord": "отказываться",
    "transcription": "[ˈrefjuːs]"
  },
  {
    "engWord": "clerk",
    "rusWord": "служащий",
    "transcription": "[klɑːk]"
  },
  {
    "engWord": "restore",
    "rusWord": "восстановить",
    "transcription": "[rɪsˈtɔː]"
  },
  {
    "engWord": "incident",
    "rusWord": "инцидент",
    "transcription": "[ˈɪnsɪdənt]"
  },
  {
    "engWord": "adapt",
    "rusWord": "приспособиться",
    "transcription": "[əˈdæpt]"
  },
  {
    "engWord": "comfort",
    "rusWord": "комфорт",
    "transcription": "[ˈkʌmfət]"
  },
  {
    "engWord": "talent",
    "rusWord": "талант",
    "transcription": "[ˈtælənt]"
  },
  {
    "engWord": "goodness",
    "rusWord": "доброта",
    "transcription": "[ˈgʊdnɪs]"
  },
  {
    "engWord": "pocket",
    "rusWord": "карман",
    "transcription": "[ˈpɒkɪt]"
  },
  {
    "engWord": "seriously",
    "rusWord": "серьезно",
    "transcription": "[ˈsɪərɪəslɪ]"
  },
  {
    "engWord": "important",
    "rusWord": "важный",
    "transcription": "[ɪmˈpɔːtənt]"
  },
  {
    "engWord": "simple",
    "rusWord": "простой",
    "transcription": "[sɪmpl]"
  },
  {
    "engWord": "mouth",
    "rusWord": "рот",
    "transcription": "[maʊθ]"
  },
  {
    "engWord": "find",
    "rusWord": "находить",
    "transcription": "[faɪnd]"
  },
  {
    "engWord": "single",
    "rusWord": "одиночный",
    "transcription": "[sɪŋgl]"
  },
  {
    "engWord": "office",
    "rusWord": "офис",
    "transcription": "[ˈɒfɪs]"
  },
  {
    "engWord": "division",
    "rusWord": "деление",
    "transcription": "[dɪˈvɪʒən]"
  },
  {
    "engWord": "hard",
    "rusWord": "трудный",
    "transcription": "[hɑːd]"
  },
  {
    "engWord": "outline",
    "rusWord": "контур",
    "transcription": "[ˈaʊtlaɪn]"
  },
  {
    "engWord": "anyhow",
    "rusWord": "как угодно",
    "transcription": "[ˈenɪhaʊ]"
  },
  {
    "engWord": "password",
    "rusWord": "пароль",
    "transcription": "[ˈpɑːswɜːd]"
  },
  {
    "engWord": "moron",
    "rusWord": "идиот",
    "transcription": "[ˈmɔːrɒn]"
  },
  {
    "engWord": "confusion",
    "rusWord": "путаница",
    "transcription": "[kənˈfjuːʒən]"
  },
  {
    "engWord": "width",
    "rusWord": "ширина",
    "transcription": "[wɪdθ]"
  },
  {
    "engWord": "affect",
    "rusWord": "влиять",
    "transcription": "[ˈæfekt]"
  },
  {
    "engWord": "chain",
    "rusWord": "цепь",
    "transcription": "[ʧeɪn]"
  },
  {
    "engWord": "junk",
    "rusWord": "хлам",
    "transcription": "[ʤʌŋk]"
  },
  {
    "engWord": "weak",
    "rusWord": "слабый",
    "transcription": "[wiːk]"
  },
  {
    "engWord": "govern",
    "rusWord": "править",
    "transcription": "[ˈgʌvən]"
  },
  {
    "engWord": "their",
    "rusWord": "их",
    "transcription": "[ðeə]"
  },
  {
    "engWord": "song",
    "rusWord": "песня",
    "transcription": "[sɒŋ]"
  },
  {
    "engWord": "any",
    "rusWord": "любой",
    "transcription": "[ˈenɪ]"
  },
  {
    "engWord": "deal",
    "rusWord": "сделка",
    "transcription": "[diːl]"
  },
  {
    "engWord": "loaf",
    "rusWord": "буханка",
    "transcription": "[ləʊf]"
  },
  {
    "engWord": "highness",
    "rusWord": "Высочество",
    "transcription": "[ˈhaɪnɪs]"
  },
  {
    "engWord": "rug",
    "rusWord": "коврик",
    "transcription": "[rʌg]"
  },
  {
    "engWord": "yacht",
    "rusWord": "яхта",
    "transcription": "[jɒt]"
  },
  {
    "engWord": "lion",
    "rusWord": "лев",
    "transcription": "[ˈlaɪən]"
  },
  {
    "engWord": "mute",
    "rusWord": "беззвучный",
    "transcription": "[mjuːt]"
  },
  {
    "engWord": "variable",
    "rusWord": "переменная",
    "transcription": "[ˈve(ə)rɪəb(ə)l]"
  },
  {
    "engWord": "analysis",
    "rusWord": "анализ",
    "transcription": "[əˈnælɪsɪs]"
  },
  {
    "engWord": "bend",
    "rusWord": "изгиб",
    "transcription": "[bend]"
  },
  {
    "engWord": "hairy",
    "rusWord": "ворсистый",
    "transcription": "[ˈhe(ə)rɪ]"
  },
  {
    "engWord": "pillow",
    "rusWord": "подушка",
    "transcription": "[ˈpɪləʊ]"
  },
  {
    "engWord": "warehouse",
    "rusWord": "склад",
    "transcription": "[ˈweəhaʊs]"
  },
  {
    "engWord": "agriculture",
    "rusWord": "сельское хозяйство",
    "transcription": "[ˈægrɪkʌlʧə]"
  },
  {
    "engWord": "fascinating",
    "rusWord": "очаровательный",
    "transcription": "[ˈfæsɪneɪtɪŋ]"
  },
  {
    "engWord": "tag",
    "rusWord": "метка",
    "transcription": "[tæg]"
  },
  {
    "engWord": "somebody",
    "rusWord": "кто-то",
    "transcription": "[ˈsʌmbədɪ]"
  },
  {
    "engWord": "reduce",
    "rusWord": "уменьшить",
    "transcription": "[rɪˈdjuːs]"
  },
  {
    "engWord": "muscle",
    "rusWord": "мускул",
    "transcription": "[mʌsl]"
  },
  {
    "engWord": "unlikely",
    "rusWord": "вряд ли",
    "transcription": "[ʌnˈlaɪklɪ]"
  },
  {
    "engWord": "sad",
    "rusWord": "грустный",
    "transcription": "[sæd]"
  },
  {
    "engWord": "present",
    "rusWord": "подарок",
    "transcription": "[preznt]"
  },
  {
    "engWord": "exercise",
    "rusWord": "упражнение",
    "transcription": "[ˈeksəsaɪz]"
  },
  {
    "engWord": "cow",
    "rusWord": "корова",
    "transcription": "[kaʊ]"
  },
  {
    "engWord": "defeat",
    "rusWord": "поражение",
    "transcription": "[dɪˈfiːt]"
  },
  {
    "engWord": "keeper",
    "rusWord": "держатель",
    "transcription": "[ˈkiːpə]"
  },
  {
    "engWord": "heir",
    "rusWord": "наследник",
    "transcription": "[eə]"
  },
  {
    "engWord": "regional",
    "rusWord": "региональный",
    "transcription": "[ˈriːʤ(ə)nəl]"
  },
  {
    "engWord": "cafeteria",
    "rusWord": "кафетерий",
    "transcription": "[kæflˈtɪ(ə)rɪə]"
  },
  {
    "engWord": "handbag",
    "rusWord": "дамская сумочка",
    "transcription": "[ˈhændbæg]"
  },
  {
    "engWord": "storage",
    "rusWord": "хранение",
    "transcription": "[ˈstɔːrɪʤ]"
  },
  {
    "engWord": "emphasis",
    "rusWord": "акцент",
    "transcription": "[ˈemfəsɪs]"
  },
  {
    "engWord": "cheating",
    "rusWord": "мошенничество",
    "transcription": "[ˈʧiːtɪŋ]"
  },
  {
    "engWord": "historian",
    "rusWord": "историк",
    "transcription": "[hɪsˈtɔːrɪən]"
  },
  {
    "engWord": "union",
    "rusWord": "объединение",
    "transcription": "[ˈjuːnɪən]"
  },
  {
    "engWord": "reform",
    "rusWord": "реформа",
    "transcription": "[rɪˈfɔːm]"
  },
  {
    "engWord": "highest",
    "rusWord": "самый высокий",
    "transcription": "[ˈhaɪɪst]"
  },
  {
    "engWord": "credit",
    "rusWord": "кредит",
    "transcription": "[ˈkredɪt]"
  },
  {
    "engWord": "bomb",
    "rusWord": "бомба",
    "transcription": "[bɒm]"
  },
  {
    "engWord": "direction",
    "rusWord": "направление",
    "transcription": "[dɪˈrekʃn]"
  },
  {
    "engWord": "serious",
    "rusWord": "серьезный",
    "transcription": "[ˈsɪərɪəs]"
  },
  {
    "engWord": "crane",
    "rusWord": "кран",
    "transcription": "[kreɪn]"
  },
  {
    "engWord": "bath",
    "rusWord": "ванна",
    "transcription": "[bɑːθ]"
  },
  {
    "engWord": "might",
    "rusWord": "вероятно",
    "transcription": "[maɪt]"
  },
  {
    "engWord": "of",
    "rusWord": "от",
    "transcription": "[ɒv]"
  },
  {
    "engWord": "add",
    "rusWord": "добавлять",
    "transcription": "[æd]"
  },
  {
    "engWord": "build",
    "rusWord": "строить",
    "transcription": "[bɪld]"
  },
  {
    "engWord": "plane",
    "rusWord": "самолет",
    "transcription": "[pleɪn]"
  },
  {
    "engWord": "record",
    "rusWord": "запись",
    "transcription": "[ˈrekɔːd]"
  },
  {
    "engWord": "twenty",
    "rusWord": "двадцать",
    "transcription": "[ˈtwentɪ]"
  },
  {
    "engWord": "ox",
    "rusWord": "бык",
    "transcription": "[ɒks]"
  },
  {
    "engWord": "invoice",
    "rusWord": "фактура",
    "transcription": "[ˈɪnvɔɪs]"
  },
  {
    "engWord": "transportation",
    "rusWord": "перевозка",
    "transcription": "[trænspɔːˈteɪʃn]"
  },
  {
    "engWord": "auction",
    "rusWord": "аукцион",
    "transcription": "[ɔːkʃn]"
  },
  {
    "engWord": "permanently",
    "rusWord": "постоянно",
    "transcription": "[ˈpɜːmənəntlɪ]"
  },
  {
    "engWord": "journal",
    "rusWord": "журнал",
    "transcription": "[ʤɜːnl]"
  },
  {
    "engWord": "pancake",
    "rusWord": "блин",
    "transcription": "[ˈpæŋkeɪk]"
  },
  {
    "engWord": "resent",
    "rusWord": "возмущаться",
    "transcription": "[rɪˈzent]"
  },
  {
    "engWord": "jersey",
    "rusWord": "Джерси",
    "transcription": "[ˈʤɜːzɪ]"
  },
  {
    "engWord": "infant",
    "rusWord": "младенец",
    "transcription": "[ˈɪnfənt]"
  },
  {
    "engWord": "remark",
    "rusWord": "замечание",
    "transcription": "[rɪˈmɑːk]"
  },
  {
    "engWord": "organization",
    "rusWord": "организация",
    "transcription": "[ɔːgənaɪˈzeɪʃn]"
  },
  {
    "engWord": "statue",
    "rusWord": "статуя",
    "transcription": "[ˈstæʧuː]"
  },
  {
    "engWord": "universe",
    "rusWord": "вселенная",
    "transcription": "[ˈjuːnɪvɜːs]"
  },
  {
    "engWord": "programme",
    "rusWord": "программа",
    "transcription": "[ˈprəʊgræm]"
  },
  {
    "engWord": "maintenance",
    "rusWord": "поддержка",
    "transcription": "[ˈmeɪntənəns]"
  },
  {
    "engWord": "jail",
    "rusWord": "тюрьма",
    "transcription": "[ʤeɪl]"
  },
  {
    "engWord": "case",
    "rusWord": "дело",
    "transcription": "[keɪs]"
  },
  {
    "engWord": "absurd",
    "rusWord": "абсурд",
    "transcription": "[əbˈsɜːd]"
  },
  {
    "engWord": "joining",
    "rusWord": "присоединение",
    "transcription": "[ˈʤɔɪnɪŋ]"
  },
  {
    "engWord": "colourful",
    "rusWord": "красочный",
    "transcription": "[ˈkʌləf(ə)l]"
  },
  {
    "engWord": "cooperation",
    "rusWord": "сотрудничество",
    "transcription": "[kəʊɒpəˈreɪʃn]"
  },
  {
    "engWord": "blouse",
    "rusWord": "блузка",
    "transcription": "[blaʊz]"
  },
  {
    "engWord": "browse",
    "rusWord": "просматривать",
    "transcription": "[braʊz]"
  },
  {
    "engWord": "censor",
    "rusWord": "цензор",
    "transcription": "[ˈsensə]"
  },
  {
    "engWord": "urge",
    "rusWord": "побуждать",
    "transcription": "[ɜːʤ]"
  },
  {
    "engWord": "fabulous",
    "rusWord": "потрясающий",
    "transcription": "[ˈfæbjʊləs]"
  },
  {
    "engWord": "left",
    "rusWord": "левый",
    "transcription": "[left]"
  },
  {
    "engWord": "put",
    "rusWord": "класть",
    "transcription": "[pʊt]"
  },
  {
    "engWord": "city",
    "rusWord": "город",
    "transcription": "[ˈsɪtɪ]"
  },
  {
    "engWord": "verb",
    "rusWord": "глагол",
    "transcription": "[vɜːb]"
  },
  {
    "engWord": "technique",
    "rusWord": "метод",
    "transcription": "[tekˈniːk]"
  },
  {
    "engWord": "known",
    "rusWord": "известный",
    "transcription": "[nəʊn]"
  },
  {
    "engWord": "onion",
    "rusWord": "лук",
    "transcription": "[ˈʌnjən]"
  },
  {
    "engWord": "tan",
    "rusWord": "загар",
    "transcription": "[tæn]"
  },
  {
    "engWord": "paste",
    "rusWord": "вставить",
    "transcription": "[peɪst]"
  },
  {
    "engWord": "crown",
    "rusWord": "корона",
    "transcription": "[kraʊn]"
  },
  {
    "engWord": "delinquent",
    "rusWord": "правонарушитель",
    "transcription": "[dɪˈlɪŋkwənt]"
  },
  {
    "engWord": "mask",
    "rusWord": "маска",
    "transcription": "[mɑːsk]"
  },
  {
    "engWord": "ultimate",
    "rusWord": "конечный",
    "transcription": "[ˈʌltɪmɪt]"
  },
  {
    "engWord": "spin",
    "rusWord": "вращение",
    "transcription": "[spɪn]"
  },
  {
    "engWord": "shown",
    "rusWord": "показан",
    "transcription": "[ʃəʊn]"
  },
  {
    "engWord": "terms",
    "rusWord": "правила",
    "transcription": "[tɜːmz]"
  },
  {
    "engWord": "federal",
    "rusWord": "федеральный",
    "transcription": "[ˈfedərəl]"
  },
  {
    "engWord": "useful",
    "rusWord": "полезный",
    "transcription": "[ˈjuːsf(ə)l]"
  },
  {
    "engWord": "disaster",
    "rusWord": "катастрофа",
    "transcription": "[dɪˈzɑːstə]"
  },
  {
    "engWord": "the",
    "rusWord": "этот",
    "transcription": "[ðiː]"
  },
  {
    "engWord": "especially",
    "rusWord": "особенно",
    "transcription": "[ɪsˈpeʃəlɪ]"
  },
  {
    "engWord": "quick",
    "rusWord": "быстрый",
    "transcription": "[kwɪk]"
  },
  {
    "engWord": "this",
    "rusWord": "этот",
    "transcription": "[ðɪs]"
  },
  {
    "engWord": "children",
    "rusWord": "дети",
    "transcription": "[ˈʧɪldrən]"
  },
  {
    "engWord": "tempt",
    "rusWord": "искушать",
    "transcription": "[tempt]"
  },
  {
    "engWord": "attraction",
    "rusWord": "привлекательность",
    "transcription": "[əˈtrækʃn]"
  },
  {
    "engWord": "videotape",
    "rusWord": "видеокассета",
    "transcription": "[ˈvɪdɪəʊteɪp]"
  },
  {
    "engWord": "refreshment",
    "rusWord": "закуска",
    "transcription": "[rɪˈfreʃmənt]"
  },
  {
    "engWord": "repay",
    "rusWord": "возместить",
    "transcription": "[rɪˈpeɪ]"
  },
  {
    "engWord": "successful",
    "rusWord": "успешный",
    "transcription": "[səkˈsesf(ə)l]"
  },
  {
    "engWord": "overcome",
    "rusWord": "преодолеть",
    "transcription": "[əʊvəˈkʌm]"
  },
  {
    "engWord": "objective",
    "rusWord": "цель",
    "transcription": "[əbˈʤektɪv]"
  },
  {
    "engWord": "dust",
    "rusWord": "пыль",
    "transcription": "[dʌst]"
  },
  {
    "engWord": "nasty",
    "rusWord": "неприятный",
    "transcription": "[ˈnɑːstɪ]"
  },
  {
    "engWord": "porridge",
    "rusWord": "овсяная каша",
    "transcription": "[ˈpɒrɪʤ]"
  },
  {
    "engWord": "connected",
    "rusWord": "связанный",
    "transcription": "[kəˈnektɪd]"
  },
  {
    "engWord": "romantic",
    "rusWord": "романтический",
    "transcription": "[rə(ʊ)ˈmæntɪk]"
  },
  {
    "engWord": "wheel",
    "rusWord": "колесо",
    "transcription": "[wiːl]"
  },
  {
    "engWord": "caught",
    "rusWord": "пойманный",
    "transcription": "[kɔːt]"
  },
  {
    "engWord": "store",
    "rusWord": "магазин",
    "transcription": "[stɔː]"
  },
  {
    "engWord": "zoom",
    "rusWord": "зум",
    "transcription": "[zuːm]"
  },
  {
    "engWord": "flinch",
    "rusWord": "вздрагивать",
    "transcription": "[flɪnʧ]"
  },
  {
    "engWord": "burst",
    "rusWord": "взрыв",
    "transcription": "[bɜːst]"
  },
  {
    "engWord": "adoption",
    "rusWord": "принятие",
    "transcription": "[ədɒpʃn]"
  },
  {
    "engWord": "rocket",
    "rusWord": "ракета",
    "transcription": "[ˈrɒkɪt]"
  },
  {
    "engWord": "orchard",
    "rusWord": "фруктовый сад",
    "transcription": "[ˈɔːʧəd]"
  },
  {
    "engWord": "platform",
    "rusWord": "платформа",
    "transcription": "[ˈplætfɔːm]"
  },
  {
    "engWord": "starve",
    "rusWord": "голодать",
    "transcription": "[stɑːv]"
  },
  {
    "engWord": "various",
    "rusWord": "различный",
    "transcription": "[ˈve(ə)rɪəs]"
  },
  {
    "engWord": "data",
    "rusWord": "данные",
    "transcription": "[ˈdeɪtə]"
  },
  {
    "engWord": "apricot",
    "rusWord": "абрикос",
    "transcription": "[ˈeɪprɪkɒt]"
  },
  {
    "engWord": "pig",
    "rusWord": "свинья",
    "transcription": "[pɪg]"
  },
  {
    "engWord": "drop",
    "rusWord": "капля",
    "transcription": "[drɒp]"
  },
  {
    "engWord": "locate",
    "rusWord": "разместить",
    "transcription": "[ləʊˈkeɪt]"
  },
  {
    "engWord": "eat",
    "rusWord": "есть",
    "transcription": "[iːt]"
  },
  {
    "engWord": "loud",
    "rusWord": "громкий",
    "transcription": "[laʊd]"
  },
  {
    "engWord": "nun",
    "rusWord": "монахиня",
    "transcription": "[nʌn]"
  },
  {
    "engWord": "shed",
    "rusWord": "сарай",
    "transcription": "[ʃed]"
  },
  {
    "engWord": "explode",
    "rusWord": "взрывать",
    "transcription": "[ɪksˈpləʊd]"
  },
  {
    "engWord": "quietly",
    "rusWord": "тихо",
    "transcription": "[ˈkwaɪətlɪ]"
  },
  {
    "engWord": "temple",
    "rusWord": "храм",
    "transcription": "[templ]"
  },
  {
    "engWord": "vein",
    "rusWord": "жилка",
    "transcription": "[veɪn]"
  },
  {
    "engWord": "splash",
    "rusWord": "всплеск",
    "transcription": "[splæʃ]"
  },
  {
    "engWord": "fuss",
    "rusWord": "суета",
    "transcription": "[fʌs]"
  },
  {
    "engWord": "beneath",
    "rusWord": "под",
    "transcription": "[bɪˈniːθ]"
  },
  {
    "engWord": "petrol",
    "rusWord": "бензин",
    "transcription": "[ˈpetrəl]"
  },
  {
    "engWord": "kidnapping",
    "rusWord": "похищение",
    "transcription": "[ˈkɪdnæpɪŋ]"
  },
  {
    "engWord": "suddenly",
    "rusWord": "внезапно",
    "transcription": "[ˈsʌdnlɪ]"
  },
  {
    "engWord": "silence",
    "rusWord": "тишина",
    "transcription": "[ˈsaɪləns]"
  },
  {
    "engWord": "farther",
    "rusWord": "дальше",
    "transcription": "[ˈfɑːðə]"
  },
  {
    "engWord": "closet",
    "rusWord": "шкаф",
    "transcription": "[ˈklɒzɪt]"
  },
  {
    "engWord": "lucky",
    "rusWord": "удачливый",
    "transcription": "[ˈlʌkɪ]"
  },
  {
    "engWord": "interesting",
    "rusWord": "интересный",
    "transcription": "[ˈɪntrɪstɪŋ]"
  },
  {
    "engWord": "leader",
    "rusWord": "лидер",
    "transcription": "[ˈliːdə]"
  },
  {
    "engWord": "fit",
    "rusWord": "подходить",
    "transcription": "[fɪt]"
  },
  {
    "engWord": "success",
    "rusWord": "успех",
    "transcription": "[səkˈses]"
  },
  {
    "engWord": "lotion",
    "rusWord": "лосьон",
    "transcription": "[ləʊʃn]"
  },
  {
    "engWord": "fragile",
    "rusWord": "хрупкий",
    "transcription": "[ˈfræʤaɪl]"
  },
  {
    "engWord": "slam",
    "rusWord": "захлопывать",
    "transcription": "[slæm]"
  },
  {
    "engWord": "ruby",
    "rusWord": "рубин",
    "transcription": "[ˈruːbɪ]"
  },
  {
    "engWord": "scatter",
    "rusWord": "рассыпать",
    "transcription": "[ˈskætə]"
  },
  {
    "engWord": "brandy",
    "rusWord": "бренди",
    "transcription": "[ˈbrændɪ]"
  },
  {
    "engWord": "button",
    "rusWord": "кнопка",
    "transcription": "[bʌtn]"
  },
  {
    "engWord": "counsellor",
    "rusWord": "советник",
    "transcription": "[ˈkaʊnslə]"
  },
  {
    "engWord": "mayor",
    "rusWord": "мэр",
    "transcription": "[meə]"
  },
  {
    "engWord": "wherever",
    "rusWord": "где бы",
    "transcription": "[we(ə)ˈrevə]"
  },
  {
    "engWord": "throat",
    "rusWord": "горло",
    "transcription": "[θrəʊt]"
  },
  {
    "engWord": "confused",
    "rusWord": "смущенный",
    "transcription": "[kənˈfjuːzd]"
  },
  {
    "engWord": "bright",
    "rusWord": "яркий",
    "transcription": "[braɪt]"
  },
  {
    "engWord": "enough",
    "rusWord": "достаточно",
    "transcription": "[ɪˈnʌf]"
  },
  {
    "engWord": "nude",
    "rusWord": "обнаженная",
    "transcription": "[njuːd]"
  },
  {
    "engWord": "unite",
    "rusWord": "сплочение",
    "transcription": "[juːˈnaɪt]"
  },
  {
    "engWord": "physical",
    "rusWord": "физический",
    "transcription": "[ˈfɪzɪkəl]"
  },
  {
    "engWord": "font",
    "rusWord": "шрифт",
    "transcription": "[fɒnt]"
  },
  {
    "engWord": "ounce",
    "rusWord": "унция",
    "transcription": "[aʊns]"
  },
  {
    "engWord": "application",
    "rusWord": "приложение",
    "transcription": "[æplɪˈkeɪʃn]"
  },
  {
    "engWord": "election",
    "rusWord": "выборы",
    "transcription": "[ɪˈlekʃn]"
  },
  {
    "engWord": "digging",
    "rusWord": "копание",
    "transcription": "[ˈdɪgɪŋ]"
  },
  {
    "engWord": "remove",
    "rusWord": "удалить",
    "transcription": "[rɪˈmuːv]"
  },
  {
    "engWord": "strike",
    "rusWord": "удар",
    "transcription": "[straɪk]"
  },
  {
    "engWord": "recognize",
    "rusWord": "распознавать",
    "transcription": "[ˈrekəgnaɪz]"
  },
  {
    "engWord": "Friday",
    "rusWord": "пятница",
    "transcription": "[ˈfraɪdɪ]"
  },
  {
    "engWord": "dish",
    "rusWord": "блюдо",
    "transcription": "[dɪʃ]"
  },
  {
    "engWord": "video",
    "rusWord": "видео",
    "transcription": "[ˈvɪdɪəʊ]"
  },
  {
    "engWord": "horse",
    "rusWord": "лошадь",
    "transcription": "[hɔːs]"
  },
  {
    "engWord": "fair",
    "rusWord": "справедливый",
    "transcription": "[feə]"
  },
  {
    "engWord": "market",
    "rusWord": "рынок",
    "transcription": "[ˈmɑːkɪt]"
  },
  {
    "engWord": "new",
    "rusWord": "новый",
    "transcription": "[njuː]"
  },
  {
    "engWord": "reason",
    "rusWord": "причина",
    "transcription": "[riːzn]"
  },
  {
    "engWord": "push",
    "rusWord": "толкать",
    "transcription": "[pʊʃ]"
  },
  {
    "engWord": "banking",
    "rusWord": "банковское дело",
    "transcription": "[ˈbæŋkɪŋ]"
  },
  {
    "engWord": "longitude",
    "rusWord": "долгота",
    "transcription": "[ˈlɒnʤɪtjuːd]"
  },
  {
    "engWord": "bureau",
    "rusWord": "бюро",
    "transcription": "[ˈbjʊ(ə)rəʊ]"
  },
  {
    "engWord": "sensation",
    "rusWord": "чувство",
    "transcription": "[senˈseɪʃn]"
  },
  {
    "engWord": "inland",
    "rusWord": "внутренний",
    "transcription": "[ˈɪnlənd]"
  },
  {
    "engWord": "medication",
    "rusWord": "лечение",
    "transcription": "[medɪˈkeɪʃn]"
  },
  {
    "engWord": "fighter",
    "rusWord": "борец",
    "transcription": "[ˈfaɪtə]"
  },
  {
    "engWord": "invest",
    "rusWord": "вкладывать деньги",
    "transcription": "[ɪnˈvest]"
  },
  {
    "engWord": "awfully",
    "rusWord": "чрезвычайно",
    "transcription": "[ˈɔːflɪ]"
  },
  {
    "engWord": "creature",
    "rusWord": "существо",
    "transcription": "[ˈkriːʧə]"
  },
  {
    "engWord": "raw",
    "rusWord": "вид сырья",
    "transcription": "[rɔː]"
  },
  {
    "engWord": "battery",
    "rusWord": "батарея",
    "transcription": "[ˈbætərɪ]"
  },
  {
    "engWord": "fortune",
    "rusWord": "состояние",
    "transcription": "[ˈfɔːʧən]"
  },
  {
    "engWord": "bust",
    "rusWord": "бюст",
    "transcription": "[bʌst]"
  },
  {
    "engWord": "army",
    "rusWord": "армия",
    "transcription": "[ˈɑːmɪ]"
  },
  {
    "engWord": "tonight",
    "rusWord": "сегодняшний вечер",
    "transcription": "[təˈnaɪt]"
  },
  {
    "engWord": "kick",
    "rusWord": "пинать",
    "transcription": "[kɪk]"
  },
  {
    "engWord": "promise",
    "rusWord": "обещать",
    "transcription": "[ˈprɒmɪs]"
  },
  {
    "engWord": "limit",
    "rusWord": "предел",
    "transcription": "[ˈlɪmɪt]"
  },
  {
    "engWord": "beard",
    "rusWord": "борода",
    "transcription": "[bɪəd]"
  },
  {
    "engWord": "believe",
    "rusWord": "верить",
    "transcription": "[bɪˈliːv]"
  },
  {
    "engWord": "grass",
    "rusWord": "трава",
    "transcription": "[grɑːs]"
  },
  {
    "engWord": "rather",
    "rusWord": "скорее",
    "transcription": "[ˈrɑːðə]"
  },
  {
    "engWord": "marrow",
    "rusWord": "костный мозг",
    "transcription": "[ˈmærəʊ]"
  },
  {
    "engWord": "suspend",
    "rusWord": "приостановить",
    "transcription": "[səsˈpend]"
  },
  {
    "engWord": "bachelor",
    "rusWord": "бакалавр",
    "transcription": "[ˈbæʧələ]"
  },
  {
    "engWord": "orientation",
    "rusWord": "ориентация",
    "transcription": "[ɔːrɪenˈteɪʃn]"
  },
  {
    "engWord": "solo",
    "rusWord": "сольный",
    "transcription": "[ˈsəʊləʊ]"
  },
  {
    "engWord": "export",
    "rusWord": "экспорт",
    "transcription": "[ˈekspɔːt]"
  },
  {
    "engWord": "semester",
    "rusWord": "семестр",
    "transcription": "[sɪˈmestə]"
  },
  {
    "engWord": "passport",
    "rusWord": "паспорт",
    "transcription": "[ˈpɑːspɔːt]"
  },
  {
    "engWord": "necessity",
    "rusWord": "необходимость",
    "transcription": "[nɪˈsesɪtɪ]"
  },
  {
    "engWord": "newly",
    "rusWord": "вновь",
    "transcription": "[ˈnjuːlɪ]"
  },
  {
    "engWord": "eaten",
    "rusWord": "съеденный",
    "transcription": "[iːtn]"
  },
  {
    "engWord": "frequently",
    "rusWord": "часто",
    "transcription": "[ˈfriːkwəntlɪ]"
  },
  {
    "engWord": "capacity",
    "rusWord": "вместимость",
    "transcription": "[kəˈpæsɪtɪ]"
  },
  {
    "engWord": "strain",
    "rusWord": "напряжение",
    "transcription": "[streɪn]"
  },
  {
    "engWord": "exhaust",
    "rusWord": "истощить",
    "transcription": "[ɪgˈzɔːst]"
  },
  {
    "engWord": "glasses",
    "rusWord": "очки",
    "transcription": "[ˈɡlɑːsɪz]"
  },
  {
    "engWord": "hearing",
    "rusWord": "слышащий",
    "transcription": "[ˈhɪərɪŋ]"
  },
  {
    "engWord": "snake",
    "rusWord": "змея",
    "transcription": "[sneɪk]"
  },
  {
    "engWord": "worth",
    "rusWord": "стоимость",
    "transcription": "[wɜːθ]"
  },
  {
    "engWord": "lamp",
    "rusWord": "лампа",
    "transcription": "[læmp]"
  },
  {
    "engWord": "man",
    "rusWord": "мужчина",
    "transcription": "[mæn]"
  },
  {
    "engWord": "insect",
    "rusWord": "насекомое",
    "transcription": "[ˈɪnsekt]"
  },
  {
    "engWord": "stab",
    "rusWord": "удар",
    "transcription": "[stæb]"
  },
  {
    "engWord": "peculiar",
    "rusWord": "своеобразный",
    "transcription": "[pɪˈkjuːlɪə]"
  },
  {
    "engWord": "inspection",
    "rusWord": "осмотр",
    "transcription": "[ɪnˈspekʃn]"
  },
  {
    "engWord": "instinct",
    "rusWord": "инстинкт",
    "transcription": "[ˈɪnstɪŋkt]"
  },
  {
    "engWord": "hazard",
    "rusWord": "опасность",
    "transcription": "[ˈhæzəd]"
  },
  {
    "engWord": "tremendous",
    "rusWord": "огромный",
    "transcription": "[trɪˈmendəs]"
  },
  {
    "engWord": "coincident",
    "rusWord": "совпадающий",
    "transcription": "[kəʊˈɪnsɪdənt]"
  },
  {
    "engWord": "succeed",
    "rusWord": "успешный",
    "transcription": "[səkˈsiːd]"
  },
  {
    "engWord": "adequate",
    "rusWord": "адекватный",
    "transcription": "[ˈædɪkwɪt]"
  },
  {
    "engWord": "encourage",
    "rusWord": "поощрять",
    "transcription": "[ɪnˈkʌrɪʤ]"
  },
  {
    "engWord": "financial",
    "rusWord": "финансовый",
    "transcription": "[faɪˈnænʃəl]"
  },
  {
    "engWord": "ash",
    "rusWord": "пепел",
    "transcription": "[æʃ]"
  },
  {
    "engWord": "deliver",
    "rusWord": "доставлять",
    "transcription": "[dɪˈlɪvə]"
  },
  {
    "engWord": "sick",
    "rusWord": "больной",
    "transcription": "[sɪk]"
  },
  {
    "engWord": "hang",
    "rusWord": "вешать",
    "transcription": "[hæŋ]"
  },
  {
    "engWord": "funny",
    "rusWord": "смешной",
    "transcription": "[ˈfʌnɪ]"
  },
  {
    "engWord": "covered",
    "rusWord": "покрытый",
    "transcription": "[ˈkʌvəd]"
  },
  {
    "engWord": "all",
    "rusWord": "все",
    "transcription": "[ɔːl]"
  },
  {
    "engWord": "minute",
    "rusWord": "минута",
    "transcription": "[ˈmɪnɪt]"
  },
  {
    "engWord": "point",
    "rusWord": "точка",
    "transcription": "[pɔɪnt]"
  },
  {
    "engWord": "flour",
    "rusWord": "мука",
    "transcription": "[ˈflaʊə]"
  },
  {
    "engWord": "singular",
    "rusWord": "единственное число",
    "transcription": "[ˈsɪŋgjʊlə]"
  },
  {
    "engWord": "vertical",
    "rusWord": "вертикаль",
    "transcription": "[ˈvɜːtɪkəl]"
  },
  {
    "engWord": "technical",
    "rusWord": "технический",
    "transcription": "[ˈteknɪkəl]"
  },
  {
    "engWord": "remote",
    "rusWord": "дистанционный",
    "transcription": "[rɪˈməʊt]"
  },
  {
    "engWord": "civilian",
    "rusWord": "штатский",
    "transcription": "[sɪˈvɪlɪən]"
  },
  {
    "engWord": "vacant",
    "rusWord": "вакантный",
    "transcription": "[ˈveɪkənt]"
  },
  {
    "engWord": "benefit",
    "rusWord": "выгода",
    "transcription": "[ˈbenɪfɪt]"
  },
  {
    "engWord": "squad",
    "rusWord": "команда",
    "transcription": "[skwɒd]"
  },
  {
    "engWord": "trousers",
    "rusWord": "брюки",
    "transcription": "[ˈtraʊzəz]"
  },
  {
    "engWord": "along",
    "rusWord": "вдоль",
    "transcription": "[əˈlɒŋ]"
  },
  {
    "engWord": "excited",
    "rusWord": "возбужденный",
    "transcription": "[ɪkˈsaɪtɪd]"
  },
  {
    "engWord": "impossible",
    "rusWord": "невозможный",
    "transcription": "[ɪmˈpɒsəbl]"
  },
  {
    "engWord": "wealth",
    "rusWord": "богатство",
    "transcription": "[welθ]"
  },
  {
    "engWord": "barely",
    "rusWord": "едва",
    "transcription": "[ˈbeəlɪ]"
  },
  {
    "engWord": "provide",
    "rusWord": "обеспечивать",
    "transcription": "[prəˈvaɪd]"
  },
  {
    "engWord": "broad",
    "rusWord": "широкий",
    "transcription": "[brɔːd]"
  },
  {
    "engWord": "visit",
    "rusWord": "посетить",
    "transcription": "[ˈvɪzɪt]"
  },
  {
    "engWord": "center",
    "rusWord": "центр",
    "transcription": "[ˈsentə]"
  },
  {
    "engWord": "check",
    "rusWord": "проверять",
    "transcription": "[ʧek]"
  },
  {
    "engWord": "about",
    "rusWord": "о",
    "transcription": "[əˈbaʊt]"
  },
  {
    "engWord": "who",
    "rusWord": "кто",
    "transcription": "[huː]"
  },
  {
    "engWord": "frequency",
    "rusWord": "частота",
    "transcription": "[ˈfriːkwənsɪ]"
  },
  {
    "engWord": "defensive",
    "rusWord": "оборонительный",
    "transcription": "[dɪˈfensɪv]"
  },
  {
    "engWord": "nationality",
    "rusWord": "национальность",
    "transcription": "[næʃəˈnælɪtɪ]"
  },
  {
    "engWord": "rural",
    "rusWord": "сельский",
    "transcription": "[ˈrʊərəl]"
  },
  {
    "engWord": "arrogant",
    "rusWord": "высокомерный",
    "transcription": "[ˈærəgənt]"
  },
  {
    "engWord": "cloth",
    "rusWord": "ткань",
    "transcription": "[klɒθ]"
  },
  {
    "engWord": "awhile",
    "rusWord": "некоторое время",
    "transcription": "[əˈwaɪl]"
  },
  {
    "engWord": "liver",
    "rusWord": "печень",
    "transcription": "[ˈlɪvə]"
  },
  {
    "engWord": "purple",
    "rusWord": "фиолетовый",
    "transcription": "[pɜːpl]"
  },
  {
    "engWord": "junior",
    "rusWord": "младший",
    "transcription": "[ˈʤuːnɪə]"
  },
  {
    "engWord": "usual",
    "rusWord": "обычный",
    "transcription": "[ˈjuːʒʊəl]"
  },
  {
    "engWord": "stone",
    "rusWord": "камень",
    "transcription": "[stəʊn]"
  },
  {
    "engWord": "more",
    "rusWord": "больше",
    "transcription": "[mɔː]"
  },
  {
    "engWord": "perhaps",
    "rusWord": "возможно",
    "transcription": "[pəˈhæps]"
  },
  {
    "engWord": "tribe",
    "rusWord": "племя",
    "transcription": "[traɪb]"
  },
  {
    "engWord": "scold",
    "rusWord": "ругать",
    "transcription": "[skəʊld]"
  },
  {
    "engWord": "electrical",
    "rusWord": "электрический",
    "transcription": "[ɪˈlektrɪkəl]"
  },
  {
    "engWord": "Canadian",
    "rusWord": "канадский",
    "transcription": "[kəˈneɪdɪən]"
  },
  {
    "engWord": "lodge",
    "rusWord": "домик",
    "transcription": "[lɒʤ]"
  },
  {
    "engWord": "explosion",
    "rusWord": "взрыв",
    "transcription": "[ɪksˈpləʊʒən]"
  },
  {
    "engWord": "rum",
    "rusWord": "ром",
    "transcription": "[rʌm]"
  },
  {
    "engWord": "childhood",
    "rusWord": "детство",
    "transcription": "[ˈʧaɪldhʊd]"
  },
  {
    "engWord": "lobby",
    "rusWord": "вестибюль",
    "transcription": "[ˈlɒbɪ]"
  },
  {
    "engWord": "expert",
    "rusWord": "эксперт",
    "transcription": "[ˈekspɜːt]"
  },
  {
    "engWord": "email",
    "rusWord": "электронная почта",
    "transcription": "[ɪˈmeɪl]"
  },
  {
    "engWord": "gentleman",
    "rusWord": "джентльмен",
    "transcription": "[ˈʤentlmən]"
  },
  {
    "engWord": "television",
    "rusWord": "телевидение",
    "transcription": "[ˈtelɪvɪʒən]"
  },
  {
    "engWord": "ruin",
    "rusWord": "разрушать",
    "transcription": "[ˈruːɪn]"
  },
  {
    "engWord": "send",
    "rusWord": "посылать",
    "transcription": "[send]"
  },
  {
    "engWord": "near",
    "rusWord": "рядом",
    "transcription": "[nɪə]"
  },
  {
    "engWord": "contain",
    "rusWord": "содержать",
    "transcription": "[kənˈteɪn]"
  },
  {
    "engWord": "desert",
    "rusWord": "пустыня",
    "transcription": "[ˈdezət]"
  },
  {
    "engWord": "tattoo",
    "rusWord": "татуировка",
    "transcription": "[ˈtætuː]"
  },
  {
    "engWord": "dock",
    "rusWord": "док",
    "transcription": "[dɒk]"
  },
  {
    "engWord": "hunch",
    "rusWord": "горб",
    "transcription": "[hʌnʧ]"
  },
  {
    "engWord": "biased",
    "rusWord": "пристрастный",
    "transcription": "[ˈbaɪəst]"
  },
  {
    "engWord": "particle",
    "rusWord": "крупица",
    "transcription": "[ˈpɑːtɪkl]"
  },
  {
    "engWord": "foolish",
    "rusWord": "глупый",
    "transcription": "[ˈfuːlɪʃ]"
  },
  {
    "engWord": "significance",
    "rusWord": "значимость",
    "transcription": "[sɪgˈnɪfɪkəns]"
  },
  {
    "engWord": "fur",
    "rusWord": "мех",
    "transcription": "[fɜː]"
  },
  {
    "engWord": "cane",
    "rusWord": "тростник",
    "transcription": "[keɪn]"
  },
  {
    "engWord": "hiring",
    "rusWord": "найм",
    "transcription": "[ˈhaɪərɪŋ]"
  },
  {
    "engWord": "leftover",
    "rusWord": "остаток",
    "transcription": "[ˈleftəʊvə]"
  },
  {
    "engWord": "extreme",
    "rusWord": "крайность",
    "transcription": "[ɪksˈtriːm]"
  },
  {
    "engWord": "protest",
    "rusWord": "возразить",
    "transcription": "[ˈprəʊtest]"
  },
  {
    "engWord": "physically",
    "rusWord": "физически",
    "transcription": "[ˈfɪzɪklɪ]"
  },
  {
    "engWord": "Italian",
    "rusWord": "итальянский",
    "transcription": "[ɪˈtælɪən]"
  },
  {
    "engWord": "elderly",
    "rusWord": "пожилой",
    "transcription": "[ˈeldəlɪ]"
  },
  {
    "engWord": "useless",
    "rusWord": "бесполезный",
    "transcription": "[ˈjuːslɪs]"
  },
  {
    "engWord": "phone",
    "rusWord": "телефон",
    "transcription": "[fəʊn]"
  },
  {
    "engWord": "servant",
    "rusWord": "слуга",
    "transcription": "[ˈsɜːvənt]"
  },
  {
    "engWord": "oil",
    "rusWord": "масло",
    "transcription": "[ɔɪl]"
  },
  {
    "engWord": "gun",
    "rusWord": "пушка",
    "transcription": "[gʌn]"
  },
  {
    "engWord": "episode",
    "rusWord": "эпизод",
    "transcription": "[ˈepɪsəʊd]"
  },
  {
    "engWord": "kidnap",
    "rusWord": "похищать",
    "transcription": "[ˈkɪdnæp]"
  },
  {
    "engWord": "respectful",
    "rusWord": "уважаемый",
    "transcription": "[rɪˈspektf(ə)l]"
  },
  {
    "engWord": "peek",
    "rusWord": "заглядывать",
    "transcription": "[piːk]"
  },
  {
    "engWord": "felony",
    "rusWord": "уголовное преступление",
    "transcription": "[ˈfelənɪ]"
  },
  {
    "engWord": "lifestyle",
    "rusWord": "стиль жизни",
    "transcription": "[ˈlaɪfstaɪl]"
  },
  {
    "engWord": "tabloid",
    "rusWord": "бульварный",
    "transcription": "[ˈtæblɔɪd]"
  },
  {
    "engWord": "firing",
    "rusWord": "обжиг",
    "transcription": "[ˈfaɪərɪŋ]"
  },
  {
    "engWord": "plausible",
    "rusWord": "правдоподобный",
    "transcription": "[ˈplɔːzəbl]"
  },
  {
    "engWord": "underground",
    "rusWord": "подземный",
    "transcription": "[ˈʌndəgraʊnd]"
  },
  {
    "engWord": "sexy",
    "rusWord": "чувственный",
    "transcription": "[ˈseksɪ]"
  },
  {
    "engWord": "anybody",
    "rusWord": "никто",
    "transcription": "[ˈenɪbɒdɪ]"
  },
  {
    "engWord": "damage",
    "rusWord": "повреждение",
    "transcription": "[ˈdæmɪʤ]"
  },
  {
    "engWord": "storm",
    "rusWord": "буря",
    "transcription": "[stɔːm]"
  },
  {
    "engWord": "young",
    "rusWord": "юный",
    "transcription": "[jʌŋ]"
  },
  {
    "engWord": "experience",
    "rusWord": "опыт",
    "transcription": "[ɪksˈpɪərɪəns]"
  },
  {
    "engWord": "strong",
    "rusWord": "сильный",
    "transcription": "[strɒŋ]"
  },
  {
    "engWord": "fact",
    "rusWord": "факт",
    "transcription": "[fækt]"
  },
  {
    "engWord": "trophy",
    "rusWord": "трофей",
    "transcription": "[ˈtrəʊfɪ]"
  },
  {
    "engWord": "safeguard",
    "rusWord": "защищать",
    "transcription": "[ˈseɪfgɑːd]"
  },
  {
    "engWord": "entitled",
    "rusWord": "озаглавленный",
    "transcription": "[ɪnˈtaɪtld]"
  },
  {
    "engWord": "assistance",
    "rusWord": "помощь",
    "transcription": "[əˈsɪstəns]"
  },
  {
    "engWord": "billiards",
    "rusWord": "бильярд",
    "transcription": "[ˈbɪljədz]"
  },
  {
    "engWord": "rabbit",
    "rusWord": "кролик",
    "transcription": "[ˈræbɪt]"
  },
  {
    "engWord": "fail",
    "rusWord": "потерпеть неудачу",
    "transcription": "[feɪl]"
  },
  {
    "engWord": "district",
    "rusWord": "район",
    "transcription": "[ˈdɪstrɪkt]"
  },
  {
    "engWord": "exciting",
    "rusWord": "захватывающий",
    "transcription": "[ɪkˈsaɪtɪŋ]"
  },
  {
    "engWord": "abuse",
    "rusWord": "злоупотребление",
    "transcription": "[əˈbjuːs]"
  },
  {
    "engWord": "planet",
    "rusWord": "планета",
    "transcription": "[ˈplænɪt]"
  },
  {
    "engWord": "log",
    "rusWord": "бревно",
    "transcription": "[lɒg]"
  },
  {
    "engWord": "outfit",
    "rusWord": "снаряжение",
    "transcription": "[ˈaʊtfɪt]"
  },
  {
    "engWord": "ruler",
    "rusWord": "правитель",
    "transcription": "[ˈruːlə]"
  },
  {
    "engWord": "remarkable",
    "rusWord": "замечательный",
    "transcription": "[rɪˈmɑːkəbl]"
  },
  {
    "engWord": "hardware",
    "rusWord": "аппаратура",
    "transcription": "[ˈhɑːdweə]"
  },
  {
    "engWord": "clip",
    "rusWord": "зажим",
    "transcription": "[klɪp]"
  },
  {
    "engWord": "marketing",
    "rusWord": "маркетинг",
    "transcription": "[ˈmɑːkɪtɪŋ]"
  },
  {
    "engWord": "gourmet",
    "rusWord": "лакомка",
    "transcription": "[ˈgʊəmeɪ]"
  },
  {
    "engWord": "neglect",
    "rusWord": "пренебрегать",
    "transcription": "[nɪˈglekt]"
  },
  {
    "engWord": "import",
    "rusWord": "импортировать",
    "transcription": "[ˈɪmpɔːt]"
  },
  {
    "engWord": "mighty",
    "rusWord": "могущественный",
    "transcription": "[ˈmaɪtɪ]"
  },
  {
    "engWord": "bean",
    "rusWord": "боб",
    "transcription": "[biːn]"
  },
  {
    "engWord": "pride",
    "rusWord": "гордость",
    "transcription": "[praɪd]"
  },
  {
    "engWord": "bush",
    "rusWord": "Буш",
    "transcription": "[bʊʃ]"
  },
  {
    "engWord": "secondly",
    "rusWord": "во вторых",
    "transcription": "[ˈsekəndlɪ]"
  },
  {
    "engWord": "virgin",
    "rusWord": "дева",
    "transcription": "[ˈvɜːʤɪn]"
  },
  {
    "engWord": "breath",
    "rusWord": "дыхание",
    "transcription": "[breθ]"
  },
  {
    "engWord": "ate",
    "rusWord": "Ате",
    "transcription": "[et]"
  },
  {
    "engWord": "stranger",
    "rusWord": "незнакомец",
    "transcription": "[ˈstreɪnʤə]"
  },
  {
    "engWord": "toilet",
    "rusWord": "туалет",
    "transcription": "[ˈtɔɪlɪt]"
  },
  {
    "engWord": "session",
    "rusWord": "сессия",
    "transcription": "[seʃn]"
  },
  {
    "engWord": "weapon",
    "rusWord": "оружие",
    "transcription": "[ˈwepən]"
  },
  {
    "engWord": "write",
    "rusWord": "писать",
    "transcription": "[raɪt]"
  },
  {
    "engWord": "country",
    "rusWord": "страна",
    "transcription": "[ˈkʌntrɪ]"
  },
  {
    "engWord": "position",
    "rusWord": "позиция",
    "transcription": "[pəˈzɪʃn]"
  },
  {
    "engWord": "product",
    "rusWord": "товар",
    "transcription": "[ˈprɒdʌkt]"
  },
  {
    "engWord": "effect",
    "rusWord": "эффект",
    "transcription": "[ɪˈfekt]"
  },
  {
    "engWord": "accent",
    "rusWord": "акцент",
    "transcription": "[ˈæksənt]"
  },
  {
    "engWord": "deadline",
    "rusWord": "крайний срок",
    "transcription": "[ˈdedlaɪn]"
  },
  {
    "engWord": "deceive",
    "rusWord": "обманывать",
    "transcription": "[dɪˈsiːv]"
  },
  {
    "engWord": "creation",
    "rusWord": "создание",
    "transcription": "[krɪˈeɪʃn]"
  },
  {
    "engWord": "fitness",
    "rusWord": "хорошая физическая форма",
    "transcription": "[ˈfɪtnɪs]"
  },
  {
    "engWord": "Russian",
    "rusWord": "русский",
    "transcription": "[rʌʃn]"
  },
  {
    "engWord": "narrative",
    "rusWord": "повествовательный",
    "transcription": "[ˈnærətɪv]"
  },
  {
    "engWord": "adopt",
    "rusWord": "принять",
    "transcription": "[əˈdɒpt]"
  },
  {
    "engWord": "opposition",
    "rusWord": "оппозиционный",
    "transcription": "[ɒpəˈzɪʃn]"
  },
  {
    "engWord": "collar",
    "rusWord": "воротник",
    "transcription": "[ˈkɒlə]"
  },
  {
    "engWord": "crush",
    "rusWord": "раздавить",
    "transcription": "[krʌʃ]"
  },
  {
    "engWord": "leather",
    "rusWord": "кожа",
    "transcription": "[ˈleðə]"
  },
  {
    "engWord": "incredibly",
    "rusWord": "невероятно",
    "transcription": "[ɪnˈkredəblɪ]"
  },
  {
    "engWord": "compound",
    "rusWord": "соединение",
    "transcription": "[ˈkɒmpaʊnd]"
  },
  {
    "engWord": "revenge",
    "rusWord": "месть",
    "transcription": "[rɪˈvenʤ]"
  },
  {
    "engWord": "swear",
    "rusWord": "клятва",
    "transcription": "[sweə]"
  },
  {
    "engWord": "okay",
    "rusWord": "окей",
    "transcription": "[əʊˈkeɪ]"
  },
  {
    "engWord": "dance",
    "rusWord": "танец",
    "transcription": "[dɑːns]"
  },
  {
    "engWord": "clear",
    "rusWord": "четкий",
    "transcription": "[klɪə]"
  },
  {
    "engWord": "trip",
    "rusWord": "поездка",
    "transcription": "[trɪp]"
  },
  {
    "engWord": "past",
    "rusWord": "прошлое",
    "transcription": "[pɑːst]"
  },
  {
    "engWord": "prepare",
    "rusWord": "подготовить",
    "transcription": "[prɪˈpeə]"
  },
  {
    "engWord": "conquer",
    "rusWord": "завоевывать",
    "transcription": "[ˈkɒŋkə]"
  },
  {
    "engWord": "slack",
    "rusWord": "слабина",
    "transcription": "[slæk]"
  },
  {
    "engWord": "addicted",
    "rusWord": "зависимый",
    "transcription": "[əˈdɪktɪd]"
  },
  {
    "engWord": "tumor",
    "rusWord": "опухоль",
    "transcription": "[ˈtjuːmə]"
  },
  {
    "engWord": "wallpaper",
    "rusWord": "обои",
    "transcription": "[ˈwɔːlpeɪpə]"
  },
  {
    "engWord": "fluent",
    "rusWord": "беглый",
    "transcription": "[ˈfluːənt]"
  },
  {
    "engWord": "fried",
    "rusWord": "жареный",
    "transcription": "[fraɪd]"
  },
  {
    "engWord": "cutter",
    "rusWord": "резец",
    "transcription": "[ˈkʌtə]"
  },
  {
    "engWord": "humanity",
    "rusWord": "человечество",
    "transcription": "[hjʊˈmænɪtɪ]"
  },
  {
    "engWord": "clarify",
    "rusWord": "уточнить",
    "transcription": "[ˈklærɪfaɪ]"
  },
  {
    "engWord": "payback",
    "rusWord": "окупаемость",
    "transcription": "[ˈpeɪbæk]"
  },
  {
    "engWord": "shelter",
    "rusWord": "укрытие",
    "transcription": "[ˈʃeltə]"
  },
  {
    "engWord": "facing",
    "rusWord": "облицовка",
    "transcription": "[ˈfeɪsɪŋ]"
  },
  {
    "engWord": "ideal",
    "rusWord": "идеал",
    "transcription": "[aɪˈdɪəl]"
  },
  {
    "engWord": "radar",
    "rusWord": "радар",
    "transcription": "[ˈreɪdɑː]"
  },
  {
    "engWord": "girlfriend",
    "rusWord": "девушка",
    "transcription": "[ˈɡɜːlfrɛnd]"
  },
  {
    "engWord": "tongue",
    "rusWord": "язык",
    "transcription": "[tʌŋ]"
  },
  {
    "engWord": "loose",
    "rusWord": "свободный",
    "transcription": "[luːs]"
  },
  {
    "engWord": "half",
    "rusWord": "половина",
    "transcription": "[hɑːf]"
  },
  {
    "engWord": "only",
    "rusWord": "только",
    "transcription": "[ˈəʊnlɪ]"
  },
  {
    "engWord": "night",
    "rusWord": "ночь",
    "transcription": "[naɪt]"
  },
  {
    "engWord": "think",
    "rusWord": "думать",
    "transcription": "[θɪŋk]"
  },
  {
    "engWord": "eagle",
    "rusWord": "орел",
    "transcription": "[iːgl]"
  },
  {
    "engWord": "appropriate",
    "rusWord": "подходящий",
    "transcription": "[]"
  },
  {
    "engWord": "jug",
    "rusWord": "кувшин",
    "transcription": "[ʤʌg]"
  },
  {
    "engWord": "resolve",
    "rusWord": "разрешить",
    "transcription": "[rɪˈzɒlv]"
  },
  {
    "engWord": "dash",
    "rusWord": "тире",
    "transcription": "[dæʃ]"
  },
  {
    "engWord": "commit",
    "rusWord": "совершить",
    "transcription": "[kəˈmɪt]"
  },
  {
    "engWord": "lounge",
    "rusWord": "гостиная",
    "transcription": "[laʊnʤ]"
  },
  {
    "engWord": "soda",
    "rusWord": "сода",
    "transcription": "[ˈsəʊdə]"
  },
  {
    "engWord": "depth",
    "rusWord": "глубина",
    "transcription": "[depθ]"
  },
  {
    "engWord": "faster",
    "rusWord": "более быстрый",
    "transcription": "[ˈfɑːstər]"
  },
  {
    "engWord": "blind",
    "rusWord": "слепой",
    "transcription": "[blaɪnd]"
  },
  {
    "engWord": "tea",
    "rusWord": "чай",
    "transcription": "[tiː]"
  },
  {
    "engWord": "open",
    "rusWord": "открывать",
    "transcription": "[ˈəʊpən]"
  },
  {
    "engWord": "noun",
    "rusWord": "существительное",
    "transcription": "[naʊn]"
  },
  {
    "engWord": "rise",
    "rusWord": "подъем",
    "transcription": "[raɪz]"
  },
  {
    "engWord": "room",
    "rusWord": "комната",
    "transcription": "[rum]"
  },
  {
    "engWord": "calorie",
    "rusWord": "калория",
    "transcription": "[ˈkælərɪ]"
  },
  {
    "engWord": "graduation",
    "rusWord": "выпускной",
    "transcription": "[græʤʊˈeɪʃn]"
  },
  {
    "engWord": "risky",
    "rusWord": "рискованный",
    "transcription": "[ˈrɪskɪ]"
  },
  {
    "engWord": "outdoor",
    "rusWord": "на открытом воздухе",
    "transcription": "[ˈaʊtdɔː]"
  },
  {
    "engWord": "mock",
    "rusWord": "издеваться",
    "transcription": "[mɒk]"
  },
  {
    "engWord": "ambitious",
    "rusWord": "амбициозный",
    "transcription": "[æmˈbɪʃəs]"
  },
  {
    "engWord": "shark",
    "rusWord": "акула",
    "transcription": "[ʃɑːk]"
  },
  {
    "engWord": "outrageous",
    "rusWord": "возмутительный",
    "transcription": "[aʊtˈreɪʤəs]"
  },
  {
    "engWord": "potential",
    "rusWord": "потенциал",
    "transcription": "[pəˈtenʃ(ə)l]"
  },
  {
    "engWord": "monkey",
    "rusWord": "обезьяна",
    "transcription": "[ˈmʌŋkɪ]"
  },
  {
    "engWord": "leaf",
    "rusWord": "лист",
    "transcription": "[liːf]"
  },
  {
    "engWord": "formula",
    "rusWord": "формула",
    "transcription": "[ˈfɔːmjʊlə]"
  },
  {
    "engWord": "grab",
    "rusWord": "захватывать",
    "transcription": "[græb]"
  },
  {
    "engWord": "excuse",
    "rusWord": "оправдание",
    "transcription": "[ɪksˈkjuːs]"
  },
  {
    "engWord": "secret",
    "rusWord": "секрет",
    "transcription": "[ˈsiːkrɪt]"
  },
  {
    "engWord": "win",
    "rusWord": "выиграть",
    "transcription": "[wɪn]"
  },
  {
    "engWord": "yet",
    "rusWord": "пока",
    "transcription": "[jet]"
  },
  {
    "engWord": "between",
    "rusWord": "между",
    "transcription": "[bɪˈtwiːn]"
  },
  {
    "engWord": "show",
    "rusWord": "показывать",
    "transcription": "[ʃəʊ]"
  },
  {
    "engWord": "patron",
    "rusWord": "покровитель",
    "transcription": "[ˈpeɪtrən]"
  },
  {
    "engWord": "goods",
    "rusWord": "товары",
    "transcription": "[gʊdz]"
  },
  {
    "engWord": "solar",
    "rusWord": "солнечный",
    "transcription": "[ˈsəʊlə]"
  },
  {
    "engWord": "obituary",
    "rusWord": "некролог",
    "transcription": "[əˈbɪʧʊ(ə)rɪ]"
  },
  {
    "engWord": "weep",
    "rusWord": "плакать",
    "transcription": "[wiːp]"
  },
  {
    "engWord": "muse",
    "rusWord": "муза",
    "transcription": "[mjuːz]"
  },
  {
    "engWord": "compromise",
    "rusWord": "компромисс",
    "transcription": "[ˈkɒmprəmaɪz]"
  },
  {
    "engWord": "madness",
    "rusWord": "безумие",
    "transcription": "[ˈmædnɪs]"
  },
  {
    "engWord": "chapter",
    "rusWord": "глава",
    "transcription": "[ˈʧæptə]"
  },
  {
    "engWord": "hip",
    "rusWord": "бедро",
    "transcription": "[hɪp]"
  },
  {
    "engWord": "destiny",
    "rusWord": "судьба",
    "transcription": "[ˈdestɪnɪ]"
  },
  {
    "engWord": "economy",
    "rusWord": "экономика",
    "transcription": "[ɪˈkɒnəmɪ]"
  },
  {
    "engWord": "fourth",
    "rusWord": "четвертый",
    "transcription": "[fɔːθ]"
  },
  {
    "engWord": "forever",
    "rusWord": "навсегда",
    "transcription": "[fəˈrevə]"
  },
  {
    "engWord": "blame",
    "rusWord": "вина",
    "transcription": "[bleɪm]"
  },
  {
    "engWord": "furniture",
    "rusWord": "мебель",
    "transcription": "[ˈfɜːnɪʧə]"
  },
  {
    "engWord": "fan",
    "rusWord": "вентилятор",
    "transcription": "[fæn]"
  },
  {
    "engWord": "nowhere",
    "rusWord": "нигде",
    "transcription": "[ˈnəʊweə]"
  },
  {
    "engWord": "champagne",
    "rusWord": "шампанское",
    "transcription": "[ʃæmˈpeɪn]"
  },
  {
    "engWord": "different",
    "rusWord": "различный",
    "transcription": "[ˈdɪfrənt]"
  },
  {
    "engWord": "cover",
    "rusWord": "покрытие",
    "transcription": "[ˈkʌvə]"
  },
  {
    "engWord": "dog",
    "rusWord": "собака",
    "transcription": "[dɒg]"
  },
  {
    "engWord": "beloved",
    "rusWord": "возлюбленный",
    "transcription": "[bɪˈlʌvɪd]"
  },
  {
    "engWord": "numb",
    "rusWord": "онемевший",
    "transcription": "[nʌm]"
  },
  {
    "engWord": "scout",
    "rusWord": "разведчик",
    "transcription": "[skaʊt]"
  },
  {
    "engWord": "explore",
    "rusWord": "исследовать",
    "transcription": "[ɪksˈplɔː]"
  },
  {
    "engWord": "acceptance",
    "rusWord": "принятие",
    "transcription": "[əkˈseptəns]"
  },
  {
    "engWord": "location",
    "rusWord": "местоположение",
    "transcription": "[ləʊˈkeɪʃn]"
  },
  {
    "engWord": "waitress",
    "rusWord": "официантка",
    "transcription": "[ˈweɪtrɪs]"
  },
  {
    "engWord": "cooper",
    "rusWord": "Бондарь",
    "transcription": "[ˈkuːpə]"
  },
  {
    "engWord": "emotion",
    "rusWord": "эмоция",
    "transcription": "[ɪˈməʊʃn]"
  },
  {
    "engWord": "gap",
    "rusWord": "пробел",
    "transcription": "[gæp]"
  },
  {
    "engWord": "freedom",
    "rusWord": "свобода",
    "transcription": "[ˈfriːdəm]"
  },
  {
    "engWord": "skill",
    "rusWord": "навык",
    "transcription": "[skɪl]"
  },
  {
    "engWord": "you",
    "rusWord": "вы",
    "transcription": "[juː]"
  },
  {
    "engWord": "study",
    "rusWord": "изучать",
    "transcription": "[ˈstʌdɪ]"
  },
  {
    "engWord": "thing",
    "rusWord": "вещь",
    "transcription": "[θɪŋ]"
  },
  {
    "engWord": "three",
    "rusWord": "три",
    "transcription": "[θriː]"
  },
  {
    "engWord": "common",
    "rusWord": "общий",
    "transcription": "[ˈkɒmən]"
  },
  {
    "engWord": "domestic",
    "rusWord": "внутренний",
    "transcription": "[dəˈmestɪk]"
  },
  {
    "engWord": "ghost",
    "rusWord": "привидение",
    "transcription": "[gəʊst]"
  },
  {
    "engWord": "subsequently",
    "rusWord": "впоследствии",
    "transcription": "[ˈsʌbsɪkwəntlɪ]"
  },
  {
    "engWord": "removal",
    "rusWord": "удаление",
    "transcription": "[rɪˈmuːvəl]"
  },
  {
    "engWord": "upstairs",
    "rusWord": "верхний этаж",
    "transcription": "[ʌpˈsteəz]"
  },
  {
    "engWord": "vehicle",
    "rusWord": "транспортное средство",
    "transcription": "[ˈviːɪkl]"
  },
  {
    "engWord": "diner",
    "rusWord": "обедающий",
    "transcription": "[ˈdaɪnə]"
  },
  {
    "engWord": "novel",
    "rusWord": "роман",
    "transcription": "[ˈnɒvəl]"
  },
  {
    "engWord": "counsel",
    "rusWord": "адвокат",
    "transcription": "[ˈkaʊnsəl]"
  },
  {
    "engWord": "creepy",
    "rusWord": "жуткий",
    "transcription": "[ˈkriːpɪ]"
  },
  {
    "engWord": "understanding",
    "rusWord": "понимание",
    "transcription": "[ʌndəˈstændɪŋ]"
  },
  {
    "engWord": "increase",
    "rusWord": "увеличение",
    "transcription": "[]"
  },
  {
    "engWord": "prince",
    "rusWord": "царевич",
    "transcription": "[prɪns]"
  },
  {
    "engWord": "step",
    "rusWord": "шаг",
    "transcription": "[step]"
  },
  {
    "engWord": "though",
    "rusWord": "хотя",
    "transcription": "[ðəʊ]"
  },
  {
    "engWord": "travel",
    "rusWord": "путешествовать",
    "transcription": "[trævl]"
  },
  {
    "engWord": "worship",
    "rusWord": "поклонение",
    "transcription": "[ˈwɜːʃɪp]"
  },
  {
    "engWord": "hostess",
    "rusWord": "стюардесса",
    "transcription": "[ˈhəʊstɪs]"
  },
  {
    "engWord": "relatively",
    "rusWord": "относительно",
    "transcription": "[ˈrelətɪvlɪ]"
  },
  {
    "engWord": "infected",
    "rusWord": "инфицированный",
    "transcription": "[ɪnˈfektɪd]"
  },
  {
    "engWord": "distract",
    "rusWord": "отвлекать",
    "transcription": "[dɪsˈtrækt]"
  },
  {
    "engWord": "mould",
    "rusWord": "формовать",
    "transcription": "[məʊld]"
  },
  {
    "engWord": "scent",
    "rusWord": "запах",
    "transcription": "[sent]"
  },
  {
    "engWord": "donor",
    "rusWord": "донор",
    "transcription": "[ˈdəʊnə]"
  },
  {
    "engWord": "bribery",
    "rusWord": "взяточничество",
    "transcription": "[ˈbraɪbərɪ]"
  },
  {
    "engWord": "grocer",
    "rusWord": "бакалейщик",
    "transcription": "[ˈgrəʊsə]"
  },
  {
    "engWord": "attempt",
    "rusWord": "попытка",
    "transcription": "[əˈtempt]"
  },
  {
    "engWord": "debate",
    "rusWord": "дебаты",
    "transcription": "[dɪˈbeɪt]"
  },
  {
    "engWord": "pace",
    "rusWord": "темп",
    "transcription": "[peɪs]"
  },
  {
    "engWord": "conference",
    "rusWord": "конференция",
    "transcription": "[ˈkɒnfərəns]"
  },
  {
    "engWord": "honestly",
    "rusWord": "честно",
    "transcription": "[ˈɒnɪstlɪ]"
  },
  {
    "engWord": "really",
    "rusWord": "действительно",
    "transcription": "[ˈrɪəlɪ]"
  },
  {
    "engWord": "timing",
    "rusWord": "синхронизация",
    "transcription": "[ˈtaɪmɪŋ]"
  },
  {
    "engWord": "difference",
    "rusWord": "разница",
    "transcription": "[ˈdɪfrəns]"
  },
  {
    "engWord": "eventually",
    "rusWord": "в итоге",
    "transcription": "[ɪˈvenʧʊ(ə)lɪ]"
  },
  {
    "engWord": "nail",
    "rusWord": "ноготь",
    "transcription": "[neɪl]"
  },
  {
    "engWord": "fire",
    "rusWord": "огонь",
    "transcription": "[ˈfaɪə]"
  },
  {
    "engWord": "follow",
    "rusWord": "следовать",
    "transcription": "[ˈfɒləʊ]"
  },
  {
    "engWord": "invent",
    "rusWord": "изобретать",
    "transcription": "[ɪnˈvent]"
  },
  {
    "engWord": "learn",
    "rusWord": "учить",
    "transcription": "[lɜːn]"
  },
  {
    "engWord": "search",
    "rusWord": "поиск",
    "transcription": "[sɜːʧ]"
  },
  {
    "engWord": "silent",
    "rusWord": "тихий",
    "transcription": "[ˈsaɪlənt]"
  },
  {
    "engWord": "limb",
    "rusWord": "конечность",
    "transcription": "[lɪm]"
  },
  {
    "engWord": "sieve",
    "rusWord": "сито",
    "transcription": "[sɪv]"
  },
  {
    "engWord": "recall",
    "rusWord": "отзыв",
    "transcription": "[rɪˈkɔːl]"
  },
  {
    "engWord": "economics",
    "rusWord": "экономика",
    "transcription": "[ekəˈnɒmɪks]"
  },
  {
    "engWord": "sixteen",
    "rusWord": "шестнадцать",
    "transcription": "[sɪkˈstiːn]"
  },
  {
    "engWord": "practically",
    "rusWord": "почти",
    "transcription": "[ˈpræktɪkəlɪ]"
  },
  {
    "engWord": "hate",
    "rusWord": "ненавидеть",
    "transcription": "[heɪt]"
  },
  {
    "engWord": "shall",
    "rusWord": "должны",
    "transcription": "[ʃæl]"
  },
  {
    "engWord": "romance",
    "rusWord": "роман",
    "transcription": "[rəʊˈmæns]"
  },
  {
    "engWord": "rigorous",
    "rusWord": "строгий",
    "transcription": "[ˈrɪgərəs]"
  },
  {
    "engWord": "delicate",
    "rusWord": "нежный",
    "transcription": "[ˈdelɪkɪt]"
  },
  {
    "engWord": "patent",
    "rusWord": "патент",
    "transcription": "[ˈpeɪtənt]"
  },
  {
    "engWord": "sideways",
    "rusWord": "в бок",
    "transcription": "[ˈsaɪdweɪz]"
  },
  {
    "engWord": "qualified",
    "rusWord": "квалифицированный",
    "transcription": "[ˈkwɒlɪfaɪd]"
  },
  {
    "engWord": "racing",
    "rusWord": "гоночный",
    "transcription": "[ˈreɪsɪŋ]"
  },
  {
    "engWord": "impact",
    "rusWord": "влияние",
    "transcription": "[ˈɪmpækt]"
  },
  {
    "engWord": "trapped",
    "rusWord": "захваченный",
    "transcription": "[træpt]"
  },
  {
    "engWord": "shut",
    "rusWord": "закрыть",
    "transcription": "[ʃʌt]"
  },
  {
    "engWord": "nicely",
    "rusWord": "мило",
    "transcription": "[ˈnaɪslɪ]"
  },
  {
    "engWord": "French",
    "rusWord": "французский",
    "transcription": "[frenʧ]"
  },
  {
    "engWord": "orange",
    "rusWord": "оранжевый",
    "transcription": "[ˈɒrɪnʤ]"
  },
  {
    "engWord": "cream",
    "rusWord": "крем",
    "transcription": "[kriːm]"
  },
  {
    "engWord": "view",
    "rusWord": "вид",
    "transcription": "[vjuː]"
  },
  {
    "engWord": "trade",
    "rusWord": "торговля",
    "transcription": "[treɪd]"
  },
  {
    "engWord": "but",
    "rusWord": "но",
    "transcription": "[bʌt]"
  },
  {
    "engWord": "firework",
    "rusWord": "фейерверк",
    "transcription": "[ˈfaɪəwɜːk]"
  },
  {
    "engWord": "flame",
    "rusWord": "пламя",
    "transcription": "[fleɪm]"
  },
  {
    "engWord": "discussion",
    "rusWord": "обсуждение",
    "transcription": "[dɪsˈkʌʃn]"
  },
  {
    "engWord": "shaft",
    "rusWord": "вал",
    "transcription": "[ʃɑːft]"
  },
  {
    "engWord": "semicolon",
    "rusWord": "точка с запятой",
    "transcription": "[semɪˈkəʊlən]"
  },
  {
    "engWord": "missing",
    "rusWord": "потерянный",
    "transcription": "[ˈmɪsɪŋ]"
  },
  {
    "engWord": "bikini",
    "rusWord": "бикини",
    "transcription": "[bɪˈkiːnɪ]"
  },
  {
    "engWord": "executed",
    "rusWord": "выполненный",
    "transcription": "[ˈeksɪkjuːtɪd]"
  },
  {
    "engWord": "ceiling",
    "rusWord": "потолок",
    "transcription": "[ˈsiːlɪŋ]"
  },
  {
    "engWord": "pollution",
    "rusWord": "загрязнение",
    "transcription": "[pəˈluːʃn]"
  },
  {
    "engWord": "actual",
    "rusWord": "фактический",
    "transcription": "[ˈækʧʊəl]"
  },
  {
    "engWord": "independent",
    "rusWord": "независимый",
    "transcription": "[ɪndɪˈpendənt]"
  },
  {
    "engWord": "golf",
    "rusWord": "гольф",
    "transcription": "[gɒlf]"
  },
  {
    "engWord": "pond",
    "rusWord": "пруд",
    "transcription": "[pɒnd]"
  },
  {
    "engWord": "itself",
    "rusWord": "сам",
    "transcription": "[ɪtˈself]"
  },
  {
    "engWord": "institute",
    "rusWord": "учреждение",
    "transcription": "[ˈɪnstɪtjuːt]"
  },
  {
    "engWord": "admit",
    "rusWord": "признавать",
    "transcription": "[ədˈmɪt]"
  },
  {
    "engWord": "busy",
    "rusWord": "оживленный",
    "transcription": "[ˈbɪzɪ]"
  },
  {
    "engWord": "child",
    "rusWord": "ребенок",
    "transcription": "[ʧaɪld]"
  },
  {
    "engWord": "gas",
    "rusWord": "газ",
    "transcription": "[gæs]"
  },
  {
    "engWord": "ready",
    "rusWord": "готовый",
    "transcription": "[ˈredɪ]"
  },
  {
    "engWord": "ride",
    "rusWord": "ездить",
    "transcription": "[raɪd]"
  },
  {
    "engWord": "settle",
    "rusWord": "селиться",
    "transcription": "[setl]"
  },
  {
    "engWord": "until",
    "rusWord": "до того момента",
    "transcription": "[ʌnˈtɪl]"
  },
  {
    "engWord": "eternal",
    "rusWord": "вечный",
    "transcription": "[ɪˈtɜːn(ə)l]"
  },
  {
    "engWord": "dial",
    "rusWord": "набирать номер",
    "transcription": "[ˈdaɪəl]"
  },
  {
    "engWord": "lemonade",
    "rusWord": "лимонад",
    "transcription": "[leməˈneɪd]"
  },
  {
    "engWord": "strengthen",
    "rusWord": "усилить",
    "transcription": "[ˈstreŋθən]"
  },
  {
    "engWord": "questioning",
    "rusWord": "вопросительный",
    "transcription": "[ˈkwesʧənɪŋ]"
  },
  {
    "engWord": "jet",
    "rusWord": "самолет",
    "transcription": "[ʤet]"
  },
  {
    "engWord": "withdraw",
    "rusWord": "вывести",
    "transcription": "[wɪðˈdrɔː]"
  },
  {
    "engWord": "precise",
    "rusWord": "точный",
    "transcription": "[prɪˈsaɪs]"
  },
  {
    "engWord": "finest",
    "rusWord": "великолепнейший",
    "transcription": "[ˈfɪnɪst]"
  },
  {
    "engWord": "paperwork",
    "rusWord": "делопроизводство",
    "transcription": "[ˈpeɪpəwɜːk]"
  },
  {
    "engWord": "reaction",
    "rusWord": "реакция",
    "transcription": "[rɪˈækʃn]"
  },
  {
    "engWord": "forgive",
    "rusWord": "прощать",
    "transcription": "[fəˈgɪv]"
  },
  {
    "engWord": "magazine",
    "rusWord": "журнал",
    "transcription": "[mægəˈziːn]"
  },
  {
    "engWord": "crap",
    "rusWord": "отстой",
    "transcription": "[kræp]"
  },
  {
    "engWord": "connect",
    "rusWord": "связываться",
    "transcription": "[kəˈnekt]"
  },
  {
    "engWord": "under",
    "rusWord": "под",
    "transcription": "[ˈʌndə]"
  },
  {
    "engWord": "lot",
    "rusWord": "много",
    "transcription": "[lɒt]"
  },
  {
    "engWord": "hair",
    "rusWord": "шерсть",
    "transcription": "[heə]"
  },
  {
    "engWord": "speak",
    "rusWord": "говорить",
    "transcription": "[spiːk]"
  },
  {
    "engWord": "painter",
    "rusWord": "художник",
    "transcription": "[ˈpeɪntə]"
  },
  {
    "engWord": "canal",
    "rusWord": "канал",
    "transcription": "[kəˈnæl]"
  },
  {
    "engWord": "slender",
    "rusWord": "стройный",
    "transcription": "[ˈslendə]"
  },
  {
    "engWord": "lit",
    "rusWord": "освещенный",
    "transcription": "[lɪt]"
  },
  {
    "engWord": "toy",
    "rusWord": "игрушка",
    "transcription": "[tɔɪ]"
  },
  {
    "engWord": "poet",
    "rusWord": "поэт",
    "transcription": "[ˈpəʊɪt]"
  },
  {
    "engWord": "outcome",
    "rusWord": "исход",
    "transcription": "[ˈaʊtkʌm]"
  },
  {
    "engWord": "greatest",
    "rusWord": "величайший",
    "transcription": "[ˈgreɪtɪst]"
  },
  {
    "engWord": "fry",
    "rusWord": "жарить",
    "transcription": "[fraɪ]"
  },
  {
    "engWord": "popular",
    "rusWord": "популярный",
    "transcription": "[ˈpɒpjʊlə]"
  },
  {
    "engWord": "guy",
    "rusWord": "парень",
    "transcription": "[gaɪ]"
  },
  {
    "engWord": "science",
    "rusWord": "наука",
    "transcription": "[ˈsaɪəns]"
  },
  {
    "engWord": "shape",
    "rusWord": "форма",
    "transcription": "[ʃeɪp]"
  },
  {
    "engWord": "wind",
    "rusWord": "ветер",
    "transcription": "[]"
  },
  {
    "engWord": "seaside",
    "rusWord": "побережье",
    "transcription": "[ˈsiːsaɪd]"
  },
  {
    "engWord": "suck",
    "rusWord": "посасывать",
    "transcription": "[sʌk]"
  },
  {
    "engWord": "complaint",
    "rusWord": "жалоба",
    "transcription": "[kəmˈpleɪnt]"
  },
  {
    "engWord": "passive",
    "rusWord": "пассивный",
    "transcription": "[ˈpæsɪv]"
  },
  {
    "engWord": "beginner",
    "rusWord": "начинающий",
    "transcription": "[bɪˈgɪnə]"
  },
  {
    "engWord": "gasp",
    "rusWord": "дышать с трудом",
    "transcription": "[gɑːsp]"
  },
  {
    "engWord": "fearful",
    "rusWord": "напуганный",
    "transcription": "[ˈfɪəf(ə)l]"
  },
  {
    "engWord": "mint",
    "rusWord": "мята",
    "transcription": "[mɪnt]"
  },
  {
    "engWord": "robbery",
    "rusWord": "грабеж",
    "transcription": "[ˈrɒbərɪ]"
  },
  {
    "engWord": "proportion",
    "rusWord": "пропорция",
    "transcription": "[prəˈpɔːʃn]"
  },
  {
    "engWord": "accommodation",
    "rusWord": "жилье",
    "transcription": "[əkɒməˈdeɪʃn]"
  },
  {
    "engWord": "advise",
    "rusWord": "советовать",
    "transcription": "[ədˈvaɪz]"
  },
  {
    "engWord": "loan",
    "rusWord": "кредит",
    "transcription": "[ləʊn]"
  },
  {
    "engWord": "delivery",
    "rusWord": "доставка",
    "transcription": "[dɪˈlɪvərɪ]"
  },
  {
    "engWord": "sonny",
    "rusWord": "сынок",
    "transcription": "[ˈsɒnɪ]"
  },
  {
    "engWord": "style",
    "rusWord": "стиль",
    "transcription": "[staɪl]"
  },
  {
    "engWord": "uncle",
    "rusWord": "дядя",
    "transcription": "[ʌŋkl]"
  },
  {
    "engWord": "up",
    "rusWord": "вверх",
    "transcription": "[ʌp]"
  },
  {
    "engWord": "heart",
    "rusWord": "сердце",
    "transcription": "[hɑːt]"
  },
  {
    "engWord": "island",
    "rusWord": "остров",
    "transcription": "[ˈaɪlənd]"
  },
  {
    "engWord": "nausea",
    "rusWord": "тошнота",
    "transcription": "[ˈnɔːzɪə]"
  },
  {
    "engWord": "broadcast",
    "rusWord": "телепередача",
    "transcription": "[ˈbrɒdkɑːst]"
  },
  {
    "engWord": "breed",
    "rusWord": "разводить",
    "transcription": "[briːd]"
  },
  {
    "engWord": "cannon",
    "rusWord": "пушка",
    "transcription": "[ˈkænən]"
  },
  {
    "engWord": "needle",
    "rusWord": "игла",
    "transcription": "[niːdl]"
  },
  {
    "engWord": "recipe",
    "rusWord": "рецепт",
    "transcription": "[ˈresɪpɪ]"
  },
  {
    "engWord": "fortunate",
    "rusWord": "удачливый",
    "transcription": "[ˈfɔːʧ(ə)nət]"
  },
  {
    "engWord": "agenda",
    "rusWord": "повестка дня",
    "transcription": "[əˈʤendə]"
  },
  {
    "engWord": "audience",
    "rusWord": "аудитория",
    "transcription": "[ˈɔːdɪəns]"
  },
  {
    "engWord": "argument",
    "rusWord": "аргумент",
    "transcription": "[ˈɑːgjʊmənt]"
  },
  {
    "engWord": "land",
    "rusWord": "земля",
    "transcription": "[lænd]"
  },
  {
    "engWord": "condition",
    "rusWord": "состояние",
    "transcription": "[kənˈdɪʃn]"
  },
  {
    "engWord": "lost",
    "rusWord": "потерянный",
    "transcription": "[lɒst]"
  },
  {
    "engWord": "eye",
    "rusWord": "глаз",
    "transcription": "[aɪ]"
  },
  {
    "engWord": "bank",
    "rusWord": "банк",
    "transcription": "[bæŋk]"
  },
  {
    "engWord": "chess",
    "rusWord": "шахматы",
    "transcription": "[ʧes]"
  },
  {
    "engWord": "blackmail",
    "rusWord": "шантаж",
    "transcription": "[ˈblækmeɪl]"
  },
  {
    "engWord": "rely",
    "rusWord": "полагаться",
    "transcription": "[rɪˈlaɪ]"
  },
  {
    "engWord": "frisk",
    "rusWord": "обыскивать",
    "transcription": "[frɪsk]"
  },
  {
    "engWord": "smash",
    "rusWord": "громить",
    "transcription": "[smæʃ]"
  },
  {
    "engWord": "upward",
    "rusWord": "вверх",
    "transcription": "[ˈʌpwəd]"
  },
  {
    "engWord": "wine",
    "rusWord": "вино",
    "transcription": "[waɪn]"
  },
  {
    "engWord": "champion",
    "rusWord": "чемпион",
    "transcription": "[ˈʧæmpɪən]"
  },
  {
    "engWord": "of course",
    "rusWord": "конечно",
    "transcription": "[ɒv kɔːs]"
  },
  {
    "engWord": "tax",
    "rusWord": "налог",
    "transcription": "[tæks]"
  },
  {
    "engWord": "split",
    "rusWord": "расщеплять",
    "transcription": "[splɪt]"
  },
  {
    "engWord": "bacon",
    "rusWord": "бекон",
    "transcription": "[ˈbeɪkən]"
  },
  {
    "engWord": "rude",
    "rusWord": "грубый",
    "transcription": "[ruːd]"
  },
  {
    "engWord": "or",
    "rusWord": "или",
    "transcription": "[ɔː]"
  },
  {
    "engWord": "force",
    "rusWord": "сила",
    "transcription": "[fɔːs]"
  },
  {
    "engWord": "tore",
    "rusWord": "Тор",
    "transcription": "[tɔː]"
  },
  {
    "engWord": "draft",
    "rusWord": "проект",
    "transcription": "[drɑːft]"
  },
  {
    "engWord": "miser",
    "rusWord": "жмот",
    "transcription": "[ˈmaɪzə]"
  },
  {
    "engWord": "decorate",
    "rusWord": "украшать",
    "transcription": "[ˈdekəreɪt]"
  },
  {
    "engWord": "granddad",
    "rusWord": "дедушка",
    "transcription": "[ˈgrændæd]"
  },
  {
    "engWord": "stir",
    "rusWord": "мешать",
    "transcription": "[stɜː]"
  },
  {
    "engWord": "heap",
    "rusWord": "куча",
    "transcription": "[hiːp]"
  },
  {
    "engWord": "ally",
    "rusWord": "союзник",
    "transcription": "[ˈælaɪ]"
  },
  {
    "engWord": "columnist",
    "rusWord": "обозреватель",
    "transcription": "[ˈkɒləmnɪst]"
  },
  {
    "engWord": "proclaim",
    "rusWord": "провозглашать",
    "transcription": "[prəˈkleɪm]"
  },
  {
    "engWord": "visible",
    "rusWord": "видимый",
    "transcription": "[ˈvɪzəbl]"
  },
  {
    "engWord": "demand",
    "rusWord": "требовать",
    "transcription": "[dɪˈmɑːnd]"
  },
  {
    "engWord": "virtue",
    "rusWord": "добродетель",
    "transcription": "[ˈvɜːʧuː]"
  },
  {
    "engWord": "determined",
    "rusWord": "определенный",
    "transcription": "[dɪˈtɜːmɪnd]"
  },
  {
    "engWord": "chat",
    "rusWord": "болтать",
    "transcription": "[ʧæt]"
  },
  {
    "engWord": "handle",
    "rusWord": "справляться",
    "transcription": "[hændl]"
  },
  {
    "engWord": "awake",
    "rusWord": "проснуться",
    "transcription": "[əˈweɪk]"
  },
  {
    "engWord": "miserable",
    "rusWord": "несчастный",
    "transcription": "[ˈmɪz(ə)rəb(ə)l]"
  },
  {
    "engWord": "tap",
    "rusWord": "кран",
    "transcription": "[tæp]"
  },
  {
    "engWord": "escape",
    "rusWord": "побег",
    "transcription": "[ɪsˈkeɪp]"
  },
  {
    "engWord": "club",
    "rusWord": "клуб",
    "transcription": "[klʌb]"
  },
  {
    "engWord": "river",
    "rusWord": "речной",
    "transcription": "[ˈrɪvə]"
  },
  {
    "engWord": "list",
    "rusWord": "список",
    "transcription": "[lɪst]"
  },
  {
    "engWord": "warrior",
    "rusWord": "воин",
    "transcription": "[ˈwɒrɪə]"
  },
  {
    "engWord": "challenge",
    "rusWord": "вызов",
    "transcription": "[ˈʧælɪnʤ]"
  },
  {
    "engWord": "drawer",
    "rusWord": "выдвижной ящик",
    "transcription": "[ˈdrɔːə]"
  },
  {
    "engWord": "eruption",
    "rusWord": "извержение",
    "transcription": "[ɪˈrʌpʃn]"
  },
  {
    "engWord": "attorney",
    "rusWord": "адвокат",
    "transcription": "[əˈtɜːnɪ]"
  },
  {
    "engWord": "limp",
    "rusWord": "хромать",
    "transcription": "[lɪmp]"
  },
  {
    "engWord": "suitcase",
    "rusWord": "чемодан",
    "transcription": "[ˈsjuːtkeɪs]"
  },
  {
    "engWord": "seize",
    "rusWord": "схватить",
    "transcription": "[siːz]"
  },
  {
    "engWord": "illness",
    "rusWord": "болезнь",
    "transcription": "[ˈɪlnɪs]"
  },
  {
    "engWord": "acceptable",
    "rusWord": "допустимый",
    "transcription": "[əkˈseptəbl]"
  },
  {
    "engWord": "wrap",
    "rusWord": "заворачивать",
    "transcription": "[ræp]"
  },
  {
    "engWord": "customer",
    "rusWord": "покупатель",
    "transcription": "[ˈkʌstəmə]"
  },
  {
    "engWord": "desire",
    "rusWord": "жаждать",
    "transcription": "[dɪˈzaɪə]"
  },
  {
    "engWord": "candidate",
    "rusWord": "кандидат",
    "transcription": "[ˈkændɪdət]"
  },
  {
    "engWord": "forget",
    "rusWord": "забывать",
    "transcription": "[fəˈget]"
  },
  {
    "engWord": "taxi",
    "rusWord": "такси",
    "transcription": "[ˈtæksɪ]"
  },
  {
    "engWord": "candy",
    "rusWord": "конфета",
    "transcription": "[ˈkændɪ]"
  },
  {
    "engWord": "ladder",
    "rusWord": "лестница",
    "transcription": "[ˈlædə]"
  },
  {
    "engWord": "plenty",
    "rusWord": "множество",
    "transcription": "[ˈplentɪ]"
  },
  {
    "engWord": "public",
    "rusWord": "общественный",
    "transcription": "[ˈpʌblɪk]"
  },
  {
    "engWord": "son",
    "rusWord": "сын",
    "transcription": "[sʌn]"
  },
  {
    "engWord": "year",
    "rusWord": "год",
    "transcription": "[jɪə]"
  },
  {
    "engWord": "hunt",
    "rusWord": "охота",
    "transcription": "[hʌnt]"
  },
  {
    "engWord": "trouble",
    "rusWord": "беда",
    "transcription": "[trʌbl]"
  },
  {
    "engWord": "obsession",
    "rusWord": "одержимость",
    "transcription": "[əbˈseʃn]"
  },
  {
    "engWord": "disappointment",
    "rusWord": "разочарование",
    "transcription": "[dɪsəˈpɔɪntmənt]"
  },
  {
    "engWord": "cargo",
    "rusWord": "груз",
    "transcription": "[ˈkɑːgəʊ]"
  },
  {
    "engWord": "being",
    "rusWord": "бытие",
    "transcription": "[ˈbiːɪŋ]"
  },
  {
    "engWord": "delegate",
    "rusWord": "делегат",
    "transcription": "[ˈdelɪgɪt]"
  },
  {
    "engWord": "approximately",
    "rusWord": "примерно",
    "transcription": "[əˈprɒksɪmɪtlɪ]"
  },
  {
    "engWord": "dam",
    "rusWord": "дамба",
    "transcription": "[dæm]"
  },
  {
    "engWord": "despite",
    "rusWord": "несмотря на",
    "transcription": "[dɪsˈpaɪt]"
  },
  {
    "engWord": "excellent",
    "rusWord": "отличный",
    "transcription": "[ˈeksələnt]"
  },
  {
    "engWord": "stuff",
    "rusWord": "материал",
    "transcription": "[stʌf]"
  },
  {
    "engWord": "respect",
    "rusWord": "уважение",
    "transcription": "[rɪsˈpekt]"
  },
  {
    "engWord": "focus",
    "rusWord": "сосредоточить",
    "transcription": "[ˈfəʊkəs]"
  },
  {
    "engWord": "no way",
    "rusWord": "не получится",
    "transcription": "[nəʊ weɪ]"
  },
  {
    "engWord": "lose",
    "rusWord": "терять",
    "transcription": "[luːz]"
  },
  {
    "engWord": "able",
    "rusWord": "способный",
    "transcription": "[eɪbl]"
  },
  {
    "engWord": "nor",
    "rusWord": "ни",
    "transcription": "[nɔː]"
  },
  {
    "engWord": "body",
    "rusWord": "тело",
    "transcription": "[ˈbɒdɪ]"
  },
  {
    "engWord": "wife",
    "rusWord": "жена",
    "transcription": "[waɪf]"
  },
  {
    "engWord": "original",
    "rusWord": "оригинал",
    "transcription": "[əˈrɪʤɪn(ə)l]"
  },
  {
    "engWord": "intimate",
    "rusWord": "глубокий",
    "transcription": "[ˈɪntɪmɪt]"
  },
  {
    "engWord": "audition",
    "rusWord": "прослушивание",
    "transcription": "[ɔːˈdɪʃn]"
  },
  {
    "engWord": "bedtime",
    "rusWord": "время сна",
    "transcription": "[ˈbedtaɪm]"
  },
  {
    "engWord": "orbit",
    "rusWord": "орбита",
    "transcription": "[ˈɔːbɪt]"
  },
  {
    "engWord": "seafood",
    "rusWord": "морепродукты",
    "transcription": "[ˈsiːfuːd]"
  },
  {
    "engWord": "speaker",
    "rusWord": "оратор",
    "transcription": "[ˈspiːkə]"
  },
  {
    "engWord": "landscape",
    "rusWord": "пейзаж",
    "transcription": "[ˈlændskeɪp]"
  },
  {
    "engWord": "dramatic",
    "rusWord": "драматичный",
    "transcription": "[drəˈmætɪk]"
  },
  {
    "engWord": "thumb",
    "rusWord": "большой палец",
    "transcription": "[θʌm]"
  },
  {
    "engWord": "twins",
    "rusWord": "близнецы",
    "transcription": "[twɪnz]"
  },
  {
    "engWord": "reserve",
    "rusWord": "резервный",
    "transcription": "[rɪˈzɜːv]"
  },
  {
    "engWord": "diet",
    "rusWord": "диета",
    "transcription": "[ˈdaɪət]"
  },
  {
    "engWord": "presumably",
    "rusWord": "предположительно",
    "transcription": "[prɪˈzjuːməblɪ]"
  },
  {
    "engWord": "unless",
    "rusWord": "если",
    "transcription": "[ənˈles]"
  },
  {
    "engWord": "interview",
    "rusWord": "интервью",
    "transcription": "[ˈɪntəvjuː]"
  },
  {
    "engWord": "desperate",
    "rusWord": "отчаянный",
    "transcription": "[ˈdespərɪt]"
  },
  {
    "engWord": "gold",
    "rusWord": "золото",
    "transcription": "[gəʊld]"
  },
  {
    "engWord": "flat",
    "rusWord": "квартира",
    "transcription": "[flæt]"
  },
  {
    "engWord": "century",
    "rusWord": "век",
    "transcription": "[ˈsenʧərɪ]"
  },
  {
    "engWord": "side",
    "rusWord": "сторона",
    "transcription": "[saɪd]"
  },
  {
    "engWord": "turn",
    "rusWord": "очередь",
    "transcription": "[tɜːn]"
  },
  {
    "engWord": "root",
    "rusWord": "корень",
    "transcription": "[ruːt]"
  },
  {
    "engWord": "few",
    "rusWord": "несколько",
    "transcription": "[fjuː]"
  },
  {
    "engWord": "mini",
    "rusWord": "мини",
    "transcription": "[ˈmɪnɪ]"
  },
  {
    "engWord": "rhythm",
    "rusWord": "ритм",
    "transcription": "[ˈrɪðəm]"
  },
  {
    "engWord": "slavery",
    "rusWord": "невольничество",
    "transcription": "[ˈsleɪvərɪ]"
  },
  {
    "engWord": "dated",
    "rusWord": "датированный",
    "transcription": "[ˈdeɪtɪd]"
  },
  {
    "engWord": "wandering",
    "rusWord": "блуждающий",
    "transcription": "[ˈwɒndərɪŋ]"
  },
  {
    "engWord": "nursery",
    "rusWord": "питомник",
    "transcription": "[ˈnɜːsərɪ]"
  },
  {
    "engWord": "mixture",
    "rusWord": "смесь",
    "transcription": "[ˈmɪksʧə]"
  },
  {
    "engWord": "collapse",
    "rusWord": "коллапс",
    "transcription": "[kəˈlæps]"
  },
  {
    "engWord": "priest",
    "rusWord": "священник",
    "transcription": "[priːst]"
  },
  {
    "engWord": "airport",
    "rusWord": "аэропорт",
    "transcription": "[ˈeəpɔːt]"
  },
  {
    "engWord": "butcher",
    "rusWord": "мясник",
    "transcription": "[ˈbʊʧə]"
  },
  {
    "engWord": "date",
    "rusWord": "дата",
    "transcription": "[deɪt]"
  },
  {
    "engWord": "told",
    "rusWord": "сказанный",
    "transcription": "[təʊld]"
  },
  {
    "engWord": "chew",
    "rusWord": "жевать",
    "transcription": "[ʧuː]"
  },
  {
    "engWord": "stare",
    "rusWord": "глазеть",
    "transcription": "[steə]"
  },
  {
    "engWord": "daytime",
    "rusWord": "дневное время",
    "transcription": "[ˈdeɪtaɪm]"
  },
  {
    "engWord": "moist",
    "rusWord": "влажный",
    "transcription": "[mɔɪst]"
  },
  {
    "engWord": "tub",
    "rusWord": "ванна",
    "transcription": "[tʌb]"
  },
  {
    "engWord": "coincidence",
    "rusWord": "совпадение",
    "transcription": "[kəʊˈɪnsɪdəns]"
  },
  {
    "engWord": "highly",
    "rusWord": "высоко",
    "transcription": "[ˈhaɪlɪ]"
  },
  {
    "engWord": "poster",
    "rusWord": "плакат",
    "transcription": "[ˈpəʊstə]"
  },
  {
    "engWord": "handy",
    "rusWord": "удобный",
    "transcription": "[ˈhændɪ]"
  },
  {
    "engWord": "noisy",
    "rusWord": "шумный",
    "transcription": "[ˈnɔɪzɪ]"
  },
  {
    "engWord": "boiler",
    "rusWord": "бойлерная",
    "transcription": "[ˈbɔɪlə]"
  },
  {
    "engWord": "parliament",
    "rusWord": "парламент",
    "transcription": "[ˈpɑːləmənt]"
  },
  {
    "engWord": "mostly",
    "rusWord": "в основном",
    "transcription": "[ˈməʊstlɪ]"
  },
  {
    "engWord": "patience",
    "rusWord": "терпение",
    "transcription": "[ˈpeɪʃns]"
  },
  {
    "engWord": "afford",
    "rusWord": "позволить себе",
    "transcription": "[əˈfɔːd]"
  },
  {
    "engWord": "unit",
    "rusWord": "единица измерения",
    "transcription": "[ˈjuːnɪt]"
  },
  {
    "engWord": "wood",
    "rusWord": "дерево",
    "transcription": "[wʊd]"
  },
  {
    "engWord": "spill",
    "rusWord": "проливать",
    "transcription": "[spɪl]"
  },
  {
    "engWord": "part-time",
    "rusWord": "неполный рабочий день",
    "transcription": "[pɑːt taɪm]"
  },
  {
    "engWord": "honoured",
    "rusWord": "заслуженный",
    "transcription": "[ˈɒnəd]"
  },
  {
    "engWord": "inward",
    "rusWord": "внутренне",
    "transcription": "[ˈɪnwəd]"
  },
  {
    "engWord": "dependent",
    "rusWord": "зависимый",
    "transcription": "[dɪˈpendənt]"
  },
  {
    "engWord": "fifth",
    "rusWord": "пятый",
    "transcription": "[fɪfθ]"
  },
  {
    "engWord": "alley",
    "rusWord": "аллея",
    "transcription": "[ˈælɪ]"
  },
  {
    "engWord": "sixty",
    "rusWord": "шестьдесят",
    "transcription": "[ˈsɪkstɪ]"
  },
  {
    "engWord": "wreck",
    "rusWord": "развалина",
    "transcription": "[rek]"
  },
  {
    "engWord": "slight",
    "rusWord": "незначительный",
    "transcription": "[slaɪt]"
  },
  {
    "engWord": "growing",
    "rusWord": "растущий",
    "transcription": "[ˈgrəʊɪŋ]"
  },
  {
    "engWord": "tape",
    "rusWord": "лента",
    "transcription": "[teɪp]"
  },
  {
    "engWord": "autumn",
    "rusWord": "осень",
    "transcription": "[ˈɔːtəm]"
  },
  {
    "engWord": "northern",
    "rusWord": "северный",
    "transcription": "[ˈnɔːðən]"
  },
  {
    "engWord": "radio",
    "rusWord": "радио",
    "transcription": "[ˈreɪdɪəʊ]"
  },
  {
    "engWord": "way",
    "rusWord": "путь",
    "transcription": "[weɪ]"
  },
  {
    "engWord": "heat",
    "rusWord": "жара",
    "transcription": "[hiːt]"
  },
  {
    "engWord": "table",
    "rusWord": "стол",
    "transcription": "[teɪbl]"
  },
  {
    "engWord": "on",
    "rusWord": "на",
    "transcription": "[ɒn]"
  },
  {
    "engWord": "forest",
    "rusWord": "лес",
    "transcription": "[ˈfɒrɪst]"
  },
  {
    "engWord": "stream",
    "rusWord": "поток",
    "transcription": "[striːm]"
  },
  {
    "engWord": "tiny",
    "rusWord": "крошечный",
    "transcription": "[ˈtaɪnɪ]"
  },
  {
    "engWord": "industrial",
    "rusWord": "промышленный",
    "transcription": "[ɪnˈdʌstrɪəl]"
  },
  {
    "engWord": "cock",
    "rusWord": "петух",
    "transcription": "[kɒk]"
  },
  {
    "engWord": "motorcycle",
    "rusWord": "мотоцикл",
    "transcription": "[ˈməʊtəsaɪk(ə)l]"
  },
  {
    "engWord": "colonist",
    "rusWord": "колонист",
    "transcription": "[ˈkɒlənɪst]"
  },
  {
    "engWord": "stink",
    "rusWord": "вонь",
    "transcription": "[stɪŋk]"
  },
  {
    "engWord": "junction",
    "rusWord": "соединение",
    "transcription": "[ʤʌŋkʃn]"
  },
  {
    "engWord": "regulation",
    "rusWord": "регулирование",
    "transcription": "[regjʊˈleɪʃn]"
  },
  {
    "engWord": "uneasy",
    "rusWord": "беспокойный",
    "transcription": "[ʌnˈiːzɪ]"
  },
  {
    "engWord": "twin",
    "rusWord": "близнец",
    "transcription": "[twɪn]"
  },
  {
    "engWord": "inviting",
    "rusWord": "приглашающий",
    "transcription": "[ɪnˈvaɪtɪŋ]"
  },
  {
    "engWord": "margarine",
    "rusWord": "маргарин",
    "transcription": "[mɑːʤəˈriːn]"
  },
  {
    "engWord": "prompt",
    "rusWord": "срочный",
    "transcription": "[prɒmpt]"
  },
  {
    "engWord": "staircase",
    "rusWord": "лестница",
    "transcription": "[ˈsteəkeɪs]"
  },
  {
    "engWord": "crack",
    "rusWord": "трещина",
    "transcription": "[kræk]"
  },
  {
    "engWord": "else",
    "rusWord": "еще",
    "transcription": "[els]"
  },
  {
    "engWord": "fun",
    "rusWord": "веселье",
    "transcription": "[fʌn]"
  },
  {
    "engWord": "worthy",
    "rusWord": "достойный",
    "transcription": "[ˈwɜːðɪ]"
  },
  {
    "engWord": "scarlet",
    "rusWord": "алый",
    "transcription": "[ˈskɑːlɪt]"
  },
  {
    "engWord": "resistance",
    "rusWord": "сопротивление",
    "transcription": "[rɪˈzɪstəns]"
  },
  {
    "engWord": "punk",
    "rusWord": "панк",
    "transcription": "[pʌŋk]"
  },
  {
    "engWord": "tulip",
    "rusWord": "тюльпан",
    "transcription": "[ˈtjuːlɪp]"
  },
  {
    "engWord": "tropical",
    "rusWord": "тропический",
    "transcription": "[ˈtrɒpɪkəl]"
  },
  {
    "engWord": "divorced",
    "rusWord": "разведенный",
    "transcription": "[dɪˈvɔːst]"
  },
  {
    "engWord": "rat",
    "rusWord": "крыса",
    "transcription": "[ræt]"
  },
  {
    "engWord": "pack",
    "rusWord": "упаковка",
    "transcription": "[pæk]"
  },
  {
    "engWord": "official",
    "rusWord": "официальный",
    "transcription": "[əˈfɪʃəl]"
  },
  {
    "engWord": "everyone",
    "rusWord": "каждый",
    "transcription": "[ˈevrɪwʌn]"
  },
  {
    "engWord": "beg",
    "rusWord": "умолять",
    "transcription": "[beg]"
  },
  {
    "engWord": "sexual",
    "rusWord": "интимный",
    "transcription": "[ˈsɛkʃuəl]"
  },
  {
    "engWord": "drag",
    "rusWord": "тащить",
    "transcription": "[dræg]"
  },
  {
    "engWord": "wonder",
    "rusWord": "удивляться",
    "transcription": "[ˈwʌndə]"
  },
  {
    "engWord": "home",
    "rusWord": "домашний",
    "transcription": "[həʊm]"
  },
  {
    "engWord": "summer",
    "rusWord": "лето",
    "transcription": "[ˈsʌmə]"
  },
  {
    "engWord": "seventh",
    "rusWord": "седьмой",
    "transcription": "[sevnθ]"
  },
  {
    "engWord": "rigid",
    "rusWord": "жесткий",
    "transcription": "[ˈrɪʤɪd]"
  },
  {
    "engWord": "liking",
    "rusWord": "симпатия",
    "transcription": "[ˈlaɪkɪŋ]"
  },
  {
    "engWord": "goat",
    "rusWord": "коза",
    "transcription": "[gəʊt]"
  },
  {
    "engWord": "exhibit",
    "rusWord": "экспонат",
    "transcription": "[ɪgˈzɪbɪt]"
  },
  {
    "engWord": "parrot",
    "rusWord": "попугай",
    "transcription": "[ˈpærət]"
  },
  {
    "engWord": "utility",
    "rusWord": "полезность",
    "transcription": "[juːˈtɪlɪtɪ]"
  },
  {
    "engWord": "ham",
    "rusWord": "ветчина",
    "transcription": "[hæm]"
  },
  {
    "engWord": "robust",
    "rusWord": "крепкий",
    "transcription": "[rəˈbʌst]"
  },
  {
    "engWord": "fright",
    "rusWord": "испуг",
    "transcription": "[fraɪt]"
  },
  {
    "engWord": "calendar",
    "rusWord": "календарь",
    "transcription": "[ˈkælɪndə]"
  },
  {
    "engWord": "honorary",
    "rusWord": "почетный",
    "transcription": "[ˈɒnərərɪ]"
  },
  {
    "engWord": "priority",
    "rusWord": "приоритет",
    "transcription": "[praɪˈɒrɪtɪ]"
  },
  {
    "engWord": "innovation",
    "rusWord": "инновация",
    "transcription": "[ɪnəˈveɪʃn]"
  },
  {
    "engWord": "piano",
    "rusWord": "пианино",
    "transcription": "[pɪˈænəʊ]"
  },
  {
    "engWord": "tradition",
    "rusWord": "традиция",
    "transcription": "[trəˈdɪʃn]"
  },
  {
    "engWord": "anyone",
    "rusWord": "никто",
    "transcription": "[ˈenɪwʌn]"
  },
  {
    "engWord": "bury",
    "rusWord": "закапывать",
    "transcription": "[ˈberɪ]"
  },
  {
    "engWord": "dawn",
    "rusWord": "рассвет",
    "transcription": "[dɔːn]"
  },
  {
    "engWord": "hung",
    "rusWord": "подвешенный",
    "transcription": "[hʌŋ]"
  },
  {
    "engWord": "terribly",
    "rusWord": "ужасно",
    "transcription": "[ˈterəblɪ]"
  },
  {
    "engWord": "eighteen",
    "rusWord": "восемнадцать",
    "transcription": "[eɪˈtiːn]"
  },
  {
    "engWord": "break",
    "rusWord": "ломать",
    "transcription": "[breɪk]"
  },
  {
    "engWord": "lead",
    "rusWord": "вести",
    "transcription": "[]"
  },
  {
    "engWord": "cool",
    "rusWord": "крутой",
    "transcription": "[kuːl]"
  },
  {
    "engWord": "crowd",
    "rusWord": "толпа",
    "transcription": "[kraʊd]"
  },
  {
    "engWord": "wicked",
    "rusWord": "злой",
    "transcription": "[ˈwɪkɪd]"
  },
  {
    "engWord": "cartoon",
    "rusWord": "мультфильм",
    "transcription": "[kɑːˈtuːn]"
  },
  {
    "engWord": "contingent",
    "rusWord": "случайный",
    "transcription": "[kənˈtɪnʤənt]"
  },
  {
    "engWord": "parenting",
    "rusWord": "воспитание",
    "transcription": "[ˈpærəntɪŋ]"
  },
  {
    "engWord": "religious",
    "rusWord": "религиозный",
    "transcription": "[rɪˈlɪʤəs]"
  },
  {
    "engWord": "forgiveness",
    "rusWord": "прощение",
    "transcription": "[fəˈgɪvnɪs]"
  },
  {
    "engWord": "skier",
    "rusWord": "лыжник",
    "transcription": "[ˈskiːə]"
  },
  {
    "engWord": "rubbish",
    "rusWord": "мусор",
    "transcription": "[ˈrʌbɪʃ]"
  },
  {
    "engWord": "attached",
    "rusWord": "прикрепленный",
    "transcription": "[əˈtæʧt]"
  },
  {
    "engWord": "option",
    "rusWord": "вариант",
    "transcription": "[ɒpʃn]"
  },
  {
    "engWord": "chap",
    "rusWord": "парень",
    "transcription": "[ʧæp]"
  },
  {
    "engWord": "pin",
    "rusWord": "штифт",
    "transcription": "[pɪn]"
  },
  {
    "engWord": "clever",
    "rusWord": "умный",
    "transcription": "[ˈklevə]"
  },
  {
    "engWord": "remind",
    "rusWord": "напоминать",
    "transcription": "[ˈrɪmaɪnd]"
  },
  {
    "engWord": "net",
    "rusWord": "сеть",
    "transcription": "[net]"
  },
  {
    "engWord": "pool",
    "rusWord": "бассейн",
    "transcription": "[puːl]"
  },
  {
    "engWord": "actually",
    "rusWord": "на самом деле",
    "transcription": "[ˈækʧʊəlɪ]"
  },
  {
    "engWord": "nine",
    "rusWord": "девять",
    "transcription": "[naɪn]"
  },
  {
    "engWord": "figure",
    "rusWord": "фигура",
    "transcription": "[ˈfɪgə]"
  },
  {
    "engWord": "early",
    "rusWord": "ранний",
    "transcription": "[ˈɜːlɪ]"
  },
  {
    "engWord": "blood",
    "rusWord": "кровь",
    "transcription": "[blʌd]"
  },
  {
    "engWord": "telegraph",
    "rusWord": "Телеграф",
    "transcription": "[ˈtelɪgrɑːf]"
  },
  {
    "engWord": "medical",
    "rusWord": "медицинский",
    "transcription": "[ˈmedɪkəl]"
  },
  {
    "engWord": "artistic",
    "rusWord": "художественный",
    "transcription": "[ɑːˈtɪstɪk]"
  },
  {
    "engWord": "vacancy",
    "rusWord": "вакансия",
    "transcription": "[ˈveɪkənsɪ]"
  },
  {
    "engWord": "sympathy",
    "rusWord": "симпатия",
    "transcription": "[ˈsɪmpəθɪ]"
  },
  {
    "engWord": "walker",
    "rusWord": "ходок",
    "transcription": "[ˈwɔːkə]"
  },
  {
    "engWord": "historical",
    "rusWord": "исторический",
    "transcription": "[hɪsˈtɒrɪkəl]"
  },
  {
    "engWord": "virtually",
    "rusWord": "практически",
    "transcription": "[ˈvɜːʧʊəlɪ]"
  },
  {
    "engWord": "lock",
    "rusWord": "замок",
    "transcription": "[lɒk]"
  },
  {
    "engWord": "laboratory",
    "rusWord": "лаборатория",
    "transcription": "[ləˈbɒrətrɪ]"
  },
  {
    "engWord": "hug",
    "rusWord": "обнимать",
    "transcription": "[hʌg]"
  },
  {
    "engWord": "policeman",
    "rusWord": "полицейский",
    "transcription": "[pəˈliːsmən]"
  },
  {
    "engWord": "steal",
    "rusWord": "красть",
    "transcription": "[stiːl]"
  },
  {
    "engWord": "wage",
    "rusWord": "заработная плата",
    "transcription": "[weɪʤ]"
  },
  {
    "engWord": "buy",
    "rusWord": "покупать",
    "transcription": "[baɪ]"
  },
  {
    "engWord": "please",
    "rusWord": "пожалуйста",
    "transcription": "[pliːz]"
  },
  {
    "engWord": "beauty",
    "rusWord": "красота",
    "transcription": "[ˈbjuːtɪ]"
  },
  {
    "engWord": "mercy",
    "rusWord": "милость",
    "transcription": "[ˈmɜːsɪ]"
  },
  {
    "engWord": "tile",
    "rusWord": "плитка",
    "transcription": "[taɪl]"
  },
  {
    "engWord": "glow",
    "rusWord": "светиться",
    "transcription": "[gləʊ]"
  },
  {
    "engWord": "exam",
    "rusWord": "экзамен",
    "transcription": "[ɪgˈzæm]"
  },
  {
    "engWord": "groan",
    "rusWord": "стон",
    "transcription": "[]"
  },
  {
    "engWord": "scotch",
    "rusWord": "скотч",
    "transcription": "[skɒʧ]"
  },
  {
    "engWord": "pronunciation",
    "rusWord": "произношение",
    "transcription": "[prənʌnsɪˈeɪʃn]"
  },
  {
    "engWord": "lad",
    "rusWord": "парень",
    "transcription": "[læd]"
  },
  {
    "engWord": "employer",
    "rusWord": "работодатель",
    "transcription": "[ɪmˈplɔɪə]"
  },
  {
    "engWord": "suspicion",
    "rusWord": "подозрение",
    "transcription": "[]"
  },
  {
    "engWord": "sufficient",
    "rusWord": "достаточный",
    "transcription": "[səˈfɪʃnt]"
  },
  {
    "engWord": "gallery",
    "rusWord": "галерея",
    "transcription": "[ˈgælərɪ]"
  },
  {
    "engWord": "painting",
    "rusWord": "рисующий",
    "transcription": "[ˈpeɪntɪŋ]"
  },
  {
    "engWord": "tablet",
    "rusWord": "таблетка",
    "transcription": "[ˈtæblɪt]"
  },
  {
    "engWord": "advice",
    "rusWord": "консультация",
    "transcription": "[ədˈvaɪs]"
  },
  {
    "engWord": "heavy",
    "rusWord": "тяжелый",
    "transcription": "[ˈhevɪ]"
  },
  {
    "engWord": "vary",
    "rusWord": "различаться",
    "transcription": "[ˈve(ə)rɪ]"
  },
  {
    "engWord": "hurry",
    "rusWord": "торопиться",
    "transcription": "[ˈhʌrɪ]"
  },
  {
    "engWord": "too",
    "rusWord": "тоже",
    "transcription": "[tuː]"
  },
  {
    "engWord": "haircut",
    "rusWord": "стрижка",
    "transcription": "[ˈheəkʌt]"
  },
  {
    "engWord": "wagon",
    "rusWord": "вагон",
    "transcription": "[ˈwægən]"
  },
  {
    "engWord": "equipment",
    "rusWord": "оборудование",
    "transcription": "[ɪˈkwɪpmənt]"
  },
  {
    "engWord": "delight",
    "rusWord": "восторг",
    "transcription": "[dɪˈlaɪt]"
  },
  {
    "engWord": "integral",
    "rusWord": "неотъемлемый",
    "transcription": "[ˈɪntɪgrəl]"
  },
  {
    "engWord": "dentist",
    "rusWord": "зубной врач",
    "transcription": "[ˈdentɪst]"
  },
  {
    "engWord": "cup",
    "rusWord": "чашка",
    "transcription": "[kʌp]"
  },
  {
    "engWord": "peg",
    "rusWord": "вешалка",
    "transcription": "[peg]"
  },
  {
    "engWord": "investigation",
    "rusWord": "расследование",
    "transcription": "[ɪnvestɪˈgeɪʃn]"
  },
  {
    "engWord": "by",
    "rusWord": "по",
    "transcription": "[baɪ]"
  },
  {
    "engWord": "I",
    "rusWord": "я",
    "transcription": "[aɪ]"
  },
  {
    "engWord": "sixth",
    "rusWord": "шестой",
    "transcription": "[sɪksθ]"
  },
  {
    "engWord": "elbow",
    "rusWord": "локоть",
    "transcription": "[ˈelbəʊ]"
  },
  {
    "engWord": "verify",
    "rusWord": "проверить",
    "transcription": "[ˈverɪfaɪ]"
  },
  {
    "engWord": "straighten",
    "rusWord": "распрямить",
    "transcription": "[streɪtn]"
  },
  {
    "engWord": "avail",
    "rusWord": "выгода",
    "transcription": "[əˈveɪl]"
  },
  {
    "engWord": "deliberate",
    "rusWord": "преднамеренный",
    "transcription": "[dɪˈlɪbərɪt]"
  },
  {
    "engWord": "hidden",
    "rusWord": "скрытый",
    "transcription": "[hɪdn]"
  },
  {
    "engWord": "prosecution",
    "rusWord": "обвинение",
    "transcription": "[prɒsɪˈkjuːʃn]"
  },
  {
    "engWord": "barn",
    "rusWord": "сарай",
    "transcription": "[bɑːn]"
  },
  {
    "engWord": "darkness",
    "rusWord": "темнота",
    "transcription": "[ˈdɑːknɪs]"
  },
  {
    "engWord": "organize",
    "rusWord": "организовать",
    "transcription": "[ˈɔːgənaɪz]"
  },
  {
    "engWord": "image",
    "rusWord": "изображение",
    "transcription": "[ˈɪmɪʤ]"
  },
  {
    "engWord": "Europe",
    "rusWord": "Европа",
    "transcription": "[ˈjʊərəp]"
  },
  {
    "engWord": "involve",
    "rusWord": "вовлекать",
    "transcription": "[ɪnˈvɒlv]"
  },
  {
    "engWord": "angel",
    "rusWord": "ангел",
    "transcription": "[ˈeɪnʤəl]"
  },
  {
    "engWord": "door",
    "rusWord": "дверь",
    "transcription": "[dɔː]"
  },
  {
    "engWord": "your",
    "rusWord": "твой",
    "transcription": "[jɔː]"
  },
  {
    "engWord": "appearance",
    "rusWord": "внешний вид",
    "transcription": "[əˈpɪərəns]"
  },
  {
    "engWord": "blackmailing",
    "rusWord": "шантажирующий",
    "transcription": "[ˈblækmeɪlɪŋ]"
  },
  {
    "engWord": "regardless",
    "rusWord": "несмотря на",
    "transcription": "[rɪˈgɑːdlɪs]"
  },
  {
    "engWord": "cemetery",
    "rusWord": "кладбище",
    "transcription": "[ˈsemɪtrɪ]"
  },
  {
    "engWord": "desperately",
    "rusWord": "отчаянно",
    "transcription": "[ˈdesp(ə)rɪtlɪ]"
  },
  {
    "engWord": "homeless",
    "rusWord": "бездомный",
    "transcription": "[ˈhəʊmlɪs]"
  },
  {
    "engWord": "coffeehouse",
    "rusWord": "кафе",
    "transcription": "[ˈkɔfɪhaʊs]"
  },
  {
    "engWord": "April",
    "rusWord": "апрель",
    "transcription": "[ˈeɪprəl]"
  },
  {
    "engWord": "mid",
    "rusWord": "средний",
    "transcription": "[mɪd]"
  },
  {
    "engWord": "fatal",
    "rusWord": "роковой",
    "transcription": "[feɪtl]"
  },
  {
    "engWord": "sarcasm",
    "rusWord": "сарказм",
    "transcription": "[ˈsɑːkæzm]"
  },
  {
    "engWord": "pregnancy",
    "rusWord": "беременность",
    "transcription": "[ˈpregnənsɪ]"
  },
  {
    "engWord": "clay",
    "rusWord": "глина",
    "transcription": "[kleɪ]"
  },
  {
    "engWord": "maid",
    "rusWord": "прислуга",
    "transcription": "[meɪd]"
  },
  {
    "engWord": "lip",
    "rusWord": "губа",
    "transcription": "[lɪp]"
  },
  {
    "engWord": "unfair",
    "rusWord": "несправедливый",
    "transcription": "[ʌnˈfeə]"
  },
  {
    "engWord": "appointment",
    "rusWord": "назначение",
    "transcription": "[əˈpɔɪntmənt]"
  },
  {
    "engWord": "friendly",
    "rusWord": "дружелюбный",
    "transcription": "[ˈfrendlɪ]"
  },
  {
    "engWord": "witness",
    "rusWord": "свидетель",
    "transcription": "[]"
  },
  {
    "engWord": "now",
    "rusWord": "сейчас",
    "transcription": "[naʊ]"
  },
  {
    "engWord": "operate",
    "rusWord": "работать",
    "transcription": "[ˈɒpəreɪt]"
  },
  {
    "engWord": "base",
    "rusWord": "база",
    "transcription": "[beɪs]"
  },
  {
    "engWord": "grand",
    "rusWord": "великий",
    "transcription": "[grænd]"
  },
  {
    "engWord": "accessible",
    "rusWord": "доступный",
    "transcription": "[əkˈsesəbl]"
  },
  {
    "engWord": "disobedience",
    "rusWord": "неповиновение",
    "transcription": "[dɪsəˈbiːdjəns]"
  },
  {
    "engWord": "supposedly",
    "rusWord": "предположительно",
    "transcription": "[səˈpəʊzɪdlɪ]"
  },
  {
    "engWord": "chimney",
    "rusWord": "дымоход",
    "transcription": "[ˈʧɪmnɪ]"
  },
  {
    "engWord": "stem",
    "rusWord": "стебель",
    "transcription": "[stem]"
  },
  {
    "engWord": "erupt",
    "rusWord": "извергаться",
    "transcription": "[ɪˈrʌpt]"
  },
  {
    "engWord": "facility",
    "rusWord": "объект",
    "transcription": "[fəˈsɪlɪtɪ]"
  },
  {
    "engWord": "agency",
    "rusWord": "агентство",
    "transcription": "[ˈeɪʤənsɪ]"
  },
  {
    "engWord": "feeling",
    "rusWord": "чувство",
    "transcription": "[ˈfiːlɪŋ]"
  },
  {
    "engWord": "global",
    "rusWord": "глобальный",
    "transcription": "[ˈgləʊbəl]"
  },
  {
    "engWord": "artist",
    "rusWord": "художник",
    "transcription": "[ˈɑːtɪst]"
  },
  {
    "engWord": "tunnel",
    "rusWord": "туннель",
    "transcription": "[tʌnl]"
  },
  {
    "engWord": "cage",
    "rusWord": "клетка",
    "transcription": "[keɪʤ]"
  },
  {
    "engWord": "scratch",
    "rusWord": "царапина",
    "transcription": "[skræʧ]"
  },
  {
    "engWord": "observation",
    "rusWord": "наблюдение",
    "transcription": "[ɒbzəˈveɪʃn]"
  },
  {
    "engWord": "wander",
    "rusWord": "бродить",
    "transcription": "[ˈwɒndə]"
  },
  {
    "engWord": "scare",
    "rusWord": "пугать",
    "transcription": "[skeə]"
  },
  {
    "engWord": "upset",
    "rusWord": "расстроенный",
    "transcription": "[ʌpˈset]"
  },
  {
    "engWord": "brain",
    "rusWord": "мозг",
    "transcription": "[breɪn]"
  },
  {
    "engWord": "famous",
    "rusWord": "знаменитый",
    "transcription": "[ˈfeɪməs]"
  },
  {
    "engWord": "ice",
    "rusWord": "лед",
    "transcription": "[aɪs]"
  },
  {
    "engWord": "once",
    "rusWord": "некогда",
    "transcription": "[wʌns]"
  },
  {
    "engWord": "measure",
    "rusWord": "мера",
    "transcription": "[ˈmeʒə]"
  },
  {
    "engWord": "nature",
    "rusWord": "природа",
    "transcription": "[ˈneɪʧə]"
  },
  {
    "engWord": "soil",
    "rusWord": "почва",
    "transcription": "[sɔɪl]"
  },
  {
    "engWord": "comb",
    "rusWord": "гребень",
    "transcription": "[kəʊm]"
  },
  {
    "engWord": "donkey",
    "rusWord": "ослик",
    "transcription": "[ˈdɒŋkɪ]"
  },
  {
    "engWord": "distracted",
    "rusWord": "рассеянный",
    "transcription": "[dɪsˈtræktɪd]"
  },
  {
    "engWord": "restrain",
    "rusWord": "сдерживать",
    "transcription": "[rɪsˈtreɪn]"
  },
  {
    "engWord": "favourable",
    "rusWord": "благоприятный",
    "transcription": "[ˈfeɪv(ə)rəb(ə)l]"
  },
  {
    "engWord": "modeling",
    "rusWord": "моделирование",
    "transcription": "[ˈmɔdlɪŋ]"
  },
  {
    "engWord": "somewhere",
    "rusWord": "где-то",
    "transcription": "[ˈsʌmweə]"
  },
  {
    "engWord": "surround",
    "rusWord": "окружать",
    "transcription": "[səˈraʊnd]"
  },
  {
    "engWord": "alarm",
    "rusWord": "тревога",
    "transcription": "[əˈlɑːm]"
  },
  {
    "engWord": "dirty",
    "rusWord": "грязный",
    "transcription": "[ˈdɜːtɪ]"
  },
  {
    "engWord": "reach",
    "rusWord": "достичь",
    "transcription": "[riːʧ]"
  },
  {
    "engWord": "how",
    "rusWord": "как",
    "transcription": "[haʊ]"
  },
  {
    "engWord": "ocean",
    "rusWord": "океан",
    "transcription": "[əʊʃn]"
  },
  {
    "engWord": "represent",
    "rusWord": "представлять",
    "transcription": "[reprɪˈzent]"
  },
  {
    "engWord": "aesthetic",
    "rusWord": "эстетический",
    "transcription": "[iːsˈθetɪk]"
  },
  {
    "engWord": "popcorn",
    "rusWord": "попкорн",
    "transcription": "[ˈpɒpkɔːn]"
  },
  {
    "engWord": "recognition",
    "rusWord": "распознавание",
    "transcription": "[riːekəgˈnɪʃn]"
  },
  {
    "engWord": "attain",
    "rusWord": "достигать",
    "transcription": "[əˈteɪn]"
  },
  {
    "engWord": "theme",
    "rusWord": "тема",
    "transcription": "[θiːm]"
  },
  {
    "engWord": "gaze",
    "rusWord": "пристальный взгляд",
    "transcription": "[geɪz]"
  },
  {
    "engWord": "smith",
    "rusWord": "кузнец",
    "transcription": "[smɪθ]"
  },
  {
    "engWord": "mathematics",
    "rusWord": "математика",
    "transcription": "[mæθɪˈmætɪks]"
  },
  {
    "engWord": "lyrical",
    "rusWord": "лирический",
    "transcription": "[ˈlɪrɪkəl]"
  },
  {
    "engWord": "parole",
    "rusWord": "честное слово",
    "transcription": "[pəˈrəʊl]"
  },
  {
    "engWord": "secure",
    "rusWord": "безопасный",
    "transcription": "[sɪˈkjʊə]"
  },
  {
    "engWord": "pipe",
    "rusWord": "труба",
    "transcription": "[paɪp]"
  },
  {
    "engWord": "blast",
    "rusWord": "взрыв",
    "transcription": "[blɑːst]"
  },
  {
    "engWord": "recommend",
    "rusWord": "рекомендовать",
    "transcription": "[rekəˈmend]"
  },
  {
    "engWord": "realm",
    "rusWord": "область",
    "transcription": "[relm]"
  },
  {
    "engWord": "jury",
    "rusWord": "присяжные",
    "transcription": "[ˈʤʊərɪ]"
  },
  {
    "engWord": "flow",
    "rusWord": "поток",
    "transcription": "[fləʊ]"
  },
  {
    "engWord": "arrive",
    "rusWord": "прибывать",
    "transcription": "[əˈraɪv]"
  },
  {
    "engWord": "molecule",
    "rusWord": "молекула",
    "transcription": "[ˈmɒlɪkjuːl]"
  },
  {
    "engWord": "season",
    "rusWord": "сезон",
    "transcription": "[siːzn]"
  },
  {
    "engWord": "descend",
    "rusWord": "спускаться",
    "transcription": "[dɪˈsend]"
  },
  {
    "engWord": "deed",
    "rusWord": "поступок",
    "transcription": "[diːd]"
  },
  {
    "engWord": "realise",
    "rusWord": "понимать",
    "transcription": "[ˈrɪəlaɪz]"
  },
  {
    "engWord": "metric",
    "rusWord": "метрический",
    "transcription": "[ˈmetrɪk]"
  },
  {
    "engWord": "scholarship",
    "rusWord": "ученость",
    "transcription": "[ˈskɒləʃɪp]"
  },
  {
    "engWord": "artificial",
    "rusWord": "искусственный",
    "transcription": "[ɑːtɪˈfɪʃəl]"
  },
  {
    "engWord": "nineteen",
    "rusWord": "девятнадцать",
    "transcription": "[naɪnˈtiːn]"
  },
  {
    "engWord": "tick",
    "rusWord": "галочка",
    "transcription": "[tɪk]"
  },
  {
    "engWord": "violence",
    "rusWord": "насилие",
    "transcription": "[ˈvaɪələns]"
  },
  {
    "engWord": "painful",
    "rusWord": "болезненный",
    "transcription": "[ˈpeɪnf(ə)l]"
  },
  {
    "engWord": "burger",
    "rusWord": "бутерброд",
    "transcription": "[ˈbɜːgə]"
  },
  {
    "engWord": "suppose",
    "rusWord": "предполагать",
    "transcription": "[səˈpəʊz]"
  },
  {
    "engWord": "loss",
    "rusWord": "потеря",
    "transcription": "[lɒs]"
  },
  {
    "engWord": "easily",
    "rusWord": "легко",
    "transcription": "[ˈiːzɪlɪ]"
  },
  {
    "engWord": "worrying",
    "rusWord": "беспокоящий",
    "transcription": "[ˈwʌrijɪŋ]"
  },
  {
    "engWord": "special",
    "rusWord": "специальный",
    "transcription": "[ˈspeʃəl]"
  },
  {
    "engWord": "job",
    "rusWord": "работа",
    "transcription": "[ʤəʊb]"
  },
  {
    "engWord": "organ",
    "rusWord": "орган",
    "transcription": "[ˈɔːgən]"
  },
  {
    "engWord": "match",
    "rusWord": "спичка",
    "transcription": "[mæʧ]"
  },
  {
    "engWord": "saying",
    "rusWord": "поговорка",
    "transcription": "[ˈseɪɪŋ]"
  },
  {
    "engWord": "relevant",
    "rusWord": "подходящий",
    "transcription": "[ˈrelɪvənt]"
  },
  {
    "engWord": "lively",
    "rusWord": "оживленный",
    "transcription": "[ˈlaɪvlɪ]"
  },
  {
    "engWord": "notify",
    "rusWord": "уведомлять",
    "transcription": "[ˈnəʊtɪfaɪ]"
  },
  {
    "engWord": "coffin",
    "rusWord": "гроб",
    "transcription": "[ˈkɒfɪn]"
  },
  {
    "engWord": "stroke",
    "rusWord": "ход",
    "transcription": "[strəʊk]"
  },
  {
    "engWord": "armed",
    "rusWord": "вооруженный",
    "transcription": "[ɑːmd]"
  },
  {
    "engWord": "widely",
    "rusWord": "широко",
    "transcription": "[ˈwaɪdlɪ]"
  },
  {
    "engWord": "latest",
    "rusWord": "самый последний",
    "transcription": "[ˈleɪtɪst]"
  },
  {
    "engWord": "beside",
    "rusWord": "рядом",
    "transcription": "[bɪˈsaɪd]"
  },
  {
    "engWord": "victory",
    "rusWord": "победа",
    "transcription": "[ˈvɪktərɪ]"
  },
  {
    "engWord": "disease",
    "rusWord": "болезнь",
    "transcription": "[dɪˈziːz]"
  },
  {
    "engWord": "fifteen",
    "rusWord": "пятнадцать",
    "transcription": "[fɪfˈtiːn]"
  },
  {
    "engWord": "magic",
    "rusWord": "магия",
    "transcription": "[ˈmæʤɪk]"
  },
  {
    "engWord": "partner",
    "rusWord": "партнер",
    "transcription": "[ˈpɑːtnə]"
  },
  {
    "engWord": "must",
    "rusWord": "должен",
    "transcription": "[mʌst]"
  },
  {
    "engWord": "pattern",
    "rusWord": "шаблон",
    "transcription": "[ˈpætn]"
  },
  {
    "engWord": "where",
    "rusWord": "где",
    "transcription": "[weə]"
  },
  {
    "engWord": "biography",
    "rusWord": "биография",
    "transcription": "[baɪˈɒgrəfɪ]"
  },
  {
    "engWord": "sword",
    "rusWord": "меч",
    "transcription": "[sɔːd]"
  },
  {
    "engWord": "asset",
    "rusWord": "имущество",
    "transcription": "[ˈæset]"
  },
  {
    "engWord": "millennium",
    "rusWord": "тысячелетие",
    "transcription": "[mɪˈlenɪəm]"
  },
  {
    "engWord": "hydrogen",
    "rusWord": "водород",
    "transcription": "[ˈhaɪdrəʤ(ə)n]"
  },
  {
    "engWord": "gallon",
    "rusWord": "галлон",
    "transcription": "[ˈgælən]"
  },
  {
    "engWord": "surname",
    "rusWord": "фамилия",
    "transcription": "[ˈsɜːneɪm]"
  },
  {
    "engWord": "wade",
    "rusWord": "пробираться",
    "transcription": "[weɪd]"
  },
  {
    "engWord": "philosophy",
    "rusWord": "философия",
    "transcription": "[fɪˈlɒsəfɪ]"
  },
  {
    "engWord": "standard",
    "rusWord": "стандарт",
    "transcription": "[ˈstændəd]"
  },
  {
    "engWord": "mob",
    "rusWord": "толпа",
    "transcription": "[mɒb]"
  },
  {
    "engWord": "revolution",
    "rusWord": "революция",
    "transcription": "[revəˈluːʃn]"
  },
  {
    "engWord": "regime",
    "rusWord": "режим",
    "transcription": "[reɪˈʒiːm]"
  },
  {
    "engWord": "Jesus",
    "rusWord": "Иисус",
    "transcription": "[ˈʤiːzəs]"
  },
  {
    "engWord": "demon",
    "rusWord": "демон",
    "transcription": "[ˈdiːmən]"
  },
  {
    "engWord": "decay",
    "rusWord": "распад",
    "transcription": "[dɪˈkeɪ]"
  },
  {
    "engWord": "naked",
    "rusWord": "незащищенный",
    "transcription": "[ˈneɪkɪd]"
  },
  {
    "engWord": "draw",
    "rusWord": "рисовать",
    "transcription": "[drɔː]"
  },
  {
    "engWord": "pound",
    "rusWord": "фунт",
    "transcription": "[paʊnd]"
  },
  {
    "engWord": "coast",
    "rusWord": "побережье",
    "transcription": "[kəʊst]"
  },
  {
    "engWord": "walk",
    "rusWord": "идти",
    "transcription": "[wɔːk]"
  },
  {
    "engWord": "less",
    "rusWord": "меньше",
    "transcription": "[les]"
  },
  {
    "engWord": "communication",
    "rusWord": "коммуникация",
    "transcription": "[kəmjuːnɪˈkeɪʃn]"
  },
  {
    "engWord": "thread",
    "rusWord": "нитка",
    "transcription": "[θred]"
  },
  {
    "engWord": "authorities",
    "rusWord": "власти",
    "transcription": "[ɔːˈθɒrɪtɪz]"
  },
  {
    "engWord": "beige",
    "rusWord": "бежевый",
    "transcription": "[beɪʒ]"
  },
  {
    "engWord": "sailor",
    "rusWord": "моряк",
    "transcription": "[ˈseɪlə]"
  },
  {
    "engWord": "management",
    "rusWord": "управление",
    "transcription": "[ˈmænɪʤmənt]"
  },
  {
    "engWord": "habitual",
    "rusWord": "обычный",
    "transcription": "[həˈbɪʧʊəl]"
  },
  {
    "engWord": "inherit",
    "rusWord": "наследовать",
    "transcription": "[ɪnˈherɪt]"
  },
  {
    "engWord": "transform",
    "rusWord": "преобразиться",
    "transcription": "[trænsˈfɔːm]"
  },
  {
    "engWord": "investigate",
    "rusWord": "исследовать",
    "transcription": "[ɪnˈvestɪgeɪt]"
  },
  {
    "engWord": "monitor",
    "rusWord": "монитор",
    "transcription": "[ˈmɒnɪtə]"
  },
  {
    "engWord": "salesman",
    "rusWord": "продавец",
    "transcription": "[ˈseɪlzmən]"
  },
  {
    "engWord": "vain",
    "rusWord": "тщеславный",
    "transcription": "[veɪn]"
  },
  {
    "engWord": "sober",
    "rusWord": "трезвый",
    "transcription": "[ˈsəʊbə]"
  },
  {
    "engWord": "confess",
    "rusWord": "исповедоваться",
    "transcription": "[kənˈfes]"
  },
  {
    "engWord": "snap",
    "rusWord": "щелкать",
    "transcription": "[snæp]"
  },
  {
    "engWord": "fully",
    "rusWord": "полностью",
    "transcription": "[ˈfʊlɪ]"
  },
  {
    "engWord": "covering",
    "rusWord": "покрытие",
    "transcription": "[ˈkʌvərɪŋ]"
  },
  {
    "engWord": "guilty",
    "rusWord": "виновный",
    "transcription": "[ˈgɪltɪ]"
  },
  {
    "engWord": "report",
    "rusWord": "доклад",
    "transcription": "[rɪˈpɔːt]"
  },
  {
    "engWord": "forty",
    "rusWord": "сорок",
    "transcription": "[ˈfɔːtɪ]"
  },
  {
    "engWord": "affair",
    "rusWord": "дело",
    "transcription": "[əˈfeə]"
  },
  {
    "engWord": "habit",
    "rusWord": "привычка",
    "transcription": "[ˈhæbɪt]"
  },
  {
    "engWord": "loving",
    "rusWord": "любящий",
    "transcription": "[ˈlʌvɪŋ]"
  },
  {
    "engWord": "sheet",
    "rusWord": "лист",
    "transcription": "[ʃiːt]"
  },
  {
    "engWord": "dry",
    "rusWord": "сухой",
    "transcription": "[draɪ]"
  },
  {
    "engWord": "could",
    "rusWord": "мочь",
    "transcription": "[kʊd]"
  },
  {
    "engWord": "honour",
    "rusWord": "честь",
    "transcription": "[ˈɒnə]"
  },
  {
    "engWord": "biology",
    "rusWord": "биология",
    "transcription": "[baɪˈɒləʤɪ]"
  },
  {
    "engWord": "sophisticated",
    "rusWord": "утонченный",
    "transcription": "[səˈfɪstɪkeɪtɪd]"
  },
  {
    "engWord": "flexible",
    "rusWord": "гибкий",
    "transcription": "[ˈfleksəbl]"
  },
  {
    "engWord": "beaten",
    "rusWord": "избитый",
    "transcription": "[biːtn]"
  },
  {
    "engWord": "nest",
    "rusWord": "гнездо",
    "transcription": "[nest]"
  },
  {
    "engWord": "communicate",
    "rusWord": "общаться",
    "transcription": "[kəˈmjuːnɪkeɪt]"
  },
  {
    "engWord": "whale",
    "rusWord": "кит",
    "transcription": "[weɪl]"
  },
  {
    "engWord": "postman",
    "rusWord": "почтальон",
    "transcription": "[ˈpəʊstmən]"
  },
  {
    "engWord": "gorgeous",
    "rusWord": "великолепный",
    "transcription": "[ˈgɔːʤəs]"
  },
  {
    "engWord": "influence",
    "rusWord": "влияние",
    "transcription": "[ˈɪnflʊəns]"
  },
  {
    "engWord": "counter",
    "rusWord": "счетчик",
    "transcription": "[ˈkaʊntə]"
  },
  {
    "engWord": "authority",
    "rusWord": "авторитет",
    "transcription": "[ɔːˈθɒrɪtɪ]"
  },
  {
    "engWord": "entire",
    "rusWord": "весь",
    "transcription": "[ɪnˈtaɪə]"
  },
  {
    "engWord": "reality",
    "rusWord": "реальность",
    "transcription": "[rɪˈælɪtɪ]"
  },
  {
    "engWord": "leg",
    "rusWord": "нога",
    "transcription": "[leg]"
  },
  {
    "engWord": "broke",
    "rusWord": "разоренный",
    "transcription": "[brəʊk]"
  },
  {
    "engWord": "dream",
    "rusWord": "мечта",
    "transcription": "[driːm]"
  },
  {
    "engWord": "test",
    "rusWord": "тест",
    "transcription": "[test]"
  },
  {
    "engWord": "spell",
    "rusWord": "заклинание",
    "transcription": "[spel]"
  },
  {
    "engWord": "camel",
    "rusWord": "верблюд",
    "transcription": "[ˈkæməl]"
  },
  {
    "engWord": "caption",
    "rusWord": "подпись",
    "transcription": "[kæpʃn]"
  },
  {
    "engWord": "camouflage",
    "rusWord": "камуфляжный",
    "transcription": "[ˈkæməflɑːʒ]"
  },
  {
    "engWord": "overdone",
    "rusWord": "преувеличенный",
    "transcription": "[əʊvəˈdʌn]"
  },
  {
    "engWord": "conversation",
    "rusWord": "разговор",
    "transcription": "[kɒnvəˈseɪʃn]"
  },
  {
    "engWord": "rocky",
    "rusWord": "скалистый",
    "transcription": "[ˈrɒkɪ]"
  },
  {
    "engWord": "affection",
    "rusWord": "привязанность",
    "transcription": "[əˈfekʃn]"
  },
  {
    "engWord": "personnel",
    "rusWord": "персонал",
    "transcription": "[pɜːsəˈnel]"
  },
  {
    "engWord": "exclaim",
    "rusWord": "воскликнуть",
    "transcription": "[ɪksˈkleɪm]"
  },
  {
    "engWord": "custom",
    "rusWord": "традиция",
    "transcription": "[ˈkʌstəm]"
  },
  {
    "engWord": "cable",
    "rusWord": "кабель",
    "transcription": "[keɪbl]"
  },
  {
    "engWord": "cap",
    "rusWord": "шапка",
    "transcription": "[kæp]"
  },
  {
    "engWord": "wipe",
    "rusWord": "протирать",
    "transcription": "[waɪp]"
  },
  {
    "engWord": "tear",
    "rusWord": "рвать",
    "transcription": "[teə]"
  },
  {
    "engWord": "nope",
    "rusWord": "нет",
    "transcription": "[nəʊp]"
  },
  {
    "engWord": "computer",
    "rusWord": "компьютер",
    "transcription": "[kəmˈpjuːtə]"
  },
  {
    "engWord": "forgotten",
    "rusWord": "забытый",
    "transcription": "[fəˈgɒtn]"
  },
  {
    "engWord": "law",
    "rusWord": "закон",
    "transcription": "[lɔː]"
  },
  {
    "engWord": "sign",
    "rusWord": "знак",
    "transcription": "[saɪn]"
  },
  {
    "engWord": "whose",
    "rusWord": "чья",
    "transcription": "[huːz]"
  },
  {
    "engWord": "five",
    "rusWord": "пять",
    "transcription": "[faɪv]"
  },
  {
    "engWord": "happen",
    "rusWord": "случаться",
    "transcription": "[ˈhæpən]"
  },
  {
    "engWord": "beat",
    "rusWord": "бить",
    "transcription": "[biːt]"
  },
  {
    "engWord": "garden",
    "rusWord": "сад",
    "transcription": "[gɑːdn]"
  },
  {
    "engWord": "paint",
    "rusWord": "красить",
    "transcription": "[peɪnt]"
  },
  {
    "engWord": "adorable",
    "rusWord": "обожаемый",
    "transcription": "[əˈdɔːrəbl]"
  },
  {
    "engWord": "tackle",
    "rusWord": "снасти",
    "transcription": "[tækl]"
  },
  {
    "engWord": "eyebrow",
    "rusWord": "бровь",
    "transcription": "[ˈaɪbraʊ]"
  },
  {
    "engWord": "specific",
    "rusWord": "специфический",
    "transcription": "[spɪˈsɪfɪk]"
  },
  {
    "engWord": "outer",
    "rusWord": "внешний",
    "transcription": "[ˈaʊtə]"
  },
  {
    "engWord": "merit",
    "rusWord": "заслуга",
    "transcription": "[ˈmerɪt]"
  },
  {
    "engWord": "snail",
    "rusWord": "улитка",
    "transcription": "[sneɪl]"
  },
  {
    "engWord": "gross",
    "rusWord": "валовой",
    "transcription": "[grəʊs]"
  },
  {
    "engWord": "Tuesday",
    "rusWord": "вторник",
    "transcription": "[ˈtjuːzdɪ]"
  },
  {
    "engWord": "code",
    "rusWord": "код",
    "transcription": "[kəʊd]"
  },
  {
    "engWord": "stress",
    "rusWord": "стресс",
    "transcription": "[stres]"
  },
  {
    "engWord": "ill",
    "rusWord": "больной",
    "transcription": "[ɪl]"
  },
  {
    "engWord": "cake",
    "rusWord": "торт",
    "transcription": "[keɪk]"
  },
  {
    "engWord": "boring",
    "rusWord": "скучный",
    "transcription": "[ˈbɔːrɪŋ]"
  },
  {
    "engWord": "hassle",
    "rusWord": "перебранка",
    "transcription": "[hæsl]"
  },
  {
    "engWord": "casualty",
    "rusWord": "несчастный случай",
    "transcription": "[ˈkæʒʊəltɪ]"
  },
  {
    "engWord": "drugged",
    "rusWord": "одурманенный",
    "transcription": "[]"
  },
  {
    "engWord": "pamper",
    "rusWord": "баловать",
    "transcription": "[ˈpæmpə]"
  },
  {
    "engWord": "breastfeeding",
    "rusWord": "кормление грудью",
    "transcription": "[ˈbrestfiːdɪŋ]"
  },
  {
    "engWord": "bowl",
    "rusWord": "чаша",
    "transcription": "[bəʊl]"
  },
  {
    "engWord": "winner",
    "rusWord": "победитель",
    "transcription": "[ˈwɪnə]"
  },
  {
    "engWord": "boots",
    "rusWord": "обувь",
    "transcription": "[buːts]"
  },
  {
    "engWord": "opera",
    "rusWord": "опера",
    "transcription": "[ˈɒpərə]"
  },
  {
    "engWord": "hall",
    "rusWord": "зал",
    "transcription": "[hɔːl]"
  },
  {
    "engWord": "link",
    "rusWord": "ссылка",
    "transcription": "[lɪŋk]"
  },
  {
    "engWord": "news",
    "rusWord": "новость",
    "transcription": "[njuːz]"
  },
  {
    "engWord": "gum",
    "rusWord": "жвачка",
    "transcription": "[gʌm]"
  },
  {
    "engWord": "stronger",
    "rusWord": "более сильный",
    "transcription": "[strɒŋɡə]"
  },
  {
    "engWord": "dead",
    "rusWord": "мертвый",
    "transcription": "[ded]"
  },
  {
    "engWord": "bad",
    "rusWord": "плохой",
    "transcription": "[bæd]"
  },
  {
    "engWord": "snow",
    "rusWord": "снег",
    "transcription": "[snəʊ]"
  },
  {
    "engWord": "due",
    "rusWord": "надлежащий",
    "transcription": "[djuː]"
  },
  {
    "engWord": "beep",
    "rusWord": "гудок",
    "transcription": "[biːp]"
  },
  {
    "engWord": "nourish",
    "rusWord": "питать",
    "transcription": "[ˈnʌrɪʃ]"
  },
  {
    "engWord": "commission",
    "rusWord": "комиссия",
    "transcription": "[kəˈmɪʃn]"
  },
  {
    "engWord": "deteriorate",
    "rusWord": "ухудшаться",
    "transcription": "[dɪˈtɪərɪəreɪt]"
  },
  {
    "engWord": "resign",
    "rusWord": "уходить в отставку",
    "transcription": "[riːˈsaɪn]"
  },
  {
    "engWord": "trace",
    "rusWord": "проследить",
    "transcription": "[treɪs]"
  },
  {
    "engWord": "hesitate",
    "rusWord": "колебаться",
    "transcription": "[ˈhezɪteɪt]"
  },
  {
    "engWord": "brick",
    "rusWord": "кирпич",
    "transcription": "[brɪk]"
  },
  {
    "engWord": "site",
    "rusWord": "сайт",
    "transcription": "[saɪt]"
  },
  {
    "engWord": "responsibility",
    "rusWord": "ответственность",
    "transcription": "[rɪspɒnsəˈbɪlɪtɪ]"
  },
  {
    "engWord": "freeze",
    "rusWord": "замерзать",
    "transcription": "[friːz]"
  },
  {
    "engWord": "lunchtime",
    "rusWord": "обеденное время",
    "transcription": "[ˈlʌnʧtaɪm]"
  },
  {
    "engWord": "lousy",
    "rusWord": "никудышный",
    "transcription": "[ˈlaʊzɪ]"
  },
  {
    "engWord": "square",
    "rusWord": "площадь",
    "transcription": "[skweə]"
  },
  {
    "engWord": "type",
    "rusWord": "тип",
    "transcription": "[taɪp]"
  },
  {
    "engWord": "foot",
    "rusWord": "нога",
    "transcription": "[]"
  },
  {
    "engWord": "chin",
    "rusWord": "подбородок",
    "transcription": "[ʧɪn]"
  },
  {
    "engWord": "rifle",
    "rusWord": "винтовка",
    "transcription": "[raɪfl]"
  },
  {
    "engWord": "funds",
    "rusWord": "денежные средства",
    "transcription": "[fʌndz]"
  },
  {
    "engWord": "billboard",
    "rusWord": "рекламный щит",
    "transcription": "[ˈbɪlbɔːd]"
  },
  {
    "engWord": "was",
    "rusWord": "был",
    "transcription": "[]"
  },
  {
    "engWord": "hockey",
    "rusWord": "хоккей",
    "transcription": "[ˈhɒkɪ]"
  },
  {
    "engWord": "gathering",
    "rusWord": "сбор",
    "transcription": "[ˈgæðərɪŋ]"
  },
  {
    "engWord": "preparation",
    "rusWord": "подготовка",
    "transcription": "[prepəˈreɪʃn]"
  },
  {
    "engWord": "coverage",
    "rusWord": "покрытие",
    "transcription": "[ˈkʌvərɪʤ]"
  },
  {
    "engWord": "automatic",
    "rusWord": "автомат",
    "transcription": "[ɔːtəˈmætɪk]"
  },
  {
    "engWord": "contest",
    "rusWord": "соревнование",
    "transcription": "[ˈkɒntest]"
  },
  {
    "engWord": "lap",
    "rusWord": "круг",
    "transcription": "[læp]"
  },
  {
    "engWord": "sauce",
    "rusWord": "соус",
    "transcription": "[sɔːs]"
  },
  {
    "engWord": "pub",
    "rusWord": "паб",
    "transcription": "[pʌb]"
  },
  {
    "engWord": "load",
    "rusWord": "загружать",
    "transcription": "[ləʊd]"
  },
  {
    "engWord": "circumstance",
    "rusWord": "условие",
    "transcription": "[ˈsɜːkəmstæns]"
  },
  {
    "engWord": "advantage",
    "rusWord": "преимущество",
    "transcription": "[ədˈvɑːntɪʤ]"
  },
  {
    "engWord": "bloody",
    "rusWord": "кровавый",
    "transcription": "[ˈblʌdɪ]"
  },
  {
    "engWord": "enable",
    "rusWord": "включить",
    "transcription": "[ɪˈneɪbl]"
  },
  {
    "engWord": "cold",
    "rusWord": "простуда",
    "transcription": "[kəʊld]"
  },
  {
    "engWord": "part",
    "rusWord": "часть",
    "transcription": "[pɑːt]"
  },
  {
    "engWord": "several",
    "rusWord": "несколько",
    "transcription": "[ˈsevrəl]"
  },
  {
    "engWord": "ten",
    "rusWord": "десять",
    "transcription": "[ten]"
  },
  {
    "engWord": "lobster",
    "rusWord": "рак",
    "transcription": "[ˈlɒbstə]"
  },
  {
    "engWord": "restless",
    "rusWord": "беспокойный",
    "transcription": "[ˈrestlɪs]"
  },
  {
    "engWord": "resident",
    "rusWord": "житель",
    "transcription": "[ˈrezɪdənt]"
  },
  {
    "engWord": "repair",
    "rusWord": "ремонт",
    "transcription": "[rɪˈpeə]"
  },
  {
    "engWord": "welfare",
    "rusWord": "благосостояние",
    "transcription": "[ˈwelfeə]"
  },
  {
    "engWord": "whenever",
    "rusWord": "в любое время",
    "transcription": "[wenˈevə]"
  },
  {
    "engWord": "civil",
    "rusWord": "гражданский",
    "transcription": "[sɪvl]"
  },
  {
    "engWord": "directly",
    "rusWord": "непосредственно",
    "transcription": "[dɪˈrektlɪ]"
  },
  {
    "engWord": "reasonable",
    "rusWord": "разумный",
    "transcription": "[ˈriːznəbl]"
  },
  {
    "engWord": "bull",
    "rusWord": "бык",
    "transcription": "[bʊl]"
  },
  {
    "engWord": "underwear",
    "rusWord": "нижнее белье",
    "transcription": "[ˈʌndəweə]"
  },
  {
    "engWord": "dare",
    "rusWord": "осмеливаться",
    "transcription": "[deə]"
  },
  {
    "engWord": "cancer",
    "rusWord": "рак",
    "transcription": "[ˈkænsə]"
  },
  {
    "engWord": "keen",
    "rusWord": "проницательный",
    "transcription": "[kiːn]"
  },
  {
    "engWord": "stuck",
    "rusWord": "застрявший",
    "transcription": "[stʌk]"
  },
  {
    "engWord": "bout",
    "rusWord": "раз",
    "transcription": "[baʊt]"
  },
  {
    "engWord": "tooth",
    "rusWord": "зуб",
    "transcription": "[tuːθ]"
  },
  {
    "engWord": "listen",
    "rusWord": "слушать",
    "transcription": "[lɪsn]"
  },
  {
    "engWord": "describe",
    "rusWord": "описывать",
    "transcription": "[dɪsˈkraɪb]"
  },
  {
    "engWord": "my",
    "rusWord": "мой",
    "transcription": "[maɪ]"
  },
  {
    "engWord": "garbage",
    "rusWord": "мусор",
    "transcription": "[ˈgɑːbɪʤ]"
  },
  {
    "engWord": "loop",
    "rusWord": "петля",
    "transcription": "[luːp]"
  },
  {
    "engWord": "had",
    "rusWord": "было",
    "transcription": "[]"
  },
  {
    "engWord": "suggestion",
    "rusWord": "предложение",
    "transcription": "[səˈʤesʧən]"
  },
  {
    "engWord": "kit",
    "rusWord": "комплект",
    "transcription": "[kɪt]"
  },
  {
    "engWord": "abroad",
    "rusWord": "за границей",
    "transcription": "[əˈbrɔːd]"
  },
  {
    "engWord": "feature",
    "rusWord": "особенность",
    "transcription": "[ˈfiːʧə]"
  },
  {
    "engWord": "labour",
    "rusWord": "труд",
    "transcription": "[ˈleɪbə]"
  },
  {
    "engWord": "thanksgiving",
    "rusWord": "благодарение",
    "transcription": "[θæŋksˈgɪvɪŋ]"
  },
  {
    "engWord": "scene",
    "rusWord": "сцена",
    "transcription": "[siːn]"
  },
  {
    "engWord": "develop",
    "rusWord": "развивать",
    "transcription": "[dɪˈveləp]"
  },
  {
    "engWord": "such",
    "rusWord": "подобный",
    "transcription": "[sʌʧ]"
  },
  {
    "engWord": "climb",
    "rusWord": "взбираться",
    "transcription": "[klaɪm]"
  },
  {
    "engWord": "crawling",
    "rusWord": "ползающий",
    "transcription": "[ˈkrɔːlɪŋ]"
  },
  {
    "engWord": "helicopter",
    "rusWord": "вертолет",
    "transcription": "[ˈhelɪkɒptə]"
  },
  {
    "engWord": "stadium",
    "rusWord": "стадион",
    "transcription": "[ˈsteɪdɪəm]"
  },
  {
    "engWord": "rear",
    "rusWord": "задний",
    "transcription": "[rɪə]"
  },
  {
    "engWord": "were",
    "rusWord": "были",
    "transcription": "[]"
  },
  {
    "engWord": "pearl",
    "rusWord": "жемчужина",
    "transcription": "[pɜːl]"
  },
  {
    "engWord": "toll",
    "rusWord": "пошлина",
    "transcription": "[təʊl]"
  },
  {
    "engWord": "hangover",
    "rusWord": "похмелье",
    "transcription": "[ˈhæŋəʊvə]"
  },
  {
    "engWord": "eraser",
    "rusWord": "ластик",
    "transcription": "[ɪˈreɪzə]"
  },
  {
    "engWord": "cassette",
    "rusWord": "кассета",
    "transcription": "[kəˈset]"
  },
  {
    "engWord": "dope",
    "rusWord": "наркотик",
    "transcription": "[dəʊp]"
  },
  {
    "engWord": "cave",
    "rusWord": "пещера",
    "transcription": "[keɪv]"
  },
  {
    "engWord": "skip",
    "rusWord": "пропускать",
    "transcription": "[skɪp]"
  },
  {
    "engWord": "England",
    "rusWord": "Англия",
    "transcription": "[ˈɪŋglənd]"
  },
  {
    "engWord": "off",
    "rusWord": "прочь",
    "transcription": "[ɒf]"
  },
  {
    "engWord": "row",
    "rusWord": "ряд",
    "transcription": "[rəʊ]"
  },
  {
    "engWord": "final",
    "rusWord": "окончательный",
    "transcription": "[faɪnl]"
  },
  {
    "engWord": "and",
    "rusWord": "и",
    "transcription": "[ænd]"
  },
  {
    "engWord": "does",
    "rusWord": "делает",
    "transcription": "[]"
  },
  {
    "engWord": "horoscope",
    "rusWord": "гороскоп",
    "transcription": "[ˈhɒrəskəʊp]"
  },
  {
    "engWord": "confirm",
    "rusWord": "подтверждать",
    "transcription": "[kənˈfɜːm]"
  },
  {
    "engWord": "generally",
    "rusWord": "обычно",
    "transcription": "[ˈʤenərəlɪ]"
  },
  {
    "engWord": "heal",
    "rusWord": "лечить",
    "transcription": "[hiːl]"
  },
  {
    "engWord": "editor",
    "rusWord": "редактор",
    "transcription": "[ˈedɪtə]"
  },
  {
    "engWord": "dump",
    "rusWord": "свалка",
    "transcription": "[dʌmp]"
  },
  {
    "engWord": "decision",
    "rusWord": "решение",
    "transcription": "[dɪˈsɪʒən]"
  },
  {
    "engWord": "release",
    "rusWord": "освобождать",
    "transcription": "[rɪˈliːs]"
  },
  {
    "engWord": "overseas",
    "rusWord": "за рубежом",
    "transcription": "[əʊvəˈsiːz]"
  },
  {
    "engWord": "sudden",
    "rusWord": "внезапный",
    "transcription": "[sʌdn]"
  },
  {
    "engWord": "made",
    "rusWord": "сделанный",
    "transcription": "[meɪd]"
  },
  {
    "engWord": "meet",
    "rusWord": "встречать",
    "transcription": "[miːt]"
  },
  {
    "engWord": "cry",
    "rusWord": "плакать",
    "transcription": "[kraɪ]"
  },
  {
    "engWord": "impulsive",
    "rusWord": "импульсивный",
    "transcription": "[ɪmˈpʌlsɪv]"
  },
  {
    "engWord": "convenient",
    "rusWord": "удобный",
    "transcription": "[kənˈviːnɪənt]"
  },
  {
    "engWord": "inhale",
    "rusWord": "вдыхать",
    "transcription": "[ɪnˈheɪl]"
  },
  {
    "engWord": "men",
    "rusWord": "мужчины",
    "transcription": "[]"
  },
  {
    "engWord": "rage",
    "rusWord": "ярость",
    "transcription": "[reɪʤ]"
  },
  {
    "engWord": "bait",
    "rusWord": "заманить",
    "transcription": "[beɪt]"
  },
  {
    "engWord": "flee",
    "rusWord": "бежать",
    "transcription": "[fliː]"
  },
  {
    "engWord": "obedient",
    "rusWord": "послушный",
    "transcription": "[əˈbiːdɪənt]"
  },
  {
    "engWord": "lick",
    "rusWord": "лизнуть",
    "transcription": "[lɪk]"
  },
  {
    "engWord": "underneath",
    "rusWord": "под",
    "transcription": "[ʌndəˈniːθ]"
  },
  {
    "engWord": "costume",
    "rusWord": "костюм",
    "transcription": "[ˈkɒstjʊm]"
  },
  {
    "engWord": "emerge",
    "rusWord": "всплывать",
    "transcription": "[ɪˈmɜːʤ]"
  },
  {
    "engWord": "visual",
    "rusWord": "визуальный",
    "transcription": "[ˈvɪʒʊəl]"
  },
  {
    "engWord": "marriage",
    "rusWord": "брак",
    "transcription": "[ˈmærɪʤ]"
  },
  {
    "engWord": "territory",
    "rusWord": "территория",
    "transcription": "[ˈterɪtərɪ]"
  },
  {
    "engWord": "possibility",
    "rusWord": "возможность",
    "transcription": "[pɒsəˈbɪlɪtɪ]"
  },
  {
    "engWord": "insurance",
    "rusWord": "страхование",
    "transcription": "[ɪnˈʃʊərəns]"
  },
  {
    "engWord": "it",
    "rusWord": "он",
    "transcription": "[ɪt]"
  },
  {
    "engWord": "Jewish",
    "rusWord": "еврейский",
    "transcription": "[ˈʤuːɪʃ]"
  },
  {
    "engWord": "coalition",
    "rusWord": "коалиция",
    "transcription": "[kəʊəˈlɪʃn]"
  },
  {
    "engWord": "went",
    "rusWord": "пошел",
    "transcription": "[]"
  },
  {
    "engWord": "slum",
    "rusWord": "трущоба",
    "transcription": "[slʌm]"
  },
  {
    "engWord": "ethnic",
    "rusWord": "этнический",
    "transcription": "[ˈeθnɪk]"
  },
  {
    "engWord": "jewellery",
    "rusWord": "ювелирные изделия",
    "transcription": "[ˈʤuːəlrɪ]"
  },
  {
    "engWord": "naturally",
    "rusWord": "естественно",
    "transcription": "[ˈnæʧrəlɪ]"
  },
  {
    "engWord": "wake",
    "rusWord": "просыпаться",
    "transcription": "[weɪk]"
  },
  {
    "engWord": "upon",
    "rusWord": "на",
    "transcription": "[əˈpɒn]"
  },
  {
    "engWord": "eastern",
    "rusWord": "восточный",
    "transcription": "[ˈiːstən]"
  },
  {
    "engWord": "fate",
    "rusWord": "судьба",
    "transcription": "[feɪt]"
  },
  {
    "engWord": "bond",
    "rusWord": "связь",
    "transcription": "[bɒnd]"
  },
  {
    "engWord": "wire",
    "rusWord": "провод",
    "transcription": "[ˈwaɪə]"
  },
  {
    "engWord": "yard",
    "rusWord": "двор",
    "transcription": "[jɑːd]"
  },
  {
    "engWord": "finish",
    "rusWord": "заканчивать",
    "transcription": "[ˈfɪnɪʃ]"
  },
  {
    "engWord": "thorough",
    "rusWord": "тщательный",
    "transcription": "[ˈθʌrə]"
  },
  {
    "engWord": "compassion",
    "rusWord": "сострадание",
    "transcription": "[kəmˈpæʃn]"
  },
  {
    "engWord": "promising",
    "rusWord": "многообещающий",
    "transcription": "[ˈprɒmɪsɪŋ]"
  },
  {
    "engWord": "unpleasant",
    "rusWord": "неприятный",
    "transcription": "[ʌnˈpleznt]"
  },
  {
    "engWord": "came",
    "rusWord": "пришел",
    "transcription": "[]"
  },
  {
    "engWord": "contrary",
    "rusWord": "противоположность",
    "transcription": "[ˈkɒntrərɪ]"
  },
  {
    "engWord": "requirement",
    "rusWord": "требование",
    "transcription": "[rɪˈkwaɪəmənt]"
  },
  {
    "engWord": "vocational",
    "rusWord": "профессиональный",
    "transcription": "[vəʊˈkeɪʃnəl]"
  },
  {
    "engWord": "insure",
    "rusWord": "страховать",
    "transcription": "[ɪnˈʃʊə]"
  },
  {
    "engWord": "imagination",
    "rusWord": "воображение",
    "transcription": "[ɪmæʤɪˈneɪʃn]"
  },
  {
    "engWord": "investment",
    "rusWord": "инвестиции",
    "transcription": "[ɪnˈvestmənt]"
  },
  {
    "engWord": "drama",
    "rusWord": "драма",
    "transcription": "[ˈdrɑːmə]"
  },
  {
    "engWord": "boxing",
    "rusWord": "бокс",
    "transcription": "[ˈbɒksɪŋ]"
  },
  {
    "engWord": "essay",
    "rusWord": "сочинение",
    "transcription": "[ˈeseɪ]"
  },
  {
    "engWord": "holy",
    "rusWord": "святой",
    "transcription": "[ˈhəʊlɪ]"
  },
  {
    "engWord": "custody",
    "rusWord": "опека",
    "transcription": "[ˈkʌstədɪ]"
  },
  {
    "engWord": "selfish",
    "rusWord": "эгоистичный",
    "transcription": "[ˈselfɪʃ]"
  },
  {
    "engWord": "concise",
    "rusWord": "краткий",
    "transcription": "[kənˈsaɪs]"
  },
  {
    "engWord": "familiar",
    "rusWord": "знакомый",
    "transcription": "[fəˈmɪlɪə]"
  },
  {
    "engWord": "count",
    "rusWord": "считать",
    "transcription": "[kaʊnt]"
  },
  {
    "engWord": "circle",
    "rusWord": "круг",
    "transcription": "[sɜːkl]"
  },
  {
    "engWord": "alibi",
    "rusWord": "алиби",
    "transcription": "[ˈælɪbaɪ]"
  },
  {
    "engWord": "heel",
    "rusWord": "каблук",
    "transcription": "[hiːl]"
  },
  {
    "engWord": "them",
    "rusWord": "их",
    "transcription": "[]"
  },
  {
    "engWord": "isolate",
    "rusWord": "изолировать",
    "transcription": "[ˈaɪsəleɪt]"
  },
  {
    "engWord": "governor",
    "rusWord": "губернатор",
    "transcription": "[ˈgʌvənə]"
  },
  {
    "engWord": "mill",
    "rusWord": "мельница",
    "transcription": "[mɪl]"
  },
  {
    "engWord": "courtesy",
    "rusWord": "вежливость",
    "transcription": "[ˈkɜːtɪsɪ]"
  },
  {
    "engWord": "complain",
    "rusWord": "жаловаться",
    "transcription": "[kəmˈpleɪn]"
  },
  {
    "engWord": "spite",
    "rusWord": "злоба",
    "transcription": "[spaɪt]"
  },
  {
    "engWord": "detest",
    "rusWord": "ненавидеть",
    "transcription": "[dɪˈtest]"
  },
  {
    "engWord": "ding",
    "rusWord": "звенеть",
    "transcription": "[dɪŋ]"
  },
  {
    "engWord": "nutrition",
    "rusWord": "питание",
    "transcription": "[njuːˈtrɪʃn]"
  },
  {
    "engWord": "hence",
    "rusWord": "следовательно",
    "transcription": "[hens]"
  },
  {
    "engWord": "fox",
    "rusWord": "лиса",
    "transcription": "[fɒks]"
  },
  {
    "engWord": "sample",
    "rusWord": "образец",
    "transcription": "[sɑːmpl]"
  },
  {
    "engWord": "goal",
    "rusWord": "цель",
    "transcription": "[gəʊl]"
  },
  {
    "engWord": "relative",
    "rusWord": "родственник",
    "transcription": "[ˈrelətɪv]"
  },
  {
    "engWord": "regret",
    "rusWord": "сожаление",
    "transcription": "[rɪˈgret]"
  },
  {
    "engWord": "folks",
    "rusWord": "ребята",
    "transcription": "[fəʊks]"
  },
  {
    "engWord": "soup",
    "rusWord": "суп",
    "transcription": "[suːp]"
  },
  {
    "engWord": "stamp",
    "rusWord": "печать",
    "transcription": "[stæmp]"
  },
  {
    "engWord": "remain",
    "rusWord": "оставаться",
    "transcription": "[rɪˈmeɪn]"
  },
  {
    "engWord": "protect",
    "rusWord": "защищать",
    "transcription": "[prəˈtekt]"
  },
  {
    "engWord": "subject",
    "rusWord": "предмет",
    "transcription": "[ˈsʌbʤɪkt]"
  },
  {
    "engWord": "fly",
    "rusWord": "летать",
    "transcription": "[flaɪ]"
  },
  {
    "engWord": "save",
    "rusWord": "сохранить",
    "transcription": "[seɪv]"
  },
  {
    "engWord": "hop",
    "rusWord": "прыгать",
    "transcription": "[hɒp]"
  },
  {
    "engWord": "civilization",
    "rusWord": "цивилизация",
    "transcription": "[sɪv(ə)laɪˈzeɪʃn]"
  },
  {
    "engWord": "blend",
    "rusWord": "смесь",
    "transcription": "[blend]"
  },
  {
    "engWord": "undo",
    "rusWord": "отменить",
    "transcription": "[ʌnˈduː]"
  },
  {
    "engWord": "spiritual",
    "rusWord": "духовный",
    "transcription": "[ˈspɪrɪʧʊəl]"
  },
  {
    "engWord": "him",
    "rusWord": "его",
    "transcription": "[]"
  },
  {
    "engWord": "destruction",
    "rusWord": "разрушение",
    "transcription": "[dɪsˈtrʌkʃn]"
  },
  {
    "engWord": "hostile",
    "rusWord": "враждебный",
    "transcription": "[ˈhɒstaɪl]"
  },
  {
    "engWord": "bubble",
    "rusWord": "пузырь",
    "transcription": "[bʌbl]"
  },
  {
    "engWord": "progress",
    "rusWord": "прогресс",
    "transcription": "[ˈprəʊgres]"
  },
  {
    "engWord": "enormous",
    "rusWord": "огромный",
    "transcription": "[ɪˈnɔːməs]"
  },
  {
    "engWord": "comparable",
    "rusWord": "сходный",
    "transcription": "[ˈkʌmp(ə)rəb(ə)l]"
  },
  {
    "engWord": "heading",
    "rusWord": "заголовок",
    "transcription": "[ˈhedɪŋ]"
  },
  {
    "engWord": "hurt",
    "rusWord": "ранить",
    "transcription": "[hɜːt]"
  },
  {
    "engWord": "driving",
    "rusWord": "вождение",
    "transcription": "[ˈdraɪvɪŋ]"
  },
  {
    "engWord": "Sunday",
    "rusWord": "воскресенье",
    "transcription": "[ˈsʌndɪ]"
  },
  {
    "engWord": "script",
    "rusWord": "скрипт",
    "transcription": "[skrɪpt]"
  },
  {
    "engWord": "stomach",
    "rusWord": "желудок",
    "transcription": "[ˈstʌmək]"
  },
  {
    "engWord": "lorry",
    "rusWord": "грузовик",
    "transcription": "[ˈlɒrɪ]"
  },
  {
    "engWord": "red",
    "rusWord": "красный",
    "transcription": "[red]"
  },
  {
    "engWord": "noon",
    "rusWord": "полдень",
    "transcription": "[nuːn]"
  },
  {
    "engWord": "white",
    "rusWord": "белый",
    "transcription": "[waɪt]"
  },
  {
    "engWord": "clean",
    "rusWord": "чистый",
    "transcription": "[kliːn]"
  },
  {
    "engWord": "blow",
    "rusWord": "дуть",
    "transcription": "[bləʊ]"
  },
  {
    "engWord": "against",
    "rusWord": "против",
    "transcription": "[əˈgenst]"
  },
  {
    "engWord": "human",
    "rusWord": "человек",
    "transcription": "[ˈhjuːmən]"
  },
  {
    "engWord": "meat",
    "rusWord": "мясо",
    "transcription": "[miːt]"
  },
  {
    "engWord": "make",
    "rusWord": "делать",
    "transcription": "[meɪk]"
  },
  {
    "engWord": "tube",
    "rusWord": "трубка",
    "transcription": "[tjuːb]"
  },
  {
    "engWord": "wax",
    "rusWord": "воск",
    "transcription": "[wæks]"
  },
  {
    "engWord": "carriage",
    "rusWord": "коляска",
    "transcription": "[ˈkærɪʤ]"
  },
  {
    "engWord": "condo",
    "rusWord": "квартира",
    "transcription": "[ˈkɒndəʊ]"
  },
  {
    "engWord": "has",
    "rusWord": "есть",
    "transcription": "[]"
  },
  {
    "engWord": "unemployment",
    "rusWord": "безработица",
    "transcription": "[ʌnɪmˈplɔɪment]"
  },
  {
    "engWord": "choir",
    "rusWord": "хор",
    "transcription": "[ˈkwaɪə]"
  },
  {
    "engWord": "skull",
    "rusWord": "череп",
    "transcription": "[skʌl]"
  },
  {
    "engWord": "peer",
    "rusWord": "равный",
    "transcription": "[pɪə]"
  },
  {
    "engWord": "appeal",
    "rusWord": "обращение",
    "transcription": "[əˈpiːl]"
  },
  {
    "engWord": "relief",
    "rusWord": "рельеф",
    "transcription": "[rɪˈliːf]"
  },
  {
    "engWord": "award",
    "rusWord": "награда",
    "transcription": "[əˈwɔːd]"
  },
  {
    "engWord": "guts",
    "rusWord": "мужество",
    "transcription": "[gʌts]"
  },
  {
    "engWord": "spoon",
    "rusWord": "ложка",
    "transcription": "[spuːn]"
  },
  {
    "engWord": "spirit",
    "rusWord": "дух",
    "transcription": "[ˈspɪrɪt]"
  },
  {
    "engWord": "ticket",
    "rusWord": "билет",
    "transcription": "[ˈtɪkɪt]"
  },
  {
    "engWord": "fridge",
    "rusWord": "холодильник",
    "transcription": "[frɪʤ]"
  },
  {
    "engWord": "bill",
    "rusWord": "законопроект",
    "transcription": "[bɪl]"
  },
  {
    "engWord": "arm",
    "rusWord": "рука",
    "transcription": "[ɑːm]"
  },
  {
    "engWord": "range",
    "rusWord": "диапазон",
    "transcription": "[reɪnʤ]"
  },
  {
    "engWord": "speech",
    "rusWord": "речь",
    "transcription": "[spiːʧ]"
  },
  {
    "engWord": "sell",
    "rusWord": "продавать",
    "transcription": "[sel]"
  },
  {
    "engWord": "eyelash",
    "rusWord": "ресница",
    "transcription": "[ˈaɪlæʃ]"
  },
  {
    "engWord": "leak",
    "rusWord": "утечка",
    "transcription": "[liːk]"
  },
  {
    "engWord": "sealed",
    "rusWord": "запечатанный",
    "transcription": "[siːld]"
  },
  {
    "engWord": "salvation",
    "rusWord": "спасение",
    "transcription": "[sælˈveɪʃn]"
  },
  {
    "engWord": "diagram",
    "rusWord": "диаграмма",
    "transcription": "[ˈdaɪəgræm]"
  },
  {
    "engWord": "July",
    "rusWord": "июль",
    "transcription": "[ʤʊˈlaɪ]"
  },
  {
    "engWord": "republic",
    "rusWord": "Республика",
    "transcription": "[rɪˈpʌblɪk]"
  },
  {
    "engWord": "pedestrian",
    "rusWord": "пешеход",
    "transcription": "[pɪˈdestrɪən]"
  },
  {
    "engWord": "pork",
    "rusWord": "свинина",
    "transcription": "[pɔːk]"
  },
  {
    "engWord": "inform",
    "rusWord": "сообщать",
    "transcription": "[ɪnˈfɔːm]"
  },
  {
    "engWord": "colonialism",
    "rusWord": "колониализм",
    "transcription": "[kəˈləʊnɪəlɪz(ə)m]"
  },
  {
    "engWord": "clearance",
    "rusWord": "зазор",
    "transcription": "[ˈklɪərəns]"
  },
  {
    "engWord": "aid",
    "rusWord": "помощь",
    "transcription": "[eɪd]"
  },
  {
    "engWord": "unfortunate",
    "rusWord": "несчастный",
    "transcription": "[ʌnˈfɔːʧʊnɪt]"
  },
  {
    "engWord": "torture",
    "rusWord": "пытка",
    "transcription": "[ˈtɔːʧə]"
  },
  {
    "engWord": "channel",
    "rusWord": "канал",
    "transcription": "[ʧænl]"
  },
  {
    "engWord": "enhance",
    "rusWord": "усилить",
    "transcription": "[ɪnˈhɑːns]"
  },
  {
    "engWord": "lately",
    "rusWord": "недавно",
    "transcription": "[ˈleɪtlɪ]"
  },
  {
    "engWord": "bin",
    "rusWord": "мусорное ведро",
    "transcription": "[bɪn]"
  },
  {
    "engWord": "driver",
    "rusWord": "водитель",
    "transcription": "[ˈdraɪvə]"
  },
  {
    "engWord": "accident",
    "rusWord": "авария",
    "transcription": "[ˈæksɪdənt]"
  },
  {
    "engWord": "primarily",
    "rusWord": "прежде всего",
    "transcription": "[ˈpraɪm(ə)rəlɪ]"
  },
  {
    "engWord": "vision",
    "rusWord": "видение",
    "transcription": "[ˈvɪʒən]"
  },
  {
    "engWord": "pair",
    "rusWord": "пара",
    "transcription": "[peə]"
  },
  {
    "engWord": "band",
    "rusWord": "группа",
    "transcription": "[bænd]"
  },
  {
    "engWord": "delighted",
    "rusWord": "восхищенный",
    "transcription": "[dɪˈlaɪtɪd]"
  },
  {
    "engWord": "tissue",
    "rusWord": "ткань",
    "transcription": "[ˈtɪʃuː]"
  },
  {
    "engWord": "rehearsal",
    "rusWord": "репетиция",
    "transcription": "[rɪˈhɜːsəl]"
  },
  {
    "engWord": "clause",
    "rusWord": "пункт",
    "transcription": "[klɔːz]"
  },
  {
    "engWord": "minimal",
    "rusWord": "минимальный",
    "transcription": "[ˈmɪnɪml]"
  },
  {
    "engWord": "been",
    "rusWord": "были",
    "transcription": "[]"
  },
  {
    "engWord": "feather",
    "rusWord": "перо",
    "transcription": "[ˈfeðə]"
  },
  {
    "engWord": "baker",
    "rusWord": "пекарь",
    "transcription": "[ˈbeɪkə]"
  },
  {
    "engWord": "rival",
    "rusWord": "соперник",
    "transcription": "[ˈraɪvəl]"
  },
  {
    "engWord": "implication",
    "rusWord": "вовлечение",
    "transcription": "[ɪmplɪˈkeɪʃn]"
  },
  {
    "engWord": "frozen",
    "rusWord": "замороженный",
    "transcription": "[frəʊzn]"
  },
  {
    "engWord": "museum",
    "rusWord": "музей",
    "transcription": "[mjuːˈzɪəm]"
  },
  {
    "engWord": "bid",
    "rusWord": "заявка",
    "transcription": "[bɪd]"
  },
  {
    "engWord": "chips",
    "rusWord": "стружка",
    "transcription": "[ʧɪps]"
  },
  {
    "engWord": "worry",
    "rusWord": "беспокоиться",
    "transcription": "[ˈwʌrɪ]"
  },
  {
    "engWord": "worse",
    "rusWord": "хуже",
    "transcription": "[wɜːs]"
  },
  {
    "engWord": "exactly",
    "rusWord": "именно",
    "transcription": "[ɪgˈzæktlɪ]"
  },
  {
    "engWord": "drug",
    "rusWord": "медикамент",
    "transcription": "[drʌg]"
  },
  {
    "engWord": "riding",
    "rusWord": "верховой",
    "transcription": "[ˈraɪdɪŋ]"
  },
  {
    "engWord": "budget",
    "rusWord": "бюджет",
    "transcription": "[ˈbʌʤɪt]"
  },
  {
    "engWord": "pony",
    "rusWord": "пони",
    "transcription": "[ˈpəʊnɪ]"
  },
  {
    "engWord": "took",
    "rusWord": "взял",
    "transcription": "[]"
  },
  {
    "engWord": "filling",
    "rusWord": "заполнение",
    "transcription": "[ˈfɪlɪŋ]"
  },
  {
    "engWord": "herbal",
    "rusWord": "травяной",
    "transcription": "[ˈhɜːbəl]"
  },
  {
    "engWord": "healing",
    "rusWord": "исцеление",
    "transcription": "[ˈhiːlɪŋ]"
  },
  {
    "engWord": "absent",
    "rusWord": "отсутствовать",
    "transcription": "[ˈæbsənt]"
  },
  {
    "engWord": "agile",
    "rusWord": "проворный",
    "transcription": "[ˈæʤaɪl]"
  },
  {
    "engWord": "shilling",
    "rusWord": "шиллинг",
    "transcription": "[ˈʃɪlɪŋ]"
  },
  {
    "engWord": "kitty",
    "rusWord": "котенок",
    "transcription": "[ˈkɪtɪ]"
  },
  {
    "engWord": "overall",
    "rusWord": "общий",
    "transcription": "[ˈəʊvərɔːl]"
  },
  {
    "engWord": "tank",
    "rusWord": "бак",
    "transcription": "[tæŋk]"
  },
  {
    "engWord": "occupation",
    "rusWord": "занятость",
    "transcription": "[ɒkjʊˈpeɪʃn]"
  },
  {
    "engWord": "guilt",
    "rusWord": "чувство вины",
    "transcription": "[gɪlt]"
  },
  {
    "engWord": "accept",
    "rusWord": "принимать",
    "transcription": "[əkˈsept]"
  },
  {
    "engWord": "quite",
    "rusWord": "довольно",
    "transcription": "[kwaɪt]"
  },
  {
    "engWord": "our",
    "rusWord": "наш",
    "transcription": "[ˈaʊə]"
  },
  {
    "engWord": "master",
    "rusWord": "мастер",
    "transcription": "[ˈmɑːstə]"
  },
  {
    "engWord": "burn",
    "rusWord": "жечь",
    "transcription": "[bɜːn]"
  },
  {
    "engWord": "filing",
    "rusWord": "подача",
    "transcription": "[ˈfaɪlɪŋ]"
  },
  {
    "engWord": "humiliated",
    "rusWord": "оскорбленный",
    "transcription": "[hjuːˈmɪlɪeɪtɪd]"
  },
  {
    "engWord": "parsley",
    "rusWord": "петрушка",
    "transcription": "[ˈpɑːslɪ]"
  },
  {
    "engWord": "weed",
    "rusWord": "сорняк",
    "transcription": "[wiːd]"
  },
  {
    "engWord": "irony",
    "rusWord": "ирония",
    "transcription": "[ˈaɪərənɪ]"
  },
  {
    "engWord": "locally",
    "rusWord": "в местном масштабе",
    "transcription": "[ˈləʊkəlɪ]"
  },
  {
    "engWord": "tense",
    "rusWord": "натянутый",
    "transcription": "[tens]"
  },
  {
    "engWord": "honesty",
    "rusWord": "честность",
    "transcription": "[ˈɒnɪstɪ]"
  },
  {
    "engWord": "roof",
    "rusWord": "крыша",
    "transcription": "[ruːf]"
  },
  {
    "engWord": "pigeon",
    "rusWord": "голубь",
    "transcription": "[ˈpɪʤɪn]"
  },
  {
    "engWord": "move",
    "rusWord": "двигаться",
    "transcription": "[muːv]"
  },
  {
    "engWord": "wing",
    "rusWord": "крыло",
    "transcription": "[wɪŋ]"
  },
  {
    "engWord": "wall",
    "rusWord": "стена",
    "transcription": "[wɔːl]"
  },
  {
    "engWord": "race",
    "rusWord": "гонка",
    "transcription": "[reɪs]"
  },
  {
    "engWord": "track",
    "rusWord": "дорожка",
    "transcription": "[træk]"
  },
  {
    "engWord": "book",
    "rusWord": "книга",
    "transcription": "[bʊk]"
  },
  {
    "engWord": "equivalent",
    "rusWord": "аналог",
    "transcription": "[ɪˈkwɪvələnt]"
  },
  {
    "engWord": "tremble",
    "rusWord": "дрожать",
    "transcription": "[trembl]"
  },
  {
    "engWord": "reminder",
    "rusWord": "напоминание",
    "transcription": "[rɪˈmaɪndə]"
  },
  {
    "engWord": "cart",
    "rusWord": "корзина",
    "transcription": "[kɑːt]"
  },
  {
    "engWord": "feet",
    "rusWord": "ноги",
    "transcription": "[]"
  },
  {
    "engWord": "bowling",
    "rusWord": "игра в кегли",
    "transcription": "[ˈbəʊlɪŋ]"
  },
  {
    "engWord": "haul",
    "rusWord": "тащить",
    "transcription": "[hɔːl]"
  },
  {
    "engWord": "incredible",
    "rusWord": "невероятный",
    "transcription": "[ɪnˈkredəbl]"
  },
  {
    "engWord": "compete",
    "rusWord": "конкурировать",
    "transcription": "[kəmˈpiːt]"
  },
  {
    "engWord": "guidance",
    "rusWord": "руководство",
    "transcription": "[ˈgaɪdəns]"
  },
  {
    "engWord": "tragic",
    "rusWord": "трагический",
    "transcription": "[ˈtræʤɪk]"
  },
  {
    "engWord": "blonde",
    "rusWord": "блондин",
    "transcription": "[blɒnd]"
  },
  {
    "engWord": "typical",
    "rusWord": "типичный",
    "transcription": "[ˈtɪpɪkəl]"
  },
  {
    "engWord": "flag",
    "rusWord": "флаг",
    "transcription": "[flæg]"
  },
  {
    "engWord": "nevertheless",
    "rusWord": "однако",
    "transcription": "[nevəðəˈles]"
  },
  {
    "engWord": "index",
    "rusWord": "индекс",
    "transcription": "[ˈɪndeks]"
  },
  {
    "engWord": "bounce",
    "rusWord": "подпрыгивать",
    "transcription": "[baʊns]"
  },
  {
    "engWord": "organic",
    "rusWord": "органический",
    "transcription": "[ɔːˈgænɪk]"
  },
  {
    "engWord": "mixed",
    "rusWord": "смешанный",
    "transcription": "[mɪkst]"
  },
  {
    "engWord": "file",
    "rusWord": "файл",
    "transcription": "[faɪl]"
  },
  {
    "engWord": "access",
    "rusWord": "доступ",
    "transcription": "[ˈækses]"
  },
  {
    "engWord": "pass",
    "rusWord": "проходить",
    "transcription": "[pɑːs]"
  },
  {
    "engWord": "window",
    "rusWord": "окно",
    "transcription": "[ˈwɪndəʊ]"
  },
  {
    "engWord": "doctor",
    "rusWord": "врач",
    "transcription": "[ˈdɒktə]"
  },
  {
    "engWord": "chance",
    "rusWord": "шанс",
    "transcription": "[ʧɑːns]"
  },
  {
    "engWord": "month",
    "rusWord": "месяц",
    "transcription": "[mʌnθ]"
  },
  {
    "engWord": "stew",
    "rusWord": "тушить",
    "transcription": "[stjuː]"
  },
  {
    "engWord": "patch",
    "rusWord": "заплатка",
    "transcription": "[pæʧ]"
  },
  {
    "engWord": "knew",
    "rusWord": "знал",
    "transcription": "[]"
  },
  {
    "engWord": "absolute",
    "rusWord": "абсолютный",
    "transcription": "[ˈæbsəluːt]"
  },
  {
    "engWord": "tractor",
    "rusWord": "трактор",
    "transcription": "[ˈtræktə]"
  },
  {
    "engWord": "caller",
    "rusWord": "абонент",
    "transcription": "[ˈkɔːlə]"
  },
  {
    "engWord": "freshman",
    "rusWord": "студент первого курса",
    "transcription": "[ˈfreʃmən]"
  },
  {
    "engWord": "appetite",
    "rusWord": "аппетит",
    "transcription": "[ˈæpɪtaɪt]"
  },
  {
    "engWord": "substitute",
    "rusWord": "замена",
    "transcription": "[ˈsʌbstɪtjuːt]"
  },
  {
    "engWord": "propose",
    "rusWord": "предлагать",
    "transcription": "[prəˈpəʊz]"
  },
  {
    "engWord": "studio",
    "rusWord": "студия",
    "transcription": "[ˈstjuːdɪəʊ]"
  },
  {
    "engWord": "personally",
    "rusWord": "сам",
    "transcription": "[ˈpɜːs(ə)n(ə)lɪ]"
  },
  {
    "engWord": "potato",
    "rusWord": "картофель",
    "transcription": "[pəˈteɪtəʊ]"
  },
  {
    "engWord": "charity",
    "rusWord": "благотворительная деятельность",
    "transcription": "[ˈʧærɪtɪ]"
  },
  {
    "engWord": "seek",
    "rusWord": "искать",
    "transcription": "[siːk]"
  },
  {
    "engWord": "post office",
    "rusWord": "почтовое отделение",
    "transcription": "[pəʊst ˈɒfɪs]"
  },
  {
    "engWord": "smile",
    "rusWord": "улыбка",
    "transcription": "[smaɪl]"
  },
  {
    "engWord": "ski",
    "rusWord": "кататься на лыжах",
    "transcription": "[]"
  },
  {
    "engWord": "commodity",
    "rusWord": "товар",
    "transcription": "[kəˈmɒdɪtɪ]"
  },
  {
    "engWord": "modest",
    "rusWord": "скромный",
    "transcription": "[ˈmɒdɪst]"
  },
  {
    "engWord": "wit",
    "rusWord": "остроумие",
    "transcription": "[wɪt]"
  },
  {
    "engWord": "ran",
    "rusWord": "побежал",
    "transcription": "[]"
  },
  {
    "engWord": "thrust",
    "rusWord": "тяга",
    "transcription": "[θrʌst]"
  },
  {
    "engWord": "convention",
    "rusWord": "Конвенция",
    "transcription": "[kənˈvenʃn]"
  },
  {
    "engWord": "comment",
    "rusWord": "комментарий",
    "transcription": "[ˈkɒment]"
  },
  {
    "engWord": "version",
    "rusWord": "версия",
    "transcription": "[vɜːʃn]"
  },
  {
    "engWord": "sunshine",
    "rusWord": "солнечный свет",
    "transcription": "[ˈsʌnʃaɪn]"
  },
  {
    "engWord": "obvious",
    "rusWord": "очевидный",
    "transcription": "[ˈɒbvɪəs]"
  },
  {
    "engWord": "connection",
    "rusWord": "соединение",
    "transcription": "[kəˈnekʃn]"
  },
  {
    "engWord": "shock",
    "rusWord": "шок",
    "transcription": "[ʃɒk]"
  },
  {
    "engWord": "inside",
    "rusWord": "внутри",
    "transcription": "[ɪnˈsaɪd]"
  },
  {
    "engWord": "control",
    "rusWord": "контроль",
    "transcription": "[kɒnˈtrəʊl]"
  },
  {
    "engWord": "key",
    "rusWord": "ключ",
    "transcription": "[kiː]"
  },
  {
    "engWord": "idea",
    "rusWord": "идея",
    "transcription": "[aɪˈdɪə]"
  },
  {
    "engWord": "counselling",
    "rusWord": "консультирование",
    "transcription": "[]"
  },
  {
    "engWord": "firmly",
    "rusWord": "твердо",
    "transcription": "[fɜːmli]"
  },
  {
    "engWord": "gave",
    "rusWord": "дал",
    "transcription": "[]"
  },
  {
    "engWord": "babysitter",
    "rusWord": "Няня",
    "transcription": "[ˈbeɪbɪsɪtə]"
  },
  {
    "engWord": "employment",
    "rusWord": "трудоустройство",
    "transcription": "[ɪmˈplɔɪmənt]"
  },
  {
    "engWord": "expansion",
    "rusWord": "расширение",
    "transcription": "[ɪksˈpænʃn]"
  },
  {
    "engWord": "shot",
    "rusWord": "выстрел",
    "transcription": "[ʃɒt]"
  },
  {
    "engWord": "kill",
    "rusWord": "убивать",
    "transcription": "[kɪl]"
  },
  {
    "engWord": "repeat",
    "rusWord": "повторять",
    "transcription": "[rɪˈpiːt]"
  },
  {
    "engWord": "intelligence",
    "rusWord": "интеллект",
    "transcription": "[ɪnˈtelɪʤəns]"
  },
  {
    "engWord": "plague",
    "rusWord": "чума",
    "transcription": "[pleɪg]"
  },
  {
    "engWord": "exact",
    "rusWord": "точное",
    "transcription": "[]"
  },
  {
    "engWord": "entering",
    "rusWord": "входящий",
    "transcription": "[ˈentərɪŋ]"
  },
  {
    "engWord": "technically",
    "rusWord": "технически",
    "transcription": "[ˈteknɪklɪ]"
  },
  {
    "engWord": "conclusion",
    "rusWord": "заключение",
    "transcription": "[kənˈkluːʒən]"
  },
  {
    "engWord": "Wednesday",
    "rusWord": "среда",
    "transcription": "[ˈwenzdɪ]"
  },
  {
    "engWord": "sin",
    "rusWord": "грех",
    "transcription": "[sɪn]"
  },
  {
    "engWord": "response",
    "rusWord": "ответ",
    "transcription": "[rɪsˈpɒns]"
  },
  {
    "engWord": "information",
    "rusWord": "информация",
    "transcription": "[ɪnfəˈmeɪʃn]"
  },
  {
    "engWord": "cash",
    "rusWord": "наличные деньги",
    "transcription": "[kæʃ]"
  },
  {
    "engWord": "purse",
    "rusWord": "кошелек",
    "transcription": "[pɜːs]"
  },
  {
    "engWord": "title",
    "rusWord": "заглавие",
    "transcription": "[taɪtl]"
  },
  {
    "engWord": "Thursday",
    "rusWord": "четверг",
    "transcription": "[ˈθɜːzdɪ]"
  },
  {
    "engWord": "depend",
    "rusWord": "зависеть",
    "transcription": "[dɪˈpend]"
  },
  {
    "engWord": "tone",
    "rusWord": "тон",
    "transcription": "[təʊn]"
  },
  {
    "engWord": "copy",
    "rusWord": "копировать",
    "transcription": "[ˈkɒpɪ]"
  },
  {
    "engWord": "generic",
    "rusWord": "родовой",
    "transcription": "[ʤɪˈnerɪk]"
  },
  {
    "engWord": "deserted",
    "rusWord": "пустынный",
    "transcription": "[dɪˈzɜːtɪd]"
  },
  {
    "engWord": "frost",
    "rusWord": "мороз",
    "transcription": "[frɒst]"
  },
  {
    "engWord": "majesty",
    "rusWord": "величие",
    "transcription": "[ˈmæʤɪstɪ]"
  },
  {
    "engWord": "briefcase",
    "rusWord": "портфель",
    "transcription": "[ˈbriːfkeɪs]"
  },
  {
    "engWord": "wrote",
    "rusWord": "написал",
    "transcription": "[]"
  },
  {
    "engWord": "sour",
    "rusWord": "кислый",
    "transcription": "[ˈsaʊə]"
  },
  {
    "engWord": "compensation",
    "rusWord": "компенсация",
    "transcription": "[kɒmpənˈseɪʃn]"
  },
  {
    "engWord": "stimulate",
    "rusWord": "стимулировать",
    "transcription": "[ˈstɪmjʊleɪt]"
  },
  {
    "engWord": "cozy",
    "rusWord": "уютный",
    "transcription": "[ˈkəʊzɪ]"
  },
  {
    "engWord": "bump",
    "rusWord": "врезаться",
    "transcription": "[bʌmp]"
  },
  {
    "engWord": "sneak",
    "rusWord": "пробираться",
    "transcription": "[sniːk]"
  },
  {
    "engWord": "grandma",
    "rusWord": "бабуля",
    "transcription": "[ˈgrænmɑː]"
  },
  {
    "engWord": "almost",
    "rusWord": "почти",
    "transcription": "[ˈɔːlməʊst]"
  },
  {
    "engWord": "murder",
    "rusWord": "убийство",
    "transcription": "[ˈmɜːdə]"
  },
  {
    "engWord": "logical",
    "rusWord": "логический",
    "transcription": "[ˈlɒʤɪkəl]"
  },
  {
    "engWord": "lend",
    "rusWord": "одалживать",
    "transcription": "[lend]"
  },
  {
    "engWord": "valley",
    "rusWord": "долина",
    "transcription": "[ˈvælɪ]"
  },
  {
    "engWord": "thin",
    "rusWord": "тонкий",
    "transcription": "[θɪn]"
  },
  {
    "engWord": "when",
    "rusWord": "когда",
    "transcription": "[wen]"
  },
  {
    "engWord": "compare",
    "rusWord": "сравнивать",
    "transcription": "[kəmˈpeə]"
  },
  {
    "engWord": "softly",
    "rusWord": "мягко",
    "transcription": "[ˈsɒftlɪ]"
  },
  {
    "engWord": "closest",
    "rusWord": "ближайший",
    "transcription": "[ˈkləʊsɪst]"
  },
  {
    "engWord": "grew",
    "rusWord": "выросла",
    "transcription": "[]"
  },
  {
    "engWord": "registration",
    "rusWord": "регистрация",
    "transcription": "[reʤɪsˈtreɪʃn]"
  },
  {
    "engWord": "calf",
    "rusWord": "теленок",
    "transcription": "[kɑːf]"
  },
  {
    "engWord": "barrel",
    "rusWord": "бочка",
    "transcription": "[ˈbærəl]"
  },
  {
    "engWord": "hobby",
    "rusWord": "хобби",
    "transcription": "[ˈhɒbɪ]"
  },
  {
    "engWord": "fitting",
    "rusWord": "примерка",
    "transcription": "[ˈfɪtɪŋ]"
  },
  {
    "engWord": "debut",
    "rusWord": "дебют",
    "transcription": "[ˈdeɪbuː]"
  },
  {
    "engWord": "efficiency",
    "rusWord": "эффективность",
    "transcription": "[ɪˈfɪʃnsɪ]"
  },
  {
    "engWord": "beforehand",
    "rusWord": "заранее",
    "transcription": "[bɪˈfɔːhænd]"
  },
  {
    "engWord": "flash",
    "rusWord": "вспышка",
    "transcription": "[flæʃ]"
  },
  {
    "engWord": "jacket",
    "rusWord": "куртка",
    "transcription": "[ˈʤækɪt]"
  },
  {
    "engWord": "male",
    "rusWord": "мужчина",
    "transcription": "[meɪl]"
  },
  {
    "engWord": "bet",
    "rusWord": "ставка",
    "transcription": "[bet]"
  },
  {
    "engWord": "photo",
    "rusWord": "фото",
    "transcription": "[ˈfəʊtəʊ]"
  },
  {
    "engWord": "private",
    "rusWord": "частный",
    "transcription": "[ˈpraɪvɪt]"
  },
  {
    "engWord": "coffee",
    "rusWord": "кофе",
    "transcription": "[ˈkɒfɪ]"
  },
  {
    "engWord": "theatre",
    "rusWord": "театр",
    "transcription": "[ˈθɪətə]"
  },
  {
    "engWord": "professional",
    "rusWord": "профессиональный",
    "transcription": "[prəˈfeʃnəl]"
  },
  {
    "engWord": "lift",
    "rusWord": "поднимать",
    "transcription": "[lɪft]"
  },
  {
    "engWord": "poor",
    "rusWord": "бедный",
    "transcription": "[pʊə]"
  },
  {
    "engWord": "family",
    "rusWord": "семья",
    "transcription": "[ˈfæm(ə)lɪ]"
  },
  {
    "engWord": "hope",
    "rusWord": "надеяться",
    "transcription": "[həʊp]"
  },
  {
    "engWord": "feed",
    "rusWord": "кормить",
    "transcription": "[fiːd]"
  },
  {
    "engWord": "inch",
    "rusWord": "вершок",
    "transcription": "[ɪnʧ]"
  },
  {
    "engWord": "card",
    "rusWord": "карта",
    "transcription": "[kɑːd]"
  },
  {
    "engWord": "supply",
    "rusWord": "поставка",
    "transcription": "[səˈplaɪ]"
  },
  {
    "engWord": "refrigerator",
    "rusWord": "холодильник",
    "transcription": "[rɪˈfrɪʤəreɪtə]"
  },
  {
    "engWord": "popularity",
    "rusWord": "популярность",
    "transcription": "[pɒpjʊˈlærɪtɪ]"
  },
  {
    "engWord": "washing",
    "rusWord": "стирка",
    "transcription": "[ˈwɒʃɪŋ]"
  },
  {
    "engWord": "teeth",
    "rusWord": "зубы",
    "transcription": "[]"
  },
  {
    "engWord": "circus",
    "rusWord": "цирк",
    "transcription": "[ˈsɜːkəs]"
  },
  {
    "engWord": "medal",
    "rusWord": "медаль",
    "transcription": "[medl]"
  },
  {
    "engWord": "international",
    "rusWord": "международный",
    "transcription": "[ɪntəˈnæʃnəl]"
  },
  {
    "engWord": "confident",
    "rusWord": "уверенный",
    "transcription": "[ˈkɒnfɪdənt]"
  },
  {
    "engWord": "pressure",
    "rusWord": "давление",
    "transcription": "[ˈpreʃə]"
  },
  {
    "engWord": "mention",
    "rusWord": "упоминать",
    "transcription": "[menʃn]"
  },
  {
    "engWord": "hole",
    "rusWord": "отверстие",
    "transcription": "[həʊl]"
  },
  {
    "engWord": "volcano",
    "rusWord": "вулкан",
    "transcription": "[vɒlˈkeɪnəʊ]"
  },
  {
    "engWord": "ultimately",
    "rusWord": "в конце концов",
    "transcription": "[ˈʌltɪmɪtlɪ]"
  },
  {
    "engWord": "oxygen",
    "rusWord": "кислород",
    "transcription": "[]"
  },
  {
    "engWord": "obese",
    "rusWord": "толстый",
    "transcription": "[əʊˈbiːs]"
  },
  {
    "engWord": "gamble",
    "rusWord": "азартная игра",
    "transcription": "[gæmbl]"
  },
  {
    "engWord": "saddle",
    "rusWord": "седло",
    "transcription": "[sædl]"
  },
  {
    "engWord": "British",
    "rusWord": "британский",
    "transcription": "[ˈbrɪtɪʃ]"
  },
  {
    "engWord": "August",
    "rusWord": "август",
    "transcription": "[ˈɔːgəst]"
  },
  {
    "engWord": "gloomy",
    "rusWord": "мрачный",
    "transcription": "[ˈgluːmɪ]"
  },
  {
    "engWord": "sheriff",
    "rusWord": "шериф",
    "transcription": "[ˈʃerɪf]"
  },
  {
    "engWord": "poverty",
    "rusWord": "бедность",
    "transcription": "[ˈpɒvətɪ]"
  },
  {
    "engWord": "torn",
    "rusWord": "порванный",
    "transcription": "[tɔːn]"
  },
  {
    "engWord": "providing",
    "rusWord": "обеспечение",
    "transcription": "[prəˈvaɪdɪŋ]"
  },
  {
    "engWord": "nearly",
    "rusWord": "приблизительно",
    "transcription": "[ˈnɪəlɪ]"
  },
  {
    "engWord": "tour",
    "rusWord": "тур",
    "transcription": "[tʊə]"
  },
  {
    "engWord": "praise",
    "rusWord": "хвалить",
    "transcription": "[preɪz]"
  },
  {
    "engWord": "shoot",
    "rusWord": "стрелять",
    "transcription": "[ʃuːt]"
  },
  {
    "engWord": "suggest",
    "rusWord": "предлагать",
    "transcription": "[səˈʤest]"
  },
  {
    "engWord": "design",
    "rusWord": "дизайн",
    "transcription": "[dɪˈzaɪn]"
  },
  {
    "engWord": "bell",
    "rusWord": "колокол",
    "transcription": "[bel]"
  },
  {
    "engWord": "potatoes",
    "rusWord": "картофель",
    "transcription": "[]"
  },
  {
    "engWord": "surprisingly",
    "rusWord": "удивительный образом",
    "transcription": "[səˈpraɪzɪŋlɪ]"
  },
  {
    "engWord": "steer",
    "rusWord": "управлять",
    "transcription": "[stɪə]"
  },
  {
    "engWord": "lighter",
    "rusWord": "зажигалка",
    "transcription": "[ˈlaɪtə]"
  },
  {
    "engWord": "mortgage",
    "rusWord": "ипотечный кредит",
    "transcription": "[ˈmɔːgɪʤ]"
  },
  {
    "engWord": "unconscious",
    "rusWord": "без сознания",
    "transcription": "[ʌnˈkɒnʃəs]"
  },
  {
    "engWord": "zero",
    "rusWord": "ноль",
    "transcription": "[ˈzɪərəʊ]"
  },
  {
    "engWord": "declare",
    "rusWord": "объявлять",
    "transcription": "[dɪˈkleə]"
  },
  {
    "engWord": "brief",
    "rusWord": "краткий",
    "transcription": "[briːf]"
  },
  {
    "engWord": "sold",
    "rusWord": "распроданный",
    "transcription": "[səʊld]"
  },
  {
    "engWord": "sake",
    "rusWord": "сакэ",
    "transcription": "[seɪk]"
  },
  {
    "engWord": "duty",
    "rusWord": "обязанность",
    "transcription": "[ˈdjuːtɪ]"
  },
  {
    "engWord": "brown",
    "rusWord": "коричневый",
    "transcription": "[braʊn]"
  },
  {
    "engWord": "nose",
    "rusWord": "нос",
    "transcription": "[nəʊz]"
  },
  {
    "engWord": "space",
    "rusWord": "пространство",
    "transcription": "[speɪs]"
  },
  {
    "engWord": "train",
    "rusWord": "поезд",
    "transcription": "[treɪn]"
  },
  {
    "engWord": "accusation",
    "rusWord": "обвинение",
    "transcription": "[ækjʊˈzeɪʃn]"
  },
  {
    "engWord": "sticky",
    "rusWord": "липкий",
    "transcription": "[ˈstɪkɪ]"
  },
  {
    "engWord": "flew",
    "rusWord": "летал",
    "transcription": "[]"
  },
  {
    "engWord": "crystal",
    "rusWord": "кристалл",
    "transcription": "[krɪstl]"
  },
  {
    "engWord": "nanny",
    "rusWord": "нянюшка",
    "transcription": "[ˈnænɪ]"
  },
  {
    "engWord": "whack",
    "rusWord": "сильный удар",
    "transcription": "[wæk]"
  },
  {
    "engWord": "unfortunately",
    "rusWord": "к несчастью",
    "transcription": "[ʌnˈfɔːʧʊnɪtlɪ]"
  },
  {
    "engWord": "spy",
    "rusWord": "шпион",
    "transcription": "[spaɪ]"
  },
  {
    "engWord": "terror",
    "rusWord": "террор",
    "transcription": "[ˈterə]"
  },
  {
    "engWord": "minister",
    "rusWord": "министр",
    "transcription": "[ˈmɪnɪstə]"
  },
  {
    "engWord": "empire",
    "rusWord": "империя",
    "transcription": "[ˈempaɪə]"
  },
  {
    "engWord": "envelope",
    "rusWord": "конверт",
    "transcription": "[ˈenvələʊp]"
  },
  {
    "engWord": "basement",
    "rusWord": "подвал",
    "transcription": "[ˈbeɪsmənt]"
  },
  {
    "engWord": "native",
    "rusWord": "родной",
    "transcription": "[ˈneɪtɪv]"
  },
  {
    "engWord": "occupy",
    "rusWord": "занимать",
    "transcription": "[ˈɒkjʊpaɪ]"
  },
  {
    "engWord": "brilliant",
    "rusWord": "бриллиант",
    "transcription": "[ˈbrɪlɪənt]"
  },
  {
    "engWord": "treatment",
    "rusWord": "лечение",
    "transcription": "[ˈtriːtmənt]"
  },
  {
    "engWord": "probable",
    "rusWord": "вероятный",
    "transcription": "[ˈprɒbəbl]"
  },
  {
    "engWord": "content",
    "rusWord": "содержание",
    "transcription": "[]"
  },
  {
    "engWord": "canyon",
    "rusWord": "каньон",
    "transcription": "[ˈkænjən]"
  },
  {
    "engWord": "dearest",
    "rusWord": "самый дорогой",
    "transcription": "[ˈdɪərɪst]"
  },
  {
    "engWord": "expense",
    "rusWord": "расход",
    "transcription": "[ɪksˈpens]"
  },
  {
    "engWord": "causless",
    "rusWord": "causless",
    "transcription": "[]"
  },
  {
    "engWord": "killer",
    "rusWord": "убийца",
    "transcription": "[ˈkɪlə]"
  },
  {
    "engWord": "conformity",
    "rusWord": "соответствие",
    "transcription": "[kənˈfɔːmɪtɪ]"
  },
  {
    "engWord": "fortunately",
    "rusWord": "к счастью",
    "transcription": "[ˈfɔːʧənətlɪ]"
  },
  {
    "engWord": "fleet",
    "rusWord": "флот",
    "transcription": "[fliːt]"
  },
  {
    "engWord": "translation",
    "rusWord": "перевод",
    "transcription": "[trænsˈleɪʃən]"
  },
  {
    "engWord": "cab",
    "rusWord": "кэб",
    "transcription": "[kæb]"
  },
  {
    "engWord": "trigger",
    "rusWord": "спусковой крючок",
    "transcription": "[ˈtrɪgə]"
  },
  {
    "engWord": "military",
    "rusWord": "военный",
    "transcription": "[ˈmɪlɪtərɪ]"
  },
  {
    "engWord": "define",
    "rusWord": "определять",
    "transcription": "[dɪˈfaɪn]"
  },
  {
    "engWord": "apologise",
    "rusWord": "извиняться",
    "transcription": "[əˈpɒləʤɪz]"
  },
  {
    "engWord": "technology",
    "rusWord": "технология",
    "transcription": "[tekˈnɒləʤɪ]"
  },
  {
    "engWord": "everyday",
    "rusWord": "повседневный",
    "transcription": "[ˈevrɪdeɪ]"
  },
  {
    "engWord": "major",
    "rusWord": "основной",
    "transcription": "[ˈmeɪʤə]"
  },
  {
    "engWord": "print",
    "rusWord": "печатать",
    "transcription": "[prɪnt]"
  },
  {
    "engWord": "natural",
    "rusWord": "естественный",
    "transcription": "[ˈnæʧrəl]"
  },
  {
    "engWord": "stiff",
    "rusWord": "жесткий",
    "transcription": "[stɪf]"
  },
  {
    "engWord": "nonfiction",
    "rusWord": "документальный",
    "transcription": "[nɒnˈfɪkʃn]"
  },
  {
    "engWord": "drops",
    "rusWord": "драже",
    "transcription": "[drɒps]"
  },
  {
    "engWord": "so-called",
    "rusWord": "так называемый",
    "transcription": "[səʊˈkɔɪld]"
  },
  {
    "engWord": "anonymous",
    "rusWord": "анонимный",
    "transcription": "[əˈnɒnɪməs]"
  },
  {
    "engWord": "dot",
    "rusWord": "точка",
    "transcription": "[dɒt]"
  },
  {
    "engWord": "wishes",
    "rusWord": "пожелания",
    "transcription": "[]"
  },
  {
    "engWord": "teenage",
    "rusWord": "подростковый",
    "transcription": "[ˈtiːneɪʤ]"
  },
  {
    "engWord": "Gemini",
    "rusWord": "близнецы",
    "transcription": "[ˈʤemɪnaɪ]"
  },
  {
    "engWord": "overwork",
    "rusWord": "переутомление",
    "transcription": "[əʊvəˈwɜːk]"
  },
  {
    "engWord": "corridor",
    "rusWord": "коридор",
    "transcription": "[ˈkɒrɪdɔː]"
  },
  {
    "engWord": "whipped",
    "rusWord": "взбитый",
    "transcription": "[wɪpt]"
  },
  {
    "engWord": "bloke",
    "rusWord": "парень",
    "transcription": "[bləʊk]"
  },
  {
    "engWord": "fold",
    "rusWord": "сгибать",
    "transcription": "[fəʊld]"
  },
  {
    "engWord": "adjust",
    "rusWord": "регулировать",
    "transcription": "[əˈʤʌst]"
  },
  {
    "engWord": "precious",
    "rusWord": "драгоценный",
    "transcription": "[ˈpreʃəs]"
  },
  {
    "engWord": "crying",
    "rusWord": "плачущий",
    "transcription": "[ˈkraɪɪŋ]"
  },
  {
    "engWord": "ninety",
    "rusWord": "девяносто",
    "transcription": "[ˈnaɪntɪ]"
  },
  {
    "engWord": "singer",
    "rusWord": "певец",
    "transcription": "[ˈsɪŋə]"
  },
  {
    "engWord": "field",
    "rusWord": "поле",
    "transcription": "[fiːld]"
  },
  {
    "engWord": "quiet",
    "rusWord": "тихий",
    "transcription": "[ˈkwaɪət]"
  },
  {
    "engWord": "other",
    "rusWord": "другие",
    "transcription": "[ˈʌðə]"
  },
  {
    "engWord": "value",
    "rusWord": "значение",
    "transcription": "[ˈvæljuː]"
  },
  {
    "engWord": "morning",
    "rusWord": "утро",
    "transcription": "[ˈmɔːnɪŋ]"
  },
  {
    "engWord": "fir",
    "rusWord": "ель",
    "transcription": "[fɜː]"
  },
  {
    "engWord": "nosy",
    "rusWord": "любопытный",
    "transcription": "[ˈnəʊzɪ]"
  },
  {
    "engWord": "wore",
    "rusWord": "носил",
    "transcription": "[]"
  },
  {
    "engWord": "gadget",
    "rusWord": "прибор",
    "transcription": "[ˈgæʤɪt]"
  },
  {
    "engWord": "warlock",
    "rusWord": "чернокнижник",
    "transcription": "[ˈwɔːlɒk]"
  },
  {
    "engWord": "apply",
    "rusWord": "применять",
    "transcription": "[əˈplaɪ]"
  },
  {
    "engWord": "fuse",
    "rusWord": "взрыватель",
    "transcription": "[fjuːz]"
  },
  {
    "engWord": "inhabitant",
    "rusWord": "обитатель",
    "transcription": "[ɪnˈhæbɪtənt]"
  },
  {
    "engWord": "pursue",
    "rusWord": "преследовать",
    "transcription": "[pəˈsjuː]"
  },
  {
    "engWord": "thief",
    "rusWord": "вор",
    "transcription": "[θiːf]"
  },
  {
    "engWord": "genuine",
    "rusWord": "подлинный",
    "transcription": "[ˈʤenjʊɪn]"
  },
  {
    "engWord": "sweat",
    "rusWord": "пот",
    "transcription": "[swet]"
  },
  {
    "engWord": "merry",
    "rusWord": "веселый",
    "transcription": "[ˈmerɪ]"
  },
  {
    "engWord": "fault",
    "rusWord": "ошибка",
    "transcription": "[fɔːlt]"
  },
  {
    "engWord": "another",
    "rusWord": "другой",
    "transcription": "[əˈnʌðə]"
  },
  {
    "engWord": "may",
    "rusWord": "мочь",
    "transcription": "[meɪ]"
  },
  {
    "engWord": "scrub",
    "rusWord": "скраб",
    "transcription": "[skrʌb]"
  },
  {
    "engWord": "tramp",
    "rusWord": "бродяга",
    "transcription": "[træmp]"
  },
  {
    "engWord": "participation",
    "rusWord": "участие",
    "transcription": "[pɑːtɪsɪˈpeɪʃn]"
  },
  {
    "engWord": "brains",
    "rusWord": "мозги",
    "transcription": "[]"
  },
  {
    "engWord": "precedent",
    "rusWord": "прецедент",
    "transcription": "[ˈpresɪdənt]"
  },
  {
    "engWord": "graduate",
    "rusWord": "выпускник",
    "transcription": "[ˈgræʤʊɪt]"
  },
  {
    "engWord": "rally",
    "rusWord": "ралли",
    "transcription": "[ˈrælɪ]"
  },
  {
    "engWord": "ninth",
    "rusWord": "девятый",
    "transcription": "[naɪnθ]"
  },
  {
    "engWord": "ample",
    "rusWord": "обширный",
    "transcription": "[æmpl]"
  },
  {
    "engWord": "unpack",
    "rusWord": "распаковать",
    "transcription": "[ʌnˈpæk]"
  },
  {
    "engWord": "rack",
    "rusWord": "стеллаж",
    "transcription": "[ræk]"
  },
  {
    "engWord": "metaphor",
    "rusWord": "метафора",
    "transcription": "[ˈmetəfə]"
  },
  {
    "engWord": "reporter",
    "rusWord": "репортер",
    "transcription": "[rɪˈpɔːtə]"
  },
  {
    "engWord": "advertisement",
    "rusWord": "реклама",
    "transcription": "[ədˈvɜːtɪsmənt]"
  },
  {
    "engWord": "encounter",
    "rusWord": "столкновение",
    "transcription": "[ɪnˈkaʊntə]"
  },
  {
    "engWord": "thee",
    "rusWord": "тебя",
    "transcription": "[ðiː]"
  },
  {
    "engWord": "plot",
    "rusWord": "сюжет",
    "transcription": "[plɒt]"
  },
  {
    "engWord": "bigger",
    "rusWord": "больший",
    "transcription": "[ˈbɪgər]"
  },
  {
    "engWord": "situation",
    "rusWord": "ситуация",
    "transcription": "[sɪʧʊˈeɪʃn]"
  },
  {
    "engWord": "laid",
    "rusWord": "проложенный",
    "transcription": "[leɪd]"
  },
  {
    "engWord": "dying",
    "rusWord": "умирающий",
    "transcription": "[ˈdaɪɪŋ]"
  },
  {
    "engWord": "gift",
    "rusWord": "подарок",
    "transcription": "[gɪft]"
  },
  {
    "engWord": "raspberry",
    "rusWord": "малина",
    "transcription": "[ˈrɑːzbərɪ]"
  },
  {
    "engWord": "observe",
    "rusWord": "наблюдать",
    "transcription": "[əbˈzɜːv]"
  },
  {
    "engWord": "tell",
    "rusWord": "рассказывать",
    "transcription": "[tel]"
  },
  {
    "engWord": "spring",
    "rusWord": "весна",
    "transcription": "[sprɪŋ]"
  },
  {
    "engWord": "symbol",
    "rusWord": "символ",
    "transcription": "[ˈsɪmbəl]"
  },
  {
    "engWord": "evening",
    "rusWord": "вечер",
    "transcription": "[ˈiːvnɪŋ]"
  },
  {
    "engWord": "belief",
    "rusWord": "вера",
    "transcription": "[bɪˈliːf]"
  },
  {
    "engWord": "alert",
    "rusWord": "тревога",
    "transcription": "[əˈlɜːt]"
  },
  {
    "engWord": "abnormal",
    "rusWord": "ненормальный",
    "transcription": "[æbˈnɔːməl]"
  },
  {
    "engWord": "goodnight",
    "rusWord": "Спокойной ночи",
    "transcription": "[]"
  },
  {
    "engWord": "abbey",
    "rusWord": "аббатство",
    "transcription": "[ˈæbɪ]"
  },
  {
    "engWord": "sustain",
    "rusWord": "поддерживать",
    "transcription": "[səsˈteɪn]"
  },
  {
    "engWord": "dip",
    "rusWord": "макать",
    "transcription": "[dɪp]"
  },
  {
    "engWord": "marmalade",
    "rusWord": "варенье",
    "transcription": "[ˈmɑːməleɪd]"
  },
  {
    "engWord": "mistaken",
    "rusWord": "ошибочно принимать",
    "transcription": "[mɪsˈteɪkən]"
  },
  {
    "engWord": "refer",
    "rusWord": "относиться",
    "transcription": "[rɪˈfɜː]"
  },
  {
    "engWord": "reduction",
    "rusWord": "сокращение",
    "transcription": "[rɪˈdʌkʃn]"
  },
  {
    "engWord": "inevitably",
    "rusWord": "неизбежно",
    "transcription": "[ɪnˈevɪtəblɪ]"
  },
  {
    "engWord": "frog",
    "rusWord": "лягушка",
    "transcription": "[frɒg]"
  },
  {
    "engWord": "psychology",
    "rusWord": "психология",
    "transcription": "[saɪˈkɒləʤɪ]"
  },
  {
    "engWord": "invite",
    "rusWord": "приглашать",
    "transcription": "[ɪnˈvaɪt]"
  },
  {
    "engWord": "themselves",
    "rusWord": "сами",
    "transcription": "[ðəmˈselvz]"
  },
  {
    "engWord": "mechanic",
    "rusWord": "механический",
    "transcription": "[mɪˈkænɪk]"
  },
  {
    "engWord": "detention",
    "rusWord": "задержание",
    "transcription": "[dɪˈtent(ə)n]"
  },
  {
    "engWord": "fought",
    "rusWord": "воевал",
    "transcription": "[]"
  },
  {
    "engWord": "injured",
    "rusWord": "раненый",
    "transcription": "[ˈɪnʤəd]"
  },
  {
    "engWord": "triple",
    "rusWord": "тройной",
    "transcription": "[trɪpl]"
  },
  {
    "engWord": "annual",
    "rusWord": "ежегодный",
    "transcription": "[ˈænjʊəl]"
  },
  {
    "engWord": "flu",
    "rusWord": "грипп",
    "transcription": "[]"
  },
  {
    "engWord": "inquiry",
    "rusWord": "расследование",
    "transcription": "[ɪnˈkwaɪərɪ]"
  },
  {
    "engWord": "fortnight",
    "rusWord": "две недели",
    "transcription": "[ˈfɔːtnaɪt]"
  },
  {
    "engWord": "gradually",
    "rusWord": "постепенно",
    "transcription": "[ˈgræʤʊəlɪ]"
  },
  {
    "engWord": "fulfil",
    "rusWord": "выполнять",
    "transcription": "[fʊlˈfɪl]"
  },
  {
    "engWord": "address",
    "rusWord": "адрес",
    "transcription": "[əˈdres]"
  },
  {
    "engWord": "aside",
    "rusWord": "в сторону",
    "transcription": "[əˈsaɪd]"
  },
  {
    "engWord": "passion",
    "rusWord": "страсть",
    "transcription": "[pæʃn]"
  },
  {
    "engWord": "margin",
    "rusWord": "прибыль",
    "transcription": "[ˈmɑːʤɪn]"
  },
  {
    "engWord": "politician",
    "rusWord": "политик",
    "transcription": "[pɒlɪˈtɪʃn]"
  },
  {
    "engWord": "bottle",
    "rusWord": "бутылка",
    "transcription": "[bɒtl]"
  },
  {
    "engWord": "truly",
    "rusWord": "действительно",
    "transcription": "[ˈtruːlɪ]"
  },
  {
    "engWord": "dress",
    "rusWord": "платье",
    "transcription": "[dres]"
  },
  {
    "engWord": "enemy",
    "rusWord": "враг",
    "transcription": "[ˈenəmɪ]"
  },
  {
    "engWord": "thick",
    "rusWord": "толстый",
    "transcription": "[θɪk]"
  },
  {
    "engWord": "sidewalk",
    "rusWord": "тротуар",
    "transcription": "[ˈsaɪdwɔːk]"
  },
  {
    "engWord": "update",
    "rusWord": "обновление",
    "transcription": "[ʌpˈdeɪt]"
  },
  {
    "engWord": "T-shirt",
    "rusWord": "Футболка",
    "transcription": "[]"
  },
  {
    "engWord": "hollow",
    "rusWord": "полый",
    "transcription": "[ˈhɒləʊ]"
  },
  {
    "engWord": "earring",
    "rusWord": "серьга",
    "transcription": "[ˈɪərɪŋ]"
  },
  {
    "engWord": "spelling",
    "rusWord": "написание",
    "transcription": "[ˈspelɪŋ]"
  },
  {
    "engWord": "recent",
    "rusWord": "недавний",
    "transcription": "[riːsnt]"
  },
  {
    "engWord": "yell",
    "rusWord": "кричать",
    "transcription": "[jel]"
  },
  {
    "engWord": "chip",
    "rusWord": "чип",
    "transcription": "[ʧɪp]"
  },
  {
    "engWord": "gym",
    "rusWord": "тренажерный зал",
    "transcription": "[ʤɪm]"
  },
  {
    "engWord": "sometimes",
    "rusWord": "иногда",
    "transcription": "[ˈsʌmtaɪmz]"
  },
  {
    "engWord": "peace",
    "rusWord": "мир",
    "transcription": "[piːs]"
  },
  {
    "engWord": "sally",
    "rusWord": "Салли",
    "transcription": "[ˈsælɪ]"
  },
  {
    "engWord": "pencil",
    "rusWord": "карандаш",
    "transcription": "[pensl]"
  },
  {
    "engWord": "scary",
    "rusWord": "страшный",
    "transcription": "[ˈske(ə)rɪ]"
  },
  {
    "engWord": "police",
    "rusWord": "полиция",
    "transcription": "[pəˈliːs]"
  },
  {
    "engWord": "again",
    "rusWord": "снова",
    "transcription": "[əˈgen]"
  },
  {
    "engWord": "fruit",
    "rusWord": "фрукты",
    "transcription": "[fruːt]"
  },
  {
    "engWord": "if",
    "rusWord": "если",
    "transcription": "[ɪf]"
  },
  {
    "engWord": "canvas",
    "rusWord": "холст",
    "transcription": "[ˈkænvəs]"
  },
  {
    "engWord": "underline",
    "rusWord": "подчеркивать",
    "transcription": "[ˈʌndəlaɪn]"
  },
  {
    "engWord": "o’clock",
    "rusWord": "часов",
    "transcription": "[]"
  },
  {
    "engWord": "illusion",
    "rusWord": "иллюзия",
    "transcription": "[ɪˈluːʒən]"
  },
  {
    "engWord": "scholar",
    "rusWord": "ученый",
    "transcription": "[ˈskɒlə]"
  },
  {
    "engWord": "greed",
    "rusWord": "жадность",
    "transcription": "[griːd]"
  },
  {
    "engWord": "climbing",
    "rusWord": "восхождение",
    "transcription": "[ˈklaɪmɪŋ]"
  },
  {
    "engWord": "tickle",
    "rusWord": "щекотка",
    "transcription": "[tɪkl]"
  },
  {
    "engWord": "dehydrated",
    "rusWord": "обезвоженный",
    "transcription": "[diːˈhaɪdreɪtɪd]"
  },
  {
    "engWord": "indoor",
    "rusWord": "в помещении",
    "transcription": "[ˈɪndɔː]"
  },
  {
    "engWord": "debt",
    "rusWord": "долг",
    "transcription": "[det]"
  },
  {
    "engWord": "poison",
    "rusWord": "яд",
    "transcription": "[ˈpɔɪzn]"
  },
  {
    "engWord": "edition",
    "rusWord": "издание",
    "transcription": "[ɪˈdɪʃn]"
  },
  {
    "engWord": "printer",
    "rusWord": "принтер",
    "transcription": "[ˈprɪntə]"
  },
  {
    "engWord": "output",
    "rusWord": "выход",
    "transcription": "[ˈaʊtpʊt]"
  },
  {
    "engWord": "downtown",
    "rusWord": "в центре города",
    "transcription": "[ˈdaʊntaʊn]"
  },
  {
    "engWord": "decent",
    "rusWord": "порядочный",
    "transcription": "[diːsnt]"
  },
  {
    "engWord": "nick",
    "rusWord": "ник",
    "transcription": "[nɪk]"
  },
  {
    "engWord": "medicine",
    "rusWord": "препарат",
    "transcription": "[ˈmeds(ə)n]"
  },
  {
    "engWord": "legal",
    "rusWord": "законный",
    "transcription": "[ˈliːgəl]"
  },
  {
    "engWord": "two",
    "rusWord": "два",
    "transcription": "[tuː]"
  },
  {
    "engWord": "support",
    "rusWord": "поддержка",
    "transcription": "[səˈpɔːt]"
  },
  {
    "engWord": "engine",
    "rusWord": "двигатель",
    "transcription": "[ˈenʤɪn]"
  },
  {
    "engWord": "dodge",
    "rusWord": "изворачиваться",
    "transcription": "[dɒʤ]"
  },
  {
    "engWord": "classify",
    "rusWord": "классифицировать",
    "transcription": "[ˈklæsɪfaɪ]"
  },
  {
    "engWord": "subscribe",
    "rusWord": "подписывать",
    "transcription": "[səbˈskraɪb]"
  },
  {
    "engWord": "gloves",
    "rusWord": "перчатки",
    "transcription": "[]"
  },
  {
    "engWord": "scenery",
    "rusWord": "пейзаж",
    "transcription": "[ˈsiːnərɪ]"
  },
  {
    "engWord": "mend",
    "rusWord": "чинить",
    "transcription": "[mend]"
  },
  {
    "engWord": "wounded",
    "rusWord": "раненый",
    "transcription": "[ˈwuːndɪd]"
  },
  {
    "engWord": "spoiled",
    "rusWord": "испорченный",
    "transcription": "[ˈspɔɪld]"
  },
  {
    "engWord": "smiling",
    "rusWord": "улыбающийся",
    "transcription": "[ˈsmaɪlɪŋ]"
  },
  {
    "engWord": "heavily",
    "rusWord": "тяжело",
    "transcription": "[ˈhevɪlɪ]"
  },
  {
    "engWord": "senior",
    "rusWord": "старший",
    "transcription": "[ˈsiːnɪə]"
  },
  {
    "engWord": "marry",
    "rusWord": "жениться",
    "transcription": "[ˈmærɪ]"
  },
  {
    "engWord": "chase",
    "rusWord": "преследовать",
    "transcription": "[ʧeɪs]"
  },
  {
    "engWord": "milk",
    "rusWord": "молоко",
    "transcription": "[mɪlk]"
  },
  {
    "engWord": "fight",
    "rusWord": "битва",
    "transcription": "[faɪt]"
  },
  {
    "engWord": "pay",
    "rusWord": "платить",
    "transcription": "[peɪ]"
  },
  {
    "engWord": "plural",
    "rusWord": "множественное число",
    "transcription": "[ˈplʊərəl]"
  },
  {
    "engWord": "student",
    "rusWord": "студент",
    "transcription": "[ˈstjuːdənt]"
  },
  {
    "engWord": "why",
    "rusWord": "причина",
    "transcription": "[waɪ]"
  },
  {
    "engWord": "discuss",
    "rusWord": "обсуждать",
    "transcription": "[dɪsˈkʌs]"
  },
  {
    "engWord": "then",
    "rusWord": "затем",
    "transcription": "[ðen]"
  },
  {
    "engWord": "hostage",
    "rusWord": "заложник",
    "transcription": "[ˈhɒstɪʤ]"
  },
  {
    "engWord": "breeze",
    "rusWord": "бриз",
    "transcription": "[briːz]"
  },
  {
    "engWord": "ashame",
    "rusWord": "постыдились",
    "transcription": "[]"
  },
  {
    "engWord": "nightmare",
    "rusWord": "ночной кошмар",
    "transcription": "[ˈnaɪtmeə]"
  },
  {
    "engWord": "accelerate",
    "rusWord": "ускоряться",
    "transcription": "[əkˈseləreɪt]"
  },
  {
    "engWord": "comprehend",
    "rusWord": "постигать",
    "transcription": "[kɒmprɪˈhend]"
  },
  {
    "engWord": "loyalty",
    "rusWord": "лояльность",
    "transcription": "[ˈlɔɪəltɪ]"
  },
  {
    "engWord": "pleasant",
    "rusWord": "приятный",
    "transcription": "[pleznt]"
  },
  {
    "engWord": "graph",
    "rusWord": "диаграмма",
    "transcription": "[græf]"
  },
  {
    "engWord": "peak",
    "rusWord": "пик",
    "transcription": "[piːk]"
  },
  {
    "engWord": "turkey",
    "rusWord": "Турция",
    "transcription": "[ˈtɜːkɪ]"
  },
  {
    "engWord": "crash",
    "rusWord": "крушение",
    "transcription": "[kræʃ]"
  },
  {
    "engWord": "honey",
    "rusWord": "мед",
    "transcription": "[ˈhʌnɪ]"
  },
  {
    "engWord": "lots",
    "rusWord": "много",
    "transcription": "[lɒts]"
  },
  {
    "engWord": "catch",
    "rusWord": "ловить",
    "transcription": "[]"
  },
  {
    "engWord": "dear",
    "rusWord": "дорогой",
    "transcription": "[dɪə]"
  },
  {
    "engWord": "qualification",
    "rusWord": "квалификация",
    "transcription": "[kwɒlɪfɪˈkeɪʃn]"
  },
  {
    "engWord": "cement",
    "rusWord": "цемент",
    "transcription": "[sɪˈment]"
  },
  {
    "engWord": "razor",
    "rusWord": "бритва",
    "transcription": "[ˈreɪzə]"
  },
  {
    "engWord": "whom",
    "rusWord": "кому",
    "transcription": "[]"
  },
  {
    "engWord": "denial",
    "rusWord": "отказ",
    "transcription": "[dɪˈnaɪəl]"
  },
  {
    "engWord": "meadow",
    "rusWord": "луг",
    "transcription": "[ˈmedəʊ]"
  },
  {
    "engWord": "sore",
    "rusWord": "язва",
    "transcription": "[sɔː]"
  },
  {
    "engWord": "lint",
    "rusWord": "корпия",
    "transcription": "[lɪnt]"
  },
  {
    "engWord": "colloquial",
    "rusWord": "разговорный",
    "transcription": "[kəˈləʊkwɪəl]"
  },
  {
    "engWord": "insanity",
    "rusWord": "невменяемость",
    "transcription": "[ɪnˈsænɪtɪ]"
  },
  {
    "engWord": "theoretical",
    "rusWord": "теоретический",
    "transcription": "[θɪəˈretɪkəl]"
  },
  {
    "engWord": "widespread",
    "rusWord": "широко распространенный",
    "transcription": "[ˈwaɪdspred]"
  },
  {
    "engWord": "sex",
    "rusWord": "пол",
    "transcription": "[seks]"
  },
  {
    "engWord": "proof",
    "rusWord": "доказательство",
    "transcription": "[pruːf]"
  },
  {
    "engWord": "closely",
    "rusWord": "тесно",
    "transcription": "[ˈkləʊslɪ]"
  },
  {
    "engWord": "license",
    "rusWord": "лицензия",
    "transcription": "[ˈlaɪsəns]"
  },
  {
    "engWord": "calm",
    "rusWord": "спокойный",
    "transcription": "[kɑːm]"
  },
  {
    "engWord": "what",
    "rusWord": "что",
    "transcription": "[wɒt]"
  },
  {
    "engWord": "baby",
    "rusWord": "младенец",
    "transcription": "[ˈbeɪbɪ]"
  },
  {
    "engWord": "cause",
    "rusWord": "причина",
    "transcription": "[kɔːz]"
  },
  {
    "engWord": "with",
    "rusWord": "с",
    "transcription": "[wɪð]"
  },
  {
    "engWord": "captain",
    "rusWord": "капитан",
    "transcription": "[ˈkæptɪn]"
  },
  {
    "engWord": "floor",
    "rusWord": "этаж",
    "transcription": "[flɔː]"
  },
  {
    "engWord": "descendent",
    "rusWord": "потомок",
    "transcription": "[dɪˈsendənt]"
  },
  {
    "engWord": "paternity",
    "rusWord": "отцовство",
    "transcription": "[pəˈtɜːnɪtɪ]"
  },
  {
    "engWord": "representation",
    "rusWord": "представление",
    "transcription": "[reprɪzenˈteɪʃn]"
  },
  {
    "engWord": "veteran",
    "rusWord": "ветеран",
    "transcription": "[ˈvetərən]"
  },
  {
    "engWord": "chose",
    "rusWord": "выбрал",
    "transcription": "[]"
  },
  {
    "engWord": "significantly",
    "rusWord": "существенно",
    "transcription": "[sɪgˈnɪfɪkəntlɪ]"
  },
  {
    "engWord": "measurement",
    "rusWord": "измерение",
    "transcription": "[ˈmeʒəmənt]"
  },
  {
    "engWord": "trail",
    "rusWord": "след",
    "transcription": "[treɪl]"
  },
  {
    "engWord": "breast",
    "rusWord": "грудная клетка",
    "transcription": "[brest]"
  },
  {
    "engWord": "smooth",
    "rusWord": "гладкий",
    "transcription": "[smuːð]"
  },
  {
    "engWord": "newspaper",
    "rusWord": "газета",
    "transcription": "[ˈnjuːspeɪpə]"
  },
  {
    "engWord": "democracy",
    "rusWord": "демократия",
    "transcription": "[dɪˈmɒkrəsɪ]"
  },
  {
    "engWord": "collection",
    "rusWord": "коллекция",
    "transcription": "[kəˈlekʃn]"
  },
  {
    "engWord": "convicted",
    "rusWord": "осужденный",
    "transcription": "[kənˈvɪktɪd]"
  },
  {
    "engWord": "tired",
    "rusWord": "уставший",
    "transcription": "[ˈtaɪəd]"
  },
  {
    "engWord": "mall",
    "rusWord": "тц",
    "transcription": "[mɔːl]"
  },
  {
    "engWord": "committed",
    "rusWord": "преданный",
    "transcription": "[kəˈmɪtɪd]"
  },
  {
    "engWord": "diary",
    "rusWord": "дневник",
    "transcription": "[ˈdaɪərɪ]"
  },
  {
    "engWord": "totally",
    "rusWord": "полностью",
    "transcription": "[ˈtəʊtəlɪ]"
  },
  {
    "engWord": "united",
    "rusWord": "объединенный",
    "transcription": "[ˈjʊˈnaɪtɪd]"
  },
  {
    "engWord": "likely",
    "rusWord": "возможный",
    "transcription": "[ˈlaɪklɪ]"
  },
  {
    "engWord": "cook",
    "rusWord": "готовить",
    "transcription": "[kʊk]"
  },
  {
    "engWord": "bread",
    "rusWord": "хлеб",
    "transcription": "[bred]"
  },
  {
    "engWord": "stand",
    "rusWord": "стоять",
    "transcription": "[stænd]"
  },
  {
    "engWord": "pick",
    "rusWord": "выбирать",
    "transcription": "[pɪk]"
  },
  {
    "engWord": "bitter",
    "rusWord": "горький",
    "transcription": "[ˈbɪtə]"
  },
  {
    "engWord": "woke",
    "rusWord": "проснулся",
    "transcription": "[]"
  },
  {
    "engWord": "breathe",
    "rusWord": "дышать",
    "transcription": "[briːð]"
  },
  {
    "engWord": "lily",
    "rusWord": "лилия",
    "transcription": "[ˈlɪlɪ]"
  },
  {
    "engWord": "shortage",
    "rusWord": "нехватка",
    "transcription": "[ˈʃɔːtɪʤ]"
  },
  {
    "engWord": "smaller",
    "rusWord": "меньше",
    "transcription": "[ˈsmɔːlə]"
  },
  {
    "engWord": "legitimate",
    "rusWord": "легитимный",
    "transcription": "[lɪˈʤɪtɪmɪt]"
  },
  {
    "engWord": "erase",
    "rusWord": "стирать",
    "transcription": "[ɪˈreɪz]"
  },
  {
    "engWord": "mail",
    "rusWord": "почта",
    "transcription": "[meɪl]"
  },
  {
    "engWord": "homework",
    "rusWord": "домашнее задание",
    "transcription": "[ˈhəʊmwɜːk]"
  },
  {
    "engWord": "parcel",
    "rusWord": "посылка",
    "transcription": "[pɑːsl]"
  },
  {
    "engWord": "raise",
    "rusWord": "поднимать",
    "transcription": "[reɪz]"
  },
  {
    "engWord": "hit",
    "rusWord": "ударить",
    "transcription": "[hɪt]"
  },
  {
    "engWord": "coat",
    "rusWord": "пальто",
    "transcription": "[kəʊt]"
  },
  {
    "engWord": "every",
    "rusWord": "каждый",
    "transcription": "[ˈevrɪ]"
  },
  {
    "engWord": "reflection",
    "rusWord": "отражение",
    "transcription": "[rɪˈflekʃn]"
  },
  {
    "engWord": "became",
    "rusWord": "стал",
    "transcription": "[]"
  },
  {
    "engWord": "texture",
    "rusWord": "текстура",
    "transcription": "[ˈteksʧə]"
  },
  {
    "engWord": "confidential",
    "rusWord": "секретный",
    "transcription": "[kɒnfɪˈdenʃəl]"
  },
  {
    "engWord": "groom",
    "rusWord": "жених",
    "transcription": "[grum]"
  },
  {
    "engWord": "haste",
    "rusWord": "поспешность",
    "transcription": "[heɪst]"
  },
  {
    "engWord": "presentation",
    "rusWord": "демонстрация",
    "transcription": "[prez(ə)nˈteɪʃn]"
  },
  {
    "engWord": "hint",
    "rusWord": "подсказка",
    "transcription": "[hɪnt]"
  },
  {
    "engWord": "drawn",
    "rusWord": "нарисованный",
    "transcription": "[drɔːn]"
  },
  {
    "engWord": "foreign",
    "rusWord": "иностранный",
    "transcription": "[ˈfɒrɪn]"
  },
  {
    "engWord": "restriction",
    "rusWord": "ограничение",
    "transcription": "[rɪsˈtrɪkʃn]"
  },
  {
    "engWord": "reception",
    "rusWord": "прием",
    "transcription": "[rɪˈsepʃn]"
  },
  {
    "engWord": "massive",
    "rusWord": "массивный",
    "transcription": "[ˈmæsɪv]"
  },
  {
    "engWord": "everything",
    "rusWord": "все",
    "transcription": "[ˈevrɪθɪŋ]"
  },
  {
    "engWord": "basically",
    "rusWord": "в основном",
    "transcription": "[ˈbeɪsɪkəlɪ]"
  },
  {
    "engWord": "anywhere",
    "rusWord": "везде",
    "transcription": "[ˈenɪweə]"
  },
  {
    "engWord": "beautiful",
    "rusWord": "красивый",
    "transcription": "[ˈbjuːtɪf(ə)l]"
  },
  {
    "engWord": "meaning",
    "rusWord": "значение",
    "transcription": "[ˈmiːnɪŋ]"
  },
  {
    "engWord": "during",
    "rusWord": "в течение",
    "transcription": "[ˈdjʊərɪŋ]"
  },
  {
    "engWord": "wrong",
    "rusWord": "неправильный",
    "transcription": "[rɒŋ]"
  },
  {
    "engWord": "ask",
    "rusWord": "спрашивать",
    "transcription": "[ɑːsk]"
  },
  {
    "engWord": "want",
    "rusWord": "хотеть",
    "transcription": "[wɒnt]"
  },
  {
    "engWord": "glad",
    "rusWord": "радостный",
    "transcription": "[glæd]"
  },
  {
    "engWord": "can",
    "rusWord": "мочь",
    "transcription": "[kæn]"
  },
  {
    "engWord": "there",
    "rusWord": "там",
    "transcription": "[ðeə]"
  },
  {
    "engWord": "mobile",
    "rusWord": "мобильный",
    "transcription": "[ˈməʊbaɪl]"
  },
  {
    "engWord": "Celsius",
    "rusWord": "Цельсий",
    "transcription": "[ˈselsɪəs]"
  },
  {
    "engWord": "slang",
    "rusWord": "сленг",
    "transcription": "[slæŋ]"
  },
  {
    "engWord": "hose",
    "rusWord": "шланг",
    "transcription": "[həʊz]"
  },
  {
    "engWord": "lips",
    "rusWord": "губы",
    "transcription": "[]"
  },
  {
    "engWord": "hush",
    "rusWord": "тишина",
    "transcription": "[hʌʃ]"
  },
  {
    "engWord": "overload",
    "rusWord": "перегрузка",
    "transcription": "[ˈəʊvələʊd]"
  },
  {
    "engWord": "entertainment",
    "rusWord": "развлечение",
    "transcription": "[entəˈteɪnmənt]"
  },
  {
    "engWord": "scrape",
    "rusWord": "царапина",
    "transcription": "[skreɪp]"
  },
  {
    "engWord": "squirrel",
    "rusWord": "белка",
    "transcription": "[ˈskwɪrəl]"
  },
  {
    "engWord": "ego",
    "rusWord": "эго",
    "transcription": "[ˈiːgəʊ]"
  },
  {
    "engWord": "bound",
    "rusWord": "связанный",
    "transcription": "[baʊnd]"
  },
  {
    "engWord": "owe",
    "rusWord": "быть должным",
    "transcription": "[əʊ]"
  },
  {
    "engWord": "till",
    "rusWord": "пока",
    "transcription": "[tɪl]"
  },
  {
    "engWord": "selection",
    "rusWord": "выбор",
    "transcription": "[sɪˈlekʃn]"
  },
  {
    "engWord": "quit",
    "rusWord": "выходить",
    "transcription": "[kwɪt]"
  },
  {
    "engWord": "thirty",
    "rusWord": "тридцать",
    "transcription": "[ˈθɜːtɪ]"
  },
  {
    "engWord": "celebrate",
    "rusWord": "праздновать",
    "transcription": "[ˈselɪbreɪt]"
  },
  {
    "engWord": "business",
    "rusWord": "бизнес",
    "transcription": "[ˈbɪznɪs]"
  },
  {
    "engWord": "look",
    "rusWord": "смотреть",
    "transcription": "[lʊk]"
  },
  {
    "engWord": "gratitude",
    "rusWord": "благодарность",
    "transcription": "[ˈgrætɪtjuːd]"
  },
  {
    "engWord": "grudge",
    "rusWord": "зависть",
    "transcription": "[grʌʤ]"
  },
  {
    "engWord": "threw",
    "rusWord": "бросил",
    "transcription": "[]"
  },
  {
    "engWord": "development",
    "rusWord": "развитие",
    "transcription": "[dɪˈveləpmənt]"
  },
  {
    "engWord": "variety",
    "rusWord": "разнообразие",
    "transcription": "[vəˈraɪətɪ]"
  },
  {
    "engWord": "worm",
    "rusWord": "червь",
    "transcription": "[wɜːm]"
  },
  {
    "engWord": "misery",
    "rusWord": "страдание",
    "transcription": "[ˈmɪzərɪ]"
  },
  {
    "engWord": "foundation",
    "rusWord": "основа",
    "transcription": "[faʊnˈdeɪʃn]"
  },
  {
    "engWord": "birthday",
    "rusWord": "день рождения",
    "transcription": "[ˈbɜːθdeɪ]"
  },
  {
    "engWord": "usually",
    "rusWord": "обычно",
    "transcription": "[ˈjuːʒʊəlɪ]"
  },
  {
    "engWord": "rare",
    "rusWord": "редкий",
    "transcription": "[reə]"
  },
  {
    "engWord": "pan",
    "rusWord": "кастрюля",
    "transcription": "[pæn]"
  },
  {
    "engWord": "brave",
    "rusWord": "храбрый",
    "transcription": "[breɪv]"
  },
  {
    "engWord": "trash",
    "rusWord": "мусор",
    "transcription": "[træʃ]"
  },
  {
    "engWord": "held",
    "rusWord": "удерживаемый",
    "transcription": "[held]"
  },
  {
    "engWord": "jump",
    "rusWord": "скакать",
    "transcription": "[ʤʌmp]"
  },
  {
    "engWord": "parent",
    "rusWord": "родитель",
    "transcription": "[ˈpe(ə)rənt]"
  },
  {
    "engWord": "touch",
    "rusWord": "прикасаться",
    "transcription": "[tʌʧ]"
  },
  {
    "engWord": "choke",
    "rusWord": "задыхаться",
    "transcription": "[ʧəʊk]"
  },
  {
    "engWord": "cannot",
    "rusWord": "не может",
    "transcription": "[]"
  },
  {
    "engWord": "extinguish",
    "rusWord": "затушить",
    "transcription": "[ɪksˈtɪŋgwɪʃ]"
  },
  {
    "engWord": "cooked",
    "rusWord": "приготовленный",
    "transcription": "[kʊkt]"
  },
  {
    "engWord": "vitamin",
    "rusWord": "витамин",
    "transcription": "[ˈvɪtəmɪn]"
  },
  {
    "engWord": "esteem",
    "rusWord": "уважение",
    "transcription": "[ɪsˈtiːm]"
  },
  {
    "engWord": "dizzy",
    "rusWord": "головокружительный",
    "transcription": "[ˈdɪzɪ]"
  },
  {
    "engWord": "afterward",
    "rusWord": "потом",
    "transcription": "[ˈɑːftəwəd]"
  },
  {
    "engWord": "arrangement",
    "rusWord": "расположение",
    "transcription": "[əˈreɪnʤmənt]"
  },
  {
    "engWord": "sack",
    "rusWord": "мешок",
    "transcription": "[sæk]"
  },
  {
    "engWord": "flesh",
    "rusWord": "плоть",
    "transcription": "[fleʃ]"
  },
  {
    "engWord": "opinion",
    "rusWord": "мнение",
    "transcription": "[əˈpɪnjən]"
  },
  {
    "engWord": "bunch",
    "rusWord": "пучок",
    "transcription": "[bʌntl]"
  },
  {
    "engWord": "breakfast",
    "rusWord": "завтрак",
    "transcription": "[ˈbrekfəst]"
  },
  {
    "engWord": "rescue",
    "rusWord": "спасение",
    "transcription": "[ˈreskjuː]"
  },
  {
    "engWord": "lunch",
    "rusWord": "обед",
    "transcription": "[lʌnʧ]"
  },
  {
    "engWord": "interrupt",
    "rusWord": "прерывать",
    "transcription": "[ɪntəˈrʌpt]"
  },
  {
    "engWord": "suit",
    "rusWord": "подходить",
    "transcription": "[sjuːt]"
  },
  {
    "engWord": "camp",
    "rusWord": "лагерь",
    "transcription": "[kæmp]"
  },
  {
    "engWord": "class",
    "rusWord": "класс",
    "transcription": "[klɑːs]"
  },
  {
    "engWord": "industry",
    "rusWord": "промышленность",
    "transcription": "[ˈɪndəstrɪ]"
  },
  {
    "engWord": "idiom",
    "rusWord": "идиома",
    "transcription": "[ˈɪdɪəm]"
  },
  {
    "engWord": "decoy",
    "rusWord": "приманка",
    "transcription": "[dɪˈkɔɪ]"
  },
  {
    "engWord": "participant",
    "rusWord": "участник",
    "transcription": "[pɑːˈtɪsɪpənt]"
  },
  {
    "engWord": "eighth",
    "rusWord": "восьмой",
    "transcription": "[eɪtθ]"
  },
  {
    "engWord": "simultaneous",
    "rusWord": "одновременный",
    "transcription": "[sɪm(ə)lˈteɪnɪəs]"
  },
  {
    "engWord": "sooner",
    "rusWord": "рано",
    "transcription": "[]"
  },
  {
    "engWord": "handwriting",
    "rusWord": "почерк",
    "transcription": "[ˈhændraɪtɪŋ]"
  },
  {
    "engWord": "nearest",
    "rusWord": "ближайший",
    "transcription": "[ˈnɪərɪst]"
  },
  {
    "engWord": "mist",
    "rusWord": "туман",
    "transcription": "[mɪst]"
  },
  {
    "engWord": "vulnerable",
    "rusWord": "уязвимый",
    "transcription": "[ˈvʌln(ə)rəb(ə)l]"
  },
  {
    "engWord": "ourselves",
    "rusWord": "сами",
    "transcription": "[aʊəˈselvz]"
  },
  {
    "engWord": "classic",
    "rusWord": "классический",
    "transcription": "[ˈklæsɪk]"
  },
  {
    "engWord": "pole",
    "rusWord": "полюс",
    "transcription": "[pəʊl]"
  },
  {
    "engWord": "carter",
    "rusWord": "возчик",
    "transcription": "[ˈkɑːtə]"
  },
  {
    "engWord": "infection",
    "rusWord": "инфекция",
    "transcription": "[ɪnˈfekʃn]"
  },
  {
    "engWord": "prior",
    "rusWord": "прежде",
    "transcription": "[ˈpraɪə]"
  },
  {
    "engWord": "worst",
    "rusWord": "наихудший",
    "transcription": "[wɜːst]"
  },
  {
    "engWord": "reputation",
    "rusWord": "репутация",
    "transcription": "[repjʊˈteɪʃn]"
  },
  {
    "engWord": "behave",
    "rusWord": "вести себя",
    "transcription": "[bɪˈheɪv]"
  },
  {
    "engWord": "meeting",
    "rusWord": "встреча",
    "transcription": "[ˈmiːtɪŋ]"
  },
  {
    "engWord": "odd",
    "rusWord": "странный",
    "transcription": "[ɒd]"
  },
  {
    "engWord": "road",
    "rusWord": "дорога",
    "transcription": "[rəʊd]"
  },
  {
    "engWord": "try",
    "rusWord": "пробовать",
    "transcription": "[traɪ]"
  },
  {
    "engWord": "temperature",
    "rusWord": "температура",
    "transcription": "[ˈtemp(ə)rəʧə]"
  },
  {
    "engWord": "entry",
    "rusWord": "вход",
    "transcription": "[ˈentrɪ]"
  },
  {
    "engWord": "shoes",
    "rusWord": "обувь",
    "transcription": "[]"
  },
  {
    "engWord": "production",
    "rusWord": "производство",
    "transcription": "[prəˈdʌkʃn]"
  },
  {
    "engWord": "calculation",
    "rusWord": "расчет",
    "transcription": "[kælkjʊˈleɪʃn]"
  },
  {
    "engWord": "confidence",
    "rusWord": "уверенность",
    "transcription": "[ˈkɒnfɪdəns]"
  },
  {
    "engWord": "pizza",
    "rusWord": "пицца",
    "transcription": "[ˈpiːtsə]"
  },
  {
    "engWord": "pop",
    "rusWord": "хлопок",
    "transcription": "[pɒp]"
  },
  {
    "engWord": "wardrobe",
    "rusWord": "гардероб",
    "transcription": "[ˈwɔːdrəʊb]"
  },
  {
    "engWord": "impression",
    "rusWord": "впечатление",
    "transcription": "[ɪmˈpreʃn]"
  },
  {
    "engWord": "sugar",
    "rusWord": "сахар",
    "transcription": "[ˈʃʊgə]"
  },
  {
    "engWord": "chart",
    "rusWord": "диаграмма",
    "transcription": "[ʧɑːt]"
  },
  {
    "engWord": "exclusive",
    "rusWord": "эксклюзивный",
    "transcription": "[ɪksˈkluːsɪv]"
  },
  {
    "engWord": "wealthy",
    "rusWord": "состоятельный",
    "transcription": "[ˈwelθɪ]"
  },
  {
    "engWord": "wives",
    "rusWord": "жен",
    "transcription": "[]"
  },
  {
    "engWord": "forbid",
    "rusWord": "запретить",
    "transcription": "[fəˈbɪd]"
  },
  {
    "engWord": "ace",
    "rusWord": "туз",
    "transcription": "[eɪs]"
  },
  {
    "engWord": "punctual",
    "rusWord": "пунктуальный",
    "transcription": "[ˈpʌŋkʧʊəl]"
  },
  {
    "engWord": "intruder",
    "rusWord": "нарушитель",
    "transcription": "[ɪnˈtruːdə]"
  },
  {
    "engWord": "massage",
    "rusWord": "массаж",
    "transcription": "[ˈmæsɑːʒ]"
  },
  {
    "engWord": "apologizing",
    "rusWord": "извинение",
    "transcription": "[]"
  },
  {
    "engWord": "detective",
    "rusWord": "детектив",
    "transcription": "[dɪˈtektɪv]"
  },
  {
    "engWord": "immediate",
    "rusWord": "немедленный",
    "transcription": "[ɪˈmiːdɪət]"
  },
  {
    "engWord": "savage",
    "rusWord": "дикарь",
    "transcription": "[ˈsævɪʤ]"
  },
  {
    "engWord": "cheer",
    "rusWord": "подбадривать",
    "transcription": "[ʧɪə]"
  },
  {
    "engWord": "rolling",
    "rusWord": "катающийся",
    "transcription": "[ˈrəʊlɪŋ]"
  },
  {
    "engWord": "candle",
    "rusWord": "свеча",
    "transcription": "[kændl]"
  },
  {
    "engWord": "stock",
    "rusWord": "склад",
    "transcription": "[stɒk]"
  },
  {
    "engWord": "perfectly",
    "rusWord": "идеально",
    "transcription": "[ˈpɜːfɪktlɪ]"
  },
  {
    "engWord": "bother",
    "rusWord": "беспокоить",
    "transcription": "[ˈbɒðə]"
  },
  {
    "engWord": "create",
    "rusWord": "творить",
    "transcription": "[krɪˈeɪt]"
  },
  {
    "engWord": "wash",
    "rusWord": "мыть",
    "transcription": "[wɒʃ]"
  },
  {
    "engWord": "very",
    "rusWord": "очень",
    "transcription": "[ˈverɪ]"
  },
  {
    "engWord": "four",
    "rusWord": "четыре",
    "transcription": "[fɔː]"
  },
  {
    "engWord": "item",
    "rusWord": "пункт",
    "transcription": "[ˈaɪtəm]"
  },
  {
    "engWord": "horizon",
    "rusWord": "горизонт",
    "transcription": "[həˈraɪzn]"
  },
  {
    "engWord": "prick",
    "rusWord": "колоть",
    "transcription": "[prɪk]"
  },
  {
    "engWord": "dealt",
    "rusWord": "рассматриваются",
    "transcription": "[]"
  },
  {
    "engWord": "predominant",
    "rusWord": "преобладающий",
    "transcription": "[prɪˈdɒmɪnənt]"
  },
  {
    "engWord": "teen",
    "rusWord": "подросток",
    "transcription": "[tiːn]"
  },
  {
    "engWord": "betrayal",
    "rusWord": "предательство",
    "transcription": "[bɪˈtreɪəl]"
  },
  {
    "engWord": "betray",
    "rusWord": "предавать",
    "transcription": "[bɪˈtreɪ]"
  },
  {
    "engWord": "visitor",
    "rusWord": "посетитель",
    "transcription": "[ˈvɪzɪtə]"
  },
  {
    "engWord": "spike",
    "rusWord": "шип",
    "transcription": "[spaɪk]"
  },
  {
    "engWord": "vice",
    "rusWord": "порок",
    "transcription": "[vaɪs]"
  },
  {
    "engWord": "hers",
    "rusWord": "ее",
    "transcription": "[hɜːz]"
  },
  {
    "engWord": "notably",
    "rusWord": "особенно",
    "transcription": "[ˈnəʊtəblɪ]"
  },
  {
    "engWord": "ceremony",
    "rusWord": "церемония",
    "transcription": "[ˈserɪmənɪ]"
  },
  {
    "engWord": "become",
    "rusWord": "становиться",
    "transcription": "[bɪˈkʌm]"
  },
  {
    "engWord": "prom",
    "rusWord": "выпускной",
    "transcription": "[prɒm]"
  },
  {
    "engWord": "pure",
    "rusWord": "чистый",
    "transcription": "[pjʊə]"
  },
  {
    "engWord": "everybody",
    "rusWord": "все",
    "transcription": "[ˈevrɪbɒdɪ]"
  },
  {
    "engWord": "school",
    "rusWord": "школа",
    "transcription": "[skuːl]"
  },
  {
    "engWord": "big",
    "rusWord": "большой",
    "transcription": "[bɪg]"
  },
  {
    "engWord": "metal",
    "rusWord": "металл",
    "transcription": "[metl]"
  },
  {
    "engWord": "always",
    "rusWord": "всегда",
    "transcription": "[ˈɔːlw(e)ɪz]"
  },
  {
    "engWord": "marsh",
    "rusWord": "болото",
    "transcription": "[mɑːʃ]"
  },
  {
    "engWord": "hid",
    "rusWord": "спрятал",
    "transcription": "[]"
  },
  {
    "engWord": "conflict",
    "rusWord": "конфликт",
    "transcription": "[ˈkɒnflɪkt]"
  },
  {
    "engWord": "bishop",
    "rusWord": "епископ",
    "transcription": "[ˈbɪʃəp]"
  },
  {
    "engWord": "abstract",
    "rusWord": "абстрактный",
    "transcription": "[ˈæbstrækt]"
  },
  {
    "engWord": "parallel",
    "rusWord": "параллельный",
    "transcription": "[ˈpærəlel]"
  },
  {
    "engWord": "dairy",
    "rusWord": "молочный",
    "transcription": "[ˈdeərɪ]"
  },
  {
    "engWord": "hike",
    "rusWord": "гулять",
    "transcription": "[haɪk]"
  },
  {
    "engWord": "gut",
    "rusWord": "кишка",
    "transcription": "[gʌt]"
  },
  {
    "engWord": "grant",
    "rusWord": "грант",
    "transcription": "[grɑːnt]"
  },
  {
    "engWord": "beetle",
    "rusWord": "жук",
    "transcription": "[biːtl]"
  },
  {
    "engWord": "volume",
    "rusWord": "объем",
    "transcription": "[ˈvɒljuːm]"
  },
  {
    "engWord": "stage",
    "rusWord": "этап",
    "transcription": "[steɪʤ]"
  },
  {
    "engWord": "chicken",
    "rusWord": "курица",
    "transcription": "[ˈʧɪkɪn]"
  },
  {
    "engWord": "roll",
    "rusWord": "рулон",
    "transcription": "[rəʊl]"
  },
  {
    "engWord": "we",
    "rusWord": "мы",
    "transcription": "[wiː]"
  },
  {
    "engWord": "ease",
    "rusWord": "легкость",
    "transcription": "[iːz]"
  },
  {
    "engWord": "laser",
    "rusWord": "лазер",
    "transcription": "[ˈleɪzə]"
  },
  {
    "engWord": "command",
    "rusWord": "команда",
    "transcription": "[kəˈmɑːnd]"
  },
  {
    "engWord": "herb",
    "rusWord": "трава",
    "transcription": "[hɜːb]"
  },
  {
    "engWord": "drank",
    "rusWord": "пили",
    "transcription": "[]"
  },
  {
    "engWord": "pumpkin",
    "rusWord": "тыква",
    "transcription": "[ˈpʌmpkɪn]"
  },
  {
    "engWord": "module",
    "rusWord": "модуль",
    "transcription": "[ˈmɒdjuːl]"
  },
  {
    "engWord": "testimony",
    "rusWord": "свидетельство",
    "transcription": "[ˈtestɪmənɪ]"
  },
  {
    "engWord": "despise",
    "rusWord": "презирать",
    "transcription": "[dɪsˈpaɪz]"
  },
  {
    "engWord": "sorrow",
    "rusWord": "печаль",
    "transcription": "[ˈsɒrəʊ]"
  },
  {
    "engWord": "peaceful",
    "rusWord": "мирный",
    "transcription": "[ˈpiːsf(ə)l]"
  },
  {
    "engWord": "worries",
    "rusWord": "треволнения",
    "transcription": "[ˈwʌriz]"
  },
  {
    "engWord": "gear",
    "rusWord": "механизм",
    "transcription": "[gɪə]"
  },
  {
    "engWord": "chill",
    "rusWord": "расслабляться",
    "transcription": "[ʧɪl]"
  },
  {
    "engWord": "player",
    "rusWord": "игрок",
    "transcription": "[ˈpleɪə]"
  },
  {
    "engWord": "ability",
    "rusWord": "способность",
    "transcription": "[əˈbɪlɪtɪ]"
  },
  {
    "engWord": "perform",
    "rusWord": "выполнять",
    "transcription": "[pəˈfɔːm]"
  },
  {
    "engWord": "landlord",
    "rusWord": "хозяин квартиры",
    "transcription": "[ˈlændlɔːd]"
  },
  {
    "engWord": "each",
    "rusWord": "каждый",
    "transcription": "[iːʧ]"
  },
  {
    "engWord": "edge",
    "rusWord": "край",
    "transcription": "[eʤ]"
  },
  {
    "engWord": "inspiration",
    "rusWord": "вдохновение",
    "transcription": "[ɪnspɪˈreɪʃn]"
  },
  {
    "engWord": "significant",
    "rusWord": "значительный",
    "transcription": "[sɪgˈnɪfɪkənt]"
  },
  {
    "engWord": "subsequent",
    "rusWord": "последующий",
    "transcription": "[ˈsʌbsɪkwənt]"
  },
  {
    "engWord": "distrustul",
    "rusWord": "distrustul",
    "transcription": "[]"
  },
  {
    "engWord": "hysterical",
    "rusWord": "истерический",
    "transcription": "[hɪsˈterɪkəl]"
  },
  {
    "engWord": "awkward",
    "rusWord": "неловкий",
    "transcription": "[ˈɔːkwəd]"
  },
  {
    "engWord": "transition",
    "rusWord": "переход",
    "transcription": "[]"
  },
  {
    "engWord": "upside",
    "rusWord": "потенциал",
    "transcription": "[ˈʌpsaɪd]"
  },
  {
    "engWord": "fond",
    "rusWord": "любящий",
    "transcription": "[fɒnd]"
  },
  {
    "engWord": "primary",
    "rusWord": "первичный",
    "transcription": "[ˈpraɪmərɪ]"
  },
  {
    "engWord": "percentage",
    "rusWord": "процент",
    "transcription": "[pəˈsentɪʤ]"
  },
  {
    "engWord": "improve",
    "rusWord": "улучшать",
    "transcription": "[ɪmˈpruːv]"
  },
  {
    "engWord": "punch",
    "rusWord": "удар кулаком",
    "transcription": "[pʌnʧ]"
  },
  {
    "engWord": "virus",
    "rusWord": "вирус",
    "transcription": "[ˈvaɪərəs]"
  },
  {
    "engWord": "briefly",
    "rusWord": "кратко",
    "transcription": "[ˈbriːflɪ]"
  },
  {
    "engWord": "risk",
    "rusWord": "рисковать",
    "transcription": "[rɪsk]"
  },
  {
    "engWord": "absolutely",
    "rusWord": "абсолютно",
    "transcription": "[ˈæbsəluːtlɪ]"
  },
  {
    "engWord": "healthy",
    "rusWord": "здоровый",
    "transcription": "[ˈhelθɪ]"
  },
  {
    "engWord": "victim",
    "rusWord": "жертва",
    "transcription": "[ˈvɪktɪm]"
  },
  {
    "engWord": "opportunity",
    "rusWord": "возможность",
    "transcription": "[ɒpəˈtjuːnɪtɪ]"
  },
  {
    "engWord": "well",
    "rusWord": "хорошо",
    "transcription": "[wel]"
  },
  {
    "engWord": "fish",
    "rusWord": "рыба",
    "transcription": "[fɪʃ]"
  },
  {
    "engWord": "most",
    "rusWord": "наибольший",
    "transcription": "[məʊst]"
  },
  {
    "engWord": "boat",
    "rusWord": "лодка",
    "transcription": "[bəʊt]"
  },
  {
    "engWord": "sharp",
    "rusWord": "острый",
    "transcription": "[ʃɑːp]"
  },
  {
    "engWord": "conditions",
    "rusWord": "условия",
    "transcription": "[kənˈdɪʃnz]"
  },
  {
    "engWord": "partnership",
    "rusWord": "сотрудничество",
    "transcription": "[ˈpɑːtnəʃɪp]"
  },
  {
    "engWord": "nicer",
    "rusWord": "лучше",
    "transcription": "[]"
  },
  {
    "engWord": "conspiracy",
    "rusWord": "конспирация",
    "transcription": "[kənˈspɪrəsɪ]"
  },
  {
    "engWord": "sensual",
    "rusWord": "чувственный",
    "transcription": "[ˈsenʃʊəl]"
  },
  {
    "engWord": "sponge",
    "rusWord": "губка",
    "transcription": "[spʌnʤ]"
  },
  {
    "engWord": "species",
    "rusWord": "вид",
    "transcription": "[ˈspiːʃiːz]"
  },
  {
    "engWord": "jealous",
    "rusWord": "ревнивый",
    "transcription": "[ˈʤeləs]"
  },
  {
    "engWord": "daft",
    "rusWord": "глупый",
    "transcription": "[dɑːft]"
  },
  {
    "engWord": "pet",
    "rusWord": "домашнее животное",
    "transcription": "[pet]"
  },
  {
    "engWord": "inflation",
    "rusWord": "инфляция",
    "transcription": "[ɪnˈfleɪʃn]"
  },
  {
    "engWord": "nonsense",
    "rusWord": "ерунда",
    "transcription": "[ˈnɒnsəns]"
  },
  {
    "engWord": "balance",
    "rusWord": "баланс",
    "transcription": "[ˈbæləns]"
  },
  {
    "engWord": "instead",
    "rusWord": "вместо",
    "transcription": "[ɪnˈsted]"
  },
  {
    "engWord": "eleven",
    "rusWord": "одиннадцать",
    "transcription": "[ɪˈlevn]"
  },
  {
    "engWord": "drunk",
    "rusWord": "пьяный",
    "transcription": "[drʌŋk]"
  },
  {
    "engWord": "matter",
    "rusWord": "вопрос",
    "transcription": "[ˈmætə]"
  },
  {
    "engWord": "call",
    "rusWord": "вызов",
    "transcription": "[kɔːl]"
  },
  {
    "engWord": "afraid",
    "rusWord": "испуганный",
    "transcription": "[əˈfreɪd]"
  },
  {
    "engWord": "fat",
    "rusWord": "жир",
    "transcription": "[fæt]"
  },
  {
    "engWord": "system",
    "rusWord": "система",
    "transcription": "[ˈsɪstɪm]"
  },
  {
    "engWord": "sperm",
    "rusWord": "семя",
    "transcription": "[spɜːm]"
  },
  {
    "engWord": "alternative",
    "rusWord": "альтернатива",
    "transcription": "[ɔːlˈtɜːnətɪv]"
  },
  {
    "engWord": "immediately",
    "rusWord": "немедленно",
    "transcription": "[ɪˈmiːdɪətlɪ]"
  },
  {
    "engWord": "libel",
    "rusWord": "клевета",
    "transcription": "[ˈlaɪbəl]"
  },
  {
    "engWord": "slightest",
    "rusWord": "малейшее",
    "transcription": "[]"
  },
  {
    "engWord": "construct",
    "rusWord": "сооружать",
    "transcription": "[ˈkɒnstrʌkt]"
  },
  {
    "engWord": "culture",
    "rusWord": "культура",
    "transcription": "[ˈkʌlʧə]"
  },
  {
    "engWord": "harvest",
    "rusWord": "урожай",
    "transcription": "[ˈhɑːvɪst]"
  },
  {
    "engWord": "perceive",
    "rusWord": "воспринимать",
    "transcription": "[pəˈsiːv]"
  },
  {
    "engWord": "golden",
    "rusWord": "золотой",
    "transcription": "[ˈgəʊldən]"
  },
  {
    "engWord": "violent",
    "rusWord": "насильственный",
    "transcription": "[ˈvaɪələnt]"
  },
  {
    "engWord": "satisfaction",
    "rusWord": "удовлетворение",
    "transcription": "[sætɪsˈfækʃn]"
  },
  {
    "engWord": "assistant",
    "rusWord": "помощник",
    "transcription": "[əˈsɪstənt]"
  },
  {
    "engWord": "garage",
    "rusWord": "гараж",
    "transcription": "[ˈgærɑːʒ]"
  },
  {
    "engWord": "fool",
    "rusWord": "дурак",
    "transcription": "[fuːl]"
  },
  {
    "engWord": "harmony",
    "rusWord": "гармония",
    "transcription": "[ˈhɑːmənɪ]"
  },
  {
    "engWord": "clothe",
    "rusWord": "одевать",
    "transcription": "[kləʊð]"
  },
  {
    "engWord": "form",
    "rusWord": "форма",
    "transcription": "[fɔːm]"
  },
  {
    "engWord": "modern",
    "rusWord": "современный",
    "transcription": "[mɒdn]"
  },
  {
    "engWord": "also",
    "rusWord": "тоже",
    "transcription": "[ˈɔːlsəʊ]"
  },
  {
    "engWord": "low",
    "rusWord": "низкий",
    "transcription": "[ləʊ]"
  },
  {
    "engWord": "share",
    "rusWord": "делиться",
    "transcription": "[ʃeə]"
  },
  {
    "engWord": "cut",
    "rusWord": "резать",
    "transcription": "[kʌt]"
  },
  {
    "engWord": "ditch",
    "rusWord": "ров",
    "transcription": "[dɪʧ]"
  },
  {
    "engWord": "flames",
    "rusWord": "пламя",
    "transcription": "[]"
  },
  {
    "engWord": "waist",
    "rusWord": "талия",
    "transcription": "[weɪst]"
  },
  {
    "engWord": "horror",
    "rusWord": "ужас",
    "transcription": "[ˈhɒrə]"
  },
  {
    "engWord": "sculpture",
    "rusWord": "скульптура",
    "transcription": "[ˈskʌlpʧə]"
  },
  {
    "engWord": "sandy",
    "rusWord": "песчаный",
    "transcription": "[ˈsændɪ]"
  },
  {
    "engWord": "twisted",
    "rusWord": "скрученный",
    "transcription": "[ˈtwɪstɪd]"
  },
  {
    "engWord": "boil",
    "rusWord": "варить",
    "transcription": "[bɔɪl]"
  },
  {
    "engWord": "agent",
    "rusWord": "агент",
    "transcription": "[ˈeɪʤənt]"
  },
  {
    "engWord": "bake",
    "rusWord": "испечь",
    "transcription": "[beɪk]"
  },
  {
    "engWord": "dirt",
    "rusWord": "грязь",
    "transcription": "[dɜːt]"
  },
  {
    "engWord": "pregnant",
    "rusWord": "чреватый",
    "transcription": "[ˈpregnənt]"
  },
  {
    "engWord": "president",
    "rusWord": "президент",
    "transcription": "[ˈprezɪdənt]"
  },
  {
    "engWord": "away",
    "rusWord": "прочь",
    "transcription": "[əˈweɪ]"
  },
  {
    "engWord": "rope",
    "rusWord": "веревка",
    "transcription": "[rəʊp]"
  },
  {
    "engWord": "fear",
    "rusWord": "страх",
    "transcription": "[fɪə]"
  },
  {
    "engWord": "his",
    "rusWord": "свой",
    "transcription": "[hɪz]"
  },
  {
    "engWord": "advanced",
    "rusWord": "прогрессивный",
    "transcription": "[ədˈvɑːnst]"
  },
  {
    "engWord": "fisherman",
    "rusWord": "рыбак",
    "transcription": "[ˈfɪʃəmən]"
  },
  {
    "engWord": "innocence",
    "rusWord": "невинность",
    "transcription": "[ˈɪnəsəns]"
  },
  {
    "engWord": "fertile",
    "rusWord": "плодовитый",
    "transcription": "[ˈfɜːtaɪl]"
  },
  {
    "engWord": "lid",
    "rusWord": "крышка",
    "transcription": "[lɪd]"
  },
  {
    "engWord": "inclose",
    "rusWord": "окружить",
    "transcription": "[ɪnˈkləʊz]"
  },
  {
    "engWord": "louder",
    "rusWord": "громче",
    "transcription": "[]"
  },
  {
    "engWord": "fringe",
    "rusWord": "бахрома",
    "transcription": "[frɪnʤ]"
  },
  {
    "engWord": "insecure",
    "rusWord": "небезопасный",
    "transcription": "[ɪnsɪˈkjʊə]"
  },
  {
    "engWord": "steep",
    "rusWord": "крутой",
    "transcription": "[stiːp]"
  },
  {
    "engWord": "illogical",
    "rusWord": "нелогичный",
    "transcription": "[ɪˈlɒʤɪkəl]"
  },
  {
    "engWord": "thirst",
    "rusWord": "жажда",
    "transcription": "[θɜːst]"
  },
  {
    "engWord": "expression",
    "rusWord": "выражение",
    "transcription": "[ɪksˈpreʃn]"
  },
  {
    "engWord": "biological",
    "rusWord": "биологический",
    "transcription": "[baɪəˈlɒʤɪk(ə)l]"
  },
  {
    "engWord": "climate",
    "rusWord": "климат",
    "transcription": "[ˈklaɪmɪt]"
  },
  {
    "engWord": "preposition",
    "rusWord": "предлог",
    "transcription": "[prepəˈzɪʃn]"
  },
  {
    "engWord": "beef",
    "rusWord": "говядина",
    "transcription": "[biːf]"
  },
  {
    "engWord": "oppose",
    "rusWord": "противопоставлять",
    "transcription": "[əˈpəʊz]"
  },
  {
    "engWord": "aggravation",
    "rusWord": "ухудшение",
    "transcription": "[ægrəˈveɪʃn]"
  },
  {
    "engWord": "branch",
    "rusWord": "ветка",
    "transcription": "[brɑːnʧ]"
  },
  {
    "engWord": "danger",
    "rusWord": "опасность",
    "transcription": "[ˈdeɪnʤə]"
  },
  {
    "engWord": "degree",
    "rusWord": "степень",
    "transcription": "[dɪˈgriː]"
  },
  {
    "engWord": "forward",
    "rusWord": "вперед",
    "transcription": "[ˈfɔːwəd]"
  },
  {
    "engWord": "imagine",
    "rusWord": "воображать",
    "transcription": "[ɪˈmæʤɪn]"
  },
  {
    "engWord": "ignate",
    "rusWord": "ignate",
    "transcription": "[]"
  },
  {
    "engWord": "bizarre",
    "rusWord": "странный",
    "transcription": "[bɪˈzɑː]"
  },
  {
    "engWord": "satisfactory",
    "rusWord": "удовлетворительный",
    "transcription": "[sætɪsˈfæktərɪ]"
  },
  {
    "engWord": "gang",
    "rusWord": "банда",
    "transcription": "[gæŋ]"
  },
  {
    "engWord": "annulment",
    "rusWord": "аннулирование",
    "transcription": "[əˈnʌlmənt]"
  },
  {
    "engWord": "regard",
    "rusWord": "уважение",
    "transcription": "[rɪˈgɑːd]"
  },
  {
    "engWord": "ruined",
    "rusWord": "разрушенный",
    "transcription": "[ˈruːɪnd]"
  },
  {
    "engWord": "aunt",
    "rusWord": "тетя",
    "transcription": "[ɑːnt]"
  },
  {
    "engWord": "freak",
    "rusWord": "чудить",
    "transcription": "[friːk]"
  },
  {
    "engWord": "mess",
    "rusWord": "беспорядок",
    "transcription": "[mes]"
  },
  {
    "engWord": "chair",
    "rusWord": "стул",
    "transcription": "[ʧeə]"
  },
  {
    "engWord": "map",
    "rusWord": "карта",
    "transcription": "[mæp]"
  },
  {
    "engWord": "press",
    "rusWord": "нажимать",
    "transcription": "[pres]"
  },
  {
    "engWord": "writer",
    "rusWord": "писатель",
    "transcription": "[ˈraɪtə]"
  },
  {
    "engWord": "favourite",
    "rusWord": "любимый",
    "transcription": "[ˈfeɪvərɪt]"
  },
  {
    "engWord": "carsick",
    "rusWord": "укачало",
    "transcription": "[]"
  },
  {
    "engWord": "distraction",
    "rusWord": "отвлечение",
    "transcription": "[dɪsˈtrækʃn]"
  },
  {
    "engWord": "pajamas",
    "rusWord": "пижама",
    "transcription": "[pəˈʤɑːməz]"
  },
  {
    "engWord": "coherence",
    "rusWord": "взаимосвязанность",
    "transcription": "[kəʊˈhɪərəns]"
  },
  {
    "engWord": "spectacle",
    "rusWord": "спектакль",
    "transcription": "[ˈspektəkl]"
  },
  {
    "engWord": "chilly",
    "rusWord": "прохладно",
    "transcription": "[ˈʧɪlɪ]"
  },
  {
    "engWord": "protective",
    "rusWord": "защитный",
    "transcription": "[prəˈtektɪv]"
  },
  {
    "engWord": "conscious",
    "rusWord": "сознательный",
    "transcription": "[ˈkɒnʃəs]"
  },
  {
    "engWord": "bark",
    "rusWord": "кора",
    "transcription": "[bɑːk]"
  },
  {
    "engWord": "central",
    "rusWord": "центральный",
    "transcription": "[ˈsentrəl]"
  },
  {
    "engWord": "illustrate",
    "rusWord": "иллюстрировать",
    "transcription": "[ˈɪləstreɪt]"
  },
  {
    "engWord": "otherwise",
    "rusWord": "иначе",
    "transcription": "[ˈʌðəwaɪz]"
  },
  {
    "engWord": "besides",
    "rusWord": "кроме того",
    "transcription": "[bɪˈsaɪdz]"
  },
  {
    "engWord": "react",
    "rusWord": "реагировать",
    "transcription": "[rɪˈækt]"
  },
  {
    "engWord": "officer",
    "rusWord": "чиновник",
    "transcription": "[ˈɒfɪsə]"
  },
  {
    "engWord": "nephew",
    "rusWord": "племянник",
    "transcription": "[ˈnɛfju]"
  },
  {
    "engWord": "towel",
    "rusWord": "полотенце",
    "transcription": "[ˈtaʊəl]"
  },
  {
    "engWord": "network",
    "rusWord": "сеть",
    "transcription": "[ˈnetwɜːk]"
  },
  {
    "engWord": "after",
    "rusWord": "после",
    "transcription": "[ˈɑːftə]"
  },
  {
    "engWord": "sea",
    "rusWord": "море",
    "transcription": "[siː]"
  },
  {
    "engWord": "get",
    "rusWord": "получить",
    "transcription": "[get]"
  },
  {
    "engWord": "shoulder",
    "rusWord": "плечо",
    "transcription": "[ˈʃəʊldə]"
  },
  {
    "engWord": "fraud",
    "rusWord": "мошенничество",
    "transcription": "[frɔːd]"
  },
  {
    "engWord": "sleepy",
    "rusWord": "сонный",
    "transcription": "[ˈsliːpɪ]"
  },
  {
    "engWord": "etc.",
    "rusWord": "и т. д.",
    "transcription": "[]"
  },
  {
    "engWord": "rational",
    "rusWord": "рациональный",
    "transcription": "[ˈræl(ə)nəl]"
  },
  {
    "engWord": "vinegar",
    "rusWord": "уксус",
    "transcription": "[ˈvɪnɪgə]"
  },
  {
    "engWord": "brag",
    "rusWord": "хвастать",
    "transcription": "[bræg]"
  },
  {
    "engWord": "explicit",
    "rusWord": "явный",
    "transcription": "[ɪksˈplɪsɪt]"
  },
  {
    "engWord": "sort",
    "rusWord": "сортировать",
    "transcription": "[sɔːt]"
  },
  {
    "engWord": "convenience",
    "rusWord": "удобство",
    "transcription": "[kənˈviːnɪəns]"
  },
  {
    "engWord": "doomed",
    "rusWord": "обреченный",
    "transcription": "[duːmd]"
  },
  {
    "engWord": "cape",
    "rusWord": "плащ",
    "transcription": "[keɪp]"
  },
  {
    "engWord": "dense",
    "rusWord": "плотный",
    "transcription": "[dens]"
  },
  {
    "engWord": "generalize",
    "rusWord": "обобщать",
    "transcription": "[ˈʤenərəlaɪz]"
  },
  {
    "engWord": "defend",
    "rusWord": "защищать",
    "transcription": "[dɪˈfend]"
  },
  {
    "engWord": "bless",
    "rusWord": "благословить",
    "transcription": "[bles]"
  },
  {
    "engWord": "festival",
    "rusWord": "фестиваль",
    "transcription": "[ˈfestɪv(ə)l]"
  },
  {
    "engWord": "campaign",
    "rusWord": "кампания",
    "transcription": "[kæmˈpeɪn]"
  },
  {
    "engWord": "fee",
    "rusWord": "взнос",
    "transcription": "[fiː]"
  },
  {
    "engWord": "chemical",
    "rusWord": "химический",
    "transcription": "[ˈkemɪkəl]"
  },
  {
    "engWord": "centre",
    "rusWord": "центр",
    "transcription": "[ˈsentə]"
  },
  {
    "engWord": "easier",
    "rusWord": "облегчающий",
    "transcription": "[ˈiːziə]"
  },
  {
    "engWord": "certainly",
    "rusWord": "конечно",
    "transcription": "[ˈsɜːtnlɪ]"
  },
  {
    "engWord": "safety",
    "rusWord": "безопасность",
    "transcription": "[ˈseɪftɪ]"
  },
  {
    "engWord": "hotel",
    "rusWord": "отель",
    "transcription": "[həʊˈtel]"
  },
  {
    "engWord": "pretty",
    "rusWord": "хорошенький",
    "transcription": "[ˈprɪtɪ]"
  },
  {
    "engWord": "slip",
    "rusWord": "скользить",
    "transcription": "[slɪp]"
  },
  {
    "engWord": "ridiculous",
    "rusWord": "нелепый",
    "transcription": "[rɪˈdɪkjʊləs]"
  },
  {
    "engWord": "exit",
    "rusWord": "выход",
    "transcription": "[ˈegzɪt]"
  },
  {
    "engWord": "sentimental",
    "rusWord": "сентиментальный",
    "transcription": "[sentɪˈmentl]"
  },
  {
    "engWord": "hypocrisy",
    "rusWord": "лицемерие",
    "transcription": "[hɪˈpɒkrɪsɪ]"
  },
  {
    "engWord": "barefoot",
    "rusWord": "босиком",
    "transcription": "[ˈbeəfʊt]"
  },
  {
    "engWord": "oily",
    "rusWord": "маслянистая",
    "transcription": "[]"
  },
  {
    "engWord": "hesitation",
    "rusWord": "нерешительность",
    "transcription": "[hezɪˈteɪʃn]"
  },
  {
    "engWord": "mistress",
    "rusWord": "хозяйка",
    "transcription": "[ˈmɪstrɪs]"
  },
  {
    "engWord": "earl",
    "rusWord": "граф",
    "transcription": "[ɜːl]"
  },
  {
    "engWord": "electricity",
    "rusWord": "электричество",
    "transcription": "[ɪlekˈtrɪsɪtɪ]"
  },
  {
    "engWord": "category",
    "rusWord": "категория",
    "transcription": "[ˈkætɪgərɪ]"
  },
  {
    "engWord": "ratio",
    "rusWord": "соотношение",
    "transcription": "[ˈreɪʃɪəʊ]"
  },
  {
    "engWord": "shiver",
    "rusWord": "дрожать",
    "transcription": "[ˈʃɪvə]"
  },
  {
    "engWord": "bee",
    "rusWord": "пчела",
    "transcription": "[biː]"
  },
  {
    "engWord": "pertinent",
    "rusWord": "уместный",
    "transcription": "[ˈpɜːtɪnənt]"
  },
  {
    "engWord": "operation",
    "rusWord": "операция",
    "transcription": "[ɒpəˈreɪʃn]"
  },
  {
    "engWord": "line",
    "rusWord": "линия",
    "transcription": "[laɪn]"
  },
  {
    "engWord": "expedition",
    "rusWord": "экспедиция",
    "transcription": "[ekspɪˈdɪʃn]"
  },
  {
    "engWord": "tray",
    "rusWord": "лоток",
    "transcription": "[treɪ]"
  },
  {
    "engWord": "postpone",
    "rusWord": "откладывать",
    "transcription": "[pə(ʊ)ˈspəʊn]"
  },
  {
    "engWord": "old-fashioned",
    "rusWord": "старомодный",
    "transcription": "[]"
  },
  {
    "engWord": "solemn",
    "rusWord": "торжественный",
    "transcription": "[ˈsɒləm]"
  },
  {
    "engWord": "impressive",
    "rusWord": "впечатляющий",
    "transcription": "[ɪmˈpresɪv]"
  },
  {
    "engWord": "wrist",
    "rusWord": "запястье",
    "transcription": "[rɪst]"
  },
  {
    "engWord": "chopper",
    "rusWord": "мясорубка",
    "transcription": "[ˈʧɒpə]"
  },
  {
    "engWord": "unlucky",
    "rusWord": "несчастливый",
    "transcription": "[ʌnˈlʌkɪ]"
  },
  {
    "engWord": "random",
    "rusWord": "случайность",
    "transcription": "[ˈrændəm]"
  },
  {
    "engWord": "resort",
    "rusWord": "курорт",
    "transcription": "[rɪˈzɔːt]"
  },
  {
    "engWord": "prime",
    "rusWord": "главный",
    "transcription": "[praɪm]"
  },
  {
    "engWord": "tyre",
    "rusWord": "шина",
    "transcription": "[ˈtaɪə]"
  },
  {
    "engWord": "fashion",
    "rusWord": "мода",
    "transcription": "[fæʃn]"
  },
  {
    "engWord": "sale",
    "rusWord": "продажа",
    "transcription": "[seɪl]"
  },
  {
    "engWord": "out",
    "rusWord": "из",
    "transcription": "[aʊt]"
  },
  {
    "engWord": "peasant",
    "rusWord": "крестьянин",
    "transcription": "[ˈpezənt]"
  },
  {
    "engWord": "troubles",
    "rusWord": "перипетии",
    "transcription": "[trʌblz]"
  },
  {
    "engWord": "clothing",
    "rusWord": "одежда",
    "transcription": "[ˈkləʊðɪŋ]"
  },
  {
    "engWord": "ours",
    "rusWord": "наша",
    "transcription": "[]"
  },
  {
    "engWord": "circuit",
    "rusWord": "схема",
    "transcription": "[ˈsɜːkɪt]"
  },
  {
    "engWord": "specifically",
    "rusWord": "конкретно",
    "transcription": "[spɪˈsɪfɪk(ə)lɪ]"
  },
  {
    "engWord": "barber",
    "rusWord": "парикмахер",
    "transcription": "[ˈbɑːbə]"
  },
  {
    "engWord": "amnesia",
    "rusWord": "амнезия",
    "transcription": "[æmˈniːzɪə]"
  },
  {
    "engWord": "respond",
    "rusWord": "откликнуться",
    "transcription": "[rɪsˈpɒnd]"
  },
  {
    "engWord": "database",
    "rusWord": "база данных",
    "transcription": "[ˈdeɪtəbeɪs]"
  },
  {
    "engWord": "meanwhile",
    "rusWord": "между тем",
    "transcription": "[ˈmiːnwaɪl]"
  },
  {
    "engWord": "fairy",
    "rusWord": "фея",
    "transcription": "[ˈfe(ə)rɪ]"
  },
  {
    "engWord": "practical",
    "rusWord": "практичный",
    "transcription": "[ˈpræktɪkəl]"
  },
  {
    "engWord": "castle",
    "rusWord": "замок",
    "transcription": "[kɑːsl]"
  },
  {
    "engWord": "rapid",
    "rusWord": "быстрый",
    "transcription": "[ˈræpɪd]"
  },
  {
    "engWord": "unbelievable",
    "rusWord": "невероятный",
    "transcription": "[ʌnbɪˈliːvəb(ə)l]"
  },
  {
    "engWord": "website",
    "rusWord": "вебсайт",
    "transcription": "[ˈwebsaɪt]"
  },
  {
    "engWord": "mum",
    "rusWord": "мама",
    "transcription": "[mʌm]"
  },
  {
    "engWord": "harry",
    "rusWord": "Гарри",
    "transcription": "[ˈhærɪ]"
  },
  {
    "engWord": "journey",
    "rusWord": "путешествие",
    "transcription": "[ˈʤɜːnɪ]"
  },
  {
    "engWord": "huge",
    "rusWord": "огромный",
    "transcription": "[hjuːʤ]"
  },
  {
    "engWord": "plant",
    "rusWord": "завод",
    "transcription": "[plɑːnt]"
  },
  {
    "engWord": "wave",
    "rusWord": "волна",
    "transcription": "[weɪv]"
  },
  {
    "engWord": "dive",
    "rusWord": "нырять",
    "transcription": "[daɪv]"
  },
  {
    "engWord": "foul",
    "rusWord": "грязный",
    "transcription": "[faʊl]"
  },
  {
    "engWord": "exclude",
    "rusWord": "исключать",
    "transcription": "[ɪksˈkluːd]"
  },
  {
    "engWord": "pops",
    "rusWord": "попса",
    "transcription": "[pɔps]"
  },
  {
    "engWord": "tobacco",
    "rusWord": "табак",
    "transcription": "[təˈbækəʊ]"
  },
  {
    "engWord": "sank",
    "rusWord": "Санк",
    "transcription": "[]"
  },
  {
    "engWord": "cleaner",
    "rusWord": "уборщик",
    "transcription": "[ˈkliːnə]"
  },
  {
    "engWord": "hardy",
    "rusWord": "выносливый",
    "transcription": "[ˈhɑːdɪ]"
  },
  {
    "engWord": "bold",
    "rusWord": "жирный",
    "transcription": "[bəʊld]"
  },
  {
    "engWord": "profile",
    "rusWord": "профиль",
    "transcription": "[ˈprəʊfaɪl]"
  },
  {
    "engWord": "certificate",
    "rusWord": "сертификат",
    "transcription": "[səˈtɪfɪkət]"
  },
  {
    "engWord": "rush",
    "rusWord": "спешка",
    "transcription": "[rʌʃ]"
  },
  {
    "engWord": "whoever",
    "rusWord": "кто",
    "transcription": "[huːˈevə]"
  },
  {
    "engWord": "mistake",
    "rusWord": "ошибка",
    "transcription": "[mɪsˈteɪk]"
  },
  {
    "engWord": "express",
    "rusWord": "курьерский",
    "transcription": "[ɪksˈpres]"
  },
  {
    "engWord": "percent",
    "rusWord": "процент",
    "transcription": "[pəˈsent]"
  },
  {
    "engWord": "wish",
    "rusWord": "желание",
    "transcription": "[wɪʃ]"
  },
  {
    "engWord": "finger",
    "rusWord": "палец",
    "transcription": "[ˈfɪŋgə]"
  },
  {
    "engWord": "me",
    "rusWord": "я",
    "transcription": "[miː]"
  },
  {
    "engWord": "area",
    "rusWord": "область",
    "transcription": "[ˈe(ə)rɪə]"
  },
  {
    "engWord": "reply",
    "rusWord": "отвечать",
    "transcription": "[rɪˈplaɪ]"
  },
  {
    "engWord": "ever",
    "rusWord": "всегда",
    "transcription": "[ˈevə]"
  },
  {
    "engWord": "colony",
    "rusWord": "колония",
    "transcription": "[ˈkɒlənɪ]"
  },
  {
    "engWord": "tide",
    "rusWord": "прилив",
    "transcription": "[taɪd]"
  },
  {
    "engWord": "situated",
    "rusWord": "расположенный",
    "transcription": "[ˈsɪʧʊeɪtɪd]"
  },
  {
    "engWord": "earphones",
    "rusWord": "наушники",
    "transcription": "[]"
  },
  {
    "engWord": "worker",
    "rusWord": "работник",
    "transcription": "[ˈwɜːkə]"
  },
  {
    "engWord": "palm",
    "rusWord": "ладонь",
    "transcription": "[pɑːm]"
  },
  {
    "engWord": "grasp",
    "rusWord": "схватывание",
    "transcription": "[grɑːsp]"
  },
  {
    "engWord": "congratulations",
    "rusWord": "поздравлять",
    "transcription": "[kənɡraʧəˈleɪʃnz]"
  },
  {
    "engWord": "supportive",
    "rusWord": "поддерживающий",
    "transcription": "[səˈpɔːtɪv]"
  },
  {
    "engWord": "shorts",
    "rusWord": "шорты",
    "transcription": "[ʃɔːts]"
  },
  {
    "engWord": "pot",
    "rusWord": "горшок",
    "transcription": "[pɒt]"
  },
  {
    "engWord": "employee",
    "rusWord": "работник",
    "transcription": "[emplɔɪˈiː]"
  },
  {
    "engWord": "disk",
    "rusWord": "диск",
    "transcription": "[dɪsk]"
  },
  {
    "engWord": "spit",
    "rusWord": "плевать",
    "transcription": "[spɪt]"
  },
  {
    "engWord": "overnight",
    "rusWord": "заночевать",
    "transcription": "[əʊvəˈnaɪt]"
  },
  {
    "engWord": "sacrifice",
    "rusWord": "жертвовать",
    "transcription": "[ˈsækrɪfaɪs]"
  },
  {
    "engWord": "retire",
    "rusWord": "уходить в отставку",
    "transcription": "[rɪˈtaɪə]"
  },
  {
    "engWord": "prevent",
    "rusWord": "предотвращать",
    "transcription": "[prɪˈvent]"
  },
  {
    "engWord": "tough",
    "rusWord": "жесткий",
    "transcription": "[tʌf]"
  },
  {
    "engWord": "western",
    "rusWord": "западный",
    "transcription": "[ˈwestən]"
  },
  {
    "engWord": "bag",
    "rusWord": "сумка",
    "transcription": "[bæg]"
  },
  {
    "engWord": "older",
    "rusWord": "более старый",
    "transcription": "[ˈəʊldə]"
  },
  {
    "engWord": "lavatory",
    "rusWord": "туалет",
    "transcription": "[ˈlævətərɪ]"
  },
  {
    "engWord": "arrange",
    "rusWord": "организовать",
    "transcription": "[əˈreɪnʤ]"
  },
  {
    "engWord": "far",
    "rusWord": "далеко",
    "transcription": "[fɑː]"
  },
  {
    "engWord": "wide",
    "rusWord": "широкий",
    "transcription": "[waɪd]"
  },
  {
    "engWord": "seat",
    "rusWord": "сиденье",
    "transcription": "[siːt]"
  },
  {
    "engWord": "committee",
    "rusWord": "комитет",
    "transcription": "[kəˈmɪtɪ]"
  },
  {
    "engWord": "defendant",
    "rusWord": "подсудимый",
    "transcription": "[dɪˈfendənt]"
  },
  {
    "engWord": "lever",
    "rusWord": "рычаг",
    "transcription": "[ˈliːvə]"
  },
  {
    "engWord": "dug",
    "rusWord": "вырытый",
    "transcription": "[dʌg]"
  },
  {
    "engWord": "bargain",
    "rusWord": "сделка",
    "transcription": "[ˈbɑːgɪn]"
  },
  {
    "engWord": "concept",
    "rusWord": "концепция",
    "transcription": "[ˈkɒnsept]"
  },
  {
    "engWord": "tenth",
    "rusWord": "десятый",
    "transcription": "[tenθ]"
  },
  {
    "engWord": "downstairs",
    "rusWord": "внизу",
    "transcription": "[daʊnˈsteəz]"
  },
  {
    "engWord": "cosmetics",
    "rusWord": "косметические средства",
    "transcription": "[kɒzˈmetɪks]"
  },
  {
    "engWord": "route",
    "rusWord": "маршрут",
    "transcription": "[ruːt]"
  },
  {
    "engWord": "unlike",
    "rusWord": "в отличие от",
    "transcription": "[ʌnˈlaɪk]"
  },
  {
    "engWord": "nurse",
    "rusWord": "медсестра",
    "transcription": "[nɜːs]"
  },
  {
    "engWord": "tomato",
    "rusWord": "помидор",
    "transcription": "[təˈmɑːtəʊ]"
  },
  {
    "engWord": "stole",
    "rusWord": "палантин",
    "transcription": "[stəʊl]"
  },
  {
    "engWord": "colour",
    "rusWord": "цвет",
    "transcription": "[ˈkʌlə]"
  },
  {
    "engWord": "cabin",
    "rusWord": "кабина",
    "transcription": "[ˈkæbɪn]"
  },
  {
    "engWord": "jam",
    "rusWord": "варенье",
    "transcription": "[ʤæm]"
  },
  {
    "engWord": "while",
    "rusWord": "время",
    "transcription": "[waɪl]"
  },
  {
    "engWord": "mass",
    "rusWord": "масса",
    "transcription": "[mæs]"
  },
  {
    "engWord": "spread",
    "rusWord": "распространение",
    "transcription": "[spred]"
  },
  {
    "engWord": "sent",
    "rusWord": "отправленный",
    "transcription": "[sent]"
  },
  {
    "engWord": "good",
    "rusWord": "хороший",
    "transcription": "[gʊd]"
  },
  {
    "engWord": "cycle",
    "rusWord": "цикл",
    "transcription": "[saɪkl]"
  },
  {
    "engWord": "gel",
    "rusWord": "гель",
    "transcription": "[ʤel]"
  },
  {
    "engWord": "yield",
    "rusWord": "уступать",
    "transcription": "[jiːld]"
  },
  {
    "engWord": "shooter",
    "rusWord": "стрелок",
    "transcription": "[ˈʃuːtə]"
  },
  {
    "engWord": "tease",
    "rusWord": "дразнить",
    "transcription": "[tiːz]"
  },
  {
    "engWord": "vague",
    "rusWord": "неопределенный",
    "transcription": "[veɪg]"
  },
  {
    "engWord": "repel",
    "rusWord": "отталкивать",
    "transcription": "[rɪˈpel]"
  },
  {
    "engWord": "habitat",
    "rusWord": "среда обитания",
    "transcription": "[ˈhæbɪtæt]"
  },
  {
    "engWord": "invention",
    "rusWord": "изобретение",
    "transcription": "[ɪnˈvenʃn]"
  },
  {
    "engWord": "wrapped",
    "rusWord": "завернутый",
    "transcription": "[ræpt]"
  },
  {
    "engWord": "goose",
    "rusWord": "гусь",
    "transcription": "[guːs]"
  },
  {
    "engWord": "detect",
    "rusWord": "обнаружить",
    "transcription": "[dɪˈtekt]"
  },
  {
    "engWord": "unusual",
    "rusWord": "необычный",
    "transcription": "[ʌnˈjuːʒʊəl]"
  },
  {
    "engWord": "fellow",
    "rusWord": "товарищ",
    "transcription": "[ˈfeləʊ]"
  },
  {
    "engWord": "letting",
    "rusWord": "сдача в аренду",
    "transcription": "[ˈletɪŋ]"
  },
  {
    "engWord": "film",
    "rusWord": "фильм",
    "transcription": "[fɪlm]"
  },
  {
    "engWord": "sheep",
    "rusWord": "овца",
    "transcription": "[ʃiːp]"
  },
  {
    "engWord": "need",
    "rusWord": "необходимость",
    "transcription": "[niːd]"
  },
  {
    "engWord": "term",
    "rusWord": "срок",
    "transcription": "[tɜːm]"
  },
  {
    "engWord": "post",
    "rusWord": "пост",
    "transcription": "[pəʊst]"
  },
  {
    "engWord": "lawsuit",
    "rusWord": "судебный процесс",
    "transcription": "[ˈlɔːsjuːt]"
  },
  {
    "engWord": "bra",
    "rusWord": "бюстгальтер",
    "transcription": "[brɑː]"
  },
  {
    "engWord": "convincing",
    "rusWord": "убедительный",
    "transcription": "[kənˈvɪnsɪŋ]"
  },
  {
    "engWord": "straw",
    "rusWord": "солома",
    "transcription": "[strɔː]"
  },
  {
    "engWord": "tendency",
    "rusWord": "тенденция",
    "transcription": "[ˈtendənsɪ]"
  },
  {
    "engWord": "offense",
    "rusWord": "нападение",
    "transcription": "[əˈfens]"
  },
  {
    "engWord": "necessarily",
    "rusWord": "обязательно",
    "transcription": "[ˈnesɪsərɪlɪ]"
  },
  {
    "engWord": "glance",
    "rusWord": "взгляд",
    "transcription": "[glɑːns]"
  },
  {
    "engWord": "warning",
    "rusWord": "предупреждение",
    "transcription": "[ˈwɔːnɪŋ]"
  },
  {
    "engWord": "toast",
    "rusWord": "тост",
    "transcription": "[təʊst]"
  },
  {
    "engWord": "boyfriend",
    "rusWord": "бойфренд",
    "transcription": "[ˈbɔɪfrend]"
  },
  {
    "engWord": "transport",
    "rusWord": "транспорт",
    "transcription": "[ˈtrænspɔːt]"
  },
  {
    "engWord": "perfect",
    "rusWord": "идеальный",
    "transcription": "[ˈpɜːfɪkt]"
  },
  {
    "engWord": "expect",
    "rusWord": "ожидать",
    "transcription": "[ɪksˈpekt]"
  },
  {
    "engWord": "main",
    "rusWord": "главный",
    "transcription": "[meɪn]"
  },
  {
    "engWord": "catching",
    "rusWord": "привлекательный",
    "transcription": "[ˈkæʧɪŋ]"
  },
  {
    "engWord": "executive",
    "rusWord": "исполнительный",
    "transcription": "[ɪgˈzekjʊtɪv]"
  },
  {
    "engWord": "insult",
    "rusWord": "оскорбление",
    "transcription": "[ˈɪnsʌlt]"
  },
  {
    "engWord": "balcony",
    "rusWord": "балкон",
    "transcription": "[ˈbælkənɪ]"
  },
  {
    "engWord": "inevitable",
    "rusWord": "неизбежный",
    "transcription": "[ɪnˈevɪtəbl]"
  },
  {
    "engWord": "strategy",
    "rusWord": "стратегия",
    "transcription": "[ˈstrætɪʤɪ]"
  },
  {
    "engWord": "survival",
    "rusWord": "выживание",
    "transcription": "[səˈvaɪvəl]"
  },
  {
    "engWord": "cooking",
    "rusWord": "приготовление еды",
    "transcription": "[ˈkʊkɪŋ]"
  },
  {
    "engWord": "yourself",
    "rusWord": "сам",
    "transcription": "[jəˈself]"
  },
  {
    "engWord": "positive",
    "rusWord": "положительный",
    "transcription": "[ˈpɒzɪtɪv]"
  },
  {
    "engWord": "princess",
    "rusWord": "княгиня",
    "transcription": "[prɪnˈses]"
  },
  {
    "engWord": "basic",
    "rusWord": "основной",
    "transcription": "[ˈbeɪsɪk]"
  },
  {
    "engWord": "free",
    "rusWord": "свободный",
    "transcription": "[friː]"
  },
  {
    "engWord": "ground",
    "rusWord": "земля",
    "transcription": "[graʊnd]"
  },
  {
    "engWord": "glue",
    "rusWord": "клей",
    "transcription": "[gluː]"
  },
  {
    "engWord": "beneficial",
    "rusWord": "выгодный",
    "transcription": "[benɪˈfɪʃəl]"
  },
  {
    "engWord": "mustard",
    "rusWord": "горчица",
    "transcription": "[ˈmʌstəd]"
  },
  {
    "engWord": "yearly",
    "rusWord": "ежегодный",
    "transcription": "[ˈjɪəlɪ]"
  },
  {
    "engWord": "insulting",
    "rusWord": "оскорбительный",
    "transcription": "[ɪnˈsʌltɪŋ]"
  },
  {
    "engWord": "paradise",
    "rusWord": "рай",
    "transcription": "[ˈpærədaɪs]"
  },
  {
    "engWord": "noodle",
    "rusWord": "лапша",
    "transcription": "[nuːdl]"
  },
  {
    "engWord": "freeway",
    "rusWord": "автострада",
    "transcription": "[ˈfriːweɪ]"
  },
  {
    "engWord": "psychological",
    "rusWord": "психологический",
    "transcription": "[saɪkəˈlɔʤɪkəl]"
  },
  {
    "engWord": "maintain",
    "rusWord": "поддерживать",
    "transcription": "[meɪnˈteɪn]"
  },
  {
    "engWord": "path",
    "rusWord": "путь",
    "transcription": "[pɑːθ]"
  },
  {
    "engWord": "father",
    "rusWord": "отец",
    "transcription": "[ˈfɑːðə]"
  },
  {
    "engWord": "use",
    "rusWord": "использовать",
    "transcription": "[]"
  },
  {
    "engWord": "receive",
    "rusWord": "получить",
    "transcription": "[rɪˈsiːv]"
  },
  {
    "engWord": "care",
    "rusWord": "забота",
    "transcription": "[keə]"
  },
  {
    "engWord": "logic",
    "rusWord": "логика",
    "transcription": "[ˈlɒʤɪk]"
  },
  {
    "engWord": "breadth",
    "rusWord": "ширина",
    "transcription": "[bredθ]"
  },
  {
    "engWord": "miller",
    "rusWord": "фрезеровщик",
    "transcription": "[ˈmɪlə]"
  },
  {
    "engWord": "mistrust",
    "rusWord": "недоверие",
    "transcription": "[mɪsˈtrʌst]"
  },
  {
    "engWord": "Japanese",
    "rusWord": "японский",
    "transcription": "[ʤæpəˈniːz]"
  },
  {
    "engWord": "rehabilitation",
    "rusWord": "реабилитация",
    "transcription": "[riː(h)əbɪlɪˈteɪʃn]"
  },
  {
    "engWord": "necklace",
    "rusWord": "бусы",
    "transcription": "[ˈneklɪs]"
  },
  {
    "engWord": "frustrating",
    "rusWord": "разочаровывающий",
    "transcription": "[frʌˈstreɪtɪŋ]"
  },
  {
    "engWord": "cabbage",
    "rusWord": "капуста",
    "transcription": "[ˈkæbɪʤ]"
  },
  {
    "engWord": "cautious",
    "rusWord": "осторожный",
    "transcription": "[ˈkɔːʃəs]"
  },
  {
    "engWord": "chalk",
    "rusWord": "мел",
    "transcription": "[ʧɔːk]"
  },
  {
    "engWord": "fever",
    "rusWord": "лихорадка",
    "transcription": "[ˈfiːvə]"
  },
  {
    "engWord": "behalf",
    "rusWord": "ради",
    "transcription": "[bɪˈhɑːf]"
  },
  {
    "engWord": "aisle",
    "rusWord": "проход",
    "transcription": "[aɪl]"
  },
  {
    "engWord": "coin",
    "rusWord": "монета",
    "transcription": "[kɔɪn]"
  },
  {
    "engWord": "valuable",
    "rusWord": "ценный",
    "transcription": "[ˈvæljʊ(ə)b(ə)l]"
  },
  {
    "engWord": "mainly",
    "rusWord": "в основном",
    "transcription": "[ˈmeɪnlɪ]"
  },
  {
    "engWord": "daughter",
    "rusWord": "дочь",
    "transcription": "[ˈdɔːtə]"
  },
  {
    "engWord": "bucket",
    "rusWord": "ведро",
    "transcription": "[ˈbʌkɪt]"
  },
  {
    "engWord": "myself",
    "rusWord": "сам",
    "transcription": "[maɪˈself]"
  },
  {
    "engWord": "church",
    "rusWord": "церковь",
    "transcription": "[ʧɜːʧ]"
  },
  {
    "engWord": "alone",
    "rusWord": "один",
    "transcription": "[əˈləʊn]"
  },
  {
    "engWord": "ring",
    "rusWord": "кольцо",
    "transcription": "[rɪŋ]"
  },
  {
    "engWord": "than",
    "rusWord": "чем",
    "transcription": "[ðæn]"
  },
  {
    "engWord": "dark",
    "rusWord": "темный",
    "transcription": "[dɑːk]"
  },
  {
    "engWord": "cotton",
    "rusWord": "хлопок",
    "transcription": "[kɒtn]"
  },
  {
    "engWord": "material",
    "rusWord": "материал",
    "transcription": "[məˈtɪərɪəl]"
  },
  {
    "engWord": "correctly",
    "rusWord": "правильно",
    "transcription": "[kəˈrektlɪ]"
  },
  {
    "engWord": "geography",
    "rusWord": "география",
    "transcription": "[ʤɪˈɒgrəfɪ]"
  },
  {
    "engWord": "altar",
    "rusWord": "жертвенник",
    "transcription": "[ˈɔːltə]"
  },
  {
    "engWord": "ritual",
    "rusWord": "ритуальный",
    "transcription": "[ˈrɪʧʊəl]"
  },
  {
    "engWord": "wheelchair",
    "rusWord": "инвалидная коляска",
    "transcription": "[ˈwiːlʧeə]"
  },
  {
    "engWord": "forecast",
    "rusWord": "прогноз",
    "transcription": "[ˈfɔːkɑːst]"
  },
  {
    "engWord": "pessimistic",
    "rusWord": "пессимистический",
    "transcription": "[pesɪˈmɪstɪk]"
  },
  {
    "engWord": "respectively",
    "rusWord": "соответственно",
    "transcription": "[rɪsˈpektɪvlɪ]"
  },
  {
    "engWord": "backyard",
    "rusWord": "задний двор",
    "transcription": "[ˈbækjɑːd]"
  },
  {
    "engWord": "gig",
    "rusWord": "двуколка",
    "transcription": "[gɪg]"
  },
  {
    "engWord": "scientific",
    "rusWord": "научный",
    "transcription": "[saɪənˈtɪfɪk]"
  },
  {
    "engWord": "intention",
    "rusWord": "намерение",
    "transcription": "[ɪnˈtenʃn]"
  },
  {
    "engWord": "noble",
    "rusWord": "благородный",
    "transcription": "[nəʊbl]"
  },
  {
    "engWord": "mental",
    "rusWord": "умственный",
    "transcription": "[mentl]"
  },
  {
    "engWord": "manager",
    "rusWord": "менеджер",
    "transcription": "[ˈmænɪʤə]"
  },
  {
    "engWord": "handsome",
    "rusWord": "красивый",
    "transcription": "[ˈhænsəm]"
  },
  {
    "engWord": "same",
    "rusWord": "одинаковый",
    "transcription": "[seɪm]"
  },
  {
    "engWord": "he",
    "rusWord": "он",
    "transcription": "[hiː]"
  },
  {
    "engWord": "dexterity",
    "rusWord": "ловкость",
    "transcription": "[deksˈterɪtɪ]"
  },
  {
    "engWord": "faithful",
    "rusWord": "верный",
    "transcription": "[ˈfeɪθf(ə)l]"
  },
  {
    "engWord": "refund",
    "rusWord": "возврат",
    "transcription": "[ˈriːfʌnd]"
  },
  {
    "engWord": "growth",
    "rusWord": "рост",
    "transcription": "[grəʊθ]"
  },
  {
    "engWord": "wee",
    "rusWord": "крошечный",
    "transcription": "[wiː]"
  },
  {
    "engWord": "sphere",
    "rusWord": "сфера",
    "transcription": "[sfɪə]"
  },
  {
    "engWord": "muffin",
    "rusWord": "кекс",
    "transcription": "[ˈmʌfɪn]"
  },
  {
    "engWord": "merge",
    "rusWord": "поглощать",
    "transcription": "[mɜːʤ]"
  },
  {
    "engWord": "omelette",
    "rusWord": "омлет",
    "transcription": "[ˈɒmlɪt]"
  },
  {
    "engWord": "basketball",
    "rusWord": "баскетбол",
    "transcription": "[ˈbɑːskɪtbɔːl]"
  },
  {
    "engWord": "generation",
    "rusWord": "поколение",
    "transcription": "[ʤenəˈreɪʃn]"
  },
  {
    "engWord": "although",
    "rusWord": "хотя",
    "transcription": "[ɔːlˈðəʊ]"
  },
  {
    "engWord": "lean",
    "rusWord": "опираться",
    "transcription": "[liːn]"
  },
  {
    "engWord": "couch",
    "rusWord": "диван",
    "transcription": "[]"
  },
  {
    "engWord": "sausage",
    "rusWord": "сосиска",
    "transcription": "[ˈsɒsɪʤ]"
  },
  {
    "engWord": "sweet",
    "rusWord": "сладкий",
    "transcription": "[swiːt]"
  },
  {
    "engWord": "patient",
    "rusWord": "терпеливый",
    "transcription": "[ˈpeɪʃnt]"
  },
  {
    "engWord": "carry",
    "rusWord": "нести",
    "transcription": "[ˈkærɪ]"
  },
  {
    "engWord": "paper",
    "rusWord": "бумага",
    "transcription": "[ˈpeɪpə]"
  },
  {
    "engWord": "blossom",
    "rusWord": "цветение",
    "transcription": "[ˈblɒsəm]"
  },
  {
    "engWord": "guardian",
    "rusWord": "опекун",
    "transcription": "[ˈgɑːdɪən]"
  },
  {
    "engWord": "serial",
    "rusWord": "серийный",
    "transcription": "[ˈsɪərɪəl]"
  },
  {
    "engWord": "ironic",
    "rusWord": "ироничный",
    "transcription": "[aɪˈrɒnɪk]"
  },
  {
    "engWord": "jealousy",
    "rusWord": "ревность",
    "transcription": "[ˈʤeləsɪ]"
  },
  {
    "engWord": "fossil",
    "rusWord": "ископаемое",
    "transcription": "[fɒsl]"
  },
  {
    "engWord": "serving",
    "rusWord": "порция",
    "transcription": "[ˈsɜːvɪŋ]"
  },
  {
    "engWord": "used",
    "rusWord": "используемый",
    "transcription": "[juːzd]"
  },
  {
    "engWord": "freezer",
    "rusWord": "морозилка",
    "transcription": "[ˈfriːzə]"
  },
  {
    "engWord": "impose",
    "rusWord": "облагать",
    "transcription": "[]"
  },
  {
    "engWord": "sportsman",
    "rusWord": "спортсмен",
    "transcription": "[ˈspɔːtsmən]"
  },
  {
    "engWord": "abandon",
    "rusWord": "оставить",
    "transcription": "[əˈbændən]"
  },
  {
    "engWord": "telly",
    "rusWord": "телик",
    "transcription": "[ˈtelɪ]"
  },
  {
    "engWord": "mystery",
    "rusWord": "тайна",
    "transcription": "[ˈmɪstərɪ]"
  },
  {
    "engWord": "bridge",
    "rusWord": "мост",
    "transcription": "[brɪʤ]"
  },
  {
    "engWord": "breaking",
    "rusWord": "разрыв",
    "transcription": "[ˈbreɪkɪŋ]"
  },
  {
    "engWord": "idiot",
    "rusWord": "идиот",
    "transcription": "[ˈɪdɪət]"
  },
  {
    "engWord": "Monday",
    "rusWord": "понедельник",
    "transcription": "[ˈmʌndɪ]"
  },
  {
    "engWord": "alive",
    "rusWord": "живой",
    "transcription": "[əˈlaɪv]"
  },
  {
    "engWord": "say",
    "rusWord": "сказать",
    "transcription": "[seɪ]"
  },
  {
    "engWord": "hand",
    "rusWord": "рука",
    "transcription": "[hænd]"
  },
  {
    "engWord": "method",
    "rusWord": "метод",
    "transcription": "[ˈmeθəd]"
  },
  {
    "engWord": "registered",
    "rusWord": "зарегистрированный",
    "transcription": "[ˈreʤɪstəd]"
  },
  {
    "engWord": "proven",
    "rusWord": "доказанный",
    "transcription": "[ˈpruːvən]"
  },
  {
    "engWord": "outgoing",
    "rusWord": "исходящий",
    "transcription": "[aʊtˈgəʊɪŋ]"
  },
  {
    "engWord": "rib",
    "rusWord": "ребро",
    "transcription": "[rɪb]"
  },
  {
    "engWord": "oak",
    "rusWord": "дуб",
    "transcription": "[əʊk]"
  },
  {
    "engWord": "illiterate",
    "rusWord": "необразованный",
    "transcription": "[ɪˈlɪtərɪt]"
  },
  {
    "engWord": "devoted",
    "rusWord": "посвященный",
    "transcription": "[dɪˈvəʊtɪd]"
  },
  {
    "engWord": "liquor",
    "rusWord": "алкоголь",
    "transcription": "[ˈlɪkə]"
  },
  {
    "engWord": "ton",
    "rusWord": "тонна",
    "transcription": "[tʌn]"
  },
  {
    "engWord": "carefully",
    "rusWord": "внимательно",
    "transcription": "[ˈkeəf(ə)lɪ]"
  },
  {
    "engWord": "tidy",
    "rusWord": "аккуратный",
    "transcription": "[ˈtaɪdɪ]"
  },
  {
    "engWord": "passenger",
    "rusWord": "пассажир",
    "transcription": "[ˈpæsɪnʤə]"
  },
  {
    "engWord": "asleep",
    "rusWord": "спящий",
    "transcription": "[əˈsliːp]"
  },
  {
    "engWord": "stubborn",
    "rusWord": "упрямый",
    "transcription": "[ˈstʌbən]"
  },
  {
    "engWord": "fourteen",
    "rusWord": "четырнадцать",
    "transcription": "[fɔːˈtiːn]"
  },
  {
    "engWord": "attention",
    "rusWord": "внимание",
    "transcription": "[əˈtenʃn]"
  },
  {
    "engWord": "purpose",
    "rusWord": "цель",
    "transcription": "[ˈpɜːpəs]"
  },
  {
    "engWord": "period",
    "rusWord": "период",
    "transcription": "[ˈpɪərɪəd]"
  },
  {
    "engWord": "salt",
    "rusWord": "соль",
    "transcription": "[sɔːlt]"
  },
  {
    "engWord": "picture",
    "rusWord": "изображение",
    "transcription": "[ˈpɪkʧə]"
  },
  {
    "engWord": "sweep",
    "rusWord": "подметать",
    "transcription": "[swiːp]"
  },
  {
    "engWord": "lease",
    "rusWord": "аренда",
    "transcription": "[liːs]"
  },
  {
    "engWord": "elegant",
    "rusWord": "элегантный",
    "transcription": "[ˈelɪgənt]"
  },
  {
    "engWord": "ken",
    "rusWord": "кругозор",
    "transcription": "[ken]"
  },
  {
    "engWord": "troops",
    "rusWord": "войска",
    "transcription": "[truːps]"
  },
  {
    "engWord": "bronze",
    "rusWord": "бронза",
    "transcription": "[brɒnz]"
  },
  {
    "engWord": "machinery",
    "rusWord": "механизм",
    "transcription": "[məˈʃiːnərɪ]"
  },
  {
    "engWord": "bracelet",
    "rusWord": "браслет",
    "transcription": "[ˈbreɪslɪt]"
  },
  {
    "engWord": "bleed",
    "rusWord": "кровоточить",
    "transcription": "[bliːd]"
  },
  {
    "engWord": "legislation",
    "rusWord": "законодательство",
    "transcription": "[leʤɪsˈleɪʃn]"
  },
  {
    "engWord": "prospect",
    "rusWord": "перспектива",
    "transcription": "[ˈprɒspekt]"
  },
  {
    "engWord": "renovate",
    "rusWord": "ремонтировать",
    "transcription": "[ˈrenəveɪt]"
  },
  {
    "engWord": "treasure",
    "rusWord": "сокровище",
    "transcription": "[ˈtreʒə]"
  },
  {
    "engWord": "leading",
    "rusWord": "руководящий",
    "transcription": "[ˈledɪŋ]"
  },
  {
    "engWord": "decade",
    "rusWord": "десятилетие",
    "transcription": "[ˈdekeɪd]"
  },
  {
    "engWord": "device",
    "rusWord": "устройство",
    "transcription": "[dɪˈvaɪs]"
  },
  {
    "engWord": "yesterday",
    "rusWord": "вчера",
    "transcription": "[ˈjestədɪ]"
  },
  {
    "engWord": "without",
    "rusWord": "без",
    "transcription": "[wɪˈðaʊt]"
  },
  {
    "engWord": "literature",
    "rusWord": "литература",
    "transcription": "[ˈlɪt(ə)rəʧə]"
  },
  {
    "engWord": "together",
    "rusWord": "вместе",
    "transcription": "[təˈgeðə]"
  },
  {
    "engWord": "run",
    "rusWord": "бежать",
    "transcription": "[rʌn]"
  },
  {
    "engWord": "deadly",
    "rusWord": "смертоносный",
    "transcription": "[ˈdedlɪ]"
  },
  {
    "engWord": "dedicate",
    "rusWord": "посвящать",
    "transcription": "[ˈdedɪkeɪt]"
  },
  {
    "engWord": "sane",
    "rusWord": "здравомыслящий",
    "transcription": "[seɪn]"
  },
  {
    "engWord": "reservation",
    "rusWord": "бронирование",
    "transcription": "[rezəˈveɪʃn]"
  },
  {
    "engWord": "additional",
    "rusWord": "дополнительный",
    "transcription": "[əˈdɪʃnəl]"
  },
  {
    "engWord": "cellar",
    "rusWord": "подвал",
    "transcription": "[ˈselə]"
  },
  {
    "engWord": "efficient",
    "rusWord": "эффективный",
    "transcription": "[ɪˈfɪʃnt]"
  },
  {
    "engWord": "diagnose",
    "rusWord": "диагностировать",
    "transcription": "[ˈdaɪəgnəʊz]"
  },
  {
    "engWord": "aggressive",
    "rusWord": "агрессивный",
    "transcription": "[əˈgresɪv]"
  },
  {
    "engWord": "elephant",
    "rusWord": "слон",
    "transcription": "[ˈelɪfənt]"
  },
  {
    "engWord": "chemistry",
    "rusWord": "химия",
    "transcription": "[ˈkemɪstrɪ]"
  },
  {
    "engWord": "pink",
    "rusWord": "розовый",
    "transcription": "[pɪŋk]"
  },
  {
    "engWord": "shadow",
    "rusWord": "тень",
    "transcription": "[ˈʃædəʊ]"
  },
  {
    "engWord": "mouse",
    "rusWord": "мышь",
    "transcription": "[maʊs]"
  },
  {
    "engWord": "theory",
    "rusWord": "теория",
    "transcription": "[ˈθɪərɪ]"
  },
  {
    "engWord": "practise",
    "rusWord": "практиковать",
    "transcription": "[ˈpræktɪs]"
  },
  {
    "engWord": "cousin",
    "rusWord": "двоюродный брат",
    "transcription": "[kʌzn]"
  },
  {
    "engWord": "knee",
    "rusWord": "колено",
    "transcription": "[niː]"
  },
  {
    "engWord": "bathroom",
    "rusWord": "ванная",
    "transcription": "[ˈbɑːθrum]"
  },
  {
    "engWord": "apology",
    "rusWord": "извинение",
    "transcription": "[əˈpɒləʤɪ]"
  },
  {
    "engWord": "neighbour",
    "rusWord": "сосед",
    "transcription": "[ˈneɪbə]"
  },
  {
    "engWord": "plus",
    "rusWord": "плюс",
    "transcription": "[plʌs]"
  },
  {
    "engWord": "qualify",
    "rusWord": "квалифицировать",
    "transcription": "[ˈkwɒlɪfaɪ]"
  },
  {
    "engWord": "member",
    "rusWord": "участник",
    "transcription": "[ˈmembə]"
  },
  {
    "engWord": "within",
    "rusWord": "внутри",
    "transcription": "[wɪˈðɪn]"
  },
  {
    "engWord": "chief",
    "rusWord": "главный",
    "transcription": "[ʧiːf]"
  },
  {
    "engWord": "us",
    "rusWord": "США",
    "transcription": "[]"
  },
  {
    "engWord": "string",
    "rusWord": "строка",
    "transcription": "[strɪŋ]"
  },
  {
    "engWord": "story",
    "rusWord": "история",
    "transcription": "[ˈstɔːrɪ]"
  },
  {
    "engWord": "order",
    "rusWord": "порядок",
    "transcription": "[ˈɔːdə]"
  },
  {
    "engWord": "arrow",
    "rusWord": "стрела",
    "transcription": "[ˈærəʊ]"
  },
  {
    "engWord": "ignorant",
    "rusWord": "невежественный",
    "transcription": "[ˈɪgnərənt]"
  },
  {
    "engWord": "daisy",
    "rusWord": "маргаритка",
    "transcription": "[ˈdeɪzɪ]"
  },
  {
    "engWord": "glove",
    "rusWord": "перчатка",
    "transcription": "[glʌv]"
  },
  {
    "engWord": "fairly",
    "rusWord": "довольно",
    "transcription": "[ˈfeəlɪ]"
  },
  {
    "engWord": "sacred",
    "rusWord": "священный",
    "transcription": "[ˈseɪkrɪd]"
  },
  {
    "engWord": "treaty",
    "rusWord": "договор",
    "transcription": "[ˈtriːtɪ]"
  },
  {
    "engWord": "housing",
    "rusWord": "жилье",
    "transcription": "[ˈhaʊzɪŋ]"
  },
  {
    "engWord": "compliment",
    "rusWord": "комплимент",
    "transcription": "[ˈkɒmplɪmənt]"
  },
  {
    "engWord": "role",
    "rusWord": "роль",
    "transcription": "[rəʊl]"
  },
  {
    "engWord": "ordinary",
    "rusWord": "обычный",
    "transcription": "[ˈɔːdnrɪ]"
  },
  {
    "engWord": "someone",
    "rusWord": "кто-то",
    "transcription": "[ˈsʌmwʌn]"
  },
  {
    "engWord": "clue",
    "rusWord": "ключ",
    "transcription": "[kluː]"
  },
  {
    "engWord": "scale",
    "rusWord": "масштаб",
    "transcription": "[skeɪl]"
  },
  {
    "engWord": "be",
    "rusWord": "быть",
    "transcription": "[biː]"
  },
  {
    "engWord": "feel",
    "rusWord": "чувствовать",
    "transcription": "[fiːl]"
  },
  {
    "engWord": "straight",
    "rusWord": "прямой",
    "transcription": "[streɪt]"
  },
  {
    "engWord": "tire",
    "rusWord": "шина",
    "transcription": "[ˈtaɪə]"
  },
  {
    "engWord": "food",
    "rusWord": "еда",
    "transcription": "[fuːd]"
  },
  {
    "engWord": "fragrance",
    "rusWord": "аромат",
    "transcription": "[ˈfreɪgrəns]"
  },
  {
    "engWord": "ballet",
    "rusWord": "балет",
    "transcription": "[ˈbæleɪ]"
  },
  {
    "engWord": "operator",
    "rusWord": "оператор",
    "transcription": "[ˈɒpəreɪtə]"
  },
  {
    "engWord": "brand",
    "rusWord": "марка",
    "transcription": "[brænd]"
  },
  {
    "engWord": "pen",
    "rusWord": "ручка",
    "transcription": "[pen]"
  },
  {
    "engWord": "particularly",
    "rusWord": "в частности",
    "transcription": "[pəˈtɪkjʊləlɪ]"
  },
  {
    "engWord": "average",
    "rusWord": "обычный",
    "transcription": "[ˈævərɪʤ]"
  },
  {
    "engWord": "hide",
    "rusWord": "прятать",
    "transcription": "[haɪd]"
  },
  {
    "engWord": "mirror",
    "rusWord": "зеркало",
    "transcription": "[ˈmɪrə]"
  },
  {
    "engWord": "dinner",
    "rusWord": "обед",
    "transcription": "[ˈdɪnə]"
  },
  {
    "engWord": "cancel",
    "rusWord": "отменять",
    "transcription": "[ˈkænsəl]"
  },
  {
    "engWord": "across",
    "rusWord": "через",
    "transcription": "[əˈkrɒs]"
  },
  {
    "engWord": "chef",
    "rusWord": "шеф-повар",
    "transcription": "[ʃef]"
  },
  {
    "engWord": "so",
    "rusWord": "так",
    "transcription": "[səʊ]"
  },
  {
    "engWord": "cent",
    "rusWord": "цент",
    "transcription": "[sent]"
  },
  {
    "engWord": "joy",
    "rusWord": "радость",
    "transcription": "[ʤɔɪ]"
  },
  {
    "engWord": "lake",
    "rusWord": "озеро",
    "transcription": "[leɪk]"
  },
  {
    "engWord": "umbrella",
    "rusWord": "зонтик",
    "transcription": "[ʌmˈbrelə]"
  },
  {
    "engWord": "cologne",
    "rusWord": "Кельн",
    "transcription": "[kəˈləʊn]"
  },
  {
    "engWord": "colonel",
    "rusWord": "полковник",
    "transcription": "[kɜːnl]"
  },
  {
    "engWord": "cottage",
    "rusWord": "коттедж",
    "transcription": "[ˈkɒtɪʤ]"
  },
  {
    "engWord": "rubber",
    "rusWord": "резинка",
    "transcription": "[ˈrʌbə]"
  },
  {
    "engWord": "terminal",
    "rusWord": "терминал",
    "transcription": "[ˈtɜːmɪnl]"
  },
  {
    "engWord": "coherent",
    "rusWord": "последовательный",
    "transcription": "[kəʊˈhɪərənt]"
  },
  {
    "engWord": "voyage",
    "rusWord": "путешествие",
    "transcription": "[ˈvɔɪɪʤ]"
  },
  {
    "engWord": "obey",
    "rusWord": "подчиняться",
    "transcription": "[əˈbeɪ]"
  },
  {
    "engWord": "vivid",
    "rusWord": "яркий",
    "transcription": "[ˈvɪvɪd]"
  },
  {
    "engWord": "kindly",
    "rusWord": "доброжелательно",
    "transcription": "[ˈkaɪndlɪ]"
  },
  {
    "engWord": "sleeve",
    "rusWord": "рукав",
    "transcription": "[sliːv]"
  },
  {
    "engWord": "handling",
    "rusWord": "обращение",
    "transcription": "[ˈhændlɪŋ]"
  },
  {
    "engWord": "appreciate",
    "rusWord": "ценить",
    "transcription": "[əˈpriːʃɪeɪt]"
  },
  {
    "engWord": "halfway",
    "rusWord": "на полпути",
    "transcription": "[hɑːfˈweɪ]"
  },
  {
    "engWord": "admire",
    "rusWord": "восхищаться",
    "transcription": "[ədˈmaɪə]"
  },
  {
    "engWord": "intend",
    "rusWord": "намереваться",
    "transcription": "[ɪnˈtend]"
  },
  {
    "engWord": "submit",
    "rusWord": "представить",
    "transcription": "[səbˈmɪt]"
  },
  {
    "engWord": "explanation",
    "rusWord": "объяснение",
    "transcription": "[ekspləˈneɪʃn]"
  },
  {
    "engWord": "reading",
    "rusWord": "чтение",
    "transcription": "[ˈriːdɪŋ]"
  },
  {
    "engWord": "taste",
    "rusWord": "вкус",
    "transcription": "[teɪst]"
  },
  {
    "engWord": "deserve",
    "rusWord": "заслуживать",
    "transcription": "[dɪˈzɜːv]"
  },
  {
    "engWord": "screw",
    "rusWord": "винт",
    "transcription": "[skruː]"
  },
  {
    "engWord": "director",
    "rusWord": "директор",
    "transcription": "[dɪˈrektə]"
  },
  {
    "engWord": "found",
    "rusWord": "найдено",
    "transcription": "[faʊnd]"
  },
  {
    "engWord": "monarchy",
    "rusWord": "монархия",
    "transcription": "[ˈmɒnəkɪ]"
  },
  {
    "engWord": "impress",
    "rusWord": "впечатление",
    "transcription": "[ˈɪmpres]"
  },
  {
    "engWord": "garnish",
    "rusWord": "гарнировать",
    "transcription": "[ˈgɑːnɪʃ]"
  },
  {
    "engWord": "eyesight",
    "rusWord": "зрение",
    "transcription": "[ˈaɪsaɪt]"
  },
  {
    "engWord": "lard",
    "rusWord": "сало",
    "transcription": "[lɑːd]"
  },
  {
    "engWord": "inner",
    "rusWord": "внутренний",
    "transcription": "[ˈɪnə]"
  },
  {
    "engWord": "scandal",
    "rusWord": "скандал",
    "transcription": "[skændl]"
  },
  {
    "engWord": "annoying",
    "rusWord": "раздражающий",
    "transcription": "[əˈnɔɪɪŋ]"
  },
  {
    "engWord": "privacy",
    "rusWord": "конфиденциальность",
    "transcription": "[ˈprɪvəsɪ]"
  },
  {
    "engWord": "championship",
    "rusWord": "чемпионат",
    "transcription": "[ˈʧæmpɪənʃɪp]"
  },
  {
    "engWord": "protection",
    "rusWord": "защита",
    "transcription": "[prəˈtekʃn]"
  },
  {
    "engWord": "vote",
    "rusWord": "голос",
    "transcription": "[vəʊt]"
  },
  {
    "engWord": "regular",
    "rusWord": "регулярный",
    "transcription": "[ˈregjʊlə]"
  },
  {
    "engWord": "before",
    "rusWord": "до",
    "transcription": "[bɪˈfɔː]"
  },
  {
    "engWord": "person",
    "rusWord": "человек",
    "transcription": "[pɜːsn]"
  },
  {
    "engWord": "mix",
    "rusWord": "смесь",
    "transcription": "[mɪks]"
  },
  {
    "engWord": "traveling",
    "rusWord": "путешествие",
    "transcription": "[ˈtrævəlɪŋ]"
  },
  {
    "engWord": "tolerate",
    "rusWord": "терпеть",
    "transcription": "[ˈtɒləreɪt]"
  },
  {
    "engWord": "swallow",
    "rusWord": "глотать",
    "transcription": "[ˈswɒləʊ]"
  },
  {
    "engWord": "pear",
    "rusWord": "груша",
    "transcription": "[peə]"
  },
  {
    "engWord": "residence",
    "rusWord": "резиденция",
    "transcription": "[ˈrezɪdəns]"
  },
  {
    "engWord": "exaggerate",
    "rusWord": "преувеличивать",
    "transcription": "[ɪgˈzæʤəreɪt]"
  },
  {
    "engWord": "allergic",
    "rusWord": "аллергический",
    "transcription": "[əˈlɜːʤɪk]"
  },
  {
    "engWord": "foster",
    "rusWord": "способствовать",
    "transcription": "[ˈfɒstə]"
  },
  {
    "engWord": "incorrect",
    "rusWord": "неправильный",
    "transcription": "[ɪnkəˈrekt]"
  },
  {
    "engWord": "giant",
    "rusWord": "гигантский",
    "transcription": "[ˈʤaɪənt]"
  },
  {
    "engWord": "surrender",
    "rusWord": "сдаваться",
    "transcription": "[səˈrendə]"
  },
  {
    "engWord": "joint",
    "rusWord": "совместный",
    "transcription": "[ʤɔɪnt]"
  },
  {
    "engWord": "society",
    "rusWord": "общество",
    "transcription": "[səˈsaɪətɪ]"
  },
  {
    "engWord": "honest",
    "rusWord": "честный",
    "transcription": "[ˈɒnɪst]"
  },
  {
    "engWord": "uniform",
    "rusWord": "форма",
    "transcription": "[ˈjuːnɪfɔːm]"
  },
  {
    "engWord": "dollar",
    "rusWord": "доллар",
    "transcription": "[ˈdɒlə]"
  },
  {
    "engWord": "rub",
    "rusWord": "тереть",
    "transcription": "[rʌb]"
  },
  {
    "engWord": "come",
    "rusWord": "приходить",
    "transcription": "[kʌm]"
  },
  {
    "engWord": "question",
    "rusWord": "вопрос",
    "transcription": "[ˈkwesʧən]"
  },
  {
    "engWord": "allow",
    "rusWord": "позволять",
    "transcription": "[əˈlaʊ]"
  },
  {
    "engWord": "except",
    "rusWord": "кроме",
    "transcription": "[ɪkˈsept]"
  },
  {
    "engWord": "jay",
    "rusWord": "сойка",
    "transcription": "[ʤeɪ]"
  },
  {
    "engWord": "scaring",
    "rusWord": "отпугивание",
    "transcription": "[]"
  },
  {
    "engWord": "transparent",
    "rusWord": "прозрачный",
    "transcription": "[trænˈspærənt]"
  },
  {
    "engWord": "catholic",
    "rusWord": "католический",
    "transcription": "[ˈkæθəlɪk]"
  },
  {
    "engWord": "commerce",
    "rusWord": "торговля",
    "transcription": "[ˈkɒmɜːs]"
  },
  {
    "engWord": "bald",
    "rusWord": "лысый",
    "transcription": "[bɔːld]"
  },
  {
    "engWord": "adore",
    "rusWord": "обожать",
    "transcription": "[əˈdɔː]"
  },
  {
    "engWord": "skeleton",
    "rusWord": "скелет",
    "transcription": "[ˈskelɪtn]"
  },
  {
    "engWord": "innocent",
    "rusWord": "невинный",
    "transcription": "[ˈɪnəsənt]"
  },
  {
    "engWord": "pump",
    "rusWord": "насос",
    "transcription": "[pʌmp]"
  },
  {
    "engWord": "superior",
    "rusWord": "превосходящий",
    "transcription": "[sjuːˈpɪərɪə]"
  },
  {
    "engWord": "ancient",
    "rusWord": "древний",
    "transcription": "[ˈeɪnʃnt]"
  },
  {
    "engWord": "mysterious",
    "rusWord": "таинственный",
    "transcription": "[mɪsˈtɪərɪəs]"
  },
  {
    "engWord": "crocodile",
    "rusWord": "крокодил",
    "transcription": "[ˈkrɒkədaɪl]"
  },
  {
    "engWord": "con",
    "rusWord": "против",
    "transcription": "[kɒn]"
  },
  {
    "engWord": "poker",
    "rusWord": "покер",
    "transcription": "[ˈpəʊkə]"
  },
  {
    "engWord": "spare",
    "rusWord": "запасной",
    "transcription": "[speə]"
  },
  {
    "engWord": "largely",
    "rusWord": "в основном",
    "transcription": "[ˈlɑːʤlɪ]"
  },
  {
    "engWord": "error",
    "rusWord": "ошибка",
    "transcription": "[ˈerə]"
  },
  {
    "engWord": "opposite",
    "rusWord": "противоположный",
    "transcription": "[ˈɒpəzɪt]"
  },
  {
    "engWord": "from",
    "rusWord": "от",
    "transcription": "[frɒm]"
  },
  {
    "engWord": "surprise",
    "rusWord": "сюрприз",
    "transcription": "[səˈpraɪz]"
  },
  {
    "engWord": "tender",
    "rusWord": "нежный",
    "transcription": "[ˈtendə]"
  },
  {
    "engWord": "waiter",
    "rusWord": "официант",
    "transcription": "[ˈweɪtə]"
  },
  {
    "engWord": "daring",
    "rusWord": "отважный",
    "transcription": "[ˈdeərɪŋ]"
  },
  {
    "engWord": "fabric",
    "rusWord": "ткань",
    "transcription": "[ˈfæbrɪk]"
  },
  {
    "engWord": "grip",
    "rusWord": "захват",
    "transcription": "[grɪp]"
  },
  {
    "engWord": "eager",
    "rusWord": "жаждущий",
    "transcription": "[ˈiːgə]"
  },
  {
    "engWord": "splendid",
    "rusWord": "великолепный",
    "transcription": "[ˈsplendɪd]"
  },
  {
    "engWord": "optional",
    "rusWord": "необязательный",
    "transcription": "[ˈɒpʃnəl]"
  },
  {
    "engWord": "tricky",
    "rusWord": "хитрый",
    "transcription": "[ˈtrɪkɪ]"
  },
  {
    "engWord": "purchase",
    "rusWord": "покупка",
    "transcription": "[ˈpɜːʧɪs]"
  },
  {
    "engWord": "earn",
    "rusWord": "зарабатывать",
    "transcription": "[ɜːn]"
  },
  {
    "engWord": "concentrate",
    "rusWord": "концентрат",
    "transcription": "[ˈkɒnsəntreɪt]"
  },
  {
    "engWord": "kettle",
    "rusWord": "чайник",
    "transcription": "[ketl]"
  },
  {
    "engWord": "movement",
    "rusWord": "движение",
    "transcription": "[ˈmuːvmənt]"
  },
  {
    "engWord": "switch",
    "rusWord": "переключатель",
    "transcription": "[swɪʧ]"
  },
  {
    "engWord": "later",
    "rusWord": "позже",
    "transcription": "[ˈleɪtə]"
  },
  {
    "engWord": "water",
    "rusWord": "вода",
    "transcription": "[ˈwɔːtə]"
  },
  {
    "engWord": "king",
    "rusWord": "король",
    "transcription": "[kɪŋ]"
  },
  {
    "engWord": "have",
    "rusWord": "иметь",
    "transcription": "[hæv]"
  },
  {
    "engWord": "see",
    "rusWord": "видеть",
    "transcription": "[siː]"
  },
  {
    "engWord": "place",
    "rusWord": "место",
    "transcription": "[pleɪs]"
  },
  {
    "engWord": "art",
    "rusWord": "искусство",
    "transcription": "[ɑːt]"
  },
  {
    "engWord": "billion",
    "rusWord": "миллиард",
    "transcription": "[ˈbɪljən]"
  },
  {
    "engWord": "delightful",
    "rusWord": "восхитительный",
    "transcription": "[dɪˈlaɪtf(ə)l]"
  },
  {
    "engWord": "nod",
    "rusWord": "кивать",
    "transcription": "[nɒd]"
  },
  {
    "engWord": "immune",
    "rusWord": "иммунный",
    "transcription": "[ɪˈmjuːn]"
  },
  {
    "engWord": "ambiguous",
    "rusWord": "двусмысленный",
    "transcription": "[æmˈbɪgjʊəs]"
  },
  {
    "engWord": "insert",
    "rusWord": "вставлять",
    "transcription": "[ˈɪnsɜːt]"
  },
  {
    "engWord": "scar",
    "rusWord": "шрам",
    "transcription": "[skɑː]"
  },
  {
    "engWord": "unexpected",
    "rusWord": "неожиданный",
    "transcription": "[ʌnɪkˈspektɪd]"
  },
  {
    "engWord": "horizontal",
    "rusWord": "горизонтальный",
    "transcription": "[hɒrɪˈzɒntl]"
  },
  {
    "engWord": "cheek",
    "rusWord": "щека",
    "transcription": "[ʧiːk]"
  },
  {
    "engWord": "individual",
    "rusWord": "индивидуальный",
    "transcription": "[ɪndɪˈvɪʤʊəl]"
  },
  {
    "engWord": "twist",
    "rusWord": "твист",
    "transcription": "[twɪst]"
  },
  {
    "engWord": "feedback",
    "rusWord": "обратная связь",
    "transcription": "[ˈfiːdbæk]"
  },
  {
    "engWord": "cabinet",
    "rusWord": "кабинет",
    "transcription": "[ˈkæbɪnɪt]"
  },
  {
    "engWord": "guarantee",
    "rusWord": "гарантия",
    "transcription": "[gærənˈtiː]"
  },
  {
    "engWord": "cherry",
    "rusWord": "вишня",
    "transcription": "[ˈʧerɪ]"
  },
  {
    "engWord": "letter",
    "rusWord": "письмо",
    "transcription": "[ˈletə]"
  },
  {
    "engWord": "kind",
    "rusWord": "добрый",
    "transcription": "[kaɪnd]"
  },
  {
    "engWord": "help",
    "rusWord": "помощь",
    "transcription": "[help]"
  },
  {
    "engWord": "read",
    "rusWord": "читать",
    "transcription": "[riːd]"
  },
  {
    "engWord": "midterm",
    "rusWord": "промежуточный",
    "transcription": "[ˈmɪdtɜːm]"
  },
  {
    "engWord": "rainy",
    "rusWord": "дождливый",
    "transcription": "[ˈreɪnɪ]"
  },
  {
    "engWord": "loitering",
    "rusWord": "барражирующий",
    "transcription": "[ˈlɔɪtərɪŋ]"
  },
  {
    "engWord": "pathetic",
    "rusWord": "жалкий",
    "transcription": "[pəˈθetɪk]"
  },
  {
    "engWord": "wolf",
    "rusWord": "волк",
    "transcription": "[wʊlf]"
  },
  {
    "engWord": "catalogue",
    "rusWord": "каталог",
    "transcription": "[ˈkætəlɒg]"
  },
  {
    "engWord": "understand",
    "rusWord": "понимать",
    "transcription": "[ʌndəˈstænd]"
  },
  {
    "engWord": "via",
    "rusWord": "через",
    "transcription": "[ˈvaɪə]"
  },
  {
    "engWord": "van",
    "rusWord": "фургон",
    "transcription": "[væn]"
  },
  {
    "engWord": "emergency",
    "rusWord": "чрезвычайная ситуация",
    "transcription": "[ɪˈmɜːʤənsɪ]"
  },
  {
    "engWord": "himself",
    "rusWord": "сам",
    "transcription": "[hɪmˈself]"
  },
  {
    "engWord": "biggest",
    "rusWord": "наибольший",
    "transcription": "[ˈbɪgɪst]"
  },
  {
    "engWord": "back",
    "rusWord": "назад",
    "transcription": "[bæk]"
  },
  {
    "engWord": "motion",
    "rusWord": "движение",
    "transcription": "[məʊʃn]"
  },
  {
    "engWord": "congress",
    "rusWord": "конгресс",
    "transcription": "[ˈkɒngres]"
  },
  {
    "engWord": "complex",
    "rusWord": "сложный",
    "transcription": "[ˈkɒmpleks]"
  },
  {
    "engWord": "younger",
    "rusWord": "младший",
    "transcription": "[ˈjʌngə]"
  },
  {
    "engWord": "debris",
    "rusWord": "развалины",
    "transcription": "[ˈdeɪbriː]"
  },
  {
    "engWord": "crushed",
    "rusWord": "разгромленный",
    "transcription": "[krʌʃt]"
  },
  {
    "engWord": "barbarian",
    "rusWord": "варвар",
    "transcription": "[bɑːˈbe(ə)rɪən]"
  },
  {
    "engWord": "initial",
    "rusWord": "первоначальный",
    "transcription": "[ɪˈnɪʃəl]"
  },
  {
    "engWord": "powerful",
    "rusWord": "мощный",
    "transcription": "[ˈpaʊəf(ə)l]"
  },
  {
    "engWord": "midnight",
    "rusWord": "полночь",
    "transcription": "[ˈmɪdnaɪt]"
  },
  {
    "engWord": "extra",
    "rusWord": "дополнительный",
    "transcription": "[ˈekstrə]"
  },
  {
    "engWord": "grandfather",
    "rusWord": "дедушка",
    "transcription": "[ˈgrændfɑːðə]"
  },
  {
    "engWord": "name",
    "rusWord": "имя",
    "transcription": "[neɪm]"
  },
  {
    "engWord": "light",
    "rusWord": "свет",
    "transcription": "[laɪt]"
  },
  {
    "engWord": "notice",
    "rusWord": "уведомление",
    "transcription": "[ˈnəʊtɪs]"
  },
  {
    "engWord": "expel",
    "rusWord": "высылать",
    "transcription": "[ɪksˈpel]"
  },
  {
    "engWord": "document",
    "rusWord": "документ",
    "transcription": "[ˈdɒkjʊmənt]"
  },
  {
    "engWord": "hinder",
    "rusWord": "препятствовать",
    "transcription": "[ˈhɪndə]"
  },
  {
    "engWord": "ashes",
    "rusWord": "зола",
    "transcription": "[ˈæʃɪz]"
  },
  {
    "engWord": "charm",
    "rusWord": "очарование",
    "transcription": "[ʧɑːm]"
  },
  {
    "engWord": "unhappy",
    "rusWord": "несчастный",
    "transcription": "[ʌnˈhæpɪ]"
  },
  {
    "engWord": "cease",
    "rusWord": "прекращение",
    "transcription": "[siːs]"
  },
  {
    "engWord": "relate",
    "rusWord": "относиться",
    "transcription": "[rɪˈleɪt]"
  },
  {
    "engWord": "similarly",
    "rusWord": "аналогично",
    "transcription": "[ˈsɪmɪləlɪ]"
  },
  {
    "engWord": "academic",
    "rusWord": "академический",
    "transcription": "[ækəˈdemɪk]"
  },
  {
    "engWord": "expensive",
    "rusWord": "дорогой",
    "transcription": "[ɪksˈpensɪv]"
  },
  {
    "engWord": "emotional",
    "rusWord": "эмоциональный",
    "transcription": "[ɪˈməʊʃnəl]"
  },
  {
    "engWord": "treat",
    "rusWord": "лечить",
    "transcription": "[triːt]"
  },
  {
    "engWord": "capable",
    "rusWord": "способный",
    "transcription": "[ˈkeɪpəbl]"
  },
  {
    "engWord": "miss",
    "rusWord": "мисс",
    "transcription": "[mɪs]"
  },
  {
    "engWord": "those",
    "rusWord": "те",
    "transcription": "[ðəʊz]"
  },
  {
    "engWord": "lie",
    "rusWord": "ложь",
    "transcription": "[laɪ]"
  },
  {
    "engWord": "homecoming",
    "rusWord": "возвращение",
    "transcription": "[ˈhəʊmkʌmɪŋ]"
  },
  {
    "engWord": "corporate",
    "rusWord": "корпоративный",
    "transcription": "[ˈkɔːpərɪt]"
  },
  {
    "engWord": "insensitive",
    "rusWord": "нечуткий",
    "transcription": "[ɪnˈsensɪtɪv]"
  },
  {
    "engWord": "genetic",
    "rusWord": "генетический",
    "transcription": "[ʤɪˈnetɪk]"
  },
  {
    "engWord": "currently",
    "rusWord": "в настоящий момент",
    "transcription": "[ˈkʌrəntlɪ]"
  },
  {
    "engWord": "redundancy",
    "rusWord": "избыточность",
    "transcription": "[rɪˈdʌndənsɪ]"
  },
  {
    "engWord": "resume",
    "rusWord": "продолжить",
    "transcription": "[rɪˈzjuːm]"
  },
  {
    "engWord": "combination",
    "rusWord": "сочетание",
    "transcription": "[kɒmbɪˈneɪʃn]"
  },
  {
    "engWord": "numerous",
    "rusWord": "многочисленный",
    "transcription": "[ˈnjuːmərəs]"
  },
  {
    "engWord": "occasionally",
    "rusWord": "периодически",
    "transcription": "[əˈkeɪʒnəlɪ]"
  },
  {
    "engWord": "voluntary",
    "rusWord": "добровольный",
    "transcription": "[ˈvɒləntərɪ]"
  },
  {
    "engWord": "badge",
    "rusWord": "значок",
    "transcription": "[bæʤ]"
  },
  {
    "engWord": "barrier",
    "rusWord": "барьер",
    "transcription": "[ˈbærɪə]"
  },
  {
    "engWord": "Archaic",
    "rusWord": "архаичный",
    "transcription": "[ɑːˈkeɪɪk]"
  },
  {
    "engWord": "shift",
    "rusWord": "сдвиг",
    "transcription": "[ʃɪft]"
  },
  {
    "engWord": "alright",
    "rusWord": "в порядке",
    "transcription": "[ɔːlˈraɪt]"
  },
  {
    "engWord": "knock",
    "rusWord": "стучать",
    "transcription": "[nɒk]"
  },
  {
    "engWord": "liar",
    "rusWord": "лжец",
    "transcription": "[ˈlaɪə]"
  },
  {
    "engWord": "permission",
    "rusWord": "разрешение",
    "transcription": "[pəˈmɪʃn]"
  },
  {
    "engWord": "game",
    "rusWord": "игра",
    "transcription": "[geɪm]"
  },
  {
    "engWord": "vowel",
    "rusWord": "гласный звук",
    "transcription": "[ˈvaʊəl]"
  },
  {
    "engWord": "south",
    "rusWord": "юг",
    "transcription": "[saʊθ]"
  },
  {
    "engWord": "separate",
    "rusWord": "разделять",
    "transcription": "[]"
  },
  {
    "engWord": "shine",
    "rusWord": "сиять",
    "transcription": "[ʃaɪn]"
  },
  {
    "engWord": "zipper",
    "rusWord": "молния",
    "transcription": "[ˈzɪpə]"
  },
  {
    "engWord": "congratulate",
    "rusWord": "поздравлять",
    "transcription": "[kənˈgrætjʊleɪt]"
  },
  {
    "engWord": "undercover",
    "rusWord": "тайный",
    "transcription": "[ˈʌndəkʌvə]"
  },
  {
    "engWord": "irresponsible",
    "rusWord": "безответственный",
    "transcription": "[ɪrɪsˈpɒnsəbl]"
  },
  {
    "engWord": "made-up",
    "rusWord": "составленный",
    "transcription": "[meɪd ʌp]"
  },
  {
    "engWord": "favour",
    "rusWord": "одолжение",
    "transcription": "[ˈfeɪvə]"
  },
  {
    "engWord": "larger",
    "rusWord": "больше",
    "transcription": "[lɑːʤə]"
  },
  {
    "engWord": "partial",
    "rusWord": "частичный",
    "transcription": "[ˈpɑːʃəl]"
  },
  {
    "engWord": "fatality",
    "rusWord": "фатальность",
    "transcription": "[fəˈtælɪtɪ]"
  },
  {
    "engWord": "searching",
    "rusWord": "испытующий",
    "transcription": "[ˈsɜːʧɪŋ]"
  },
  {
    "engWord": "solid",
    "rusWord": "твердый",
    "transcription": "[ˈsɒlɪd]"
  },
  {
    "engWord": "concert",
    "rusWord": "концерт",
    "transcription": "[ˈkɒnsət]"
  },
  {
    "engWord": "per",
    "rusWord": "по",
    "transcription": "[pɜː]"
  },
  {
    "engWord": "tune",
    "rusWord": "мелодия",
    "transcription": "[tjuːn]"
  },
  {
    "engWord": "mister",
    "rusWord": "мистер",
    "transcription": "[ˈmɪstə]"
  },
  {
    "engWord": "already",
    "rusWord": "уже",
    "transcription": "[ɔːlˈredɪ]"
  },
  {
    "engWord": "husband",
    "rusWord": "муж",
    "transcription": "[ˈhʌzbənd]"
  },
  {
    "engWord": "health",
    "rusWord": "здоровье",
    "transcription": "[helθ]"
  },
  {
    "engWord": "source",
    "rusWord": "источник",
    "transcription": "[sɔːs]"
  },
  {
    "engWord": "wool",
    "rusWord": "шерсть",
    "transcription": "[wʊl]"
  },
  {
    "engWord": "choice",
    "rusWord": "выбор",
    "transcription": "[ʧɔɪs]"
  },
  {
    "engWord": "divide",
    "rusWord": "делить",
    "transcription": "[dɪˈvaɪd]"
  },
  {
    "engWord": "orthodox",
    "rusWord": "ортодоксальный",
    "transcription": "[ˈɔːθədɔks]"
  },
  {
    "engWord": "subtle",
    "rusWord": "тонкий",
    "transcription": "[sʌtl]"
  },
  {
    "engWord": "aids",
    "rusWord": "СПИД",
    "transcription": "[eɪdz]"
  },
  {
    "engWord": "sprinkle",
    "rusWord": "посыпать",
    "transcription": "[sprɪŋkl]"
  },
  {
    "engWord": "bureaucracy",
    "rusWord": "бюрократия",
    "transcription": "[bjʊəˈrɒkrəsɪ]"
  },
  {
    "engWord": "fluctuate",
    "rusWord": "колебаться",
    "transcription": "[ˈflʌkʧʊeɪt]"
  },
  {
    "engWord": "notorious",
    "rusWord": "пресловутый",
    "transcription": "[nəʊˈtɔːrɪəs]"
  },
  {
    "engWord": "thinking",
    "rusWord": "размышляющий",
    "transcription": "[ˈθɪŋkɪŋ]"
  },
  {
    "engWord": "scarf",
    "rusWord": "шарф",
    "transcription": "[skɑːf]"
  },
  {
    "engWord": "merchant",
    "rusWord": "торговец",
    "transcription": "[ˈmɜːʧənt]"
  },
  {
    "engWord": "creep",
    "rusWord": "ползать",
    "transcription": "[kriːp]"
  },
  {
    "engWord": "notion",
    "rusWord": "понятие",
    "transcription": "[nəʊʃn]"
  },
  {
    "engWord": "grief",
    "rusWord": "горе",
    "transcription": "[griːf]"
  },
  {
    "engWord": "workshop",
    "rusWord": "мастерская",
    "transcription": "[ˈwɜːkʃɒp]"
  },
  {
    "engWord": "insane",
    "rusWord": "безумный",
    "transcription": "[ɪnˈseɪn]"
  },
  {
    "engWord": "future",
    "rusWord": "будущее",
    "transcription": "[ˈfjuːʧə]"
  },
  {
    "engWord": "ignore",
    "rusWord": "игнорировать",
    "transcription": "[ɪgˈnɔː]"
  },
  {
    "engWord": "cereal",
    "rusWord": "злаковый",
    "transcription": "[ˈsɪərɪəl]"
  },
  {
    "engWord": "cheque",
    "rusWord": "чек",
    "transcription": "[ʧek]"
  },
  {
    "engWord": "tower",
    "rusWord": "башня",
    "transcription": "[ˈtaʊə]"
  },
  {
    "engWord": "truck",
    "rusWord": "грузовик",
    "transcription": "[trʌk]"
  },
  {
    "engWord": "last",
    "rusWord": "последний",
    "transcription": "[lɑːst]"
  },
  {
    "engWord": "power",
    "rusWord": "сила",
    "transcription": "[ˈpaʊə]"
  },
  {
    "engWord": "remaining",
    "rusWord": "оставшийся",
    "transcription": "[rɪˈmeɪnɪŋ]"
  },
  {
    "engWord": "negotiate",
    "rusWord": "вести переговоры",
    "transcription": "[nɪˈgəʊʃɪeɪt]"
  },
  {
    "engWord": "penalty",
    "rusWord": "штраф",
    "transcription": "[ˈpenltɪ]"
  },
  {
    "engWord": "laying",
    "rusWord": "прокладка",
    "transcription": "[ˈleɪɪŋ]"
  },
  {
    "engWord": "honourable",
    "rusWord": "достопочтенный",
    "transcription": "[ˈɒn(ə)rəb(ə)l]"
  },
  {
    "engWord": "amen",
    "rusWord": "аминь",
    "transcription": "[ˈɑːmən]"
  },
  {
    "engWord": "penny",
    "rusWord": "пенни",
    "transcription": "[ˈpenɪ]"
  },
  {
    "engWord": "trap",
    "rusWord": "ловушка",
    "transcription": "[træp]"
  },
  {
    "engWord": "outside",
    "rusWord": "снаружи",
    "transcription": "[ˈaʊtsaɪd]"
  },
  {
    "engWord": "cleaning",
    "rusWord": "уборка",
    "transcription": "[ˈkliːnɪŋ]"
  },
  {
    "engWord": "avoid",
    "rusWord": "избегать",
    "transcription": "[əˈvɔɪd]"
  },
  {
    "engWord": "normal",
    "rusWord": "нормальный",
    "transcription": "[ˈnɔːməl]"
  },
  {
    "engWord": "element",
    "rusWord": "элемент",
    "transcription": "[ˈelɪmənt]"
  },
  {
    "engWord": "east",
    "rusWord": "Восток",
    "transcription": "[iːst]"
  },
  {
    "engWord": "cat",
    "rusWord": "кошка",
    "transcription": "[kæt]"
  },
  {
    "engWord": "population",
    "rusWord": "население",
    "transcription": "[pɒpjʊˈleɪʃn]"
  },
  {
    "engWord": "stationery",
    "rusWord": "канцелярские товары",
    "transcription": "[ˈsteɪʃən(ə)rɪ]"
  },
  {
    "engWord": "drain",
    "rusWord": "истощать",
    "transcription": "[dreɪn]"
  },
  {
    "engWord": "diameter",
    "rusWord": "диаметр",
    "transcription": "[daɪˈæmɪtə]"
  },
  {
    "engWord": "grain",
    "rusWord": "зерно",
    "transcription": "[greɪn]"
  },
  {
    "engWord": "trauma",
    "rusWord": "травма",
    "transcription": "[ˈtrɔːmə]"
  },
  {
    "engWord": "careless",
    "rusWord": "беспечный",
    "transcription": "[ˈkeəlɪs]"
  },
  {
    "engWord": "fame",
    "rusWord": "слава",
    "transcription": "[feɪm]"
  },
  {
    "engWord": "lemon",
    "rusWord": "лимон",
    "transcription": "[ˈlemən]"
  },
  {
    "engWord": "formation",
    "rusWord": "формирование",
    "transcription": "[fɔːˈmeɪʃn]"
  },
  {
    "engWord": "sport",
    "rusWord": "спорт",
    "transcription": "[spɔːt]"
  },
  {
    "engWord": "receipt",
    "rusWord": "чек",
    "transcription": "[rɪˈsiːt]"
  },
  {
    "engWord": "schedule",
    "rusWord": "график",
    "transcription": "[ˈʃedjuːl]"
  },
  {
    "engWord": "rarely",
    "rusWord": "редко",
    "transcription": "[ˈreəlɪ]"
  },
  {
    "engWord": "short",
    "rusWord": "короткий",
    "transcription": "[ʃɔːt]"
  },
  {
    "engWord": "station",
    "rusWord": "станция",
    "transcription": "[steɪʃn]"
  },
  {
    "engWord": "second",
    "rusWord": "второй",
    "transcription": "[ˈsekənd]"
  },
  {
    "engWord": "north",
    "rusWord": "северный",
    "transcription": "[nɔːθ]"
  },
  {
    "engWord": "grow",
    "rusWord": "расти",
    "transcription": "[grəʊ]"
  },
  {
    "engWord": "choose",
    "rusWord": "выбирать",
    "transcription": "[ʧuːz]"
  },
  {
    "engWord": "bore",
    "rusWord": "скука",
    "transcription": "[bɔː]"
  },
  {
    "engWord": "superb",
    "rusWord": "превосходный",
    "transcription": "[sjuːˈpɜːb]"
  },
  {
    "engWord": "humiliation",
    "rusWord": "унижение",
    "transcription": "[hjuːmɪlɪˈeɪʃn]"
  },
  {
    "engWord": "midday",
    "rusWord": "полдень",
    "transcription": "[ˈmɪddeɪ]"
  },
  {
    "engWord": "boot",
    "rusWord": "ботинок",
    "transcription": "[buːt]"
  },
  {
    "engWord": "pacific",
    "rusWord": "Тихоокеанский",
    "transcription": "[pəˈsɪfɪk]"
  },
  {
    "engWord": "zip",
    "rusWord": "застежка-молния",
    "transcription": "[zɪp]"
  },
  {
    "engWord": "mania",
    "rusWord": "мания",
    "transcription": "[ˈmeɪnɪə]"
  },
  {
    "engWord": "therefore",
    "rusWord": "следовательно",
    "transcription": "[ˈðeəfɔː]"
  },
  {
    "engWord": "principle",
    "rusWord": "основа",
    "transcription": "[ˈprɪnsɪp(ə)l]"
  },
  {
    "engWord": "slowly",
    "rusWord": "медленно",
    "transcription": "[ˈsləʊlɪ]"
  },
  {
    "engWord": "improvement",
    "rusWord": "улучшение",
    "transcription": "[ɪmˈpruːvmənt]"
  },
  {
    "engWord": "implement",
    "rusWord": "осуществлять",
    "transcription": "[ˈɪmplɪmənt]"
  },
  {
    "engWord": "employ",
    "rusWord": "использовать",
    "transcription": "[ɪmˈplɔɪ]"
  },
  {
    "engWord": "heaven",
    "rusWord": "небо",
    "transcription": "[hevn]"
  },
  {
    "engWord": "crisis",
    "rusWord": "кризис",
    "transcription": "[ˈkraɪsɪs]"
  },
  {
    "engWord": "rice",
    "rusWord": "рис",
    "transcription": "[raɪs]"
  },
  {
    "engWord": "charming",
    "rusWord": "очаровательный",
    "transcription": "[ˈʧɑːmɪŋ]"
  },
  {
    "engWord": "triangle",
    "rusWord": "треугольник",
    "transcription": "[ˈtraɪæŋgl]"
  },
  {
    "engWord": "people",
    "rusWord": "люди",
    "transcription": "[piːpl]"
  },
  {
    "engWord": "whether",
    "rusWord": "ли",
    "transcription": "[ˈweðə]"
  },
  {
    "engWord": "late",
    "rusWord": "поздний",
    "transcription": "[leɪt]"
  },
  {
    "engWord": "even",
    "rusWord": "даже",
    "transcription": "[ˈiːvən]"
  },
  {
    "engWord": "atmosphere",
    "rusWord": "атмосфера",
    "transcription": "[ˈætməsfɪə]"
  },
  {
    "engWord": "ignition",
    "rusWord": "зажигание",
    "transcription": "[ɪgˈnɪʃn]"
  },
  {
    "engWord": "cucumber",
    "rusWord": "огурец",
    "transcription": "[ˈkjuːkʌmbə]"
  },
  {
    "engWord": "marble",
    "rusWord": "мрамор",
    "transcription": "[mɑːbl]"
  },
  {
    "engWord": "retain",
    "rusWord": "удерживать",
    "transcription": "[rɪˈteɪn]"
  },
  {
    "engWord": "discount",
    "rusWord": "скидка",
    "transcription": "[ˈdɪskaʊnt]"
  },
  {
    "engWord": "principal",
    "rusWord": "главный",
    "transcription": "[ˈprɪnsɪp(ə)l]"
  },
  {
    "engWord": "jockey",
    "rusWord": "жокей",
    "transcription": "[ˈʤɒkɪ]"
  },
  {
    "engWord": "proposal",
    "rusWord": "предложение",
    "transcription": "[prəˈpəʊzəl]"
  },
  {
    "engWord": "nice",
    "rusWord": "милый",
    "transcription": "[naɪs]"
  },
  {
    "engWord": "everywhere",
    "rusWord": "везде",
    "transcription": "[ˈevrɪweə]"
  },
  {
    "engWord": "attitude",
    "rusWord": "отношение",
    "transcription": "[ˈætɪtjuːd]"
  },
  {
    "engWord": "shit",
    "rusWord": "дрянь",
    "transcription": "[ʃɪt]"
  },
  {
    "engWord": "frequent",
    "rusWord": "частый",
    "transcription": "[ˈfriːkwənt]"
  },
  {
    "engWord": "hello",
    "rusWord": "привет",
    "transcription": "[həˈləʊ]"
  },
  {
    "engWord": "anger",
    "rusWord": "гнев",
    "transcription": "[ˈæŋgə]"
  },
  {
    "engWord": "spend",
    "rusWord": "тратить",
    "transcription": "[spend]"
  },
  {
    "engWord": "mother",
    "rusWord": "мама",
    "transcription": "[ˈmʌðə]"
  },
  {
    "engWord": "down",
    "rusWord": "вниз",
    "transcription": "[daʊn]"
  },
  {
    "engWord": "necessary",
    "rusWord": "необходимый",
    "transcription": "[ˈnesɪsərɪ]"
  },
  {
    "engWord": "sit",
    "rusWord": "сидеть",
    "transcription": "[sɪt]"
  },
  {
    "engWord": "balanced",
    "rusWord": "сбалансированный",
    "transcription": "[ˈbælənst]"
  },
  {
    "engWord": "inconvenience",
    "rusWord": "неудобство",
    "transcription": "[ɪnkənˈviːnɪəns]"
  },
  {
    "engWord": "associate",
    "rusWord": "связывать",
    "transcription": "[]"
  },
  {
    "engWord": "pram",
    "rusWord": "детская коляска",
    "transcription": "[præm]"
  },
  {
    "engWord": "investigator",
    "rusWord": "исследователь",
    "transcription": "[ɪnˈvestɪgeɪtə]"
  },
  {
    "engWord": "strategic",
    "rusWord": "стратегический",
    "transcription": "[strəˈtiːʤɪk]"
  },
  {
    "engWord": "sailing",
    "rusWord": "парусный спорт",
    "transcription": "[ˈseɪlɪŋ]"
  },
  {
    "engWord": "recovery",
    "rusWord": "восстановление",
    "transcription": "[rɪˈkʌvərɪ]"
  },
  {
    "engWord": "publication",
    "rusWord": "публикация",
    "transcription": "[pʌblɪˈkeɪʃn]"
  },
  {
    "engWord": "liberty",
    "rusWord": "свобода",
    "transcription": "[ˈlɪbətɪ]"
  },
  {
    "engWord": "nowadays",
    "rusWord": "в наше время",
    "transcription": "[ˈnaʊədeɪz]"
  },
  {
    "engWord": "staff",
    "rusWord": "персонал",
    "transcription": "[stɑːf]"
  },
  {
    "engWord": "bleeding",
    "rusWord": "кровотечение",
    "transcription": "[ˈbliːdɪŋ]"
  },
  {
    "engWord": "strawberry",
    "rusWord": "клубника",
    "transcription": "[ˈstrɔːbərɪ]"
  },
  {
    "engWord": "know",
    "rusWord": "знать",
    "transcription": "[nəʊ]"
  },
  {
    "engWord": "spoke",
    "rusWord": "спица",
    "transcription": "[spəʊk]"
  },
  {
    "engWord": "correct",
    "rusWord": "правильный",
    "transcription": "[kəˈrekt]"
  },
  {
    "engWord": "welcome",
    "rusWord": "добро пожаловать",
    "transcription": "[ˈwelkəm]"
  },
  {
    "engWord": "southern",
    "rusWord": "Южный",
    "transcription": "[ˈsʌðən]"
  },
  {
    "engWord": "intestine",
    "rusWord": "кишечник",
    "transcription": "[ɪnˈtestɪn]"
  },
  {
    "engWord": "avenue",
    "rusWord": "проспект",
    "transcription": "[ˈævɪnjuː]"
  },
  {
    "engWord": "hoarse",
    "rusWord": "хриплый",
    "transcription": "[hɔːs]"
  },
  {
    "engWord": "irritate",
    "rusWord": "раздражать",
    "transcription": "[ˈɪrɪteɪt]"
  },
  {
    "engWord": "makeup",
    "rusWord": "состав",
    "transcription": "[ˈmeɪkʌp]"
  },
  {
    "engWord": "glorious",
    "rusWord": "блестящий",
    "transcription": "[ˈglɔːrɪəs]"
  },
  {
    "engWord": "thirteen",
    "rusWord": "тринадцать",
    "transcription": "[θɜːˈtiːn]"
  },
  {
    "engWord": "gangster",
    "rusWord": "гангстер",
    "transcription": "[ˈgæŋstə]"
  },
  {
    "engWord": "diagnosis",
    "rusWord": "диагноз",
    "transcription": "[daɪəgˈnəʊsɪs]"
  },
  {
    "engWord": "headline",
    "rusWord": "заголовок",
    "transcription": "[ˈhedlaɪn]"
  },
  {
    "engWord": "opponent",
    "rusWord": "противник",
    "transcription": "[əˈpəʊnənt]"
  },
  {
    "engWord": "toss",
    "rusWord": "бросать жребий",
    "transcription": "[tɒs]"
  },
  {
    "engWord": "deliberately",
    "rusWord": "умышленно",
    "transcription": "[dɪˈlɪbərɪtlɪ]"
  },
  {
    "engWord": "fork",
    "rusWord": "вилка",
    "transcription": "[fɔːk]"
  },
  {
    "engWord": "keyboard",
    "rusWord": "клавиатура",
    "transcription": "[ˈkiːbɔːd]"
  },
  {
    "engWord": "will",
    "rusWord": "будет",
    "transcription": "[wɪl]"
  },
  {
    "engWord": "black",
    "rusWord": "черный",
    "transcription": "[blæk]"
  },
  {
    "engWord": "pose",
    "rusWord": "позировать",
    "transcription": "[pəʊz]"
  },
  {
    "engWord": "classmate",
    "rusWord": "одноклассник",
    "transcription": "[ˈklɑːsmeɪt]"
  },
  {
    "engWord": "monopoly",
    "rusWord": "монополия",
    "transcription": "[məˈnɒpəlɪ]"
  },
  {
    "engWord": "buffet",
    "rusWord": "буфет",
    "transcription": "[ˈbʌfɪt]"
  },
  {
    "engWord": "confectionery",
    "rusWord": "кондитерские изделия",
    "transcription": "[kənˈfekʃən(ə)rɪ]"
  },
  {
    "engWord": "harsh",
    "rusWord": "суровый",
    "transcription": "[hɑːʃ]"
  },
  {
    "engWord": "overwhelm",
    "rusWord": "подавлять",
    "transcription": "[əʊvəˈwelm]"
  },
  {
    "engWord": "booze",
    "rusWord": "пьянка",
    "transcription": "[buːz]"
  },
  {
    "engWord": "cigar",
    "rusWord": "сигара",
    "transcription": "[sɪˈgɑː]"
  },
  {
    "engWord": "bang",
    "rusWord": "взрыв",
    "transcription": "[bæŋ]"
  },
  {
    "engWord": "chosen",
    "rusWord": "выбранный",
    "transcription": "[ʧəʊzn]"
  },
  {
    "engWord": "originally",
    "rusWord": "первоначально",
    "transcription": "[əˈrɪʤɪnəlɪ]"
  },
  {
    "engWord": "frank",
    "rusWord": "Фрэнк",
    "transcription": "[fræŋk]"
  },
  {
    "engWord": "biscuit",
    "rusWord": "печенье",
    "transcription": "[ˈbɪskɪt]"
  },
  {
    "engWord": "partly",
    "rusWord": "частично",
    "transcription": "[ˈpɑːtlɪ]"
  },
  {
    "engWord": "terrible",
    "rusWord": "ужасный",
    "transcription": "[ˈterəbl]"
  },
  {
    "engWord": "signal",
    "rusWord": "сигнал",
    "transcription": "[sɪgnl]"
  },
  {
    "engWord": "bicycle",
    "rusWord": "велосипед",
    "transcription": "[ˈbaɪsɪkl]"
  },
  {
    "engWord": "lethal",
    "rusWord": "смертельный",
    "transcription": "[ˈliːθəl]"
  },
  {
    "engWord": "graphics",
    "rusWord": "графика",
    "transcription": "[ˈgræfɪks]"
  },
  {
    "engWord": "reside",
    "rusWord": "обитать",
    "transcription": "[rɪˈzaɪd]"
  },
  {
    "engWord": "limited",
    "rusWord": "ограниченный",
    "transcription": "[ˈlɪmɪtɪd]"
  },
  {
    "engWord": "violation",
    "rusWord": "нарушение",
    "transcription": "[vaɪəˈleɪʃn]"
  },
  {
    "engWord": "assertive",
    "rusWord": "утвердительный",
    "transcription": "[əˈsɜːtɪv]"
  },
  {
    "engWord": "harbour",
    "rusWord": "гавань",
    "transcription": "[ˈhɑːbə]"
  },
  {
    "engWord": "nap",
    "rusWord": "вздремнуть",
    "transcription": "[næp]"
  },
  {
    "engWord": "reader",
    "rusWord": "читатель",
    "transcription": "[ˈriːdə]"
  },
  {
    "engWord": "limitation",
    "rusWord": "ограничение",
    "transcription": "[lɪmɪˈteɪʃn]"
  },
  {
    "engWord": "achievement",
    "rusWord": "достижение",
    "transcription": "[əˈʧiːvmənt]"
  },
  {
    "engWord": "pro",
    "rusWord": "про",
    "transcription": "[prəʊ]"
  },
  {
    "engWord": "swing",
    "rusWord": "качать",
    "transcription": "[swɪŋ]"
  },
  {
    "engWord": "ray",
    "rusWord": "скат",
    "transcription": "[reɪ]"
  },
  {
    "engWord": "genius",
    "rusWord": "гений",
    "transcription": "[ˈʤiːnɪəs]"
  },
  {
    "engWord": "honeymoon",
    "rusWord": "медовый месяц",
    "transcription": "[ˈhʌnɪmuːn]"
  },
  {
    "engWord": "sensitive",
    "rusWord": "чувствительный",
    "transcription": "[ˈsensɪtɪv]"
  },
  {
    "engWord": "saving",
    "rusWord": "экономия",
    "transcription": "[ˈseɪvɪŋ]"
  },
  {
    "engWord": "through",
    "rusWord": "через",
    "transcription": "[θruː]"
  },
  {
    "engWord": "star",
    "rusWord": "звезда",
    "transcription": "[stɑː]"
  },
  {
    "engWord": "soft",
    "rusWord": "мягкий",
    "transcription": "[sɒft]"
  },
  {
    "engWord": "mark",
    "rusWord": "отметка",
    "transcription": "[mɑːk]"
  },
  {
    "engWord": "guess",
    "rusWord": "догадываться",
    "transcription": "[ges]"
  },
  {
    "engWord": "excite",
    "rusWord": "возбуждать",
    "transcription": "[ɪkˈsaɪt]"
  },
  {
    "engWord": "work",
    "rusWord": "работа",
    "transcription": "[wɜːk]"
  },
  {
    "engWord": "troop",
    "rusWord": "отряд",
    "transcription": "[truːp]"
  },
  {
    "engWord": "slap",
    "rusWord": "шлепок",
    "transcription": "[slæp]"
  },
  {
    "engWord": "forbidden",
    "rusWord": "запрещенный",
    "transcription": "[fəˈbɪdn]"
  },
  {
    "engWord": "dangerous",
    "rusWord": "опасный",
    "transcription": "[ˈdeɪnʤərəs]"
  },
  {
    "engWord": "glacier",
    "rusWord": "ледник",
    "transcription": "[ˈglæsɪə]"
  },
  {
    "engWord": "extraordinary",
    "rusWord": "необычный",
    "transcription": "[ɪksˈtrɔːdnrɪ]"
  },
  {
    "engWord": "accuse",
    "rusWord": "обвинять",
    "transcription": "[əˈkjuːz]"
  },
  {
    "engWord": "quality",
    "rusWord": "качество",
    "transcription": "[ˈkwɒlɪtɪ]"
  },
  {
    "engWord": "someday",
    "rusWord": "наступить",
    "transcription": "[ˈsʌmdeɪ]"
  },
  {
    "engWord": "plate",
    "rusWord": "тарелка",
    "transcription": "[pleɪt]"
  },
  {
    "engWord": "sure",
    "rusWord": "конечно",
    "transcription": "[ʃʊə]"
  },
  {
    "engWord": "close",
    "rusWord": "закрывать",
    "transcription": "[kləʊs]"
  },
  {
    "engWord": "many",
    "rusWord": "много",
    "transcription": "[ˈmenɪ]"
  },
  {
    "engWord": "mine",
    "rusWord": "подкопать",
    "transcription": "[maɪn]"
  },
  {
    "engWord": "hammer",
    "rusWord": "молоток",
    "transcription": "[ˈhæmə]"
  },
  {
    "engWord": "inappropriate",
    "rusWord": "несоответствующий",
    "transcription": "[ɪnəˈprəʊprɪɪt]"
  },
  {
    "engWord": "lighten",
    "rusWord": "осветлять",
    "transcription": "[laɪtn]"
  },
  {
    "engWord": "importance",
    "rusWord": "важность",
    "transcription": "[ɪmˈpɔːtəns]"
  },
  {
    "engWord": "shepherd",
    "rusWord": "пасти",
    "transcription": "[ˈʃepəd]"
  },
  {
    "engWord": "whistle",
    "rusWord": "свисток",
    "transcription": "[wɪsl]"
  },
  {
    "engWord": "calculator",
    "rusWord": "калькулятор",
    "transcription": "[ˈkælkjʊleɪtə]"
  },
  {
    "engWord": "spa",
    "rusWord": "спа",
    "transcription": "[spɑː]"
  },
  {
    "engWord": "profit",
    "rusWord": "прибыль",
    "transcription": "[ˈprɒfɪt]"
  },
  {
    "engWord": "belt",
    "rusWord": "пояс",
    "transcription": "[belt]"
  },
  {
    "engWord": "wound",
    "rusWord": "рана",
    "transcription": "[wuːnd]"
  },
  {
    "engWord": "rent",
    "rusWord": "арендовать",
    "transcription": "[rent]"
  },
  {
    "engWord": "pupil",
    "rusWord": "ученик",
    "transcription": "[pjuːpl]"
  },
  {
    "engWord": "said",
    "rusWord": "сказанный",
    "transcription": "[sed]"
  },
  {
    "engWord": "steel",
    "rusWord": "сталь",
    "transcription": "[stiːl]"
  },
  {
    "engWord": "rust",
    "rusWord": "ржавчина",
    "transcription": "[rʌst]"
  },
  {
    "engWord": "pointless",
    "rusWord": "бессмысленный",
    "transcription": "[ˈpɔɪntlɪs]"
  },
  {
    "engWord": "academy",
    "rusWord": "академия",
    "transcription": "[əˈkædəmɪ]"
  },
  {
    "engWord": "suspicious",
    "rusWord": "подозрительный",
    "transcription": "[səsˈpɪʃəs]"
  },
  {
    "engWord": "melt",
    "rusWord": "расплав",
    "transcription": "[melt]"
  },
  {
    "engWord": "mankind",
    "rusWord": "человечество",
    "transcription": "[ˈmænkaɪnd]"
  },
  {
    "engWord": "apparently",
    "rusWord": "по-видимому",
    "transcription": "[əˈpærəntlɪ]"
  },
  {
    "engWord": "prescription",
    "rusWord": "рецепт",
    "transcription": "[prɪsˈkrɪpʃn]"
  },
  {
    "engWord": "prize",
    "rusWord": "приз",
    "transcription": "[praɪz]"
  },
  {
    "engWord": "oven",
    "rusWord": "духовка",
    "transcription": "[ʌvn]"
  },
  {
    "engWord": "female",
    "rusWord": "женский",
    "transcription": "[ˈfiːmeɪl]"
  },
  {
    "engWord": "strip",
    "rusWord": "полоса",
    "transcription": "[strɪp]"
  },
  {
    "engWord": "trust",
    "rusWord": "доверие",
    "transcription": "[trʌst]"
  },
  {
    "engWord": "Saturday",
    "rusWord": "суббота",
    "transcription": "[ˈsætədɪ]"
  },
  {
    "engWord": "grandmother",
    "rusWord": "бабушка",
    "transcription": "[ˈgrænmʌðə]"
  },
  {
    "engWord": "naughty",
    "rusWord": "непослушный",
    "transcription": "[ˈnɔːtɪ]"
  },
  {
    "engWord": "signature",
    "rusWord": "подпись",
    "transcription": "[ˈsɪgnɒʧə]"
  },
  {
    "engWord": "attribute",
    "rusWord": "атрибут",
    "transcription": "[ˈætrɪbjuːt]"
  },
  {
    "engWord": "current",
    "rusWord": "текущий",
    "transcription": "[ˈkʌrənt]"
  },
  {
    "engWord": "alcoholic",
    "rusWord": "алкогольный",
    "transcription": "[ælkəˈhɒlɪk]"
  },
  {
    "engWord": "soccer",
    "rusWord": "футбол",
    "transcription": "[ˈsɒkə]"
  },
  {
    "engWord": "compass",
    "rusWord": "компас",
    "transcription": "[ˈkʌmpəs]"
  },
  {
    "engWord": "comma",
    "rusWord": "запятая",
    "transcription": "[ˈkɒmə]"
  },
  {
    "engWord": "torch",
    "rusWord": "факел",
    "transcription": "[tɔːʧ]"
  },
  {
    "engWord": "scammer",
    "rusWord": "мошенник",
    "transcription": "[ˈskæmər]"
  },
  {
    "engWord": "timber",
    "rusWord": "древесина",
    "transcription": "[ˈtɪmbə]"
  },
  {
    "engWord": "boast",
    "rusWord": "хвастовство",
    "transcription": "[bəʊst]"
  },
  {
    "engWord": "advance",
    "rusWord": "продвижение",
    "transcription": "[ədˈvɑːns]"
  },
  {
    "engWord": "nuisance",
    "rusWord": "неприятность",
    "transcription": "[njuːsns]"
  },
  {
    "engWord": "structure",
    "rusWord": "структура",
    "transcription": "[ˈstrʌkʧə]"
  },
  {
    "engWord": "spoken",
    "rusWord": "высказанный",
    "transcription": "[ˈspəʊkən]"
  },
  {
    "engWord": "personal",
    "rusWord": "личный",
    "transcription": "[ˈpɜːs(ə)nəl]"
  },
  {
    "engWord": "sue",
    "rusWord": "предъявлять иск",
    "transcription": "[sjuː]"
  },
  {
    "engWord": "dressed",
    "rusWord": "одетый",
    "transcription": "[drest]"
  },
  {
    "engWord": "justice",
    "rusWord": "справедливость",
    "transcription": "[ˈʤʌstɪs]"
  },
  {
    "engWord": "blanket",
    "rusWord": "одеяло",
    "transcription": "[ˈblæŋkɪt]"
  },
  {
    "engWord": "stick",
    "rusWord": "придерживаться",
    "transcription": "[stɪk]"
  },
  {
    "engWord": "neck",
    "rusWord": "шея",
    "transcription": "[nek]"
  },
  {
    "engWord": "bar",
    "rusWord": "бар",
    "transcription": "[bɑː]"
  },
  {
    "engWord": "machine",
    "rusWord": "машина",
    "transcription": "[məˈʃiːn]"
  },
  {
    "engWord": "tool",
    "rusWord": "инструмент",
    "transcription": "[tuːl]"
  },
  {
    "engWord": "dictionary",
    "rusWord": "словарь",
    "transcription": "[ˈdɪkʃn(ə)rɪ]"
  },
  {
    "engWord": "compute",
    "rusWord": "вычислять",
    "transcription": "[kəmˈpjuːt]"
  },
  {
    "engWord": "hatred",
    "rusWord": "ненависть",
    "transcription": "[ˈheɪtrɪd]"
  },
  {
    "engWord": "napkin",
    "rusWord": "салфетка",
    "transcription": "[ˈnæpkɪn]"
  },
  {
    "engWord": "minus",
    "rusWord": "минус",
    "transcription": "[ˈmaɪnəs]"
  },
  {
    "engWord": "lump",
    "rusWord": "кусок",
    "transcription": "[lʌmp]"
  },
  {
    "engWord": "geometry",
    "rusWord": "геометрия",
    "transcription": "[ʤɪˈɒmɪtrɪ]"
  },
  {
    "engWord": "granddaughter",
    "rusWord": "внучка",
    "transcription": "[ˈgrændɔːtə]"
  },
  {
    "engWord": "cough",
    "rusWord": "кашель",
    "transcription": "[kɒf]"
  },
  {
    "engWord": "faint",
    "rusWord": "слабый",
    "transcription": "[feɪnt]"
  },
  {
    "engWord": "breakdown",
    "rusWord": "разрушение",
    "transcription": "[ˈbreɪkdaʊn]"
  },
  {
    "engWord": "versus",
    "rusWord": "против",
    "transcription": "[ˈvɜːsəs]"
  },
  {
    "engWord": "boom",
    "rusWord": "бум",
    "transcription": "[buːm]"
  },
  {
    "engWord": "adult",
    "rusWord": "совершеннолетний",
    "transcription": "[ˈædʌlt]"
  },
  {
    "engWord": "suffer",
    "rusWord": "страдать",
    "transcription": "[ˈsʌfə]"
  },
  {
    "engWord": "fantasy",
    "rusWord": "фантазия",
    "transcription": "[ˈfæntəsɪ]"
  },
  {
    "engWord": "recently",
    "rusWord": "недавно",
    "transcription": "[ˈriːsntlɪ]"
  },
  {
    "engWord": "beach",
    "rusWord": "пляж",
    "transcription": "[biːʧ]"
  },
  {
    "engWord": "decide",
    "rusWord": "решать",
    "transcription": "[dɪˈsaɪd]"
  },
  {
    "engWord": "consider",
    "rusWord": "рассматривать",
    "transcription": "[kənˈsɪdə]"
  },
  {
    "engWord": "drive",
    "rusWord": "водить",
    "transcription": "[draɪv]"
  },
  {
    "engWord": "fresh",
    "rusWord": "свежий",
    "transcription": "[freʃ]"
  },
  {
    "engWord": "bottom",
    "rusWord": "дно",
    "transcription": "[ˈbɒtəm]"
  },
  {
    "engWord": "album",
    "rusWord": "альбом",
    "transcription": "[ˈælbəm]"
  },
  {
    "engWord": "generosity",
    "rusWord": "великодушие",
    "transcription": "[ʤenəˈrɒsɪtɪ]"
  },
  {
    "engWord": "overtake",
    "rusWord": "обогнать",
    "transcription": "[ˈəʊvəteɪk]"
  },
  {
    "engWord": "barb",
    "rusWord": "шип",
    "transcription": "[bɑːb]"
  },
  {
    "engWord": "cruise",
    "rusWord": "круиз",
    "transcription": "[kruːz]"
  },
  {
    "engWord": "minimum",
    "rusWord": "минимальный",
    "transcription": "[ˈmɪnɪməm]"
  },
  {
    "engWord": "upright",
    "rusWord": "вертикальный",
    "transcription": "[ˈʌpraɪt]"
  },
  {
    "engWord": "coke",
    "rusWord": "кокс",
    "transcription": "[kəʊk]"
  },
  {
    "engWord": "cuisine",
    "rusWord": "кухня",
    "transcription": "[kwɪˈziːn]"
  },
  {
    "engWord": "possession",
    "rusWord": "владение",
    "transcription": "[pəˈzeʃn]"
  },
  {
    "engWord": "user",
    "rusWord": "пользователь",
    "transcription": "[ˈjuːzə]"
  },
  {
    "engWord": "fence",
    "rusWord": "забор",
    "transcription": "[fens]"
  },
  {
    "engWord": "decline",
    "rusWord": "снижение",
    "transcription": "[dɪˈklaɪn]"
  },
  {
    "engWord": "seventeen",
    "rusWord": "семнадцать",
    "transcription": "[sev(ə)nˈtiːn]"
  },
  {
    "engWord": "deeply",
    "rusWord": "глубоко",
    "transcription": "[ˈdiːplɪ]"
  },
  {
    "engWord": "hardly",
    "rusWord": "едва",
    "transcription": "[ˈhɑːdlɪ]"
  },
  {
    "engWord": "nervous",
    "rusWord": "нервный",
    "transcription": "[ˈnɜːvəs]"
  },
  {
    "engWord": "drink",
    "rusWord": "пить",
    "transcription": "[drɪŋk]"
  },
  {
    "engWord": "hold",
    "rusWord": "держать",
    "transcription": "[həʊld]"
  },
  {
    "engWord": "rose",
    "rusWord": "роза",
    "transcription": "[rəʊz]"
  },
  {
    "engWord": "homicide",
    "rusWord": "убийство",
    "transcription": "[ˈhɒmɪsaɪd]"
  },
  {
    "engWord": "luggage",
    "rusWord": "багаж",
    "transcription": "[ˈlʌgɪʤ]"
  },
  {
    "engWord": "exception",
    "rusWord": "исключение",
    "transcription": "[ɪkˈsepʃn]"
  },
  {
    "engWord": "somewhat",
    "rusWord": "отчасти",
    "transcription": "[ˈsʌmwɒt]"
  },
  {
    "engWord": "invasion",
    "rusWord": "вторжение",
    "transcription": "[ɪnˈveɪʒən]"
  },
  {
    "engWord": "expectation",
    "rusWord": "ожидание",
    "transcription": "[ekspekˈteɪʃn]"
  },
  {
    "engWord": "hot",
    "rusWord": "горячий",
    "transcription": "[hɒt]"
  },
  {
    "engWord": "informal",
    "rusWord": "неформальный",
    "transcription": "[ɪnˈfɔːml]"
  },
  {
    "engWord": "arson",
    "rusWord": "поджог",
    "transcription": "[ɑːsn]"
  },
  {
    "engWord": "entrance",
    "rusWord": "вход",
    "transcription": "[ˈentrəns]"
  },
  {
    "engWord": "opt",
    "rusWord": "выбирать",
    "transcription": "[ɒpt]"
  },
  {
    "engWord": "tension",
    "rusWord": "напряжение",
    "transcription": "[tenʃn]"
  },
  {
    "engWord": "slide",
    "rusWord": "горка",
    "transcription": "[slaɪd]"
  },
  {
    "engWord": "radical",
    "rusWord": "радикальный",
    "transcription": "[ˈrædɪkəl]"
  },
  {
    "engWord": "darn",
    "rusWord": "штопать",
    "transcription": "[dɑːn]"
  },
  {
    "engWord": "media",
    "rusWord": "средства массовой информации",
    "transcription": "[]"
  },
  {
    "engWord": "presume",
    "rusWord": "предполагать",
    "transcription": "[prɪˈzjuːm]"
  },
  {
    "engWord": "cute",
    "rusWord": "милый",
    "transcription": "[kjuːt]"
  },
  {
    "engWord": "anything",
    "rusWord": "что угодно",
    "transcription": "[ˈenɪθɪŋ]"
  },
  {
    "engWord": "prison",
    "rusWord": "тюрьма",
    "transcription": "[prɪzn]"
  },
  {
    "engWord": "let",
    "rusWord": "позволять",
    "transcription": "[let]"
  },
  {
    "engWord": "street",
    "rusWord": "улица",
    "transcription": "[striːt]"
  },
  {
    "engWord": "take",
    "rusWord": "брать",
    "transcription": "[teɪk]"
  },
  {
    "engWord": "face",
    "rusWord": "лицо",
    "transcription": "[feɪs]"
  },
  {
    "engWord": "pianist",
    "rusWord": "пианист",
    "transcription": "[ˈpiənɪst]"
  },
  {
    "engWord": "disagree",
    "rusWord": "не соглашаться",
    "transcription": "[dɪsəˈgriː]"
  },
  {
    "engWord": "quiz",
    "rusWord": "викторина",
    "transcription": "[kwɪz]"
  },
  {
    "engWord": "spice",
    "rusWord": "приправа",
    "transcription": "[spaɪs]"
  },
  {
    "engWord": "stud",
    "rusWord": "шпилька",
    "transcription": "[stʌd]"
  },
  {
    "engWord": "performance",
    "rusWord": "спектакль",
    "transcription": "[pəˈfɔːməns]"
  },
  {
    "engWord": "spray",
    "rusWord": "спрей",
    "transcription": "[spreɪ]"
  },
  {
    "engWord": "privilege",
    "rusWord": "привилегия",
    "transcription": "[ˈprɪvɪlɪʤ]"
  },
  {
    "engWord": "photocopy",
    "rusWord": "фотокопия",
    "transcription": "[ˈfəʊtəkɒpɪ]"
  },
  {
    "engWord": "anymore",
    "rusWord": "больше",
    "transcription": "[enɪˈmɔː]"
  },
  {
    "engWord": "rid",
    "rusWord": "избавлять",
    "transcription": "[rɪd]"
  },
  {
    "engWord": "bus",
    "rusWord": "автобус",
    "transcription": "[bʌs]"
  },
  {
    "engWord": "damn",
    "rusWord": "черт",
    "transcription": "[dæm]"
  },
  {
    "engWord": "bye",
    "rusWord": "пока",
    "transcription": "[baɪ]"
  },
  {
    "engWord": "suffering",
    "rusWord": "страдающий",
    "transcription": "[ˈsʌfərɪŋ]"
  },
  {
    "engWord": "doubt",
    "rusWord": "сомнение",
    "transcription": "[daʊt]"
  },
  {
    "engWord": "bird",
    "rusWord": "птица",
    "transcription": "[bɜːd]"
  },
  {
    "engWord": "next",
    "rusWord": "следующий",
    "transcription": "[nekst]"
  },
  {
    "engWord": "wait",
    "rusWord": "ждать",
    "transcription": "[weɪt]"
  },
  {
    "engWord": "evidence",
    "rusWord": "доказательства",
    "transcription": "[ˈevɪdəns]"
  },
  {
    "engWord": "breakup",
    "rusWord": "распад",
    "transcription": "[ˈbreɪkʌp]"
  },
  {
    "engWord": "bustle",
    "rusWord": "суета",
    "transcription": "[bʌsl]"
  },
  {
    "engWord": "salmon",
    "rusWord": "лосось",
    "transcription": "[ˈsæmən]"
  },
  {
    "engWord": "fiscal",
    "rusWord": "финансовый",
    "transcription": "[ˈfɪskəl]"
  },
  {
    "engWord": "acute",
    "rusWord": "острый",
    "transcription": "[əˈkjuːt]"
  },
  {
    "engWord": "Olympics",
    "rusWord": "Олимпийские игры",
    "transcription": "[əˈlɪmpɪks]"
  },
  {
    "engWord": "elder",
    "rusWord": "старший",
    "transcription": "[ˈeldə]"
  },
  {
    "engWord": "pry",
    "rusWord": "любопытный",
    "transcription": "[praɪ]"
  },
  {
    "engWord": "visiting",
    "rusWord": "приезжий",
    "transcription": "[ˈvɪzɪtɪŋ]"
  },
  {
    "engWord": "neat",
    "rusWord": "аккуратный",
    "transcription": "[niːt]"
  },
  {
    "engWord": "chairman",
    "rusWord": "председатель",
    "transcription": "[ˈʧeəmən]"
  },
  {
    "engWord": "obligation",
    "rusWord": "обязательство",
    "transcription": "[ɒblɪˈgeɪʃn]"
  },
  {
    "engWord": "telephone",
    "rusWord": "телефон",
    "transcription": "[ˈtelɪfəʊn]"
  },
  {
    "engWord": "super",
    "rusWord": "супер",
    "transcription": "[ˈsjuːpə]"
  },
  {
    "engWord": "gate",
    "rusWord": "вентиль",
    "transcription": "[geɪt]"
  },
  {
    "engWord": "six",
    "rusWord": "шесть",
    "transcription": "[sɪks]"
  },
  {
    "engWord": "self",
    "rusWord": "сам",
    "transcription": "[self]"
  },
  {
    "engWord": "beginning",
    "rusWord": "начало",
    "transcription": "[bɪˈgɪnɪŋ]"
  },
  {
    "engWord": "hospitable",
    "rusWord": "гостеприимный",
    "transcription": "[ˈhɔspɪtəbl]"
  },
  {
    "engWord": "clash",
    "rusWord": "столкновение",
    "transcription": "[klæʃ]"
  },
  {
    "engWord": "generous",
    "rusWord": "щедрый",
    "transcription": "[ˈʤenərəs]"
  },
  {
    "engWord": "wig",
    "rusWord": "парик",
    "transcription": "[]"
  },
  {
    "engWord": "wink",
    "rusWord": "подмигивать",
    "transcription": "[wɪŋk]"
  },
  {
    "engWord": "aspirin",
    "rusWord": "аспирин",
    "transcription": "[ˈæsprɪn]"
  },
  {
    "engWord": "litter",
    "rusWord": "мусор",
    "transcription": "[ˈlɪtə]"
  },
  {
    "engWord": "sunny",
    "rusWord": "солнечный",
    "transcription": "[ˈsʌnɪ]"
  },
  {
    "engWord": "ethic",
    "rusWord": "этичный",
    "transcription": "[ˈeθɪk]"
  },
  {
    "engWord": "gene",
    "rusWord": "ген",
    "transcription": "[ʤiːn]"
  },
  {
    "engWord": "mature",
    "rusWord": "спелый",
    "transcription": "[məˈʧʊə]"
  },
  {
    "engWord": "knowledge",
    "rusWord": "знание",
    "transcription": "[ˈnɒlɪʤ]"
  },
  {
    "engWord": "fake",
    "rusWord": "подделка",
    "transcription": "[feɪk]"
  },
  {
    "engWord": "panic",
    "rusWord": "паника",
    "transcription": "[ˈpænɪk]"
  },
  {
    "engWord": "mommy",
    "rusWord": "мамочка",
    "transcription": "[ˈmæmɪ]"
  },
  {
    "engWord": "nobody",
    "rusWord": "никто",
    "transcription": "[ˈnəʊbədɪ]"
  },
  {
    "engWord": "centimetre",
    "rusWord": "сантиметр",
    "transcription": "[ˈsentɪmiːtə]"
  },
  {
    "engWord": "instant",
    "rusWord": "мгновенный",
    "transcription": "[ˈɪnstənt]"
  },
  {
    "engWord": "casual",
    "rusWord": "случайный",
    "transcription": "[ˈkæʒʊəl]"
  },
  {
    "engWord": "proficient",
    "rusWord": "опытный",
    "transcription": "[prəˈfɪʃnt]"
  },
  {
    "engWord": "grill",
    "rusWord": "гриль",
    "transcription": "[grɪl]"
  },
  {
    "engWord": "lumber",
    "rusWord": "пиломатериалы",
    "transcription": "[ˈlʌmbə]"
  },
  {
    "engWord": "fireplace",
    "rusWord": "камин",
    "transcription": "[ˈfaɪəpleɪs]"
  },
  {
    "engWord": "lavish",
    "rusWord": "обильный",
    "transcription": "[ˈlævɪʃ]"
  },
  {
    "engWord": "reverse",
    "rusWord": "обратный",
    "transcription": "[rɪˈvɜːs]"
  },
  {
    "engWord": "pint",
    "rusWord": "пинта",
    "transcription": "[paɪnt]"
  },
  {
    "engWord": "flip",
    "rusWord": "сальто",
    "transcription": "[flɪp]"
  },
  {
    "engWord": "threaten",
    "rusWord": "угрожать",
    "transcription": "[θretn]"
  },
  {
    "engWord": "proud",
    "rusWord": "гордый",
    "transcription": "[praʊd]"
  },
  {
    "engWord": "horrible",
    "rusWord": "ужасный",
    "transcription": "[ˈhɒrəbl]"
  },
  {
    "engWord": "amber",
    "rusWord": "янтарь",
    "transcription": "[ˈæmbə]"
  },
  {
    "engWord": "football",
    "rusWord": "футбол",
    "transcription": "[ˈfʊtbɔːl]"
  },
  {
    "engWord": "corn",
    "rusWord": "кукуруза",
    "transcription": "[kɔːn]"
  },
  {
    "engWord": "among",
    "rusWord": "среди",
    "transcription": "[əˈmʌŋ]"
  },
  {
    "engWord": "embarrass",
    "rusWord": "смущать",
    "transcription": "[ɪmˈbærəs]"
  },
  {
    "engWord": "thirsty",
    "rusWord": "жаждущий",
    "transcription": "[ˈθɜːstɪ]"
  },
  {
    "engWord": "remedy",
    "rusWord": "средство",
    "transcription": "[ˈremɪdɪ]"
  },
  {
    "engWord": "distress",
    "rusWord": "горе",
    "transcription": "[dɪsˈtres]"
  },
  {
    "engWord": "complication",
    "rusWord": "усложнение",
    "transcription": "[kɒmplɪˈkeɪʃn]"
  },
  {
    "engWord": "offensive",
    "rusWord": "агрессивный",
    "transcription": "[əˈfensɪv]"
  },
  {
    "engWord": "famine",
    "rusWord": "голод",
    "transcription": "[ˈfæmɪn]"
  },
  {
    "engWord": "cursed",
    "rusWord": "проклятый",
    "transcription": "[ˈkɜːsɪd]"
  },
  {
    "engWord": "gulf",
    "rusWord": "залив",
    "transcription": "[gʌlf]"
  },
  {
    "engWord": "gram",
    "rusWord": "грамм",
    "transcription": "[græm]"
  },
  {
    "engWord": "skirt",
    "rusWord": "юбка",
    "transcription": "[skɜːt]"
  },
  {
    "engWord": "shove",
    "rusWord": "совать",
    "transcription": "[ʃʌv]"
  },
  {
    "engWord": "fetch",
    "rusWord": "привести",
    "transcription": "[feʧ]"
  },
  {
    "engWord": "imply",
    "rusWord": "подразумевать",
    "transcription": "[ɪmˈplaɪ]"
  },
  {
    "engWord": "leisure",
    "rusWord": "досуг",
    "transcription": "[ˈleʒə]"
  },
  {
    "engWord": "dozen",
    "rusWord": "дюжина",
    "transcription": "[dʌzn]"
  },
  {
    "engWord": "introduce",
    "rusWord": "вводить",
    "transcription": "[ɪntrəˈdjuːs]"
  },
  {
    "engWord": "replace",
    "rusWord": "заменять",
    "transcription": "[rɪˈpleɪs]"
  },
  {
    "engWord": "research",
    "rusWord": "исследование",
    "transcription": "[rɪˈsɜːʧ]"
  },
  {
    "engWord": "drove",
    "rusWord": "стадо",
    "transcription": "[drəʊv]"
  },
  {
    "engWord": "quantity",
    "rusWord": "количество",
    "transcription": "[ˈkwɒntɪtɪ]"
  },
  {
    "engWord": "juice",
    "rusWord": "сок",
    "transcription": "[ʤuːs]"
  },
  {
    "engWord": "fill",
    "rusWord": "заполнять",
    "transcription": "[fɪl]"
  },
  {
    "engWord": "change",
    "rusWord": "изменение",
    "transcription": "[ʧeɪnʤ]"
  },
  {
    "engWord": "ball",
    "rusWord": "мяч",
    "transcription": "[bɔːl]"
  },
  {
    "engWord": "go",
    "rusWord": "идти",
    "transcription": "[gəʊ]"
  },
  {
    "engWord": "herd",
    "rusWord": "стадо",
    "transcription": "[hɜːd]"
  },
  {
    "engWord": "approve",
    "rusWord": "утвердить",
    "transcription": "[əˈpruːv]"
  },
  {
    "engWord": "bind",
    "rusWord": "связывать",
    "transcription": "[baɪnd]"
  },
  {
    "engWord": "curtain",
    "rusWord": "занавес",
    "transcription": "[kɜːtn]"
  },
  {
    "engWord": "instruction",
    "rusWord": "инструкция",
    "transcription": "[ɪnˈstrʌkʃn]"
  },
  {
    "engWord": "capture",
    "rusWord": "захватить",
    "transcription": "[ˈkæpʧə]"
  },
  {
    "engWord": "assume",
    "rusWord": "предполагать",
    "transcription": "[əˈsjuːm]"
  },
  {
    "engWord": "broken",
    "rusWord": "сломанный",
    "transcription": "[ˈbrəʊkən]"
  },
  {
    "engWord": "judge",
    "rusWord": "судья",
    "transcription": "[ʤʌʤ]"
  },
  {
    "engWord": "couple",
    "rusWord": "пара",
    "transcription": "[kʌpl]"
  },
  {
    "engWord": "group",
    "rusWord": "группа",
    "transcription": "[gruːp]"
  },
  {
    "engWord": "eight",
    "rusWord": "восемь",
    "transcription": "[eɪt]"
  },
  {
    "engWord": "mattress",
    "rusWord": "тюфяк",
    "transcription": "[ˈmætrɪs]"
  },
  {
    "engWord": "hunter",
    "rusWord": "охотник",
    "transcription": "[ˈhʌntə]"
  },
  {
    "engWord": "discover",
    "rusWord": "обнаруживать",
    "transcription": "[dɪsˈkʌvə]"
  },
  {
    "engWord": "shampoo",
    "rusWord": "шампунь",
    "transcription": "[ʃæmˈpuː]"
  },
  {
    "engWord": "sadness",
    "rusWord": "печаль",
    "transcription": "[ˈsædnɪs]"
  },
  {
    "engWord": "reject",
    "rusWord": "отклонять",
    "transcription": "[]"
  },
  {
    "engWord": "designer",
    "rusWord": "дизайнер",
    "transcription": "[dɪˈzaɪnə]"
  },
  {
    "engWord": "demonstration",
    "rusWord": "демонстрация",
    "transcription": "[demənsˈtreɪʃn]"
  },
  {
    "engWord": "scheme",
    "rusWord": "схема",
    "transcription": "[skiːm]"
  },
  {
    "engWord": "seventy",
    "rusWord": "семьдесят",
    "transcription": "[ˈsevntɪ]"
  },
  {
    "engWord": "vacation",
    "rusWord": "отпуск",
    "transcription": "[vəˈkeɪʃn]"
  },
  {
    "engWord": "waste",
    "rusWord": "отходы",
    "transcription": "[weɪst]"
  },
  {
    "engWord": "steak",
    "rusWord": "стейк",
    "transcription": "[steɪk]"
  },
  {
    "engWord": "stake",
    "rusWord": "доля",
    "transcription": "[steɪk]"
  },
  {
    "engWord": "awful",
    "rusWord": "ужасный",
    "transcription": "[ˈɔːf(ə)l]"
  },
  {
    "engWord": "possibly",
    "rusWord": "возможно",
    "transcription": "[ˈpɒsəblɪ]"
  },
  {
    "engWord": "obtain",
    "rusWord": "получать",
    "transcription": "[əbˈteɪn]"
  },
  {
    "engWord": "difficult",
    "rusWord": "трудный",
    "transcription": "[ˈdɪfɪkəlt]"
  },
  {
    "engWord": "region",
    "rusWord": "регион",
    "transcription": "[ˈriːʤən]"
  },
  {
    "engWord": "agree",
    "rusWord": "соглашаться",
    "transcription": "[əˈgriː]"
  },
  {
    "engWord": "shell",
    "rusWord": "ракушка",
    "transcription": "[ʃel]"
  },
  {
    "engWord": "exhibition",
    "rusWord": "выставка",
    "transcription": "[eksɪˈbɪʃn]"
  },
  {
    "engWord": "beast",
    "rusWord": "зверь",
    "transcription": "[biːst]"
  },
  {
    "engWord": "focused",
    "rusWord": "сосредоточенный",
    "transcription": "[ˈfəʊkəst]"
  },
  {
    "engWord": "vengeance",
    "rusWord": "отмщение",
    "transcription": "[ˈvenʤəns]"
  },
  {
    "engWord": "consist",
    "rusWord": "состоять",
    "transcription": "[ˈkɒnsɪst]"
  },
  {
    "engWord": "harmless",
    "rusWord": "безвредный",
    "transcription": "[ˈhɑːmlɪs]"
  },
  {
    "engWord": "grandchildren",
    "rusWord": "внучата",
    "transcription": "[ˈgrænʧɪldrən]"
  },
  {
    "engWord": "retired",
    "rusWord": "в отставке",
    "transcription": "[rɪˈtaɪəd]"
  },
  {
    "engWord": "whisper",
    "rusWord": "шепот",
    "transcription": "[ˈwɪspə]"
  },
  {
    "engWord": "fuel",
    "rusWord": "топливо",
    "transcription": "[fjʊəl]"
  },
  {
    "engWord": "politics",
    "rusWord": "политика",
    "transcription": "[ˈpɒlɪtɪks]"
  },
  {
    "engWord": "wet",
    "rusWord": "мокрый",
    "transcription": "[wet]"
  },
  {
    "engWord": "explain",
    "rusWord": "объяснять",
    "transcription": "[ɪksˈpleɪn]"
  },
  {
    "engWord": "mad",
    "rusWord": "безумный",
    "transcription": "[mæd]"
  },
  {
    "engWord": "pension",
    "rusWord": "пенсия",
    "transcription": "[penʃn]"
  },
  {
    "engWord": "English",
    "rusWord": "английский",
    "transcription": "[ˈɪŋglɪʃ]"
  },
  {
    "engWord": "blue",
    "rusWord": "синий",
    "transcription": "[bluː]"
  },
  {
    "engWord": "wear",
    "rusWord": "носить",
    "transcription": "[weə]"
  },
  {
    "engWord": "mile",
    "rusWord": "миля",
    "transcription": "[maɪl]"
  },
  {
    "engWord": "thank",
    "rusWord": "благодарить",
    "transcription": "[θæŋk]"
  },
  {
    "engWord": "blown",
    "rusWord": "взорванный",
    "transcription": "[bləʊn]"
  },
  {
    "engWord": "sincere",
    "rusWord": "искренний",
    "transcription": "[sɪnˈsɪə]"
  },
  {
    "engWord": "criticism",
    "rusWord": "критика",
    "transcription": "[ˈkrɪtɪsɪzm]"
  },
  {
    "engWord": "integration",
    "rusWord": "интеграция",
    "transcription": "[ɪntɪˈgreɪʃn]"
  },
  {
    "engWord": "verdict",
    "rusWord": "вердикт",
    "transcription": "[ˈvɜːdɪkt]"
  },
  {
    "engWord": "online",
    "rusWord": "в интернете",
    "transcription": "[ˈɒnlaɪn]"
  },
  {
    "engWord": "mineral",
    "rusWord": "минеральный",
    "transcription": "[ˈmɪnərəl]"
  },
  {
    "engWord": "realistic",
    "rusWord": "реалистичный",
    "transcription": "[rɪəˈlɪstɪk]"
  },
  {
    "engWord": "flood",
    "rusWord": "наводнение",
    "transcription": "[flʌd]"
  },
  {
    "engWord": "curse",
    "rusWord": "проклятие",
    "transcription": "[kɜːs]"
  },
  {
    "engWord": "fishing",
    "rusWord": "рыболовство",
    "transcription": "[ˈfɪʃɪŋ]"
  },
  {
    "engWord": "ahead",
    "rusWord": "впереди",
    "transcription": "[əˈhed]"
  },
  {
    "engWord": "attractive",
    "rusWord": "привлекательный",
    "transcription": "[əˈtræktɪv]"
  },
  {
    "engWord": "crew",
    "rusWord": "экипаж",
    "transcription": "[kruː]"
  },
  {
    "engWord": "client",
    "rusWord": "клиент",
    "transcription": "[ˈklaɪənt]"
  },
  {
    "engWord": "tennis",
    "rusWord": "теннис",
    "transcription": "[ˈtenɪs]"
  },
  {
    "engWord": "rough",
    "rusWord": "грубый",
    "transcription": "[rʌf]"
  },
  {
    "engWord": "total",
    "rusWord": "весь",
    "transcription": "[təʊtl]"
  },
  {
    "engWord": "cross",
    "rusWord": "пересекать",
    "transcription": "[krɒs]"
  },
  {
    "engWord": "pull",
    "rusWord": "тянуть",
    "transcription": "[pʊl]"
  },
  {
    "engWord": "lay",
    "rusWord": "класть",
    "transcription": "[leɪ]"
  },
  {
    "engWord": "noise",
    "rusWord": "шум",
    "transcription": "[nɔɪz]"
  },
  {
    "engWord": "require",
    "rusWord": "требовать",
    "transcription": "[rɪˈkwaɪə]"
  },
  {
    "engWord": "bundle",
    "rusWord": "пакет",
    "transcription": "[bʌndl]"
  },
  {
    "engWord": "vanish",
    "rusWord": "пропадать",
    "transcription": "[ˈvænɪʃ]"
  },
  {
    "engWord": "disclose",
    "rusWord": "обнаруживать",
    "transcription": "[dɪsˈkləʊz]"
  },
  {
    "engWord": "political",
    "rusWord": "политический",
    "transcription": "[pəˈlɪtɪkəl]"
  },
  {
    "engWord": "cliff",
    "rusWord": "утес",
    "transcription": "[klɪf]"
  },
  {
    "engWord": "renew",
    "rusWord": "возобновлять",
    "transcription": "[rɪˈnjuː]"
  },
  {
    "engWord": "queue",
    "rusWord": "очередь",
    "transcription": "[kjuː]"
  },
  {
    "engWord": "corporal",
    "rusWord": "телесный",
    "transcription": "[ˈkɔːpərəl]"
  },
  {
    "engWord": "pierce",
    "rusWord": "прокалывать",
    "transcription": "[pɪəs]"
  },
  {
    "engWord": "characteristic",
    "rusWord": "характеристика",
    "transcription": "[kærɪktəˈrɪstɪk]"
  },
  {
    "engWord": "shortly",
    "rusWord": "вкратце",
    "transcription": "[ˈʃɔːtlɪ]"
  },
  {
    "engWord": "readily",
    "rusWord": "без труда",
    "transcription": "[ˈredɪlɪ]"
  },
  {
    "engWord": "background",
    "rusWord": "фон",
    "transcription": "[ˈbækgraʊnd]"
  },
  {
    "engWord": "alcohol",
    "rusWord": "алкоголь",
    "transcription": "[ˈælkəhɒl]"
  },
  {
    "engWord": "scientist",
    "rusWord": "ученый",
    "transcription": "[ˈsaɪəntɪst]"
  },
  {
    "engWord": "tale",
    "rusWord": "сказка",
    "transcription": "[teɪl]"
  },
  {
    "engWord": "helpful",
    "rusWord": "полезный",
    "transcription": "[ˈhelpf(ə)l]"
  },
  {
    "engWord": "leadership",
    "rusWord": "руководящий",
    "transcription": "[ˈliːdəʃɪp]"
  },
  {
    "engWord": "price",
    "rusWord": "цена",
    "transcription": "[praɪs]"
  },
  {
    "engWord": "team",
    "rusWord": "команда",
    "transcription": "[tiːm]"
  },
  {
    "engWord": "fast",
    "rusWord": "быстрый",
    "transcription": "[fɑːst]"
  },
  {
    "engWord": "sat",
    "rusWord": "севший",
    "transcription": "[sæt]"
  },
  {
    "engWord": "like",
    "rusWord": "любить",
    "transcription": "[laɪk]"
  },
  {
    "engWord": "course",
    "rusWord": "курс",
    "transcription": "[kɔːs]"
  },
  {
    "engWord": "nothing",
    "rusWord": "ничего",
    "transcription": "[ˈnʌθɪŋ]"
  },
  {
    "engWord": "instrument",
    "rusWord": "инструмент",
    "transcription": "[ˈɪnstrəmənt]"
  },
  {
    "engWord": "carpenter",
    "rusWord": "плотник",
    "transcription": "[ˈkɑːpɪntə]"
  },
  {
    "engWord": "giggle",
    "rusWord": "хихикать",
    "transcription": "[gɪgl]"
  },
  {
    "engWord": "lunatic",
    "rusWord": "безумный",
    "transcription": "[ˈluːnətɪk]"
  },
  {
    "engWord": "puppet",
    "rusWord": "марионетка",
    "transcription": "[ˈpʌpɪt]"
  },
  {
    "engWord": "immigrant",
    "rusWord": "иммигрант",
    "transcription": "[ˈɪmɪgrənt]"
  },
  {
    "engWord": "dragon",
    "rusWord": "дракон",
    "transcription": "[ˈdrægən]"
  },
  {
    "engWord": "consultant",
    "rusWord": "консультант",
    "transcription": "[kənˈsʌltənt]"
  },
  {
    "engWord": "holder",
    "rusWord": "держатель",
    "transcription": "[ˈhəʊldə]"
  },
  {
    "engWord": "jeans",
    "rusWord": "джинсы",
    "transcription": "[ʤiːnz]"
  },
  {
    "engWord": "text",
    "rusWord": "текст",
    "transcription": "[tekst]"
  },
  {
    "engWord": "target",
    "rusWord": "цель",
    "transcription": "[ˈtɑːgɪt]"
  },
  {
    "engWord": "someplace",
    "rusWord": "где-то",
    "transcription": "[ˈsʌmpleɪs]"
  },
  {
    "engWord": "gentle",
    "rusWord": "нежный",
    "transcription": "[ʤentl]"
  },
  {
    "engWord": "join",
    "rusWord": "присоединиться",
    "transcription": "[ʤɔɪn]"
  },
  {
    "engWord": "a",
    "rusWord": "ля",
    "transcription": "[eɪ]"
  },
  {
    "engWord": "obscure",
    "rusWord": "смутный",
    "transcription": "[əbˈskjʊə]"
  },
  {
    "engWord": "envy",
    "rusWord": "зависть",
    "transcription": "[ˈenvɪ]"
  },
  {
    "engWord": "Bible",
    "rusWord": "Библия",
    "transcription": "[baɪbl]"
  },
  {
    "engWord": "heating",
    "rusWord": "отопительный",
    "transcription": "[ˈhiːtɪŋ]"
  },
  {
    "engWord": "corky",
    "rusWord": "пробковый",
    "transcription": "[ˈkɔːkɪ]"
  },
  {
    "engWord": "graduated",
    "rusWord": "градуированный",
    "transcription": "[ˈgrædjʊeɪtɪd]"
  },
  {
    "engWord": "madam",
    "rusWord": "мадам",
    "transcription": "[ˈmædəm]"
  },
  {
    "engWord": "hearty",
    "rusWord": "здоровый",
    "transcription": "[ˈhɑːtɪ]"
  },
  {
    "engWord": "planning",
    "rusWord": "планирование",
    "transcription": "[ˈplænɪŋ]"
  },
  {
    "engWord": "twice",
    "rusWord": "дважды",
    "transcription": "[twaɪs]"
  },
  {
    "engWord": "message",
    "rusWord": "сообщение",
    "transcription": "[ˈmesɪʤ]"
  },
  {
    "engWord": "cheap",
    "rusWord": "дешевый",
    "transcription": "[ʧiːp]"
  },
  {
    "engWord": "safe",
    "rusWord": "безопасный",
    "transcription": "[seɪf]"
  },
  {
    "engWord": "women",
    "rusWord": "женский",
    "transcription": "[ˈwɪmɪn]"
  },
  {
    "engWord": "shore",
    "rusWord": "берег",
    "transcription": "[ʃɔː]"
  },
  {
    "engWord": "level",
    "rusWord": "уровень",
    "transcription": "[levl]"
  },
  {
    "engWord": "sky",
    "rusWord": "небо",
    "transcription": "[skaɪ]"
  },
  {
    "engWord": "at",
    "rusWord": "на",
    "transcription": "[æt]"
  },
  {
    "engWord": "village",
    "rusWord": "деревня",
    "transcription": "[ˈvɪlɪʤ]"
  },
  {
    "engWord": "company",
    "rusWord": "компания",
    "transcription": "[ˈkʌmpənɪ]"
  },
  {
    "engWord": "instruct",
    "rusWord": "наставлять",
    "transcription": "[ɪnˈstrʌkt]"
  },
  {
    "engWord": "sickness",
    "rusWord": "болезнь",
    "transcription": "[ˈsɪknɪs]"
  },
  {
    "engWord": "talented",
    "rusWord": "талантливый",
    "transcription": "[ˈtæləntɪd]"
  },
  {
    "engWord": "moustache",
    "rusWord": "усы",
    "transcription": "[məsˈtɑːʃ]"
  },
  {
    "engWord": "eject",
    "rusWord": "выбрасывать",
    "transcription": "[ɪˈʤekt]"
  },
  {
    "engWord": "fiancée",
    "rusWord": "невеста",
    "transcription": "[fɪˈɑːnseɪ]"
  },
  {
    "engWord": "strongly",
    "rusWord": "сильно",
    "transcription": "[ˈstrɒŋlɪ]"
  },
  {
    "engWord": "literally",
    "rusWord": "буквально",
    "transcription": "[ˈlɪtərəlɪ]"
  },
  {
    "engWord": "pale",
    "rusWord": "бледный",
    "transcription": "[peɪl]"
  },
  {
    "engWord": "classroom",
    "rusWord": "класс",
    "transcription": "[ˈklɑːsrʊm]"
  },
  {
    "engWord": "ugly",
    "rusWord": "уродливый",
    "transcription": "[ˈʌglɪ]"
  },
  {
    "engWord": "court",
    "rusWord": "суд",
    "transcription": "[kɔːt]"
  },
  {
    "engWord": "shopping",
    "rusWord": "поход по магазинам",
    "transcription": "[ˈʃɒpɪŋ]"
  },
  {
    "engWord": "guest",
    "rusWord": "гость",
    "transcription": "[gest]"
  },
  {
    "engWord": "Christmas",
    "rusWord": "Рождество",
    "transcription": "[ˈkrɪsməs]"
  },
  {
    "engWord": "mountain",
    "rusWord": "гора",
    "transcription": "[ˈmaʊntɪn]"
  },
  {
    "engWord": "which",
    "rusWord": "который",
    "transcription": "[wɪʧ]"
  },
  {
    "engWord": "farm",
    "rusWord": "ферма",
    "transcription": "[fɑːm]"
  },
  {
    "engWord": "own",
    "rusWord": "собственный",
    "transcription": "[əʊn]"
  },
  {
    "engWord": "ginger",
    "rusWord": "имбирь",
    "transcription": "[ˈʤɪnʤə]"
  },
  {
    "engWord": "conduct",
    "rusWord": "провести",
    "transcription": "[ˈkɔndʌkt]"
  },
  {
    "engWord": "vocabulary",
    "rusWord": "запас слов",
    "transcription": "[vəˈkæbjʊlərɪ]"
  },
  {
    "engWord": "pee",
    "rusWord": "писать",
    "transcription": "[piː]"
  },
  {
    "engWord": "hack",
    "rusWord": "рубить",
    "transcription": "[hæk]"
  },
  {
    "engWord": "tutor",
    "rusWord": "репетитор",
    "transcription": "[ˈtjuːtə]"
  },
  {
    "engWord": "goody",
    "rusWord": "конфета",
    "transcription": "[ˈgʊdɪ]"
  },
  {
    "engWord": "ointment",
    "rusWord": "мазь",
    "transcription": "[ˈɔɪntmənt]"
  },
  {
    "engWord": "corpse",
    "rusWord": "труп",
    "transcription": "[kɔːps]"
  },
  {
    "engWord": "forgiven",
    "rusWord": "прощенный",
    "transcription": "[fəˈgɪvən]"
  },
  {
    "engWord": "developed",
    "rusWord": "развитый",
    "transcription": "[dɪˈveləpt]"
  },
  {
    "engWord": "sharply",
    "rusWord": "резко",
    "transcription": "[ˈʃɑːplɪ]"
  },
  {
    "engWord": "buyer",
    "rusWord": "покупатель",
    "transcription": "[ˈbaɪə]"
  },
  {
    "engWord": "publicity",
    "rusWord": "публичность",
    "transcription": "[pʌbˈlɪsɪtɪ]"
  },
  {
    "engWord": "income",
    "rusWord": "доход",
    "transcription": "[ˈɪnkʌm]"
  },
  {
    "engWord": "colleague",
    "rusWord": "коллега",
    "transcription": "[ˈkɒliːg]"
  },
  {
    "engWord": "status",
    "rusWord": "статус",
    "transcription": "[ˈsteɪtəs]"
  },
  {
    "engWord": "attic",
    "rusWord": "чердак",
    "transcription": "[ˈætɪk]"
  },
  {
    "engWord": "china",
    "rusWord": "Китай",
    "transcription": "[ˈʧaɪnə]"
  },
  {
    "engWord": "weakness",
    "rusWord": "слабость",
    "transcription": "[ˈwiːknɪs]"
  },
  {
    "engWord": "lovely",
    "rusWord": "прекрасный",
    "transcription": "[ˈlʌvlɪ]"
  },
  {
    "engWord": "manage",
    "rusWord": "управлять",
    "transcription": "[ˈmænɪʤ]"
  },
  {
    "engWord": "certain",
    "rusWord": "определенный",
    "transcription": "[sɜːtn]"
  },
  {
    "engWord": "definite",
    "rusWord": "определенный",
    "transcription": "[ˈdefɪnɪt]"
  },
  {
    "engWord": "eternity",
    "rusWord": "вечность",
    "transcription": "[ɪˈtɜːnɪtɪ]"
  },
  {
    "engWord": "enlarge",
    "rusWord": "расширять",
    "transcription": "[ɪnˈlɑːʤ]"
  },
  {
    "engWord": "separated",
    "rusWord": "отделенный",
    "transcription": "[ˈsepəreɪtɪd]"
  },
  {
    "engWord": "wrinkle",
    "rusWord": "морщина",
    "transcription": "[rɪŋkl]"
  },
  {
    "engWord": "fountain",
    "rusWord": "фонтан",
    "transcription": "[ˈfaʊntɪn]"
  },
  {
    "engWord": "racism",
    "rusWord": "расизм",
    "transcription": "[ˈreɪsɪzm]"
  },
  {
    "engWord": "trailer",
    "rusWord": "прицеп",
    "transcription": "[ˈtreɪlə]"
  },
  {
    "engWord": "murderer",
    "rusWord": "убийца",
    "transcription": "[ˈmɜːdərə]"
  },
  {
    "engWord": "naive",
    "rusWord": "наивный",
    "transcription": "[nɑːˈiːv]"
  },
  {
    "engWord": "clockwise",
    "rusWord": "по часовой стрелке",
    "transcription": "[ˈklɒkwaɪz]"
  },
  {
    "engWord": "monthly",
    "rusWord": "ежемесячный",
    "transcription": "[ˈmʌnθlɪ]"
  },
  {
    "engWord": "hallway",
    "rusWord": "прихожая",
    "transcription": "[ˈhɔːlweɪ]"
  },
  {
    "engWord": "urgent",
    "rusWord": "срочный",
    "transcription": "[ˈɜːʤənt]"
  },
  {
    "engWord": "approach",
    "rusWord": "подход",
    "transcription": "[əˈprəʊʧ]"
  },
  {
    "engWord": "instance",
    "rusWord": "пример",
    "transcription": "[ˈɪnstəns]"
  },
  {
    "engWord": "niece",
    "rusWord": "племянница",
    "transcription": "[niːs]"
  },
  {
    "engWord": "amount",
    "rusWord": "сумма",
    "transcription": "[əˈmaʊnt]"
  },
  {
    "engWord": "buddy",
    "rusWord": "дружище",
    "transcription": "[ˈbʌdɪ]"
  },
  {
    "engWord": "carrot",
    "rusWord": "морковь",
    "transcription": "[ˈkærət]"
  },
  {
    "engWord": "divorce",
    "rusWord": "расторжение брака",
    "transcription": "[dɪˈvɔːs]"
  },
  {
    "engWord": "cheese",
    "rusWord": "сыр",
    "transcription": "[ʧiːz]"
  },
  {
    "engWord": "bat",
    "rusWord": "летучая мышь",
    "transcription": "[bæt]"
  },
  {
    "engWord": "whole",
    "rusWord": "весь",
    "transcription": "[həʊl]"
  },
  {
    "engWord": "description",
    "rusWord": "описание",
    "transcription": "[dɪsˈkrɪpʃn]"
  },
  {
    "engWord": "leery",
    "rusWord": "подозрительный",
    "transcription": "[ˈlɪərɪ]"
  },
  {
    "engWord": "replacement",
    "rusWord": "замена",
    "transcription": "[rɪˈpleɪsmənt]"
  },
  {
    "engWord": "bare",
    "rusWord": "обнажать",
    "transcription": "[beə]"
  },
  {
    "engWord": "puppy",
    "rusWord": "щенок",
    "transcription": "[ˈpʌpɪ]"
  },
  {
    "engWord": "commentary",
    "rusWord": "комментарий",
    "transcription": "[ˈkɒməntərɪ]"
  },
  {
    "engWord": "register",
    "rusWord": "зарегистрировать",
    "transcription": "[ˈreʤɪstə]"
  },
  {
    "engWord": "German",
    "rusWord": "немецкий",
    "transcription": "[ˈʤɜːmən]"
  },
  {
    "engWord": "interfere",
    "rusWord": "вмешиваться",
    "transcription": "[ɪntəˈfɪə]"
  },
  {
    "engWord": "tin",
    "rusWord": "олово",
    "transcription": "[tɪn]"
  },
  {
    "engWord": "coal",
    "rusWord": "уголь",
    "transcription": "[kəʊl]"
  },
  {
    "engWord": "phase",
    "rusWord": "фаза",
    "transcription": "[feɪz]"
  },
  {
    "engWord": "permanent",
    "rusWord": "постоянный",
    "transcription": "[ˈpɜːmənənt]"
  },
  {
    "engWord": "cinema",
    "rusWord": "кино",
    "transcription": "[ˈsɪnɪmə]"
  },
  {
    "engWord": "program",
    "rusWord": "программа",
    "transcription": "[ˈprəʊgræm]"
  },
  {
    "engWord": "cope",
    "rusWord": "справляться",
    "transcription": "[kəʊp]"
  },
  {
    "engWord": "opening",
    "rusWord": "открытие",
    "transcription": "[ˈəʊpnɪŋ]"
  },
  {
    "engWord": "local",
    "rusWord": "местный",
    "transcription": "[ˈləʊkəl]"
  },
  {
    "engWord": "teach",
    "rusWord": "учить",
    "transcription": "[tiːʧ]"
  },
  {
    "engWord": "ship",
    "rusWord": "корабль",
    "transcription": "[ʃɪp]"
  },
  {
    "engWord": "mind",
    "rusWord": "ум",
    "transcription": "[maɪnd]"
  },
  {
    "engWord": "moment",
    "rusWord": "момент",
    "transcription": "[ˈməʊmənt]"
  },
  {
    "engWord": "number",
    "rusWord": "число",
    "transcription": "[ˈnʌmbə]"
  },
  {
    "engWord": "death",
    "rusWord": "смерть",
    "transcription": "[deθ]"
  },
  {
    "engWord": "warrant",
    "rusWord": "ордер",
    "transcription": "[ˈwɒrənt]"
  },
  {
    "engWord": "cardboard",
    "rusWord": "картон",
    "transcription": "[ˈkɑːdbɔːd]"
  },
  {
    "engWord": "dough",
    "rusWord": "тесто",
    "transcription": "[dəʊ]"
  },
  {
    "engWord": "interrogation",
    "rusWord": "допрос",
    "transcription": "[ɪnterəˈgeɪʃn]"
  },
  {
    "engWord": "wrestling",
    "rusWord": "борьба",
    "transcription": "[ˈreslɪŋ]"
  },
  {
    "engWord": "sang",
    "rusWord": "запеть",
    "transcription": "[sæŋ]"
  },
  {
    "engWord": "mild",
    "rusWord": "мягкий",
    "transcription": "[maɪld]"
  },
  {
    "engWord": "chop",
    "rusWord": "рубить",
    "transcription": "[ʧɒp]"
  },
  {
    "engWord": "mutual",
    "rusWord": "взаимный",
    "transcription": "[ˈmjuːʧʊəl]"
  },
  {
    "engWord": "peninsula",
    "rusWord": "полуостров",
    "transcription": "[pɪˈnɪnsjʊlə]"
  },
  {
    "engWord": "carry on",
    "rusWord": "продолжать",
    "transcription": "[ˈkærɪ ɒn]"
  },
  {
    "engWord": "minor",
    "rusWord": "незначительный",
    "transcription": "[ˈmaɪnə]"
  },
  {
    "engWord": "hood",
    "rusWord": "капюшон",
    "transcription": "[hʊd]"
  },
  {
    "engWord": "attract",
    "rusWord": "привлекать",
    "transcription": "[əˈtrækt]"
  },
  {
    "engWord": "origin",
    "rusWord": "происхождение",
    "transcription": "[ˈɒrɪʤɪn]"
  },
  {
    "engWord": "butter",
    "rusWord": "сливочное масло",
    "transcription": "[ˈbʌtə]"
  },
  {
    "engWord": "spent",
    "rusWord": "потраченный",
    "transcription": "[spent]"
  },
  {
    "engWord": "crime",
    "rusWord": "преступление",
    "transcription": "[kraɪm]"
  },
  {
    "engWord": "sun",
    "rusWord": "солнце",
    "transcription": "[sʌn]"
  },
  {
    "engWord": "handkerchief",
    "rusWord": "носовой платок",
    "transcription": "[ˈhæŋkəʧɪf]"
  },
  {
    "engWord": "dining",
    "rusWord": "обеденный",
    "transcription": "[ˈdaɪnɪŋ]"
  },
  {
    "engWord": "moderate",
    "rusWord": "умеренный",
    "transcription": "[ˈmɒdərɪt]"
  },
  {
    "engWord": "timetable",
    "rusWord": "расписание",
    "transcription": "[ˈtaɪmteɪbl]"
  },
  {
    "engWord": "foam",
    "rusWord": "пена",
    "transcription": "[fəʊm]"
  },
  {
    "engWord": "nickname",
    "rusWord": "прозвище",
    "transcription": "[ˈnɪkneɪm]"
  },
  {
    "engWord": "turk",
    "rusWord": "турок",
    "transcription": "[tɜːk]"
  },
  {
    "engWord": "aboard",
    "rusWord": "на борту",
    "transcription": "[əˈbɔːd]"
  },
  {
    "engWord": "chamber",
    "rusWord": "камера",
    "transcription": "[ˈʧeɪmbə]"
  },
  {
    "engWord": "fundamental",
    "rusWord": "основоположный",
    "transcription": "[fʌndəˈmentl]"
  },
  {
    "engWord": "sergeant",
    "rusWord": "сержант",
    "transcription": "[ˈsɑːʤənt]"
  },
  {
    "engWord": "zone",
    "rusWord": "зона",
    "transcription": "[zəʊn]"
  },
  {
    "engWord": "movie",
    "rusWord": "кино",
    "transcription": "[ˈmuːvɪ]"
  },
  {
    "engWord": "rate",
    "rusWord": "ставка",
    "transcription": "[reɪt]"
  },
  {
    "engWord": "going",
    "rusWord": "идущий",
    "transcription": "[ˈgəʊɪŋ]"
  },
  {
    "engWord": "round",
    "rusWord": "круглый",
    "transcription": "[raʊnd]"
  },
  {
    "engWord": "nation",
    "rusWord": "нация",
    "transcription": "[neɪʃn]"
  },
  {
    "engWord": "never",
    "rusWord": "никогда",
    "transcription": "[ˈnevə]"
  },
  {
    "engWord": "front",
    "rusWord": "фасад",
    "transcription": "[frʌnt]"
  },
  {
    "engWord": "motor",
    "rusWord": "двигатель",
    "transcription": "[ˈməʊtə]"
  },
  {
    "engWord": "textbook",
    "rusWord": "учебник",
    "transcription": "[ˈtekstbʊk]"
  },
  {
    "engWord": "calligraphy",
    "rusWord": "каллиграфия",
    "transcription": "[kəˈlɪgrəfɪ]"
  },
  {
    "engWord": "lipstick",
    "rusWord": "губная помада",
    "transcription": "[ˈlɪpstɪk]"
  },
  {
    "engWord": "saucer",
    "rusWord": "блюдце",
    "transcription": "[ˈsɔːsə]"
  },
  {
    "engWord": "dismiss",
    "rusWord": "отклонять",
    "transcription": "[dɪsˈmɪs]"
  },
  {
    "engWord": "derive",
    "rusWord": "выводить",
    "transcription": "[dɪˈraɪv]"
  },
  {
    "engWord": "therapist",
    "rusWord": "терапевт",
    "transcription": "[ˈθerəpɪst]"
  },
  {
    "engWord": "wallet",
    "rusWord": "бумажник",
    "transcription": "[ˈwɒlɪt]"
  },
  {
    "engWord": "vow",
    "rusWord": "клятва",
    "transcription": "[vaʊ]"
  },
  {
    "engWord": "review",
    "rusWord": "обзор",
    "transcription": "[rɪˈvjuː]"
  },
  {
    "engWord": "hospital",
    "rusWord": "больница",
    "transcription": "[ˈhɒspɪtl]"
  },
  {
    "engWord": "delay",
    "rusWord": "задержка",
    "transcription": "[dɪˈleɪ]"
  },
  {
    "engWord": "headache",
    "rusWord": "головная боль",
    "transcription": "[ˈhedeɪk]"
  },
  {
    "engWord": "survive",
    "rusWord": "выжить",
    "transcription": "[səˈvaɪv]"
  },
  {
    "engWord": "shelf",
    "rusWord": "полка",
    "transcription": "[ʃelf]"
  },
  {
    "engWord": "hungry",
    "rusWord": "голодный",
    "transcription": "[ˈhʌŋgrɪ]"
  },
  {
    "engWord": "first",
    "rusWord": "первый",
    "transcription": "[fɜːst]"
  },
  {
    "engWord": "cloudy",
    "rusWord": "облачный",
    "transcription": "[ˈklaʊdɪ]"
  },
  {
    "engWord": "cowboy",
    "rusWord": "ковбой",
    "transcription": "[ˈkaʊbɔɪ]"
  },
  {
    "engWord": "thermometer",
    "rusWord": "термометр",
    "transcription": "[θəˈmɒmɪtə]"
  },
  {
    "engWord": "belly",
    "rusWord": "живот",
    "transcription": "[ˈbelɪ]"
  },
  {
    "engWord": "abundant",
    "rusWord": "обильный",
    "transcription": "[əˈbʌndənt]"
  },
  {
    "engWord": "forensics",
    "rusWord": "судебная экспертиза",
    "transcription": "[fəˈrensɪks]"
  },
  {
    "engWord": "evaluate",
    "rusWord": "оценивать",
    "transcription": "[ɪˈvæljʊeɪt]"
  },
  {
    "engWord": "involvement",
    "rusWord": "участие",
    "transcription": "[ɪnˈvɒlvmənt]"
  },
  {
    "engWord": "suicide",
    "rusWord": "самоубийство",
    "transcription": "[ˈs(j)uːɪsaɪd]"
  },
  {
    "engWord": "traditional",
    "rusWord": "традиционный",
    "transcription": "[trəˈdɪʃnəl]"
  },
  {
    "engWord": "park",
    "rusWord": "парк",
    "transcription": "[pɑːk]"
  },
  {
    "engWord": "afternoon",
    "rusWord": "время после полудня",
    "transcription": "[ɑːftəˈnuːn]"
  },
  {
    "engWord": "rest",
    "rusWord": "остальное",
    "transcription": "[rest]"
  },
  {
    "engWord": "box",
    "rusWord": "коробка",
    "transcription": "[bɒks]"
  },
  {
    "engWord": "sense",
    "rusWord": "чувство",
    "transcription": "[sens]"
  },
  {
    "engWord": "fraction",
    "rusWord": "дробь",
    "transcription": "[frækʃn]"
  },
  {
    "engWord": "garment",
    "rusWord": "одежда",
    "transcription": "[ˈgɑːmənt]"
  },
  {
    "engWord": "imitate",
    "rusWord": "имитировать",
    "transcription": "[ˈɪmɪteɪt]"
  },
  {
    "engWord": "shrine",
    "rusWord": "святыня",
    "transcription": "[ʃraɪn]"
  },
  {
    "engWord": "microwave",
    "rusWord": "микроволновая печь",
    "transcription": "[ˈmaɪkrəweɪv]"
  },
  {
    "engWord": "roughly",
    "rusWord": "грубо",
    "transcription": "[ˈrʌflɪ]"
  },
  {
    "engWord": "damaged",
    "rusWord": "поврежденный",
    "transcription": "[ˈdæmɪʤd]"
  },
  {
    "engWord": "legally",
    "rusWord": "юридически",
    "transcription": "[ˈliːgəlɪ]"
  },
  {
    "engWord": "hierarchy",
    "rusWord": "иерархия",
    "transcription": "[ˈhaɪərɑːkɪ]"
  },
  {
    "engWord": "finishing",
    "rusWord": "отделка",
    "transcription": "[ˈfɪnɪʃɪŋ]"
  },
  {
    "engWord": "horn",
    "rusWord": "рог",
    "transcription": "[hɔːn]"
  },
  {
    "engWord": "highlight",
    "rusWord": "выделить",
    "transcription": "[ˈhaɪlaɪt]"
  },
  {
    "engWord": "proceed",
    "rusWord": "продолжить",
    "transcription": "[prəˈsiːd]"
  },
  {
    "engWord": "musical",
    "rusWord": "музыкальный",
    "transcription": "[ˈmjuːzɪkəl]"
  },
  {
    "engWord": "mood",
    "rusWord": "настроение",
    "transcription": "[muːd]"
  },
  {
    "engWord": "journalist",
    "rusWord": "журналист",
    "transcription": "[ˈʤɜːnəlɪst]"
  },
  {
    "engWord": "live",
    "rusWord": "жить",
    "transcription": "[]"
  },
  {
    "engWord": "solve",
    "rusWord": "решать",
    "transcription": "[sɒlv]"
  },
  {
    "engWord": "caution",
    "rusWord": "осторожность",
    "transcription": "[kɔːʃn]"
  },
  {
    "engWord": "dull",
    "rusWord": "скучный",
    "transcription": "[dʌl]"
  },
  {
    "engWord": "magnificent",
    "rusWord": "великолепный",
    "transcription": "[mægˈnɪfɪsnt]"
  },
  {
    "engWord": "volunteer",
    "rusWord": "доброволец",
    "transcription": "[vɒlənˈtɪə]"
  },
  {
    "engWord": "furnish",
    "rusWord": "обеспечивать",
    "transcription": "[ˈfɜːnɪʃ]"
  },
  {
    "engWord": "burp",
    "rusWord": "отрыжка",
    "transcription": "[bɜːp]"
  },
  {
    "engWord": "spokesman",
    "rusWord": "представитель",
    "transcription": "[ˈspəʊksmən]"
  },
  {
    "engWord": "rainbow",
    "rusWord": "радуга",
    "transcription": "[ˈreɪnbəʊ]"
  },
  {
    "engWord": "lesbian",
    "rusWord": "лесбосский",
    "transcription": "[ˈlezbɪən]"
  },
  {
    "engWord": "procedure",
    "rusWord": "процедура",
    "transcription": "[prəˈsiːʤə]"
  },
  {
    "engWord": "mosque",
    "rusWord": "мечеть",
    "transcription": "[mɒsk]"
  },
  {
    "engWord": "alien",
    "rusWord": "иностранец",
    "transcription": "[ˈeɪlɪən]"
  },
  {
    "engWord": "secondary",
    "rusWord": "вторичный",
    "transcription": "[ˈsekəndərɪ]"
  },
  {
    "engWord": "community",
    "rusWord": "сообщество",
    "transcription": "[kəˈmjuːnɪtɪ]"
  },
  {
    "engWord": "professor",
    "rusWord": "профессор",
    "transcription": "[prəˈfesə]"
  },
  {
    "engWord": "argue",
    "rusWord": "спорить",
    "transcription": "[ˈɑːgjuː]"
  },
  {
    "engWord": "aware",
    "rusWord": "осведомленный",
    "transcription": "[əˈweə]"
  },
  {
    "engWord": "camera",
    "rusWord": "камера",
    "transcription": "[ˈkæmərə]"
  },
  {
    "engWord": "lonely",
    "rusWord": "одинокий",
    "transcription": "[ˈləʊnlɪ]"
  },
  {
    "engWord": "zoo",
    "rusWord": "зоопарк",
    "transcription": "[zuː]"
  },
  {
    "engWord": "paragraph",
    "rusWord": "параграф",
    "transcription": "[ˈpærəgrɑːf]"
  },
  {
    "engWord": "begin",
    "rusWord": "начать",
    "transcription": "[bɪˈgɪn]"
  },
  {
    "engWord": "plain",
    "rusWord": "простой",
    "transcription": "[pleɪn]"
  },
  {
    "engWord": "tentative",
    "rusWord": "предварительный",
    "transcription": "[ˈtentətɪv]"
  },
  {
    "engWord": "misunderstand",
    "rusWord": "неправильно понять",
    "transcription": "[]"
  },
  {
    "engWord": "emotionally",
    "rusWord": "эмоционально",
    "transcription": "[ɪˈməʊʃnəlɪ]"
  },
  {
    "engWord": "waterfall",
    "rusWord": "водопад",
    "transcription": "[ˈwɔːtəfɔːl]"
  },
  {
    "engWord": "theirs",
    "rusWord": "их",
    "transcription": "[ðeəz]"
  },
  {
    "engWord": "skinny",
    "rusWord": "тощий",
    "transcription": "[ˈskɪnɪ]"
  },
  {
    "engWord": "chapel",
    "rusWord": "часовня",
    "transcription": "[ˈʧæpəl]"
  },
  {
    "engWord": "blessing",
    "rusWord": "благословение",
    "transcription": "[ˈblesɪŋ]"
  },
  {
    "engWord": "slightly",
    "rusWord": "слегка",
    "transcription": "[ˈslaɪtlɪ]"
  },
  {
    "engWord": "photograph",
    "rusWord": "фотография",
    "transcription": "[ˈfəʊtəgrɑːf]"
  },
  {
    "engWord": "mortal",
    "rusWord": "смертный",
    "transcription": "[mɔːtl]"
  },
  {
    "engWord": "stable",
    "rusWord": "стабильный",
    "transcription": "[steɪbl]"
  },
  {
    "engWord": "pray",
    "rusWord": "молиться",
    "transcription": "[preɪ]"
  },
  {
    "engWord": "pepper",
    "rusWord": "перец",
    "transcription": "[ˈpepə]"
  },
  {
    "engWord": "mushroom",
    "rusWord": "гриб",
    "transcription": "[ˈmʌʃrum]"
  },
  {
    "engWord": "beyond",
    "rusWord": "за",
    "transcription": "[bɪˈjɒnd]"
  },
  {
    "engWord": "eve",
    "rusWord": "канун",
    "transcription": "[iːv]"
  },
  {
    "engWord": "weekend",
    "rusWord": "выходные",
    "transcription": "[wiːkˈend]"
  },
  {
    "engWord": "faith",
    "rusWord": "вера",
    "transcription": "[feɪθ]"
  },
  {
    "engWord": "sequence",
    "rusWord": "последовательность",
    "transcription": "[ˈsiːkwəns]"
  },
  {
    "engWord": "harm",
    "rusWord": "вред",
    "transcription": "[hɑːm]"
  },
  {
    "engWord": "available",
    "rusWord": "доступный",
    "transcription": "[əˈveɪləbl]"
  },
  {
    "engWord": "whip",
    "rusWord": "кнут",
    "transcription": "[wɪp]"
  },
  {
    "engWord": "surface",
    "rusWord": "поверхность",
    "transcription": "[ˈsɜːfɪs]"
  },
  {
    "engWord": "shop",
    "rusWord": "магазин",
    "transcription": "[ʃɒp]"
  },
  {
    "engWord": "friend",
    "rusWord": "друг",
    "transcription": "[frend]"
  },
  {
    "engWord": "glass",
    "rusWord": "стекло",
    "transcription": "[glɑːs]"
  },
  {
    "engWord": "keep",
    "rusWord": "держать",
    "transcription": "[kiːp]"
  },
  {
    "engWord": "dependable",
    "rusWord": "надежный",
    "transcription": "[dɪˈpendəbl]"
  },
  {
    "engWord": "depression",
    "rusWord": "депрессия",
    "transcription": "[dɪˈpreʃn]"
  },
  {
    "engWord": "immigrate",
    "rusWord": "иммигрировать",
    "transcription": "[ˈɪmɪgreɪt]"
  },
  {
    "engWord": "coward",
    "rusWord": "трус",
    "transcription": "[ˈkaʊəd]"
  },
  {
    "engWord": "pat",
    "rusWord": "похлопывать",
    "transcription": "[pæt]"
  },
  {
    "engWord": "odds",
    "rusWord": "шансы",
    "transcription": "[ɒdz]"
  },
  {
    "engWord": "Christian",
    "rusWord": "христианин",
    "transcription": "[ˈkrɪsʧən]"
  },
  {
    "engWord": "landing",
    "rusWord": "посадка",
    "transcription": "[ˈlændɪŋ]"
  },
  {
    "engWord": "mud",
    "rusWord": "грязь",
    "transcription": "[mʌd]"
  },
  {
    "engWord": "plea",
    "rusWord": "просьба",
    "transcription": "[pliː]"
  },
  {
    "engWord": "reflect",
    "rusWord": "отражать",
    "transcription": "[rɪˈflekt]"
  },
  {
    "engWord": "bonus",
    "rusWord": "бонус",
    "transcription": "[ˈbəʊnəs]"
  },
  {
    "engWord": "chest",
    "rusWord": "грудная клетка",
    "transcription": "[ʧest]"
  },
  {
    "engWord": "soldier",
    "rusWord": "солдат",
    "transcription": "[ˈsəʊlʤə]"
  },
  {
    "engWord": "interest",
    "rusWord": "интерес",
    "transcription": "[ˈɪntrɪst]"
  },
  {
    "engWord": "practice",
    "rusWord": "практика",
    "transcription": "[ˈpræktɪs]"
  },
  {
    "engWord": "wild",
    "rusWord": "дикий",
    "transcription": "[waɪld]"
  },
  {
    "engWord": "girl",
    "rusWord": "девочка",
    "transcription": "[gɜːl]"
  },
  {
    "engWord": "silk",
    "rusWord": "шелк",
    "transcription": "[sɪlk]"
  },
  {
    "engWord": "nostril",
    "rusWord": "ноздря",
    "transcription": "[ˈnɒstrɪl]"
  },
  {
    "engWord": "glamorous",
    "rusWord": "очаровательный",
    "transcription": "[ˈglæmərəs]"
  },
  {
    "engWord": "dazzle",
    "rusWord": "ослеплять",
    "transcription": "[dæzl]"
  },
  {
    "engWord": "coach",
    "rusWord": "тренер",
    "transcription": "[]"
  },
  {
    "engWord": "grocery",
    "rusWord": "бакалея",
    "transcription": "[ˈgrəʊsərɪ]"
  },
  {
    "engWord": "anxious",
    "rusWord": "озабоченный",
    "transcription": "[ˈæŋkʃəs]"
  },
  {
    "engWord": "below",
    "rusWord": "под",
    "transcription": "[bɪˈləʊ]"
  },
  {
    "engWord": "manner",
    "rusWord": "манера",
    "transcription": "[ˈmænə]"
  },
  {
    "engWord": "exist",
    "rusWord": "существовать",
    "transcription": "[ɪgˈzɪst]"
  },
  {
    "engWord": "obviously",
    "rusWord": "очевидно",
    "transcription": "[ˈɒbvɪəslɪ]"
  },
  {
    "engWord": "satellite",
    "rusWord": "спутник",
    "transcription": "[ˈsæt(ɪ)laɪt]"
  },
  {
    "engWord": "plan",
    "rusWord": "план",
    "transcription": "[plæn]"
  },
  {
    "engWord": "better",
    "rusWord": "лучше",
    "transcription": "[ˈbetə]"
  },
  {
    "engWord": "experiment",
    "rusWord": "эксперимент",
    "transcription": "[ɪksˈperɪmənt]"
  },
  {
    "engWord": "here",
    "rusWord": "здесь",
    "transcription": "[hɪə]"
  },
  {
    "engWord": "born",
    "rusWord": "рожденный",
    "transcription": "[bɔːn]"
  },
  {
    "engWord": "silver",
    "rusWord": "серебро",
    "transcription": "[ˈsɪlvə]"
  },
  {
    "engWord": "yearbook",
    "rusWord": "ежегодник",
    "transcription": "[ˈjɪəbʊk]"
  },
  {
    "engWord": "coastline",
    "rusWord": "береговая линия",
    "transcription": "[ˈkəʊstlaɪn]"
  },
  {
    "engWord": "driveway",
    "rusWord": "дорога",
    "transcription": "[ˈdraɪvweɪ]"
  },
  {
    "engWord": "iron",
    "rusWord": "железо",
    "transcription": "[ˈaɪən]"
  },
  {
    "engWord": "stall",
    "rusWord": "стойло",
    "transcription": "[stɔːl]"
  },
  {
    "engWord": "riot",
    "rusWord": "бунт",
    "transcription": "[ˈraɪət]"
  },
  {
    "engWord": "passionate",
    "rusWord": "страстный",
    "transcription": "[ˈpæʃnɪt]"
  },
  {
    "engWord": "camping",
    "rusWord": "кемпинг",
    "transcription": "[ˈkæmpɪŋ]"
  },
  {
    "engWord": "warmth",
    "rusWord": "тепло",
    "transcription": "[wɔːmθ]"
  },
  {
    "engWord": "lack",
    "rusWord": "отсутствие",
    "transcription": "[læk]"
  },
  {
    "engWord": "wonderful",
    "rusWord": "прекрасный",
    "transcription": "[ˈwʌndəf(ə)l]"
  },
  {
    "engWord": "boss",
    "rusWord": "босс",
    "transcription": "[bɒs]"
  },
  {
    "engWord": "fifty",
    "rusWord": "пятьдесят",
    "transcription": "[ˈfɪftɪ]"
  },
  {
    "engWord": "vast",
    "rusWord": "обширный",
    "transcription": "[vɑːst]"
  },
  {
    "engWord": "silly",
    "rusWord": "глупый",
    "transcription": "[ˈsɪlɪ]"
  },
  {
    "engWord": "tie",
    "rusWord": "галстук",
    "transcription": "[taɪ]"
  },
  {
    "engWord": "complete",
    "rusWord": "полный",
    "transcription": "[kəmˈpliːt]"
  },
  {
    "engWord": "warm",
    "rusWord": "теплый",
    "transcription": "[wɔːm]"
  },
  {
    "engWord": "interested",
    "rusWord": "заинтересованный",
    "transcription": "[ˈɪntrɪstɪd]"
  },
  {
    "engWord": "lyrics",
    "rusWord": "лирика",
    "transcription": "[ˈlɪrɪks]"
  },
  {
    "engWord": "impartial",
    "rusWord": "беспристрастный",
    "transcription": "[ɪmˈpɑːʃəl]"
  },
  {
    "engWord": "blackboard",
    "rusWord": "школьная доска",
    "transcription": "[ˈblækbɔːd]"
  },
  {
    "engWord": "outdoors",
    "rusWord": "на улице",
    "transcription": "[aʊtˈdɔːz]"
  },
  {
    "engWord": "furious",
    "rusWord": "яростный",
    "transcription": "[ˈfjʊərɪəs]"
  },
  {
    "engWord": "thunder",
    "rusWord": "гром",
    "transcription": "[ˈθʌndə]"
  },
  {
    "engWord": "illegal",
    "rusWord": "незаконный",
    "transcription": "[ɪˈliːgəl]"
  },
  {
    "engWord": "pour",
    "rusWord": "наливать",
    "transcription": "[pɔː]"
  },
  {
    "engWord": "gain",
    "rusWord": "получать",
    "transcription": "[geɪn]"
  },
  {
    "engWord": "menu",
    "rusWord": "меню",
    "transcription": "[ˈmenjuː]"
  },
  {
    "engWord": "detail",
    "rusWord": "деталь",
    "transcription": "[ˈdiːteɪl]"
  },
  {
    "engWord": "birth",
    "rusWord": "рождение",
    "transcription": "[bɜːθ]"
  },
  {
    "engWord": "bride",
    "rusWord": "невеста",
    "transcription": "[braɪd]"
  },
  {
    "engWord": "egg",
    "rusWord": "яйцо",
    "transcription": "[eg]"
  },
  {
    "engWord": "cogent",
    "rusWord": "убедительный",
    "transcription": "[ˈkəʊʤənt]"
  },
  {
    "engWord": "clinical",
    "rusWord": "клинический",
    "transcription": "[ˈklɪnɪkəl]"
  },
  {
    "engWord": "confront",
    "rusWord": "противостоять",
    "transcription": "[kənˈfrʌnt]"
  },
  {
    "engWord": "undoubtedly",
    "rusWord": "несомненно",
    "transcription": "[ʌnˈdaʊtɪdlɪ]"
  },
  {
    "engWord": "fatigue",
    "rusWord": "усталость",
    "transcription": "[fəˈtiːg]"
  },
  {
    "engWord": "drown",
    "rusWord": "тонуть",
    "transcription": "[draʊn]"
  },
  {
    "engWord": "sip",
    "rusWord": "прихлебывать",
    "transcription": "[sɪp]"
  },
  {
    "engWord": "clap",
    "rusWord": "хлопать",
    "transcription": "[klæp]"
  },
  {
    "engWord": "burnt",
    "rusWord": "сгоревший",
    "transcription": "[bɜːnt]"
  },
  {
    "engWord": "cattle",
    "rusWord": "крупный рогатый скот",
    "transcription": "[kætl]"
  },
  {
    "engWord": "diamond",
    "rusWord": "бриллиант",
    "transcription": "[ˈdaɪəmənd]"
  },
  {
    "engWord": "dialect",
    "rusWord": "диалект",
    "transcription": "[ˈdaɪəlekt]"
  },
  {
    "engWord": "unique",
    "rusWord": "уникальный",
    "transcription": "[juːˈniːk]"
  },
  {
    "engWord": "host",
    "rusWord": "хозяин",
    "transcription": "[həʊst]"
  },
  {
    "engWord": "factory",
    "rusWord": "завод",
    "transcription": "[ˈfæktərɪ]"
  },
  {
    "engWord": "producer",
    "rusWord": "производитель",
    "transcription": "[prəˈdjuːsə]"
  },
  {
    "engWord": "properly",
    "rusWord": "должным образом",
    "transcription": "[ˈprɒpəlɪ]"
  },
  {
    "engWord": "vine",
    "rusWord": "лоза",
    "transcription": "[vaɪn]"
  },
  {
    "engWord": "finance",
    "rusWord": "финансы",
    "transcription": "[faɪˈnæns]"
  },
  {
    "engWord": "lane",
    "rusWord": "переулок",
    "transcription": "[leɪn]"
  },
  {
    "engWord": "jean",
    "rusWord": "Джин",
    "transcription": "[ʤiːn]"
  },
  {
    "engWord": "decrease",
    "rusWord": "уменьшать",
    "transcription": "[ˈdiːkriːs]"
  },
  {
    "engWord": "comfortless",
    "rusWord": "неуютный",
    "transcription": "[ˈkʌmfətlɪs]"
  },
  {
    "engWord": "powder",
    "rusWord": "порошок",
    "transcription": "[ˈpaʊdə]"
  },
  {
    "engWord": "transplant",
    "rusWord": "трансплантировать",
    "transcription": "[trænsˈplɑːnt]"
  },
  {
    "engWord": "orchestra",
    "rusWord": "оркестр",
    "transcription": "[ˈɔːkɪstrə]"
  },
  {
    "engWord": "mug",
    "rusWord": "кружка",
    "transcription": "[mʌg]"
  },
  {
    "engWord": "moreover",
    "rusWord": "более того",
    "transcription": "[mɔːˈrəʊvə]"
  },
  {
    "engWord": "bite",
    "rusWord": "кусать",
    "transcription": "[baɪt]"
  },
  {
    "engWord": "truth",
    "rusWord": "истина",
    "transcription": "[truːθ]"
  },
  {
    "engWord": "contact",
    "rusWord": "контакт",
    "transcription": "[ˈkɒntækt]"
  },
  {
    "engWord": "tip",
    "rusWord": "кончик",
    "transcription": "[tɪp]"
  },
  {
    "engWord": "are",
    "rusWord": "АР",
    "transcription": "[ɑː]"
  },
  {
    "engWord": "still",
    "rusWord": "до сих пор",
    "transcription": "[stɪl]"
  },
  {
    "engWord": "conviction",
    "rusWord": "убеждение",
    "transcription": "[kənˈvɪkʃn]"
  },
  {
    "engWord": "omit",
    "rusWord": "пропускать",
    "transcription": "[əˈmɪt]"
  },
  {
    "engWord": "spider",
    "rusWord": "паук",
    "transcription": "[ˈspaɪdə]"
  },
  {
    "engWord": "married",
    "rusWord": "состоящий в браке",
    "transcription": "[ˈmærɪd]"
  },
  {
    "engWord": "photographer",
    "rusWord": "фотограф",
    "transcription": "[fəˈtɒgrəfə]"
  },
  {
    "engWord": "forehead",
    "rusWord": "лоб",
    "transcription": "[ˈfɒrɪd]"
  },
  {
    "engWord": "robot",
    "rusWord": "робот",
    "transcription": "[ˈrəʊbɒt]"
  },
  {
    "engWord": "classical",
    "rusWord": "классический",
    "transcription": "[ˈklæsɪkəl]"
  },
  {
    "engWord": "recommendation",
    "rusWord": "рекомендация",
    "transcription": "[rekəmenˈdeɪʃn]"
  },
  {
    "engWord": "roller",
    "rusWord": "ролик",
    "transcription": "[ˈrəʊlə]"
  },
  {
    "engWord": "motorway",
    "rusWord": "автомагистраль",
    "transcription": "[ˈməʊtəweɪ]"
  },
  {
    "engWord": "intense",
    "rusWord": "интенсивный",
    "transcription": "[ɪnˈtens]"
  },
  {
    "engWord": "elsewhere",
    "rusWord": "в другом месте",
    "transcription": "[elsˈweə]"
  },
  {
    "engWord": "jumper",
    "rusWord": "прыгун",
    "transcription": "[ˈʤʌmpə]"
  },
  {
    "engWord": "engage",
    "rusWord": "привлекать",
    "transcription": "[ɪnˈgeɪʤ]"
  },
  {
    "engWord": "therapy",
    "rusWord": "терапия",
    "transcription": "[ˈθerəpɪ]"
  },
  {
    "engWord": "small",
    "rusWord": "маленький",
    "transcription": "[smɔːl]"
  },
  {
    "engWord": "smell",
    "rusWord": "запах",
    "transcription": "[smel]"
  },
  {
    "engWord": "size",
    "rusWord": "размер",
    "transcription": "[saɪz]"
  },
  {
    "engWord": "moon",
    "rusWord": "луна",
    "transcription": "[muːn]"
  },
  {
    "engWord": "setup",
    "rusWord": "установка",
    "transcription": "[ˈsetʌp]"
  },
  {
    "engWord": "grandparents",
    "rusWord": "бабушка с дедушкой",
    "transcription": "[ˈgrænpe(ə)rənts]"
  },
  {
    "engWord": "ancestor",
    "rusWord": "предок",
    "transcription": "[ˈænsəstə]"
  },
  {
    "engWord": "metropolitan",
    "rusWord": "митрополит",
    "transcription": "[metrəˈpɔlɪtən]"
  },
  {
    "engWord": "curiosity",
    "rusWord": "любопытство",
    "transcription": "[kjʊərɪˈɒsɪtɪ]"
  },
  {
    "engWord": "helpless",
    "rusWord": "беспомощный",
    "transcription": "[ˈhelplɪs]"
  },
  {
    "engWord": "shared",
    "rusWord": "общий",
    "transcription": "[ˈʃeəd]"
  },
  {
    "engWord": "tiger",
    "rusWord": "тигр",
    "transcription": "[ˈtaɪgə]"
  },
  {
    "engWord": "dignity",
    "rusWord": "достоинство",
    "transcription": "[ˈdɪgnɪtɪ]"
  },
  {
    "engWord": "delicious",
    "rusWord": "вкусный",
    "transcription": "[dɪˈlɪʃəs]"
  },
  {
    "engWord": "bum",
    "rusWord": "бездельник",
    "transcription": "[bʌm]"
  },
  {
    "engWord": "scope",
    "rusWord": "масштаб",
    "transcription": "[skəʊp]"
  },
  {
    "engWord": "acid",
    "rusWord": "кислота",
    "transcription": "[ˈæsɪd]"
  },
  {
    "engWord": "happily",
    "rusWord": "счастливо",
    "transcription": "[ˈhæpɪlɪ]"
  },
  {
    "engWord": "ice cream",
    "rusWord": "мороженое",
    "transcription": "[aɪsˈkriːm]"
  },
  {
    "engWord": "inn",
    "rusWord": "гостиница",
    "transcription": "[ɪn]"
  },
  {
    "engWord": "participate",
    "rusWord": "принимать участие",
    "transcription": "[pɑːˈtɪsɪpeɪt]"
  },
  {
    "engWord": "terrific",
    "rusWord": "потрясающий",
    "transcription": "[təˈrɪfɪk]"
  },
  {
    "engWord": "event",
    "rusWord": "событие",
    "transcription": "[ɪˈvent]"
  },
  {
    "engWord": "much",
    "rusWord": "много",
    "transcription": "[mʌʧ]"
  },
  {
    "engWord": "panel",
    "rusWord": "панель",
    "transcription": "[pænl]"
  },
  {
    "engWord": "postage",
    "rusWord": "почтовые расходы",
    "transcription": "[ˈpəʊstɪʤ]"
  },
  {
    "engWord": "currency",
    "rusWord": "валюта",
    "transcription": "[ˈkʌrənsɪ]"
  },
  {
    "engWord": "thrill",
    "rusWord": "трепет",
    "transcription": "[θrɪl]"
  },
  {
    "engWord": "plantation",
    "rusWord": "плантация",
    "transcription": "[plænˈteɪʃən]"
  },
  {
    "engWord": "inspector",
    "rusWord": "инспектор",
    "transcription": "[ɪnˈspektə]"
  },
  {
    "engWord": "purely",
    "rusWord": "чисто",
    "transcription": "[ˈpjʊəlɪ]"
  },
  {
    "engWord": "blink",
    "rusWord": "мерцать",
    "transcription": "[blɪŋk]"
  },
  {
    "engWord": "engineer",
    "rusWord": "инженер",
    "transcription": "[enʤɪˈnɪə]"
  },
  {
    "engWord": "trend",
    "rusWord": "тенденция",
    "transcription": "[trend]"
  },
  {
    "engWord": "kiss",
    "rusWord": "целовать",
    "transcription": "[kɪs]"
  },
  {
    "engWord": "hero",
    "rusWord": "герой",
    "transcription": "[ˈhɪərəʊ]"
  },
  {
    "engWord": "beer",
    "rusWord": "пиво",
    "transcription": "[bɪə]"
  },
  {
    "engWord": "clearly",
    "rusWord": "ясно",
    "transcription": "[ˈklɪəlɪ]"
  },
  {
    "engWord": "war",
    "rusWord": "война",
    "transcription": "[wɔː]"
  },
  {
    "engWord": "length",
    "rusWord": "длина",
    "transcription": "[leŋθ]"
  },
  {
    "engWord": "over",
    "rusWord": "над",
    "transcription": "[ˈəʊvə]"
  },
  {
    "engWord": "teacher",
    "rusWord": "учитель",
    "transcription": "[ˈtiːʧə]"
  },
  {
    "engWord": "retirement",
    "rusWord": "отставка",
    "transcription": "[rɪˈtaɪəmənt]"
  },
  {
    "engWord": "cooperate",
    "rusWord": "сотрудничать",
    "transcription": "[kəʊˈɒpəreɪt]"
  },
  {
    "engWord": "pavement",
    "rusWord": "тротуар",
    "transcription": "[ˈpeɪvmənt]"
  },
  {
    "engWord": "alike",
    "rusWord": "похожий",
    "transcription": "[əˈlaɪk]"
  },
  {
    "engWord": "disadvantage",
    "rusWord": "недостаток",
    "transcription": "[dɪsədˈvɑːntɪʤ]"
  },
  {
    "engWord": "beak",
    "rusWord": "клюв",
    "transcription": "[biːk]"
  },
  {
    "engWord": "upsetting",
    "rusWord": "тревожный",
    "transcription": "[ʌpˈsetɪŋ]"
  },
  {
    "engWord": "indirect",
    "rusWord": "косвенный",
    "transcription": "[ɪndɪˈrekt]"
  },
  {
    "engWord": "holding",
    "rusWord": "удерживание",
    "transcription": "[ˈhəʊldɪŋ]"
  },
  {
    "engWord": "definition",
    "rusWord": "определение",
    "transcription": "[defɪˈnɪʃn]"
  },
  {
    "engWord": "ambulance",
    "rusWord": "скорая помощь",
    "transcription": "[ˈæmbjʊləns]"
  },
  {
    "engWord": "nerve",
    "rusWord": "нерв",
    "transcription": "[nɜːv]"
  },
  {
    "engWord": "lazy",
    "rusWord": "ленивый",
    "transcription": "[ˈleɪzɪ]"
  },
  {
    "engWord": "kilometre",
    "rusWord": "километр",
    "transcription": "[ˈkɪləmiːtə]"
  },
  {
    "engWord": "accurate",
    "rusWord": "точный",
    "transcription": "[ˈækjərət]"
  },
  {
    "engWord": "electric",
    "rusWord": "электрический",
    "transcription": "[ɪˈlektrɪk]"
  },
  {
    "engWord": "fine",
    "rusWord": "штраф",
    "transcription": "[faɪn]"
  },
  {
    "engWord": "energy",
    "rusWord": "энергия",
    "transcription": "[ˈenəʤɪ]"
  },
  {
    "engWord": "ear",
    "rusWord": "ухо",
    "transcription": "[ɪə]"
  },
  {
    "engWord": "prove",
    "rusWord": "доказывать",
    "transcription": "[pruːv]"
  },
  {
    "engWord": "process",
    "rusWord": "процесс",
    "transcription": "[ˈprəʊsəs]"
  },
  {
    "engWord": "rock",
    "rusWord": "рок",
    "transcription": "[rɒk]"
  },
  {
    "engWord": "housewife",
    "rusWord": "домохозяйка",
    "transcription": "[ˈhaʊswaɪf]"
  },
  {
    "engWord": "deer",
    "rusWord": "олень",
    "transcription": "[dɪə]"
  },
  {
    "engWord": "peanut",
    "rusWord": "арахис",
    "transcription": "[ˈpiːnʌt]"
  },
  {
    "engWord": "January",
    "rusWord": "январь",
    "transcription": "[ˈʤænjʊərɪ]"
  },
  {
    "engWord": "coconut",
    "rusWord": "кокос",
    "transcription": "[ˈkəʊkənʌt]"
  },
  {
    "engWord": "specialist",
    "rusWord": "специалист",
    "transcription": "[ˈspeʃəlɪst]"
  },
  {
    "engWord": "hut",
    "rusWord": "хижина",
    "transcription": "[hʌt]"
  },
  {
    "engWord": "scan",
    "rusWord": "сканировать",
    "transcription": "[skæn]"
  },
  {
    "engWord": "brutal",
    "rusWord": "жестокий",
    "transcription": "[bruːtl]"
  },
  {
    "engWord": "preserve",
    "rusWord": "сохранять",
    "transcription": "[prɪˈzɜːv]"
  },
  {
    "engWord": "lighting",
    "rusWord": "освещение",
    "transcription": "[ˈlaɪtɪŋ]"
  },
  {
    "engWord": "forth",
    "rusWord": "далее",
    "transcription": "[fɔːθ]"
  },
  {
    "engWord": "supermarket",
    "rusWord": "супермаркет",
    "transcription": "[ˈsjuːpəmɑːkɪt]"
  },
  {
    "engWord": "slice",
    "rusWord": "ломтик",
    "transcription": "[slaɪs]"
  },
  {
    "engWord": "counting",
    "rusWord": "подсчет",
    "transcription": "[ˈkaʊntɪŋ]"
  },
  {
    "engWord": "payment",
    "rusWord": "оплата",
    "transcription": "[ˈpeɪmənt]"
  },
  {
    "engWord": "label",
    "rusWord": "этикетка",
    "transcription": "[leɪbl]"
  },
  {
    "engWord": "toe",
    "rusWord": "палец ноги",
    "transcription": "[təʊ]"
  },
  {
    "engWord": "voice",
    "rusWord": "голос",
    "transcription": "[vɔɪs]"
  },
  {
    "engWord": "slow",
    "rusWord": "медленный",
    "transcription": "[sləʊ]"
  },
  {
    "engWord": "rich",
    "rusWord": "богатый",
    "transcription": "[rɪʧ]"
  },
  {
    "engWord": "defective",
    "rusWord": "дефектный",
    "transcription": "[dɪˈfektɪv]"
  },
  {
    "engWord": "sled",
    "rusWord": "сани",
    "transcription": "[sled]"
  },
  {
    "engWord": "overflow",
    "rusWord": "переполнение",
    "transcription": "[ˈəʊvəfləʊ]"
  },
  {
    "engWord": "entrepreneur",
    "rusWord": "предприниматель",
    "transcription": "[ɒntrəprəˈnɜː]"
  },
  {
    "engWord": "perfume",
    "rusWord": "аромат",
    "transcription": "[ˈpɜːfjuːm]"
  },
  {
    "engWord": "dimension",
    "rusWord": "измерение",
    "transcription": "[dɪˈmenʃn]"
  },
  {
    "engWord": "fuzzy",
    "rusWord": "размытый",
    "transcription": "[ˈfʌzɪ]"
  },
  {
    "engWord": "educate",
    "rusWord": "учить",
    "transcription": "[ˈedjʊkeɪt]"
  },
  {
    "engWord": "disappoint",
    "rusWord": "разочаровывать",
    "transcription": "[dɪsəˈpɔɪnt]"
  },
  {
    "engWord": "formal",
    "rusWord": "формальный",
    "transcription": "[ˈfɔːməl]"
  },
  {
    "engWord": "firm",
    "rusWord": "твердый",
    "transcription": "[fɜːm]"
  },
  {
    "engWord": "former",
    "rusWord": "бывший",
    "transcription": "[ˈfɔːmə]"
  },
  {
    "engWord": "regularly",
    "rusWord": "регулярно",
    "transcription": "[ˈregjʊləlɪ]"
  },
  {
    "engWord": "bored",
    "rusWord": "скучающий",
    "transcription": "[bɔːd]"
  },
  {
    "engWord": "smoking",
    "rusWord": "курение",
    "transcription": "[ˈsməʊkɪŋ]"
  },
  {
    "engWord": "trial",
    "rusWord": "пробный",
    "transcription": "[ˈtraɪəl]"
  },
  {
    "engWord": "screen",
    "rusWord": "экран",
    "transcription": "[skriːn]"
  },
  {
    "engWord": "railway",
    "rusWord": "железная дорога",
    "transcription": "[ˈreɪlweɪ]"
  },
  {
    "engWord": "youth",
    "rusWord": "молодежь",
    "transcription": "[jʊθ]"
  },
  {
    "engWord": "guard",
    "rusWord": "охрана",
    "transcription": "[gɑːd]"
  },
  {
    "engWord": "statement",
    "rusWord": "заявление",
    "transcription": "[ˈsteɪtmənt]"
  },
  {
    "engWord": "sorry",
    "rusWord": "огорченный",
    "transcription": "[ˈsɒrɪ]"
  },
  {
    "engWord": "distant",
    "rusWord": "отдаленный",
    "transcription": "[ˈdɪstənt]"
  },
  {
    "engWord": "act",
    "rusWord": "действие",
    "transcription": "[ækt]"
  },
  {
    "engWord": "money",
    "rusWord": "деньги",
    "transcription": "[ˈmʌnɪ]"
  },
  {
    "engWord": "weather",
    "rusWord": "погода",
    "transcription": "[ˈweðə]"
  },
  {
    "engWord": "glory",
    "rusWord": "слава",
    "transcription": "[ˈglɔːrɪ]"
  },
  {
    "engWord": "integrity",
    "rusWord": "цельность",
    "transcription": "[ɪnˈtegrɪtɪ]"
  },
  {
    "engWord": "enquiry",
    "rusWord": "запрос",
    "transcription": "[ɪnˈkwaɪərɪ]"
  },
  {
    "engWord": "cheat",
    "rusWord": "обманывать",
    "transcription": "[ʧiːt]"
  },
  {
    "engWord": "messenger",
    "rusWord": "посланник",
    "transcription": "[ˈmes(ə)nʤə]"
  },
  {
    "engWord": "chore",
    "rusWord": "домашняя обязанность",
    "transcription": "[ʧɔː]"
  },
  {
    "engWord": "rap",
    "rusWord": "рэп",
    "transcription": "[ræp]"
  },
  {
    "engWord": "astonishing",
    "rusWord": "удивительный",
    "transcription": "[əsˈtɒnɪʃɪŋ]"
  },
  {
    "engWord": "booth",
    "rusWord": "будка",
    "transcription": "[buːð]"
  },
  {
    "engWord": "sponsor",
    "rusWord": "спонсор",
    "transcription": "[ˈspɒnsə]"
  },
  {
    "engWord": "heartbeat",
    "rusWord": "биение сердца",
    "transcription": "[ˈhɑːtbiːt]"
  },
  {
    "engWord": "relation",
    "rusWord": "отношение",
    "transcription": "[rɪˈleɪʃn]"
  },
  {
    "engWord": "reckon",
    "rusWord": "рассчитывать",
    "transcription": "[ˈrekən]"
  },
  {
    "engWord": "sandwich",
    "rusWord": "сэндвич",
    "transcription": "[ˈsænwɪʤ]"
  },
  {
    "engWord": "built",
    "rusWord": "построенный",
    "transcription": "[bɪlt]"
  },
  {
    "engWord": "desk",
    "rusWord": "рабочий стол",
    "transcription": "[desk]"
  },
  {
    "engWord": "shame",
    "rusWord": "позор",
    "transcription": "[ʃeɪm]"
  },
  {
    "engWord": "no",
    "rusWord": "нет",
    "transcription": "[nəʊ]"
  },
  {
    "engWord": "sand",
    "rusWord": "песок",
    "transcription": "[sænd]"
  },
  {
    "engWord": "property",
    "rusWord": "собственность",
    "transcription": "[ˈprɒpətɪ]"
  },
  {
    "engWord": "fixing",
    "rusWord": "крепежный",
    "transcription": "[ˈfɪksɪŋ]"
  },
  {
    "engWord": "straightforward",
    "rusWord": "простой",
    "transcription": "[streɪtˈfɔːwəd]"
  },
  {
    "engWord": "beacon",
    "rusWord": "маяк",
    "transcription": "[ˈbiːkən]"
  },
  {
    "engWord": "squeeze",
    "rusWord": "сжимать",
    "transcription": "[skwiːz]"
  },
  {
    "engWord": "scream",
    "rusWord": "крик",
    "transcription": "[skriːm]"
  },
  {
    "engWord": "supper",
    "rusWord": "ужин",
    "transcription": "[ˈsʌpə]"
  },
  {
    "engWord": "carpet",
    "rusWord": "ковер",
    "transcription": "[ˈkɑːpɪt]"
  },
  {
    "engWord": "nut",
    "rusWord": "орех",
    "transcription": "[nʌt]"
  },
  {
    "engWord": "empty",
    "rusWord": "пустой",
    "transcription": "[ˈemptɪ]"
  },
  {
    "engWord": "example",
    "rusWord": "образец",
    "transcription": "[ɪgˈzɑːmpl]"
  },
  {
    "engWord": "car",
    "rusWord": "машина",
    "transcription": "[kɑː]"
  },
  {
    "engWord": "latitude",
    "rusWord": "широта",
    "transcription": "[ˈlætɪtjuːd]"
  },
  {
    "engWord": "leap",
    "rusWord": "прыгать",
    "transcription": "[liːp]"
  },
  {
    "engWord": "heaviness",
    "rusWord": "тяжесть",
    "transcription": "[ˈhevɪnɪs]"
  },
  {
    "engWord": "snack",
    "rusWord": "закуска",
    "transcription": "[snæk]"
  },
  {
    "engWord": "manipulate",
    "rusWord": "манипулировать",
    "transcription": "[məˈnɪpjʊleɪt]"
  },
  {
    "engWord": "ignoring",
    "rusWord": "игнорирующий",
    "transcription": "[ɪgˈnɔːrɪŋ]"
  },
  {
    "engWord": "delusional",
    "rusWord": "бредовой",
    "transcription": "[dɪˈluːʒənl]"
  },
  {
    "engWord": "widow",
    "rusWord": "вдова",
    "transcription": "[ˈwɪdəʊ]"
  },
  {
    "engWord": "scramble",
    "rusWord": "карабкаться",
    "transcription": "[skræmbl]"
  },
  {
    "engWord": "driven",
    "rusWord": "управляемый",
    "transcription": "[drɪvn]"
  },
  {
    "engWord": "fog",
    "rusWord": "туман",
    "transcription": "[fɒg]"
  },
  {
    "engWord": "provision",
    "rusWord": "обеспечение",
    "transcription": "[prəˈvɪʒən]"
  },
  {
    "engWord": "assault",
    "rusWord": "нападение",
    "transcription": "[əˈsɔːlt]"
  },
  {
    "engWord": "career",
    "rusWord": "карьера",
    "transcription": "[kəˈrɪə]"
  },
  {
    "engWord": "its",
    "rusWord": "свой",
    "transcription": "[ɪts]"
  },
  {
    "engWord": "hour",
    "rusWord": "час",
    "transcription": "[ˈaʊə]"
  },
  {
    "engWord": "sail",
    "rusWord": "парус",
    "transcription": "[seɪl]"
  },
  {
    "engWord": "woman",
    "rusWord": "женщина",
    "transcription": "[ˈwʊmən]"
  },
  {
    "engWord": "do",
    "rusWord": "делать",
    "transcription": "[duː]"
  },
  {
    "engWord": "precede",
    "rusWord": "предшествовать",
    "transcription": "[prɪˈsiːd]"
  },
  {
    "engWord": "roast",
    "rusWord": "жарить",
    "transcription": "[rəʊst]"
  },
  {
    "engWord": "socket",
    "rusWord": "разъем",
    "transcription": "[ˈsɒkɪt]"
  },
  {
    "engWord": "function",
    "rusWord": "функция",
    "transcription": "[fʌŋkʃn]"
  },
  {
    "engWord": "fed",
    "rusWord": "Федеральная резервная система",
    "transcription": "[fed]"
  },
  {
    "engWord": "fund",
    "rusWord": "фонд",
    "transcription": "[fʌnd]"
  },
  {
    "engWord": "account",
    "rusWord": "счет",
    "transcription": "[əˈkaʊnt]"
  },
  {
    "engWord": "exchange",
    "rusWord": "обмен",
    "transcription": "[ɪksˈʧeɪnʤ]"
  },
  {
    "engWord": "attack",
    "rusWord": "атака",
    "transcription": "[əˈtæk]"
  },
  {
    "engWord": "marvellous",
    "rusWord": "изумительный",
    "transcription": "[ˈmɑːvələs]"
  },
  {
    "engWord": "deaf",
    "rusWord": "глухой",
    "transcription": "[def]"
  },
  {
    "engWord": "not",
    "rusWord": "нет",
    "transcription": "[nɒt]"
  },
  {
    "engWord": "soon",
    "rusWord": "скоро",
    "transcription": "[suːn]"
  },
  {
    "engWord": "talk",
    "rusWord": "говорить",
    "transcription": "[tɔːk]"
  },
  {
    "engWord": "sight",
    "rusWord": "взгляд",
    "transcription": "[saɪt]"
  },
  {
    "engWord": "often",
    "rusWord": "часто",
    "transcription": "[ˈɒf(t)ən]"
  },
  {
    "engWord": "bit",
    "rusWord": "немного",
    "transcription": "[bɪt]"
  },
  {
    "engWord": "humble",
    "rusWord": "покорный",
    "transcription": "[hʌmbl]"
  },
  {
    "engWord": "sauna",
    "rusWord": "сауна",
    "transcription": "[ˈsaʊnə]"
  },
  {
    "engWord": "nought",
    "rusWord": "ноль",
    "transcription": "[nɔːt]"
  },
  {
    "engWord": "freaky",
    "rusWord": "причудливый",
    "transcription": "[ˈfriːkɪ]"
  },
  {
    "engWord": "bunny",
    "rusWord": "кролик",
    "transcription": "[ˈbʌnɪ]"
  },
  {
    "engWord": "jungle",
    "rusWord": "джунгли",
    "transcription": "[ʤʌŋgl]"
  },
  {
    "engWord": "baggage",
    "rusWord": "багаж",
    "transcription": "[ˈbægɪʤ]"
  },
  {
    "engWord": "install",
    "rusWord": "устанавливать",
    "transcription": "[ɪnˈstɔːl]"
  },
  {
    "engWord": "surely",
    "rusWord": "конечно",
    "transcription": "[ˈʃʊəlɪ]"
  },
  {
    "engWord": "finally",
    "rusWord": "наконец",
    "transcription": "[ˈfaɪnəlɪ]"
  },
  {
    "engWord": "charge",
    "rusWord": "заряжать",
    "transcription": "[ʧɑːʤ]"
  },
  {
    "engWord": "liquid",
    "rusWord": "жидкость",
    "transcription": "[ˈlɪkwɪd]"
  },
  {
    "engWord": "both",
    "rusWord": "оба",
    "transcription": "[bəʊθ]"
  },
  {
    "engWord": "generalization",
    "rusWord": "обобщение",
    "transcription": "[ʤenərəlaɪˈzeɪʃn]"
  },
  {
    "engWord": "vessel",
    "rusWord": "судно",
    "transcription": "[vesl]"
  },
  {
    "engWord": "tights",
    "rusWord": "трико",
    "transcription": "[taɪts]"
  },
  {
    "engWord": "constantly",
    "rusWord": "постоянно",
    "transcription": "[ˈkɒnstəntlɪ]"
  },
  {
    "engWord": "touching",
    "rusWord": "трогательный",
    "transcription": "[ˈtʌʧɪŋ]"
  },
  {
    "engWord": "bruise",
    "rusWord": "синяк",
    "transcription": "[bruːz]"
  },
  {
    "engWord": "knit",
    "rusWord": "вязать",
    "transcription": "[nɪt]"
  },
  {
    "engWord": "vat",
    "rusWord": "бак",
    "transcription": "[væt]"
  },
  {
    "engWord": "floating",
    "rusWord": "плавающий",
    "transcription": "[ˈfləʊtɪŋ]"
  },
  {
    "engWord": "solicitor",
    "rusWord": "адвокат",
    "transcription": "[səˈlɪsɪtə]"
  },
  {
    "engWord": "household",
    "rusWord": "домашнее хозяйство",
    "transcription": "[ˈhaʊshəʊld]"
  },
  {
    "engWord": "moral",
    "rusWord": "моральный",
    "transcription": "[ˈmɒrəl]"
  },
  {
    "engWord": "reveal",
    "rusWord": "раскрыть",
    "transcription": "[rɪˈviːl]"
  },
  {
    "engWord": "garlic",
    "rusWord": "чеснок",
    "transcription": "[ˈgɑːlɪk]"
  },
  {
    "engWord": "saint",
    "rusWord": "святой",
    "transcription": "[seɪnt]"
  },
  {
    "engWord": "national",
    "rusWord": "национальный",
    "transcription": "[ˈnæʃnəl]"
  },
  {
    "engWord": "smart",
    "rusWord": "умный",
    "transcription": "[smɑːt]"
  },
  {
    "engWord": "wooden",
    "rusWord": "деревянный",
    "transcription": "[wʊdn]"
  },
  {
    "engWord": "ward",
    "rusWord": "палата",
    "transcription": "[wɔːd]"
  },
  {
    "engWord": "distance",
    "rusWord": "расстояние",
    "transcription": "[ˈdɪstəns]"
  },
  {
    "engWord": "lifetime",
    "rusWord": "жизненный цикл",
    "transcription": "[ˈlaɪftaɪm]"
  },
  {
    "engWord": "because",
    "rusWord": "потому что",
    "transcription": "[bɪˈkɒz]"
  },
  {
    "engWord": "world",
    "rusWord": "мир",
    "transcription": "[wɜːld]"
  },
  {
    "engWord": "page",
    "rusWord": "страница",
    "transcription": "[peɪʤ]"
  },
  {
    "engWord": "melody",
    "rusWord": "мелодия",
    "transcription": "[ˈmelədɪ]"
  },
  {
    "engWord": "middle",
    "rusWord": "средний",
    "transcription": "[mɪdl]"
  },
  {
    "engWord": "section",
    "rusWord": "раздел",
    "transcription": "[sekʃn]"
  },
  {
    "engWord": "die",
    "rusWord": "умирать",
    "transcription": "[daɪ]"
  },
  {
    "engWord": "invisible",
    "rusWord": "невидимый",
    "transcription": "[ɪnˈvɪzəbl]"
  },
  {
    "engWord": "unity",
    "rusWord": "единство",
    "transcription": "[ˈjuːnɪtɪ]"
  },
  {
    "engWord": "bathe",
    "rusWord": "купать",
    "transcription": "[beɪð]"
  },
  {
    "engWord": "deputy",
    "rusWord": "заместитель",
    "transcription": "[ˈdepjʊtɪ]"
  },
  {
    "engWord": "specify",
    "rusWord": "указывать",
    "transcription": "[ˈspesɪfaɪ]"
  },
  {
    "engWord": "swap",
    "rusWord": "менять",
    "transcription": "[swɒp]"
  },
  {
    "engWord": "effort",
    "rusWord": "усилие",
    "transcription": "[ˈefət]"
  },
  {
    "engWord": "achieve",
    "rusWord": "достигать",
    "transcription": "[əˈʧiːv]"
  },
  {
    "engWord": "objection",
    "rusWord": "возражение",
    "transcription": "[əbˈʤekʃn]"
  },
  {
    "engWord": "lamb",
    "rusWord": "ягненок",
    "transcription": "[læm]"
  },
  {
    "engWord": "struggle",
    "rusWord": "борьба",
    "transcription": "[strʌgl]"
  },
  {
    "engWord": "writing",
    "rusWord": "письменный",
    "transcription": "[ˈraɪtɪŋ]"
  },
  {
    "engWord": "bullet",
    "rusWord": "пуля",
    "transcription": "[ˈbʊlɪt]"
  },
  {
    "engWord": "apartment",
    "rusWord": "квартира",
    "transcription": "[əˈpɑːtmənt]"
  },
  {
    "engWord": "variation",
    "rusWord": "изменение",
    "transcription": "[ve(ə)rɪˈeɪʃn]"
  },
  {
    "engWord": "top",
    "rusWord": "верхний",
    "transcription": "[tɒp]"
  },
  {
    "engWord": "column",
    "rusWord": "колонка",
    "transcription": "[ˈkɒləm]"
  },
  {
    "engWord": "least",
    "rusWord": "наименьший",
    "transcription": "[liːst]"
  },
  {
    "engWord": "actress",
    "rusWord": "актриса",
    "transcription": "[]"
  },
  {
    "engWord": "comic",
    "rusWord": "комический",
    "transcription": "[ˈkɒmɪk]"
  },
  {
    "engWord": "external",
    "rusWord": "внешний",
    "transcription": "[ɪkˈstɜːn(ə)l]"
  },
  {
    "engWord": "oath",
    "rusWord": "клятва",
    "transcription": "[əʊθ]"
  },
  {
    "engWord": "mosquito",
    "rusWord": "комар",
    "transcription": "[məsˈkiːtəʊ]"
  },
  {
    "engWord": "costs",
    "rusWord": "затраты",
    "transcription": "[kɒsts]"
  },
  {
    "engWord": "framework",
    "rusWord": "рамки",
    "transcription": "[ˈfreɪmwɜːk]"
  },
  {
    "engWord": "proverb",
    "rusWord": "пословица",
    "transcription": "[ˈprɒvɜːb]"
  },
  {
    "engWord": "buff",
    "rusWord": "полировать",
    "transcription": "[bʌf]"
  },
  {
    "engWord": "uncertain",
    "rusWord": "неопределенный",
    "transcription": "[ʌnˈsɜːtn]"
  },
  {
    "engWord": "survey",
    "rusWord": "опрос",
    "transcription": "[ˈsɜːveɪ]"
  },
  {
    "engWord": "incurable",
    "rusWord": "неизлечимый",
    "transcription": "[ɪnˈkjʊ(ə)rəb(ə)l]"
  },
  {
    "engWord": "previous",
    "rusWord": "предыдущий",
    "transcription": "[ˈpriːvɪəs]"
  },
  {
    "engWord": "negative",
    "rusWord": "отрицательный",
    "transcription": "[ˈnegətɪv]"
  },
  {
    "engWord": "apart",
    "rusWord": "отдельно",
    "transcription": "[əˈpɑːt]"
  },
  {
    "engWord": "unable",
    "rusWord": "не в состоянии",
    "transcription": "[ʌnˈeɪb(ə)l]"
  },
  {
    "engWord": "defense",
    "rusWord": "защита",
    "transcription": "[dɪˈfens]"
  },
  {
    "engWord": "appear",
    "rusWord": "появиться",
    "transcription": "[əˈpɪə]"
  },
  {
    "engWord": "age",
    "rusWord": "возраст",
    "transcription": "[eɪʤ]"
  },
  {
    "engWord": "play",
    "rusWord": "играть",
    "transcription": "[pleɪ]"
  },
  {
    "engWord": "day",
    "rusWord": "день",
    "transcription": "[deɪ]"
  },
  {
    "engWord": "consultation",
    "rusWord": "консультация",
    "transcription": "[kɒnsəlˈteɪʃn]"
  },
  {
    "engWord": "fusion",
    "rusWord": "слияние",
    "transcription": "[ˈfjuːʒən]"
  },
  {
    "engWord": "income tax",
    "rusWord": "подоходный налог",
    "transcription": "[ˈɪnkʌm tæks]"
  },
  {
    "engWord": "vault",
    "rusWord": "свод",
    "transcription": "[vɔːlt]"
  },
  {
    "engWord": "grandson",
    "rusWord": "внук",
    "transcription": "[ˈgrænsʌn]"
  },
  {
    "engWord": "anniversary",
    "rusWord": "годовщина",
    "transcription": "[ænɪˈvɜːsərɪ]"
  },
  {
    "engWord": "intermediate",
    "rusWord": "промежуточный",
    "transcription": "[ɪntəˈmiːdɪət]"
  },
  {
    "engWord": "tears",
    "rusWord": "разрыдаться",
    "transcription": "[tɛəz]"
  },
  {
    "engWord": "boundary",
    "rusWord": "граница",
    "transcription": "[ˈbaʊndərɪ]"
  },
  {
    "engWord": "announcement",
    "rusWord": "объявление",
    "transcription": "[əˈnaʊnsmənt]"
  },
  {
    "engWord": "intersection",
    "rusWord": "пересечение",
    "transcription": "[ɪntəˈsekʃn]"
  },
  {
    "engWord": "worn",
    "rusWord": "изношенный",
    "transcription": "[wɔːn]"
  },
  {
    "engWord": "shrimp",
    "rusWord": "креветка",
    "transcription": "[ʃrɪmp]"
  },
  {
    "engWord": "forcing",
    "rusWord": "принуждение",
    "transcription": "[ˈfɔːsɪŋ]"
  },
  {
    "engWord": "selective",
    "rusWord": "отборный",
    "transcription": "[sɪˈlektɪv]"
  },
  {
    "engWord": "stunt",
    "rusWord": "трюк",
    "transcription": "[stʌnt]"
  },
  {
    "engWord": "backwards",
    "rusWord": "назад",
    "transcription": "[ˈbækwədz]"
  },
  {
    "engWord": "insist",
    "rusWord": "настаивать",
    "transcription": "[ɪnˈsɪst]"
  },
  {
    "engWord": "transfer",
    "rusWord": "перемещение",
    "transcription": "[ˈtrænsfɜː]"
  },
  {
    "engWord": "rapidly",
    "rusWord": "быстро",
    "transcription": "[ˈræpɪdlɪ]"
  },
  {
    "engWord": "accompany",
    "rusWord": "провожать",
    "transcription": "[əˈkʌmpənɪ]"
  },
  {
    "engWord": "none",
    "rusWord": "никто",
    "transcription": "[nʌn]"
  },
  {
    "engWord": "surgery",
    "rusWord": "хирургия",
    "transcription": "[ˈsɜːʤərɪ]"
  },
  {
    "engWord": "today",
    "rusWord": "сегодня",
    "transcription": "[təˈdeɪ]"
  },
  {
    "engWord": "parking",
    "rusWord": "парковка",
    "transcription": "[ˈpɑːkɪŋ]"
  },
  {
    "engWord": "tail",
    "rusWord": "хвост",
    "transcription": "[teɪl]"
  },
  {
    "engWord": "either",
    "rusWord": "любой",
    "transcription": "[ˈaɪðə]"
  },
  {
    "engWord": "crossing",
    "rusWord": "пересечение",
    "transcription": "[ˈkrɒsɪŋ]"
  },
  {
    "engWord": "lottery",
    "rusWord": "лотерея",
    "transcription": "[ˈlɒtərɪ]"
  },
  {
    "engWord": "combat",
    "rusWord": "бой",
    "transcription": "[ˈkɒmbæt]"
  },
  {
    "engWord": "barbecue",
    "rusWord": "барбекю",
    "transcription": "[ˈbɑːbɪkjuː]"
  },
  {
    "engWord": "accidentally",
    "rusWord": "случайно",
    "transcription": "[æksɪˈdentəlɪ]"
  },
  {
    "engWord": "disturb",
    "rusWord": "беспокоить",
    "transcription": "[dɪsˈtɜːb]"
  },
  {
    "engWord": "June",
    "rusWord": "июнь",
    "transcription": "[ʤuːn]"
  },
  {
    "engWord": "physics",
    "rusWord": "физика",
    "transcription": "[ˈfɪɪzɪks]"
  },
  {
    "engWord": "guitar",
    "rusWord": "гитара",
    "transcription": "[gɪˈtɑː]"
  },
  {
    "engWord": "consent",
    "rusWord": "согласие",
    "transcription": "[kənˈsent]"
  },
  {
    "engWord": "contract",
    "rusWord": "контракт",
    "transcription": "[ˈkɒntrækt]"
  },
  {
    "engWord": "into",
    "rusWord": "в",
    "transcription": "[ˈɪntuː]"
  },
  {
    "engWord": "particular",
    "rusWord": "определенный",
    "transcription": "[pəˈtɪkjʊlə]"
  },
  {
    "engWord": "remember",
    "rusWord": "помнить",
    "transcription": "[rɪˈmembə]"
  },
  {
    "engWord": "they",
    "rusWord": "они",
    "transcription": "[ðeɪ]"
  },
  {
    "engWord": "set",
    "rusWord": "набор",
    "transcription": "[set]"
  },
  {
    "engWord": "hear",
    "rusWord": "слышать",
    "transcription": "[hɪə]"
  },
  {
    "engWord": "she",
    "rusWord": "она",
    "transcription": "[ʃiː]"
  },
  {
    "engWord": "in",
    "rusWord": "в",
    "transcription": "[ɪn]"
  },
  {
    "engWord": "language",
    "rusWord": "язык",
    "transcription": "[ˈlæŋgwɪʤ]"
  },
  {
    "engWord": "seed",
    "rusWord": "семя",
    "transcription": "[siːd]"
  },
  {
    "engWord": "drill",
    "rusWord": "дрель",
    "transcription": "[drɪl]"
  },
  {
    "engWord": "outspoken",
    "rusWord": "откровенный",
    "transcription": "[aʊtˈspəʊkən]"
  },
  {
    "engWord": "sob",
    "rusWord": "всхлипывать",
    "transcription": "[sɒb]"
  },
  {
    "engWord": "confide",
    "rusWord": "доверять",
    "transcription": "[kənˈfaɪd]"
  },
  {
    "engWord": "internal",
    "rusWord": "внутренний",
    "transcription": "[ɪnˈtɜːnl]"
  },
  {
    "engWord": "promote",
    "rusWord": "способствовать",
    "transcription": "[prəˈməʊt]"
  },
  {
    "engWord": "upper",
    "rusWord": "верхний",
    "transcription": "[ˈʌpə]"
  },
  {
    "engWord": "secretary",
    "rusWord": "секретарь",
    "transcription": "[ˈsekrətrɪ]"
  },
  {
    "engWord": "bracket",
    "rusWord": "скобка",
    "transcription": "[ˈbrækɪt]"
  },
  {
    "engWord": "security",
    "rusWord": "безопасность",
    "transcription": "[sɪˈkjʊərɪtɪ]"
  },
  {
    "engWord": "queen",
    "rusWord": "королева",
    "transcription": "[kwiːn]"
  },
  {
    "engWord": "sentence",
    "rusWord": "предложение",
    "transcription": "[ˈsentəns]"
  },
  {
    "engWord": "history",
    "rusWord": "история",
    "transcription": "[ˈhɪstərɪ]"
  },
  {
    "engWord": "house",
    "rusWord": "дом",
    "transcription": "[haʊs]"
  },
  {
    "engWord": "seduce",
    "rusWord": "соблазнять",
    "transcription": "[sɪˈdjuːs]"
  },
  {
    "engWord": "stereo",
    "rusWord": "стерео",
    "transcription": "[ˈstɪərɪəʊ]"
  },
  {
    "engWord": "consequence",
    "rusWord": "последствие",
    "transcription": "[ˈkɒnsɪkwəns]"
  },
  {
    "engWord": "polish",
    "rusWord": "польский",
    "transcription": "[ˈpɒlɪʃ]"
  },
  {
    "engWord": "headquarter",
    "rusWord": "головной офис",
    "transcription": "[hedˈkwɔːtə]"
  },
  {
    "engWord": "unemployed",
    "rusWord": "безработный",
    "transcription": "[ʌnɪmˈplɔɪd]"
  },
  {
    "engWord": "pants",
    "rusWord": "штаны",
    "transcription": "[pænts]"
  },
  {
    "engWord": "issue",
    "rusWord": "вопрос",
    "transcription": "[ˈɪʃuː]"
  },
  {
    "engWord": "God",
    "rusWord": "Бог",
    "transcription": "[gɒd]"
  },
  {
    "engWord": "sir",
    "rusWord": "сэр",
    "transcription": "[sɜː]"
  },
  {
    "engWord": "some",
    "rusWord": "некоторые",
    "transcription": "[sʌm]"
  },
  {
    "engWord": "substance",
    "rusWord": "вещество",
    "transcription": "[ˈsʌbstəns]"
  },
  {
    "engWord": "farmer",
    "rusWord": "фермер",
    "transcription": "[ˈfɑːmə]"
  },
  {
    "engWord": "hurricane",
    "rusWord": "ураган",
    "transcription": "[ˈhʌrɪkən]"
  },
  {
    "engWord": "widen",
    "rusWord": "расширять",
    "transcription": "[waɪdn]"
  },
  {
    "engWord": "enforce",
    "rusWord": "принуждать",
    "transcription": "[ɪnˈfɔːs]"
  },
  {
    "engWord": "missile",
    "rusWord": "ракета",
    "transcription": "[ˈmɪsaɪl]"
  },
  {
    "engWord": "deduct",
    "rusWord": "вычитать",
    "transcription": "[dɪˈdʌkt]"
  },
  {
    "engWord": "rinse",
    "rusWord": "полоскать",
    "transcription": "[rɪns]"
  },
  {
    "engWord": "dealer",
    "rusWord": "дилер",
    "transcription": "[ˈdiːlə]"
  },
  {
    "engWord": "blush",
    "rusWord": "румянец",
    "transcription": "[blʌʃ]"
  },
  {
    "engWord": "skilful",
    "rusWord": "искусный",
    "transcription": "[ˈskɪlf(ə)l]"
  },
  {
    "engWord": "restrict",
    "rusWord": "ограничивать",
    "transcription": "[rɪsˈtrɪkt]"
  },
  {
    "engWord": "deck",
    "rusWord": "палуба",
    "transcription": "[dek]"
  },
  {
    "engWord": "perspective",
    "rusWord": "перспектива",
    "transcription": "[pəˈspektɪv]"
  },
  {
    "engWord": "sock",
    "rusWord": "носок",
    "transcription": "[sɒk]"
  },
  {
    "engWord": "sink",
    "rusWord": "раковина",
    "transcription": "[sɪŋk]"
  },
  {
    "engWord": "promotion",
    "rusWord": "продвижение",
    "transcription": "[prəˈməʊʃn]"
  },
  {
    "engWord": "rob",
    "rusWord": "грабить",
    "transcription": "[rɒb]"
  },
  {
    "engWord": "poll",
    "rusWord": "опрос",
    "transcription": "[pɒl]"
  },
  {
    "engWord": "angry",
    "rusWord": "сердитый",
    "transcription": "[ˈæŋgrɪ]"
  },
  {
    "engWord": "angle",
    "rusWord": "угол",
    "transcription": "[æŋgl]"
  },
  {
    "engWord": "package",
    "rusWord": "пакет",
    "transcription": "[ˈpækɪʤ]"
  },
  {
    "engWord": "meantime",
    "rusWord": "между тем",
    "transcription": "[ˈmiːntaɪm]"
  },
  {
    "engWord": "to",
    "rusWord": "к",
    "transcription": "[tuː]"
  },
  {
    "engWord": "state",
    "rusWord": "государство",
    "transcription": "[steɪt]"
  },
  {
    "engWord": "occur",
    "rusWord": "происходить",
    "transcription": "[əˈkɜː]"
  },
  {
    "engWord": "blade",
    "rusWord": "лезвие",
    "transcription": "[bleɪd]"
  },
  {
    "engWord": "located",
    "rusWord": "расположенный",
    "transcription": "[ləʊˈkeɪtɪd]"
  },
  {
    "engWord": "attend",
    "rusWord": "посещать",
    "transcription": "[əˈtend]"
  },
  {
    "engWord": "puzzle",
    "rusWord": "головоломка",
    "transcription": "[pʌzl]"
  },
  {
    "engWord": "cashier",
    "rusWord": "кассир",
    "transcription": "[kæˈʃɪə]"
  },
  {
    "engWord": "picnic",
    "rusWord": "пикник",
    "transcription": "[ˈpɪknɪk]"
  },
  {
    "engWord": "reasonably",
    "rusWord": "разумно",
    "transcription": "[ˈriːznəblɪ]"
  },
  {
    "engWord": "temper",
    "rusWord": "характер",
    "transcription": "[ˈtempə]"
  },
  {
    "engWord": "differently",
    "rusWord": "иначе",
    "transcription": "[ˈdɪfrəntlɪ]"
  },
  {
    "engWord": "resist",
    "rusWord": "сопротивляться",
    "transcription": "[rɪˈzɪst]"
  },
  {
    "engWord": "vital",
    "rusWord": "жизненно важный",
    "transcription": "[vaɪtl]"
  },
  {
    "engWord": "simply",
    "rusWord": "просто",
    "transcription": "[ˈsɪmplɪ]"
  },
  {
    "engWord": "whatsoever",
    "rusWord": "любой",
    "transcription": "[wɒtsəʊˈevə]"
  },
  {
    "engWord": "weird",
    "rusWord": "странный",
    "transcription": "[wɪəd]"
  },
  {
    "engWord": "million",
    "rusWord": "миллион",
    "transcription": "[ˈmɪljən]"
  },
  {
    "engWord": "thus",
    "rusWord": "таким образом",
    "transcription": "[]"
  },
  {
    "engWord": "tall",
    "rusWord": "высокий",
    "transcription": "[tɔːl]"
  },
  {
    "engWord": "tournament",
    "rusWord": "турнир",
    "transcription": "[ˈtʊənəmənt]"
  },
  {
    "engWord": "porch",
    "rusWord": "крыльцо",
    "transcription": "[pɔːʧ]"
  },
  {
    "engWord": "teenager",
    "rusWord": "подросток",
    "transcription": "[]"
  },
  {
    "engWord": "scenario",
    "rusWord": "сценарий",
    "transcription": "[sɪˈnɑːrɪəʊ]"
  },
  {
    "engWord": "jazz",
    "rusWord": "джаз",
    "transcription": "[ʤæz]"
  },
  {
    "engWord": "memorial",
    "rusWord": "мемориал",
    "transcription": "[mɪˈmɔːrɪəl]"
  },
  {
    "engWord": "characterize",
    "rusWord": "характеризовать",
    "transcription": "[ˈkærɪktəraɪz]"
  },
  {
    "engWord": "spirits",
    "rusWord": "алкогольные напитки",
    "transcription": "[ˈspɪrɪts]"
  },
  {
    "engWord": "parade",
    "rusWord": "парад",
    "transcription": "[pəˈreɪd]"
  },
  {
    "engWord": "mammal",
    "rusWord": "млекопитающее",
    "transcription": "[ˈmæməl]"
  },
  {
    "engWord": "chemist",
    "rusWord": "химик",
    "transcription": "[ˈkemɪst]"
  },
  {
    "engWord": "invitation",
    "rusWord": "приглашение",
    "transcription": "[ɪnvɪˈteɪʃn]"
  },
  {
    "engWord": "mobile phone",
    "rusWord": "мобильный телефон",
    "transcription": "[ˈməʊbaɪl fəʊn]"
  },
  {
    "engWord": "unknown",
    "rusWord": "неизвестный",
    "transcription": "[ʌnˈnəʊn]"
  },
  {
    "engWord": "poetry",
    "rusWord": "поэзия",
    "transcription": "[ˈpəʊɪtrɪ]"
  },
  {
    "engWord": "bay",
    "rusWord": "залив",
    "transcription": "[beɪ]"
  },
  {
    "engWord": "goodbye",
    "rusWord": "до свидания",
    "transcription": "[gʊdˈbaɪ]"
  },
  {
    "engWord": "tight",
    "rusWord": "плотно",
    "transcription": "[taɪt]"
  },
  {
    "engWord": "Chinese",
    "rusWord": "китайский",
    "transcription": "[ʧaɪˈniːz]"
  },
  {
    "engWord": "lover",
    "rusWord": "любовник",
    "transcription": "[ˈlʌvə]"
  },
  {
    "engWord": "action",
    "rusWord": "действие",
    "transcription": "[ækʃn]"
  },
  {
    "engWord": "bed",
    "rusWord": "кровать",
    "transcription": "[bed]"
  },
  {
    "engWord": "third",
    "rusWord": "третий",
    "transcription": "[θɜːd]"
  },
  {
    "engWord": "thousand",
    "rusWord": "тысяча",
    "transcription": "[ˈθaʊzənd]"
  },
  {
    "engWord": "given",
    "rusWord": "подаренный",
    "transcription": "[gɪvn]"
  },
  {
    "engWord": "brass",
    "rusWord": "латунь",
    "transcription": "[brɑːs]"
  },
  {
    "engWord": "environment",
    "rusWord": "окружающая среда",
    "transcription": "[ɪnˈvaɪərənmənt]"
  },
  {
    "engWord": "companion",
    "rusWord": "сопровождать",
    "transcription": "[kəmˈpænɪən]"
  },
  {
    "engWord": "bench",
    "rusWord": "скамейка",
    "transcription": "[benʧ]"
  },
  {
    "engWord": "pastime",
    "rusWord": "времяпровождение",
    "transcription": "[ˈpɑːstaɪm]"
  },
  {
    "engWord": "e-book",
    "rusWord": "электронная книга",
    "transcription": "[iː-bʊk]"
  },
  {
    "engWord": "grave",
    "rusWord": "могила",
    "transcription": "[greɪv]"
  },
  {
    "engWord": "temporary",
    "rusWord": "временный",
    "transcription": "[ˈtempərərɪ]"
  },
  {
    "engWord": "series",
    "rusWord": "серия",
    "transcription": "[ˈsɪəriːz]"
  },
  {
    "engWord": "bug",
    "rusWord": "жук",
    "transcription": "[buːg]"
  },
  {
    "engWord": "warn",
    "rusWord": "предупреждать",
    "transcription": "[wɔːn]"
  },
  {
    "engWord": "soap",
    "rusWord": "мыло",
    "transcription": "[səʊp]"
  },
  {
    "engWord": "curious",
    "rusWord": "любопытный",
    "transcription": "[ˈkjʊərɪəs]"
  },
  {
    "engWord": "comfortable",
    "rusWord": "удобный",
    "transcription": "[ˈkʌmf(ə)təb(ə)l]"
  },
  {
    "engWord": "criminal",
    "rusWord": "преступник",
    "transcription": "[ˈkrɪmɪnl]"
  },
  {
    "engWord": "citizen",
    "rusWord": "житель",
    "transcription": "[ˈsɪtɪzn]"
  },
  {
    "engWord": "pad",
    "rusWord": "подушечка",
    "transcription": "[pæd]"
  },
  {
    "engWord": "return",
    "rusWord": "вернуть",
    "transcription": "[rɪˈtɜːn]"
  },
  {
    "engWord": "problem",
    "rusWord": "проблема",
    "transcription": "[ˈprɒbləm]"
  },
  {
    "engWord": "block",
    "rusWord": "блок",
    "transcription": "[blɒk]"
  },
  {
    "engWord": "air",
    "rusWord": "воздух",
    "transcription": "[eə]"
  },
  {
    "engWord": "dad",
    "rusWord": "папа",
    "transcription": "[dæd]"
  },
  {
    "engWord": "produce",
    "rusWord": "производить",
    "transcription": "[ˈprɒdjuːs]"
  },
  {
    "engWord": "psychic",
    "rusWord": "психический",
    "transcription": "[ˈsaɪkɪk]"
  },
  {
    "engWord": "jar",
    "rusWord": "кувшин",
    "transcription": "[ʤɑː]"
  },
  {
    "engWord": "skyscraper",
    "rusWord": "небоскреб",
    "transcription": "[ˈskaɪskreɪpə]"
  },
  {
    "engWord": "maximum",
    "rusWord": "максимальный",
    "transcription": "[ˈmæksɪməm]"
  },
  {
    "engWord": "excitement",
    "rusWord": "волнение",
    "transcription": "[ɪkˈsaɪtmənt]"
  },
  {
    "engWord": "exceed",
    "rusWord": "превышать",
    "transcription": "[ɪkˈsiːd]"
  },
  {
    "engWord": "scam",
    "rusWord": "афера",
    "transcription": "[skæm]"
  },
  {
    "engWord": "clumsy",
    "rusWord": "неуклюжий",
    "transcription": "[ˈklʌmzɪ]"
  },
  {
    "engWord": "profession",
    "rusWord": "профессия",
    "transcription": "[prəˈfeʃn]"
  },
  {
    "engWord": "competition",
    "rusWord": "соревнование",
    "transcription": "[kɒmpɪˈtɪʃn]"
  },
  {
    "engWord": "model",
    "rusWord": "модель",
    "transcription": "[mɒdl]"
  },
  {
    "engWord": "slim",
    "rusWord": "тонкий",
    "transcription": "[slɪm]"
  },
  {
    "engWord": "translate",
    "rusWord": "переводить",
    "transcription": "[trænsˈleɪt]"
  },
  {
    "engWord": "grace",
    "rusWord": "изящество",
    "transcription": "[greɪs]"
  },
  {
    "engWord": "relationship",
    "rusWord": "отношение",
    "transcription": "[rɪˈleɪʃnʃɪp]"
  },
  {
    "engWord": "seven",
    "rusWord": "семь",
    "transcription": "[sevn]"
  },
  {
    "engWord": "as",
    "rusWord": "как",
    "transcription": "[æz]"
  },
  {
    "engWord": "numeral",
    "rusWord": "цифровой",
    "transcription": "[ˈnjuːmərəl]"
  },
  {
    "engWord": "strange",
    "rusWord": "странный",
    "transcription": "[streɪnʤ]"
  },
  {
    "engWord": "incite",
    "rusWord": "подстрекать",
    "transcription": "[ɪnˈsaɪt]"
  },
  {
    "engWord": "medium",
    "rusWord": "средний",
    "transcription": "[ˈmiːdɪəm]"
  },
  {
    "engWord": "herald",
    "rusWord": "вестник",
    "transcription": "[ˈherəld]"
  },
  {
    "engWord": "installation",
    "rusWord": "установка",
    "transcription": "[ɪnstəˈleɪʃn]"
  },
  {
    "engWord": "dancer",
    "rusWord": "танцор",
    "transcription": "[ˈdɑːnsə]"
  },
  {
    "engWord": "airplane",
    "rusWord": "воздушное судно",
    "transcription": "[ˈeəpleɪn]"
  },
  {
    "engWord": "dispel",
    "rusWord": "развеять",
    "transcription": "[dɪsˈpel]"
  },
  {
    "engWord": "hopefully",
    "rusWord": "с надеждой",
    "transcription": "[ˈhəʊpfəlɪ]"
  },
  {
    "engWord": "spoil",
    "rusWord": "портить",
    "transcription": "[spɔɪl]"
  },
  {
    "engWord": "quickly",
    "rusWord": "быстро",
    "transcription": "[ˈkwɪklɪ]"
  },
  {
    "engWord": "friendship",
    "rusWord": "дружба",
    "transcription": "[ˈfrendʃɪp]"
  },
  {
    "engWord": "destroy",
    "rusWord": "уничтожать",
    "transcription": "[dɪsˈtrɔɪ]"
  },
  {
    "engWord": "steam",
    "rusWord": "пар",
    "transcription": "[stiːm]"
  },
  {
    "engWord": "cost",
    "rusWord": "стоимость",
    "transcription": "[kɒst]"
  },
  {
    "engWord": "spot",
    "rusWord": "пятно",
    "transcription": "[spɒt]"
  },
  {
    "engWord": "serve",
    "rusWord": "обслуживать",
    "transcription": "[sɜːv]"
  },
  {
    "engWord": "underestimate",
    "rusWord": "недооценивать",
    "transcription": "[]"
  },
  {
    "engWord": "tuna",
    "rusWord": "тунец",
    "transcription": "[ˈtjuːnə]"
  },
  {
    "engWord": "grammar",
    "rusWord": "грамматика",
    "transcription": "[ˈgræmə]"
  },
  {
    "engWord": "religion",
    "rusWord": "религия",
    "transcription": "[rɪˈlɪʤən]"
  },
  {
    "engWord": "pronounce",
    "rusWord": "произносить",
    "transcription": "[prəˈnaʊns]"
  },
  {
    "engWord": "deposit",
    "rusWord": "депозит",
    "transcription": "[dɪˈpɒzɪt]"
  },
  {
    "engWord": "sole",
    "rusWord": "подошва",
    "transcription": "[səʊl]"
  },
  {
    "engWord": "entirely",
    "rusWord": "полностью",
    "transcription": "[ɪnˈtaɪəlɪ]"
  },
  {
    "engWord": "leave",
    "rusWord": "покидать",
    "transcription": "[liːv]"
  },
  {
    "engWord": "include",
    "rusWord": "включать",
    "transcription": "[ɪnˈkluːd]"
  },
  {
    "engWord": "stay",
    "rusWord": "остаться",
    "transcription": "[steɪ]"
  },
  {
    "engWord": "her",
    "rusWord": "ее",
    "transcription": "[hɜː]"
  },
  {
    "engWord": "amazing",
    "rusWord": "удивительный",
    "transcription": "[əˈmeɪzɪŋ]"
  },
  {
    "engWord": "illustration",
    "rusWord": "иллюстрация",
    "transcription": "[ɪləsˈtreɪʃn]"
  },
  {
    "engWord": "essential",
    "rusWord": "существенный",
    "transcription": "[ɪˈsenʃəl]"
  },
  {
    "engWord": "sketch",
    "rusWord": "набросок",
    "transcription": "[skeʧ]"
  },
  {
    "engWord": "hereditary",
    "rusWord": "наследственный",
    "transcription": "[hɪˈredɪtərɪ]"
  },
  {
    "engWord": "malice",
    "rusWord": "злоба",
    "transcription": "[ˈmælɪs]"
  },
  {
    "engWord": "Greek",
    "rusWord": "греческий",
    "transcription": "[griːk]"
  },
  {
    "engWord": "anxiety",
    "rusWord": "тревожность",
    "transcription": "[æŋˈzaɪətɪ]"
  },
  {
    "engWord": "discipline",
    "rusWord": "дисциплина",
    "transcription": "[ˈdɪsɪplɪn]"
  },
  {
    "engWord": "pill",
    "rusWord": "таблетка",
    "transcription": "[pɪl]"
  },
  {
    "engWord": "luckily",
    "rusWord": "к счастью",
    "transcription": "[ˈlʌkɪlɪ]"
  },
  {
    "engWord": "surprising",
    "rusWord": "удивительный",
    "transcription": "[səˈpraɪzɪŋ]"
  },
  {
    "engWord": "tent",
    "rusWord": "палатка",
    "transcription": "[tent]"
  },
  {
    "engWord": "celebration",
    "rusWord": "празднование",
    "transcription": "[selɪˈbreɪʃn]"
  },
  {
    "engWord": "September",
    "rusWord": "сентябрь",
    "transcription": "[sepˈtembə]"
  },
  {
    "engWord": "rotten",
    "rusWord": "гнилой",
    "transcription": "[rɒtn]"
  },
  {
    "engWord": "neither",
    "rusWord": "ни",
    "transcription": "[ˈnaɪðə]"
  },
  {
    "engWord": "happiness",
    "rusWord": "счастье",
    "transcription": "[ˈhæpɪnɪs]"
  },
  {
    "engWord": "shower",
    "rusWord": "душ",
    "transcription": "[ˈʃaʊə]"
  },
  {
    "engWord": "love",
    "rusWord": "любовь",
    "transcription": "[lʌv]"
  },
  {
    "engWord": "old",
    "rusWord": "старый",
    "transcription": "[əʊld]"
  },
  {
    "engWord": "time",
    "rusWord": "время",
    "transcription": "[taɪm]"
  },
  {
    "engWord": "yellow",
    "rusWord": "желтый",
    "transcription": "[ˈjeləʊ]"
  },
  {
    "engWord": "bulldog",
    "rusWord": "бульдог",
    "transcription": "[ˈbʊldɒg]"
  },
  {
    "engWord": "predatory",
    "rusWord": "хищный",
    "transcription": "[ˈpredətərɪ]"
  },
  {
    "engWord": "hawk",
    "rusWord": "ястреб",
    "transcription": "[hɔːk]"
  },
  {
    "engWord": "fax",
    "rusWord": "факс",
    "transcription": "[fæks]"
  },
  {
    "engWord": "overtime",
    "rusWord": "овертайм",
    "transcription": "[ˈəʊvətaɪm]"
  },
  {
    "engWord": "backup",
    "rusWord": "резервное копирование",
    "transcription": "[ˈbækʌp]"
  },
  {
    "engWord": "laundry",
    "rusWord": "прачечная",
    "transcription": "[ˈlɔːndrɪ]"
  },
  {
    "engWord": "prisoner",
    "rusWord": "заключенный",
    "transcription": "[ˈprɪznə]"
  },
  {
    "engWord": "pardon",
    "rusWord": "прощение",
    "transcription": "[pɑːdn]"
  },
  {
    "engWord": "palace",
    "rusWord": "дворец",
    "transcription": "[ˈpælɪs]"
  },
  {
    "engWord": "injury",
    "rusWord": "травма",
    "transcription": "[ˈɪnʤərɪ]"
  },
  {
    "engWord": "something",
    "rusWord": "что-то",
    "transcription": "[ˈsʌmθɪŋ]"
  },
  {
    "engWord": "grateful",
    "rusWord": "благодарный",
    "transcription": "[ˈgreɪtf(ə)l]"
  },
  {
    "engWord": "enjoy",
    "rusWord": "наслаждаться",
    "transcription": "[ɪnˈʤɔɪ]"
  },
  {
    "engWord": "plug",
    "rusWord": "штекер",
    "transcription": "[plʌg]"
  },
  {
    "engWord": "deep",
    "rusWord": "глубокий",
    "transcription": "[diːp]"
  },
  {
    "engWord": "duck",
    "rusWord": "утка",
    "transcription": "[dʌk]"
  },
  {
    "engWord": "bring",
    "rusWord": "приносить",
    "transcription": "[brɪŋ]"
  },
  {
    "engWord": "offer",
    "rusWord": "предложение",
    "transcription": "[ˈɒfə]"
  },
  {
    "engWord": "bone",
    "rusWord": "кость",
    "transcription": "[bəʊn]"
  },
  {
    "engWord": "bear",
    "rusWord": "медведь",
    "transcription": "[beə]"
  },
  {
    "engWord": "interval",
    "rusWord": "интервал",
    "transcription": "[ˈɪntəvəl]"
  },
  {
    "engWord": "moisture",
    "rusWord": "влажность",
    "transcription": "[ˈmɔɪsʧə]"
  },
  {
    "engWord": "scissors",
    "rusWord": "ножницы",
    "transcription": "[ˈsɪzəz]"
  },
  {
    "engWord": "harmful",
    "rusWord": "вредный",
    "transcription": "[ˈhɑːmf(ə)l]"
  },
  {
    "engWord": "constant",
    "rusWord": "постоянный",
    "transcription": "[ˈkɒnstənt]"
  },
  {
    "engWord": "faraway",
    "rusWord": "отсутствующий",
    "transcription": "[ˈfɑːrəweɪ]"
  },
  {
    "engWord": "destructive",
    "rusWord": "разрушительный",
    "transcription": "[dɪsˈtrʌktɪv]"
  },
  {
    "engWord": "righteous",
    "rusWord": "праведный",
    "transcription": "[ˈraɪʧəs]"
  },
  {
    "engWord": "quid",
    "rusWord": "услуга за услугу",
    "transcription": "[kwɪd]"
  },
  {
    "engWord": "kneel",
    "rusWord": "вставать на колени",
    "transcription": "[niːl]"
  },
  {
    "engWord": "imaginary",
    "rusWord": "воображаемый",
    "transcription": "[ɪˈmæʤ(ə)n(ə)rɪ]"
  },
  {
    "engWord": "cruel",
    "rusWord": "жестокий",
    "transcription": "[ˈkruːəl]"
  },
  {
    "engWord": "shy",
    "rusWord": "застенчивый",
    "transcription": "[ʃaɪ]"
  },
  {
    "engWord": "cookie",
    "rusWord": "печенье",
    "transcription": "[ˈkʊkɪ]"
  },
  {
    "engWord": "effective",
    "rusWord": "эффективный",
    "transcription": "[ɪˈfektɪv]"
  },
  {
    "engWord": "borrow",
    "rusWord": "одолжить",
    "transcription": "[ˈbɒrəʊ]"
  },
  {
    "engWord": "luck",
    "rusWord": "удача",
    "transcription": "[lʌk]"
  },
  {
    "engWord": "economic",
    "rusWord": "экономический",
    "transcription": "[ekəˈnɒmɪk]"
  },
  {
    "engWord": "devil",
    "rusWord": "дьявол",
    "transcription": "[devl]"
  },
  {
    "engWord": "further",
    "rusWord": "дальнейший",
    "transcription": "[ˈfɜːðə]"
  },
  {
    "engWord": "large",
    "rusWord": "большой",
    "transcription": "[lɑːʤ]"
  },
  {
    "engWord": "long",
    "rusWord": "длинный",
    "transcription": "[lɒŋ]"
  },
  {
    "engWord": "capital",
    "rusWord": "капитал",
    "transcription": "[ˈkæpɪtl]"
  },
  {
    "engWord": "ridicule",
    "rusWord": "высмеивать",
    "transcription": "[ˈrɪdɪkjuːl]"
  },
  {
    "engWord": "lawn",
    "rusWord": "лужайка",
    "transcription": "[lɔːn]"
  },
  {
    "engWord": "courthouse",
    "rusWord": "здание суда",
    "transcription": "[ˈkɔːthaʊs]"
  },
  {
    "engWord": "shade",
    "rusWord": "оттенок",
    "transcription": "[ʃeɪd]"
  },
  {
    "engWord": "knight",
    "rusWord": "рыцарь",
    "transcription": "[naɪt]"
  },
  {
    "engWord": "divine",
    "rusWord": "святой",
    "transcription": "[dɪˈvaɪn]"
  },
  {
    "engWord": "relieve",
    "rusWord": "освобождать",
    "transcription": "[rɪˈliːv]"
  },
  {
    "engWord": "permit",
    "rusWord": "разрешать",
    "transcription": "[ˈpɜːmɪt]"
  },
  {
    "engWord": "policy",
    "rusWord": "политика",
    "transcription": "[ˈpɒlɪsɪ]"
  },
  {
    "engWord": "November",
    "rusWord": "ноябрь",
    "transcription": "[nəʊˈvembə]"
  },
  {
    "engWord": "social",
    "rusWord": "общественный",
    "transcription": "[ˈsəʊʃəl]"
  },
  {
    "engWord": "trick",
    "rusWord": "уловка",
    "transcription": "[trɪk]"
  },
  {
    "engWord": "yours",
    "rusWord": "твой",
    "transcription": "[jɔːz]"
  },
  {
    "engWord": "threat",
    "rusWord": "угроза",
    "transcription": "[θret]"
  },
  {
    "engWord": "vampire",
    "rusWord": "вампир",
    "transcription": "[ˈvæmpaɪə]"
  },
  {
    "engWord": "doll",
    "rusWord": "кукла",
    "transcription": "[dɒl]"
  },
  {
    "engWord": "web",
    "rusWord": "сеть",
    "transcription": "[web]"
  },
  {
    "engWord": "sound",
    "rusWord": "звук",
    "transcription": "[saʊnd]"
  },
  {
    "engWord": "character",
    "rusWord": "характер",
    "transcription": "[ˈkærɪktə]"
  },
  {
    "engWord": "wheat",
    "rusWord": "пшеница",
    "transcription": "[wiːt]"
  },
  {
    "engWord": "struck",
    "rusWord": "пораженный",
    "transcription": "[strʌk]"
  },
  {
    "engWord": "onto",
    "rusWord": "на",
    "transcription": "[ˈɔntʊ]"
  },
  {
    "engWord": "alliance",
    "rusWord": "альянс",
    "transcription": "[əˈlaɪəns]"
  },
  {
    "engWord": "independence",
    "rusWord": "независимость",
    "transcription": "[ɪndɪˈpendəns]"
  },
  {
    "engWord": "click",
    "rusWord": "щелчок",
    "transcription": "[klɪk]"
  },
  {
    "engWord": "flavour",
    "rusWord": "аромат",
    "transcription": "[ˈfleɪvə]"
  },
  {
    "engWord": "wise",
    "rusWord": "мудрый",
    "transcription": "[waɪz]"
  },
  {
    "engWord": "twelve",
    "rusWord": "двенадцать",
    "transcription": "[twelv]"
  },
  {
    "engWord": "launch",
    "rusWord": "запуск",
    "transcription": "[lɔːnʧ]"
  },
  {
    "engWord": "monster",
    "rusWord": "чудовище",
    "transcription": "[ˈmɒnstə]"
  },
  {
    "engWord": "addition",
    "rusWord": "дополнение",
    "transcription": "[əˈdɪʃn]"
  },
  {
    "engWord": "acquire",
    "rusWord": "приобретать",
    "transcription": "[əˈkwaɪə]"
  },
  {
    "engWord": "cloud",
    "rusWord": "облако",
    "transcription": "[klaʊd]"
  },
  {
    "engWord": "music",
    "rusWord": "музыка",
    "transcription": "[ˈmjuːzɪk]"
  },
  {
    "engWord": "owing",
    "rusWord": "обязанный",
    "transcription": "[ˈəʊɪŋ]"
  },
  {
    "engWord": "laptop",
    "rusWord": "ноутбук",
    "transcription": "[ˈlæptɒp]"
  },
  {
    "engWord": "devastated",
    "rusWord": "опустошенный",
    "transcription": "[ˈdevəsteɪtɪd]"
  },
  {
    "engWord": "thesaurus",
    "rusWord": "тезаурус",
    "transcription": "[θɪˈsɔːrəs]"
  },
  {
    "engWord": "aim",
    "rusWord": "цель",
    "transcription": "[eɪm]"
  },
  {
    "engWord": "majority",
    "rusWord": "большинство",
    "transcription": "[məˈʤɒrɪtɪ]"
  },
  {
    "engWord": "hyphen",
    "rusWord": "дефис",
    "transcription": "[ˈhaɪfən]"
  },
  {
    "engWord": "smack",
    "rusWord": "привкус",
    "transcription": "[smæk]"
  },
  {
    "engWord": "youngster",
    "rusWord": "юноша",
    "transcription": "[ˈjʌŋstə]"
  },
  {
    "engWord": "tend",
    "rusWord": "иметь тенденцию",
    "transcription": "[tend]"
  },
  {
    "engWord": "departure",
    "rusWord": "отправление",
    "transcription": "[dɪˈpɑːʧə]"
  },
  {
    "engWord": "normally",
    "rusWord": "обычно",
    "transcription": "[ˈnɔːməlɪ]"
  },
  {
    "engWord": "request",
    "rusWord": "запрос",
    "transcription": "[rɪˈkwest]"
  },
  {
    "engWord": "pain",
    "rusWord": "боль",
    "transcription": "[peɪn]"
  },
  {
    "engWord": "chocolate",
    "rusWord": "шоколад",
    "transcription": "[ˈʧɒklɪt]"
  },
  {
    "engWord": "nearby",
    "rusWord": "поблизости",
    "transcription": "[ˈnɪəbaɪ]"
  },
  {
    "engWord": "laugh",
    "rusWord": "смеяться",
    "transcription": "[lɑːf]"
  },
  {
    "engWord": "shoe",
    "rusWord": "башмак",
    "transcription": "[ʃuː]"
  },
  {
    "engWord": "port",
    "rusWord": "порт",
    "transcription": "[pɔːt]"
  },
  {
    "engWord": "fare",
    "rusWord": "плата за проезд",
    "transcription": "[feə]"
  },
  {
    "engWord": "humidity",
    "rusWord": "влажность",
    "transcription": "[hjuːˈmɪdɪtɪ]"
  },
  {
    "engWord": "recycle",
    "rusWord": "перерабатывать",
    "transcription": "[riːˈsaɪkl]"
  },
  {
    "engWord": "shield",
    "rusWord": "щит",
    "transcription": "[ʃiːld]"
  },
  {
    "engWord": "balloon",
    "rusWord": "воздушный шар",
    "transcription": "[bəˈluːn]"
  },
  {
    "engWord": "graceful",
    "rusWord": "изящный",
    "transcription": "[ˈgreɪsf(ə)l]"
  },
  {
    "engWord": "vicious",
    "rusWord": "порочный",
    "transcription": "[ˈvɪʃəs]"
  },
  {
    "engWord": "gambling",
    "rusWord": "азартная игра",
    "transcription": "[ˈgæmblɪŋ]"
  },
  {
    "engWord": "phony",
    "rusWord": "фальшивый",
    "transcription": "[ˈfəʊnɪ]"
  },
  {
    "engWord": "presence",
    "rusWord": "присутствие",
    "transcription": "[prezns]"
  },
  {
    "engWord": "routine",
    "rusWord": "рутина",
    "transcription": "[ruːˈtiːn]"
  },
  {
    "engWord": "crawl",
    "rusWord": "ползти",
    "transcription": "[krɔːl]"
  },
  {
    "engWord": "persuade",
    "rusWord": "убеждать",
    "transcription": "[pəˈsweɪd]"
  },
  {
    "engWord": "salary",
    "rusWord": "зарплата",
    "transcription": "[ˈsælərɪ]"
  },
  {
    "engWord": "soul",
    "rusWord": "душа",
    "transcription": "[səʊl]"
  },
  {
    "engWord": "extremely",
    "rusWord": "чрезвычайно",
    "transcription": "[ɪksˈtriːmlɪ]"
  },
  {
    "engWord": "general",
    "rusWord": "генеральный",
    "transcription": "[ˈʤenərəl]"
  },
  {
    "engWord": "for",
    "rusWord": "для",
    "transcription": "[fɔː]"
  },
  {
    "engWord": "object",
    "rusWord": "объект",
    "transcription": "[ˈɒbʤɛkt]"
  },
  {
    "engWord": "sister",
    "rusWord": "сестра",
    "transcription": "[ˈsɪstə]"
  },
  {
    "engWord": "party",
    "rusWord": "вечеринка",
    "transcription": "[ˈpɑːtɪ]"
  },
  {
    "engWord": "visitation",
    "rusWord": "посещение",
    "transcription": "[vɪzɪˈteɪʃn]"
  },
  {
    "engWord": "dedicated",
    "rusWord": "посвященный",
    "transcription": "[ˈdedɪkeɪtɪd]"
  },
  {
    "engWord": "burglar",
    "rusWord": "взломщик",
    "transcription": "[ˈbɜːglə]"
  },
  {
    "engWord": "existence",
    "rusWord": "существование",
    "transcription": "[ɪgˈzɪstəns]"
  },
  {
    "engWord": "owner",
    "rusWord": "владелец",
    "transcription": "[ˈəʊnə]"
  },
  {
    "engWord": "county",
    "rusWord": "округ",
    "transcription": "[ˈkaʊntɪ]"
  },
  {
    "engWord": "council",
    "rusWord": "Совет",
    "transcription": "[kaʊnsl]"
  },
  {
    "engWord": "strict",
    "rusWord": "строгий",
    "transcription": "[strɪkt]"
  },
  {
    "engWord": "hire",
    "rusWord": "нанимать",
    "transcription": "[ˈhaɪə]"
  },
  {
    "engWord": "cast",
    "rusWord": "бросать",
    "transcription": "[kɑːst]"
  },
  {
    "engWord": "shirt",
    "rusWord": "рубашка",
    "transcription": "[ʃɜːt]"
  },
  {
    "engWord": "maybe",
    "rusWord": "возможно",
    "transcription": "[ˈmeɪbɪ]"
  },
  {
    "engWord": "tomorrow",
    "rusWord": "завтра",
    "transcription": "[təˈmɒrəʊ]"
  },
  {
    "engWord": "sleep",
    "rusWord": "сон",
    "transcription": "[sliːp]"
  },
  {
    "engWord": "just",
    "rusWord": "просто",
    "transcription": "[ʤʌst]"
  },
  {
    "engWord": "means",
    "rusWord": "средства",
    "transcription": "[miːnz]"
  },
  {
    "engWord": "approval",
    "rusWord": "утверждение",
    "transcription": "[əˈpruːvəl]"
  },
  {
    "engWord": "oldest",
    "rusWord": "старейший",
    "transcription": "[]"
  },
  {
    "engWord": "dining-room",
    "rusWord": "столовая",
    "transcription": "[ˈdaɪnɪŋ rum]"
  },
  {
    "engWord": "intelligent",
    "rusWord": "умный",
    "transcription": "[ɪnˈtelɪʤənt]"
  },
  {
    "engWord": "February",
    "rusWord": "февраль",
    "transcription": "[ˈfebrʊərɪ]"
  },
  {
    "engWord": "sweetheart",
    "rusWord": "возлюбленная",
    "transcription": "[ˈswiːthɑːt]"
  },
  {
    "engWord": "pledge",
    "rusWord": "залог",
    "transcription": "[pleʤ]"
  },
  {
    "engWord": "nuclear",
    "rusWord": "ядерный",
    "transcription": "[ˈnjuːklɪə]"
  },
  {
    "engWord": "junk food",
    "rusWord": "вредная пища",
    "transcription": "[ʤʌŋk fuːd]"
  },
  {
    "engWord": "towards",
    "rusWord": "к",
    "transcription": "[təˈwɔːdz]"
  },
  {
    "engWord": "project",
    "rusWord": "проект",
    "transcription": "[ˈprɒʤəkt]"
  },
  {
    "engWord": "memory",
    "rusWord": "память",
    "transcription": "[ˈmemərɪ]"
  },
  {
    "engWord": "earth",
    "rusWord": "земля",
    "transcription": "[ɜːθ]"
  },
  {
    "engWord": "winter",
    "rusWord": "зима",
    "transcription": "[ˈwɪntə]"
  },
  {
    "engWord": "rod",
    "rusWord": "стержень",
    "transcription": "[rɒd]"
  },
  {
    "engWord": "folk",
    "rusWord": "народ",
    "transcription": "[fəʊk]"
  },
  {
    "engWord": "impolite",
    "rusWord": "невежливый",
    "transcription": "[ɪmpəˈlaɪt]"
  },
  {
    "engWord": "lightning",
    "rusWord": "молния",
    "transcription": "[ˈlaɪtnɪŋ]"
  },
  {
    "engWord": "carver",
    "rusWord": "резчик",
    "transcription": "[ˈkɑːvə]"
  },
  {
    "engWord": "consideration",
    "rusWord": "рассмотрение",
    "transcription": "[kənsɪdəˈreɪʃn]"
  },
  {
    "engWord": "stepfather",
    "rusWord": "отчим",
    "transcription": "[ˈstepfɑːðə]"
  },
  {
    "engWord": "terrorist",
    "rusWord": "террорист",
    "transcription": "[ˈterərɪst]"
  },
  {
    "engWord": "filthy",
    "rusWord": "мерзкий",
    "transcription": "[ˈfɪlθɪ]"
  },
  {
    "engWord": "trunk",
    "rusWord": "багажник",
    "transcription": "[trʌŋk]"
  },
  {
    "engWord": "factor",
    "rusWord": "фактор",
    "transcription": "[ˈfæktə]"
  },
  {
    "engWord": "punishment",
    "rusWord": "наказание",
    "transcription": "[ˈpʌnɪʃmənt]"
  },
  {
    "engWord": "relax",
    "rusWord": "расслабиться",
    "transcription": "[rɪˈlæks]"
  },
  {
    "engWord": "holiday",
    "rusWord": "праздник",
    "transcription": "[ˈhɒlɪdɪ]"
  },
  {
    "engWord": "miracle",
    "rusWord": "чудо",
    "transcription": "[ˈmɪrəkl]"
  },
  {
    "engWord": "whatever",
    "rusWord": "что угодно",
    "transcription": "[wɒtˈevə]"
  },
  {
    "engWord": "battle",
    "rusWord": "битва",
    "transcription": "[bætl]"
  },
  {
    "engWord": "mean",
    "rusWord": "означать",
    "transcription": "[miːn]"
  },
  {
    "engWord": "temptation",
    "rusWord": "искушение",
    "transcription": "[tempˈteɪʃn]"
  },
  {
    "engWord": "ivory",
    "rusWord": "слоновая кость",
    "transcription": "[ˈaɪvərɪ]"
  },
  {
    "engWord": "understandable",
    "rusWord": "понятный",
    "transcription": "[ʌndəˈstændəbl]"
  },
  {
    "engWord": "format",
    "rusWord": "формат",
    "transcription": "[ˈfɔːmæt]"
  },
  {
    "engWord": "wisdom",
    "rusWord": "мудрость",
    "transcription": "[ˈwɪzdəm]"
  },
  {
    "engWord": "drawing",
    "rusWord": "рисунок",
    "transcription": "[ˈdrɔːɪŋ]"
  },
  {
    "engWord": "humorous",
    "rusWord": "юмористический",
    "transcription": "[ˈhjuːmərəs]"
  },
  {
    "engWord": "elect",
    "rusWord": "избирать",
    "transcription": "[ɪˈlekt]"
  },
  {
    "engWord": "laziness",
    "rusWord": "лень",
    "transcription": "[ˈleɪzɪnɪs]"
  },
  {
    "engWord": "legend",
    "rusWord": "легенда",
    "transcription": "[ˈleʤənd]"
  },
  {
    "engWord": "revenue",
    "rusWord": "доход",
    "transcription": "[ˈrevɪnjuː]"
  },
  {
    "engWord": "software",
    "rusWord": "программное обеспечение",
    "transcription": "[ˈsɒftweə]"
  },
  {
    "engWord": "footstep",
    "rusWord": "шаг",
    "transcription": "[ˈfʊtstep]"
  },
  {
    "engWord": "publish",
    "rusWord": "публиковать",
    "transcription": "[ˈpʌblɪʃ]"
  },
  {
    "engWord": "lower",
    "rusWord": "нижний",
    "transcription": "[ˈləʊə]"
  },
  {
    "engWord": "creative",
    "rusWord": "творческий",
    "transcription": "[krɪˈeɪtɪv]"
  },
  {
    "engWord": "kindness",
    "rusWord": "доброта",
    "transcription": "[ˈkaɪndnɪs]"
  },
  {
    "engWord": "traffic",
    "rusWord": "движение",
    "transcription": "[ˈtræfɪk]"
  },
  {
    "engWord": "motel",
    "rusWord": "мотель",
    "transcription": "[məʊˈtel]"
  },
  {
    "engWord": "quarter",
    "rusWord": "четверть",
    "transcription": "[ˈkwɔːtə]"
  },
  {
    "engWord": "pit",
    "rusWord": "яма",
    "transcription": "[pɪt]"
  },
  {
    "engWord": "gay",
    "rusWord": "веселый",
    "transcription": "[geɪ]"
  },
  {
    "engWord": "occasion",
    "rusWord": "случай",
    "transcription": "[əˈkeɪʒən]"
  },
  {
    "engWord": "cigarette",
    "rusWord": "сигарета",
    "transcription": "[sɪgəˈret]"
  },
  {
    "engWord": "frankly",
    "rusWord": "откровенно",
    "transcription": "[ˈfræŋklɪ]"
  },
  {
    "engWord": "anyway",
    "rusWord": "в любом случае",
    "transcription": "[ˈenɪweɪ]"
  },
  {
    "engWord": "full",
    "rusWord": "полный",
    "transcription": "[fʊl]"
  },
  {
    "engWord": "rail",
    "rusWord": "перила",
    "transcription": "[reɪl]"
  },
  {
    "engWord": "one",
    "rusWord": "один",
    "transcription": "[wʌn]"
  },
  {
    "engWord": "answer",
    "rusWord": "ответ",
    "transcription": "[ˈɑːnsə]"
  },
  {
    "engWord": "double",
    "rusWord": "двойной",
    "transcription": "[dʌbl]"
  }
]
